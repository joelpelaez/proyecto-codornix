﻿CREATE PROCEDURE [dbo].[usp_PeriodoInactividad_Add]
@UidPeriodo uniqueidentifier,
@UidTipoInactividad uniqueidentifier,
@UidPeriodicidad uniqueidentifier = null,
@DtFechaInicio date,
@DtFechaFin date,
@VchNotas nvarchar(200)
AS
BEGIN

SET NOCOUNT ON

INSERT INTO PeriodoInactividad (UidPeriodoInactividad, UidPeriodo, UidTipoInactividad, UidPeriodicidad, DtFechaInicio, DtFechaFin, VchNotas)
	VALUES (NEWID(), @UidPeriodo, @UidTipoInactividad, @UidPeriodicidad, @DtFechaInicio, @DtFechaFin, @VchNotas)


END