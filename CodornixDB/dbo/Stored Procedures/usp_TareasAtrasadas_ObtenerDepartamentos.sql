﻿CREATE PROCEDURE usp_TareasAtrasadas_ObtenerDepartamentos
@UidUsuario uniqueidentifier,
@UidSucursal uniqueidentifier,
@DtFecha date
AS

SELECT d.UidDepartamento, d.VchNombre
FROM Departamento d
INNER JOIN AsignacionSupervision p ON p.UidDepartamento = d.UidDepartamento
WHERE
d.UidSucursal = @UidSucursal AND
p.UidUsuario = @UidUsuario AND
(p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha)