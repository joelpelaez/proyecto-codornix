﻿CREATE PROCEDURE usp_Antecesor_Get
@UidTarea uniqueidentifier
AS
BEGIN

SET NOCOUNT ON

SELECT * FROM Antecesor WHERE UidTarea = @UidTarea

END