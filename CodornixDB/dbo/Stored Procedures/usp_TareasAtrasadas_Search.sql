﻿CREATE PROCEDURE [dbo].[usp_TareasAtrasadas_Search]
@UidUsuario uniqueidentifier,
@UidSucursal uniqueidentifier,
@DtFecha date,
@DtFechaInicio date = null,
@DtFechaFin date = null,
@UidDepartamentos nvarchar(2000) = null
AS

SELECT 
t.UidTarea,
t.VchNombre AS VchTarea,
c.UidCumplimiento,
d.UidDepartamento,
d.VchNombre AS VchDepartamento,
a.UidArea,
a.VchNombre AS VchArea,
tu.UidTurno,
tu.VchTurno,
c.DtFechaProgramada

FROM Tarea t
INNER JOIN TipoTarea tt ON t.UidTipoTarea = tt.UidTipoTarea 
INNER JOIN Estatus es ON t.UidStatus = es.UidStatus 
LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
LEFT JOIN Area a ON ta.UidArea = a.UidArea
LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
INNER JOIN Departamento d ON d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = dt.UidDepartamento
INNER JOIN AsignacionSupervision p ON d.UidDepartamento = p.UidDepartamento
INNER JOIN Usuario u ON u.UidUsuario = p.UidUsuario
INNER JOIN Turno AS tu ON p.UidTurno = tu.UidTurno
INNER JOIN Cumplimiento c ON t.UidTarea = c.UidTarea AND (c.UidTurno = tu.UidTurno OR c.UidTurno IS NULL) AND (c.UidDepartamento = dt.UidDepartamento OR c.UidArea = ta.UidArea) 
INNER JOIN EstadoCumplimiento ec ON ec.UidEstadoCumplimiento = c.UidEstadoCumplimiento
WHERE 
	t.BitCaducado = 0 AND
	es.VchStatus = 'Activo' AND
	ec.VchTipoCumplimiento = 'No Realizado' AND
	u.UidUsuario = @UidUsuario AND
	d.UidSucursal = @UidSucursal AND
	(@UidDepartamentos IS NULL OR d.UidDepartamento IN (SELECT * FROM CSVtoTable(@UidDepartamentos, ','))) AND
	(p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha) AND
	(c.UidCumplimiento IS NULL OR ((
	(@DtFechaInicio IS NULL OR DtFechaProgramada >= @DtFechaInicio) AND
	(@DtFechaFin IS NULL OR DtFechaProgramada <= @DtFechaFin)  
	) AND (c.UidTurno = tu.UidTurno OR c.UidTurno IS NULL) AND c.DtFechaProgramada < @DtFecha))