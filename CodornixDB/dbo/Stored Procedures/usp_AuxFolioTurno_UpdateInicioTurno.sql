﻿CREATE    PROCEDURE [dbo].[usp_AuxFolioTurno_UpdateInicioTurno]
@UidInicioTurno uniqueidentifier
AS
BEGIN

	DECLARE @IntCount int,
			@UidSucursal uniqueidentifier;

	SELECT @UidSucursal = d.UidSucursal FROM InicioTurno i
		INNER JOIN Periodo p On i.UidPeriodo = p.UidPeriodo
		INNER JOIN Departamento d ON p.UidDepartamento = d.UidDepartamento
		WHERE UidInicioTurno = @UidInicioTurno

	BEGIN TRANSACTION
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	SELECT @IntCount = IntCount FROM AuxFolioTurno WHERE UidSucursal = @UidSucursal

	IF @IntCount IS NULL
	BEGIN
		-- El primer elemento será 1 pero se guardará 2 como el siguiente disponible
		INSERT INTO AuxFolioTurno (UidSucursal, IntCount) VALUES (@UidSucursal, 2);
		SET @IntCount = 1;
	END
	ELSE
		UPDATE AuxFolioTurno SET IntCount = @IntCount + 1 WHERE UidSucursal = @UidSucursal;

	COMMIT TRANSACTION

	UPDATE InicioTurno SET IntFolio = @IntCount WHERE UidInicioTurno = @UidInicioTurno;

END