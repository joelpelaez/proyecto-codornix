﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TareasNoCumplidas]
	-- Add the parameters for the stored procedure here
	@UidDepartamento uniqueidentifier,
	@UidUsuario uniqueidentifier,
	@DtFecha date,
	@UidSucursal uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

select 
	t.IntFolio,
	t.UidTarea,
	d.UidDepartamento,
	a.UidArea,
	c.UidCumplimiento,
	t.VchNombre AS VchTarea,
	d.VchNombre AS VchDepartamento,
	a.VchNombre AS VchArea,
	t.TmHora,
	c.DtFechaHora,
	ec.VchTipoCumplimiento,
	pe.*,e.*,tf.*, tt.*
FROM Tarea t
INNER JOIN TipoTarea tt ON t.UidTipoTarea = tt.UidTipoTarea 
INNER JOIN Estatus es ON t.UidStatus = es.UidStatus 
LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
LEFT JOIN Area a ON ta.UidArea = a.UidArea
LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
INNER JOIN Departamento d ON d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = dt.UidDepartamento
INNER JOIN Periodo p ON d.UidDepartamento = p.UidDepartamento
INNER JOIN Usuario u ON u.UidUsuario = p.UidUsuario
INNER JOIN Turno AS tu ON p.UidTurno = tu.UidTurno
LEFT JOIN Cumplimiento c ON t.UidTarea = c.UidTarea AND (c.UidTurno = tu.UidTurno OR c.UidTurno IS NULL) AND (c.UidDepartamento = dt.UidDepartamento OR c.UidArea = ta.UidArea) 
LEFT JOIN EstadoCumplimiento ec ON ec.UidEstadoCumplimiento = c.UidEstadoCumplimiento
INNER JOIN Estatus as e on e.UidStatus=t.UidStatus
INNER JOIN Periodicidad as pe on pe.UidPeriodicidad=t.UidPeriodicidad
INNER JOIN TipoFrecuencia as tf on tf.UidTipoFrecuencia=pe.UidTipoFrecuencia
WHERE
d.UidDepartamento=@UidDepartamento and
	t.BitCaducado = 0 AND
	es.VchStatus = 'Activo' and
	u.UidUsuario = @Uidusuario
	AND
	((
	(ec.VchTipoCumplimiento <> 'Completo')
	AND
	c.DtFechaProgramada = @DtFecha
	) OR (c.UidCumplimiento IS NULL AND t.UidAntecesorTarea IS NULL))
	AND
	(p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha) 
ORDER BY t.IntFolio ASC

END