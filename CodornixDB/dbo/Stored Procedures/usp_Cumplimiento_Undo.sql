﻿CREATE   PROCEDURE [dbo].[usp_Cumplimiento_Undo]
@UidCumplimiento uniqueidentifier
AS
BEGIN

DECLARE @UidCumplimientoGenerado uniqueidentifier,
		@UidEstadoCumplimiento uniqueidentifier;

SELECT @UidCumplimientoGenerado = UidCumplimientoNuevo FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento
SELECT @UidEstadoCumplimiento = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = 'No realizado'


UPDATE Cumplimiento
SET
	UidEstadoCumplimiento = @UidEstadoCumplimiento,
	UidUsuario = null,
	DtFechaHora = null,
	VchObservacion = null,
	BitValor = null,
	DcValor1 = null,
	DcValor2 = null,
	UidOpcion = null,
	UidTurno = null,
	UrlFoto = null,
	UidCumplimientoNuevo = null
WHERE UidCumplimiento = @UidCumplimiento


DELETE FROM MensajeNotificacion WHERE UidCumplimiento = @UidCumplimientoGenerado
--DELETE FROM MensajeNotificacion WHERE UidCumplimiento = @UidCumplimiento

DELETE FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimientoGenerado


DELETE n FROM MensajeNotificacion n INNER JOIN Cumplimiento c ON n.UidCumplimiento = c.UidCumplimiento
	WHERE c.UidCumplimientoAntecesor = @UidCumplimiento AND UidEstadoCumplimiento = @UidEstadoCumplimiento

DELETE FROM Cumplimiento WHERE UidCumplimientoAntecesor = @UidCumplimiento AND UidEstadoCumplimiento = @UidEstadoCumplimiento

END