﻿CREATE PROCEDURE [dbo].[usp_Notificacion_Set]
@UidTarea uniqueidentifier,
@BlValor bit = null,
@DcMenorQue decimal(18, 4) = null,
@DcMayorQue decimal(18, 4) = null,
@BitMenorIgual bit = null,
@BitMayorIgual bit = null,
@VchOpciones nvarchar(2000) = null,
@IntMaxNotificaciones int = null
AS

BEGIN

DECLARE @found uniqueidentifier;

SELECT @found = UidNotificacion FROM Notificacion WHERE UidTarea = @UidTarea;

IF @found IS NULL
	INSERT INTO Notificacion (UidNotificacion, UidTarea, BlValor, DcMenorQue, DcMayorQue, BitMenorIgual, BitMayorIgual, VchOpciones, IntMaxNotificaciones)
		VALUES (NEWID(), @UidTarea, @BlValor, @DcMenorQue, @DcMayorQue, @BitMenorIgual, @BitMayorIgual, @VchOpciones, @IntMaxNotificaciones);
ELSE
	UPDATE Notificacion SET BlValor = @BlValor, DcMenorQue = @DcMenorQue, DcMayorQue = @DcMayorQue, 
		BitMenorIgual = @BitMenorIgual, BitMayorIgual = @BitMayorIgual,
		VchOpciones = @VchOpciones, IntMaxNotificaciones = @IntMaxNotificaciones WHERE UidTarea = @UidTarea; 


END