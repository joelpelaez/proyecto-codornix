﻿CREATE PROCEDURE usp_TurnoSupervisor_CambiarEstado
@UidTurnoSupervisor uniqueidentifier,
@VchEstado nvarchar(50)
AS
BEGIN

DECLARE @UidEstadoTurno uniqueidentifier;

SELECT @UidEstadoTurno = UidEstadoTurno FROM EstadoTurno WHERE VchEstadoTurno = @VchEstado

UPDATE TurnoSupervisor SET UidEstadoTurno = @UidEstadoTurno WHERE UidTurnoSupervisor = @UidTurnoSupervisor

END