﻿CREATE PROCEDURE [dbo].[usp_Departamento_FindByList]
@UidPeriodos nvarchar(4000) = null
AS
BEGIN
SET NOCOUNT ON

SELECT d.* FROM Departamento d INNER JOIN Periodo p ON d.UidDepartamento = p.UidDepartamento
WHERE p.UidPeriodo IN (SELECT * FROM CSVtoTable(@UidPeriodos, ','));

END