﻿CREATE PROCEDURE usp_EstadoCumplimiento_FindAll
AS

SET NOCOUNT ON

SELECT * FROM EstadoCumplimiento WHERE VchTipoCumplimiento <> 'Deshabilitado' AND VchTipoCumplimiento <> 'Incompleto'