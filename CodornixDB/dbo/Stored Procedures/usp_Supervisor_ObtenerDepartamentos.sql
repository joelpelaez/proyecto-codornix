﻿CREATE PROCEDURE usp_Supervisor_ObtenerDepartamentos
@UidUsuario uniqueidentifier,
@DtFecha date
AS
BEGIN

SET NOCOUNT ON

SELECT UidDepartamento
FROM AsignacionSupervision
WHERE UidUsuario = @UidUsuario AND DtFechaInicio <= @DtFecha AND (DtFechaFin IS NULL OR DtFechaFin >= @DtFecha)

END