﻿CREATE PROCEDURE usp_MensajeNotificacion_Generar
@UidCumplimiento uniqueidentifier
AS
BEGIN

SET NOCOUNT ON

DECLARE @UidEstadoNotificacion uniqueidentifier;

SELECT @UidEstadoNotificacion = UidEstadoNotificacion FROM EstadoNotificacion WHERE VchEstadoNotificacion = 'No leido';

INSERT INTO MensajeNotificacion (UidMensajeNotificacion, UidEstadoNotificacion, UidCumplimiento)
	VALUES (NEWID(), @UidEstadoNotificacion, @UidCumplimiento)

END