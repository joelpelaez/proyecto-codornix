﻿CREATE PROCEDURE usp_TipoInactividad_Find
@UidTipoInactividad uniqueidentifier
AS
BEGIN

SET NOCOUNT ON

SELECT * FROM TipoInactividad WHERE UidTipoInactividad = @UidTipoInactividad

END