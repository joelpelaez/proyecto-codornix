﻿CREATE PROCEDURE usp_InicioTurno_CambiarEstado 
@UidInicioTurno uniqueidentifier,
@VchEstado nvarchar(50)
AS
BEGIN

DECLARE @UidEstadoTurno uniqueidentifier;

SELECT @UidEstadoTurno = UidEstadoTurno FROM EstadoTurno WHERE VchEstadoTurno = @VchEstado

UPDATE InicioTurno SET UidEstadoTurno = @UidEstadoTurno WHERE UidInicioTurno = @UidInicioTurno

END