﻿CREATE   PROCEDURE [dbo].[usp_Departamento_FindByListReal]
@UidDepartamentos nvarchar(4000) = null
AS
BEGIN
SET NOCOUNT ON

SELECT d.* FROM Departamento d WHERE d.UidDepartamento IN (SELECT * FROM CSVtoTable(@UidDepartamentos, ','));

END