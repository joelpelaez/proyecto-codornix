﻿CREATE   PROCEDURE usp_MensajeNotificacion_Find
@UidMensajeNotificacion uniqueidentifier
AS

BEGIN

SET NOCOUNT ON

SELECT
	mn.UidCumplimiento,
	mn.UidEstadoNotificacion,
	mn.UidMensajeNotificacion,
	t.VchNombre as VchTarea,
	CASE 
		WHEN md.VchTipoMedicion = 'Seleccionable' THEN op.VchOpciones
		WHEN md.VchTipoMedicion = 'Numerico' THEN c.DcValor1
		WHEN md.VchTipoMedicion = 'Verdadero/Falso' AND c.BitValor = 1 THEN 'Sí'
		WHEN md.VchTipoMedicion = 'Verdadero/Falso' AND c.BitValor = 0 THEN 'No'
		ELSE '(Sin valor)'
	END as VchResultado,
	d.VchNombre as VchDepartamento,
	CASE WHEN a.UidArea IS NOT NULL THEN a.VchNombre ELSE '(general)' END AS VchArea,
	en.VchEstadoNotificacion
FROM MensajeNotificacion mn
INNER JOIN EstadoNotificacion en ON mn.UidEstadoNotificacion = en.UidEstadoNotificacion
INNER JOIN Cumplimiento c ON mn.UidCumplimiento = c.UidCumplimiento
LEFT JOIN Area a ON c.UidArea = a.UidArea
INNER JOIN Departamento d ON (d.UidDepartamento = c.UidDepartamento OR d.UidDepartamento = a.UidDepartamento)
INNER JOIN Tarea t ON c.UidTarea = t.UidTarea
INNER JOIN Medicion md ON t.UidTipoMedicion = md.UidTipoMedicion
LEFT JOIN Opciones op ON op.UidOpciones = c.UidOpcion
WHERE
	mn.UidMensajeNotificacion = @UidMensajeNotificacion

END