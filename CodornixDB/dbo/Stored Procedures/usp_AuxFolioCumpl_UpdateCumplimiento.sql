﻿CREATE    PROCEDURE [dbo].[usp_AuxFolioCumpl_UpdateCumplimiento]
@UidCumplimiento uniqueidentifier
AS
BEGIN

	DECLARE @IntCount int,
			@UidTarea uniqueidentifier;

	SELECT @UidTarea = UidTarea FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento

	BEGIN TRANSACTION
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	SELECT @IntCount = IntCount FROM AuxFolioCumpl WHERE UidTarea = @UidTarea

	IF @IntCount IS NULL
	BEGIN
		-- El primer elemento será 1 pero se guardará 2 como el siguiente disponible
		INSERT INTO AuxFolioCumpl (UidTarea, IntCount) VALUES (@UidTarea, 2);
		SET @IntCount = 1;
	END
	ELSE
		UPDATE AuxFolioCumpl SET IntCount = @IntCount + 1 WHERE UidTarea = @UidTarea;

	COMMIT TRANSACTION

	UPDATE Cumplimiento SET IntFolio = @IntCount WHERE UidCumplimiento = @UidCumplimiento;

END