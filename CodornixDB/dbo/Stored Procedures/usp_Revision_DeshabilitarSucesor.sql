﻿CREATE PROCEDURE usp_Revision_DeshabilitarSucesor
@UidCumplimiento uniqueidentifier
AS

BEGIN

SET NOCOUNT ON

DECLARE @UidNO uniqueidentifier,
		@UidDH uniqueidentifier;

SELECT @UidNO = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = 'No Realizado'
SELECT @UidDH = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = 'Deshabilitado'

UPDATE Cumplimiento SET UidEstadoCumplimiento = @UidDH WHERE UidCumplimiento = @UidCumplimiento AND UidEstadoCumplimiento = @UidNO

END