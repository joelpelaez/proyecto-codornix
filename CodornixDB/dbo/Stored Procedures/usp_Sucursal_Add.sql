﻿CREATE PROCEDURE [dbo].[usp_Sucursal_Add]
@VchNombre nvarchar(30),
@DtFechaRegistro date,
@UidEmpresa uniqueidentifier,
@UidTipoSucursal uniqueidentifier,
@UidSucursal uniqueidentifier OUTPUT,
@VchZonaHoraria nvarchar(50),
@VchRutaImagen	nvarchar(200) = null

AS

SET NOCOUNT ON

SET @UidSucursal = NEWID()

INSERT INTO Sucursal (UidSucursal, VchNombre, DtFechaRegistro, UidEmpresa, UidTipoSucursal, VchRutaImagen, VchZonaHoraria)
	VALUES (@UidSucursal, @VchNombre, GETDATE(), @UidEmpresa, @UidTipoSucursal, @VchRutaImagen, @VchZonaHoraria)
