﻿CREATE PROCEDURE usp_TurnoSupervisor_Find
@UidTurnoSupervisor uniqueidentifier
AS

SET NOCOUNT ON

SELECT * FROM TurnoSupervisor WHERE UidTurnoSupervisor = @UidTurnoSupervisor