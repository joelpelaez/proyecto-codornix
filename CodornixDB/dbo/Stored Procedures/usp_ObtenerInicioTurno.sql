﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_ObtenerInicioTurno]
	-- Add the parameters for the stored procedure here
	@UidUsuario uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from InicioTurno where CAST(DtFechaHoraInicio as date) = CAST(GETDATE() as date) AND DtFechaHoraFin IS NULL
END