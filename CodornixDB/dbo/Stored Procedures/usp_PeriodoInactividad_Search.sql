﻿CREATE PROCEDURE usp_PeriodoInactividad_Search
@UidPeriodo uniqueidentifier,
@DtFechaInicio date = null,
@DtFechaInicioDespues date = null,
@DtFechaFin date = null,
@DtFechaFinDespues date = null,
@UidTiposInactividad nvarchar(4000) = null
AS

BEGIN

SET NOCOUNT ON

SELECT p.*, t.VchTipoInactividad
FROM PeriodoInactividad p
INNER JOIN TipoInactividad t ON p.UidTipoInactividad = t.UidTipoInactividad
WHERE
	p.UidPeriodo = @UidPeriodo AND
	(@DtFechaInicio IS NULL OR p.DtFechaInicio = @DtFechaInicio) AND
	(@DtFechaFin IS NULL OR p.DtFechaFin = @DtFechaFin) AND
	(@UidTiposInactividad IS NULL OR p.UidTipoInactividad IN (SELECT * FROM CSVtoTable(@UidTiposInactividad, ',')))

END