﻿CREATE PROCEDURE [dbo].[usp_AsignacionPeriodoInactividad_ListaDias]
@UidPeriodoInactividad uniqueidentifier,
@DtFechaInicio date,
@DtFechaFin date
AS

BEGIN

SET NOCOUNT ON

DECLARE @UidPeriodicidad uniqueidentifier = NULL;

SELECT @UidPeriodicidad = UidPeriodicidad FROM PeriodoInactividad WHERE UidPeriodoInactividad = @UidPeriodoInactividad

SELECT
	@UidPeriodoInactividad AS UidPeriodoInactividad,
	d.DtDate,
	CASE WHEN p.UidPeriodo IS NULL THEN 0 ELSE 1 END AS BitAsignado
FROM dbo.fn_ObtenerFechasPeriodicidad(@UidPeriodicidad, @DtFechaInicio, @DtFechaFin) as d
LEFT JOIN Periodo AS p ON d.DtDate = p.DtFechaInicio AND d.DtDate = p.DtFechaFin
WHERE
	p.UidPeriodoInactividad IS NULL OR p.UidPeriodoInactividad = @UidPeriodoInactividad

END