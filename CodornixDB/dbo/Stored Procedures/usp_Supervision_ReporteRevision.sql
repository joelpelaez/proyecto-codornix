﻿CREATE   PROCEDURE [dbo].[usp_Supervision_ReporteRevision]
	@UidUsuario uniqueidentifier,
	@UidSucursal uniqueidentifier,
	@DtFecha date
AS

SET NOCOUNT ON

SELECT
	CASE WHEN c.UidCumplimiento IS NULL THEN 1 ELSE c.IntFolio END as IntFolioCumpl,
	t.IntFolio,
	t.UidTarea,
	d.UidDepartamento,
	a.UidArea,
	c.UidCumplimiento,
	t.VchNombre AS VchTarea,
	d.VchNombre AS VchDepartamento,
	CASE WHEN a.UidArea IS NOT NULL THEN a.VchNombre ELSE 'General' END AS VchArea,
	c.DtFechaHora,
	ec.VchTipoCumplimiento AS VchTipoCumplimiento,
	m.VchTipoMedicion,
	CASE 
		WHEN m.VchTipoMedicion = 'Seleccionable' THEN op.VchOpciones
		WHEN m.VchTipoMedicion = 'Numerico' THEN CAST(c.DcValor1 as nvarchar) + ' ' + um.VchTipoUnidad
		WHEN m.VchTipoMedicion = 'Verdadero/Falso' AND c.BitValor = 1 THEN 'Sí'
		WHEN m.VchTipoMedicion = 'Verdadero/Falso' AND c.BitValor = 0 THEN 'No'
		ELSE '(Sin valor)'
	END as VchResultado,
	CASE 
		WHEN m.VchTipoMedicion = 'Seleccionable' AND rop.UidOpciones IS NOT NULL THEN rop.VchOpciones
		WHEN m.VchTipoMedicion = 'Numerico' AND r.DcValor1 IS NOT NULL THEN CAST(r.DcValor1 as nvarchar) + ' ' + um.VchTipoUnidad
		WHEN m.VchTipoMedicion = 'Verdadero/Falso' AND r.BitValor = 1 THEN 'Sí'
		WHEN m.VchTipoMedicion = 'Verdadero/Falso' AND r.BitValor = 0 THEN 'No'
		ELSE '(Sin correccion)'
	END as VchCorreccion,
	c.VchObservacion,
	tt.VchTipoTarea,
	cu.VchUsuario
FROM Tarea t
INNER JOIN TipoTarea tt ON t.UidTipoTarea = tt.UidTipoTarea
LEFT JOIN UnidadMedida um On t.UidUnidadMedida = um.UidUnidadMedida
INNER JOIN Medicion m ON t.UidTipoMedicion = m.UidTipoMedicion
INNER JOIN Estatus es ON t.UidStatus = es.UidStatus 
LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
LEFT JOIN Area a ON ta.UidArea = a.UidArea
LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
INNER JOIN Departamento d ON d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = dt.UidDepartamento
INNER JOIN Cumplimiento c ON t.UidTarea = c.UidTarea AND (c.UidDepartamento = dt.UidDepartamento OR c.UidArea = ta.UidArea) 
INNER JOIN EstadoCumplimiento ec ON ec.UidEstadoCumplimiento = c.UidEstadoCumplimiento
LEFT JOIN Opciones op ON c.UidOpcion = op.UidOpciones
INNER JOIN Usuario cu ON c.UidUsuario = cu.UidUsuario
INNER JOIN Revision r ON r.UidCumplimiento = c.UidCumplimiento
LEFT JOIN Opciones rop ON rop.UidOpciones = r.UidOpcion
WHERE
	d.UidSucursal = @UidSucursal AND
	r.UidUsuario = @UidUsuario AND
	t.BitCaducado = 0 AND
	es.VchStatus = 'Activo' AND
	CAST(r.DtFechaHora as date) = @DtFecha
ORDER BY t.IntFolio, c.IntFolio