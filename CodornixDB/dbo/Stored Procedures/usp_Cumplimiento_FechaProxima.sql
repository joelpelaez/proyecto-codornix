﻿CREATE PROCEDURE usp_Cumplimiento_FechaProxima
@UidTarea uniqueidentifier,
@DtFecha date
AS
BEGIN
DECLARE @UidPeriodicidad uniqueidentifier;

SET NOCOUNT ON

SELECT @UidPeriodicidad = UidPeriodicidad FROM Tarea WHERE UidTarea = @UidTarea

SELECT dbo.f_ObtenerPeriodicidad(@UidPeriodicidad, @DtFecha) AS DtFecha
END