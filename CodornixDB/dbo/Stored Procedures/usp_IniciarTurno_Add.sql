﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_IniciarTurno_Add]
	-- Add the parameters for the stored procedure here
	@UidInicioTurno uniqueidentifier OUTPUT,
	@UidUsuario uniqueidentifier,
	@DtFechaHoraInicio datetimeoffset(2), 
	@UidPeriodo uniqueidentifier,
	@UidCreador uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	set @UidInicioTurno = NEWID();
	insert into InicioTurno(UidInicioTurno,UidUsuario, DtFechaHoraInicio, UidPeriodo, UidCreador) values(@UidInicioTurno,@UidUsuario,@DtFechaHoraInicio,@UidPeriodo,@UidCreador)
	execute usp_AuxFolioTurno_UpdateInicioTurno @UidInicioTurno = @UidInicioTurno
END