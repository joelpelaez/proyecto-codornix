﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TareaOpcion_Update]
	-- Add the parameters for the stored procedure here
	@UidOpciones uniqueidentifier,
	@VchOpciones nvarchar(50),
	@IntOrden int,
	@BitVisible bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	UPDATE Opciones SET VchOpciones = @VchOpciones, IntOrden = @IntOrden, BitVisible = @BitVisible
		WHERE UidOpciones = @UidOpciones
END