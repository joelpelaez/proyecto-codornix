﻿CREATE   PROCEDURE [dbo].[usp_Cumplimiento_ListByUser]
	@UidUsuario uniqueidentifier,
	@UidPeriodo uniqueidentifier,
	@DtFecha date,
	@VchPeriodos nvarchar(2000) = null,
	@VchNombre nvarchar(50) = null,
	@VchEstados nvarchar(2000) = null,
	@UidDepartamento uniqueidentifier = null,
	@UidArea uniqueidentifier = null,
	@UidTipoTarea uniqueidentifier = null
AS

SET NOCOUNT ON

SELECT
	CASE WHEN c.UidCumplimiento IS NULL THEN 1 ELSE c.IntFolio END as IntFolioCumpl,
	t.IntFolio,
	t.UidTarea,
	d.UidDepartamento,
	a.UidArea,
	c.UidCumplimiento,
	p.UidPeriodo,
	t.VchNombre AS VchTarea,
	d.VchNombre AS VchDepartamento,
	a.VchNombre AS VchArea,
	t.TmHora,
	c.DtFechaHora,
	ec.VchTipoCumplimiento,
	tt.VchTipoTarea
FROM Tarea t
INNER JOIN TipoTarea tt ON t.UidTipoTarea = tt.UidTipoTarea
INNER JOIN Estatus es ON t.UidStatus = es.UidStatus 
LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
LEFT JOIN Area a ON ta.UidArea = a.UidArea
LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
INNER JOIN Departamento d ON d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = dt.UidDepartamento
INNER JOIN Periodo p ON d.UidDepartamento = p.UidDepartamento
INNER JOIN Usuario u ON u.UidUsuario = p.UidUsuario
LEFT JOIN Cumplimiento c ON t.UidTarea = c.UidTarea AND (c.UidDepartamento = dt.UidDepartamento OR c.UidArea = ta.UidArea)
and (c.UidTurno =p.UidTurno OR c.UidTurno IS NULL)
LEFT JOIN EstadoCumplimiento ec ON ec.UidEstadoCumplimiento = c.UidEstadoCumplimiento
WHERE
	t.BitCaducado = 0 AND
	es.VchStatus = 'Activo' AND
	(@UidTipoTarea IS NULL OR @UidTipoTarea = '00000000-0000-0000-0000-000000000000' OR t.UidTipoTarea = @UidTipoTarea) AND 
	(@VchNombre IS NULL OR t.VchNombre LIKE '%' + @VchNombre + '%') AND
	((@VchEstados IS NULL OR (ec.UidEstadoCumplimiento IS NULL AND (SELECT 1 FROM EstadoCumplimiento WHERE UidEstadoCumplimiento IN (SELECT * FROM CSVtoTable(@VchEstados, ',')) AND VchTipoCumplimiento = 'No Realizado') = 1)) OR
	(@VchEstados IS NULL OR (ec.UidEstadoCumplimiento IN (SELECT * FROM CSVtoTable(@VchEstados, ','))))) AND
	((ec.UidEstadoCumplimiento IS NULL AND t.UidAntecesorTarea IS NULL) OR ec.VchTipoCumplimiento <> 'Deshabilitado') AND
	((@VchPeriodos IS NULL AND u.UidUsuario = @UidUsuario AND UidPeriodo = @UidPeriodo) OR UidPeriodo IN (SELECT * FROM CSVtoTable(@VchPeriodos, ','))) AND
	(p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha) AND
	((c.UidCumplimiento IS NULL) OR c.DtFechaProgramada = @DtFecha) AND
	((@UidDepartamento IS NULL OR @UidDepartamento = '00000000-0000-0000-0000-000000000000') OR
	(d.UidDepartamento = @UidDepartamento AND @UidArea = '00000000-0000-0000-0000-000000000001' AND a.UidArea IS NULL) OR
	(d.UidDepartamento = @UidDepartamento AND a.UidArea IS NOT NULL AND @UidArea = cast(cast(0 as binary) as uniqueidentifier)) OR
	(a.UidArea = @UidArea))
ORDER BY t.IntFolio