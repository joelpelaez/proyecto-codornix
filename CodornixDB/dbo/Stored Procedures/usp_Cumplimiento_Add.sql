﻿CREATE PROCEDURE [dbo].[usp_Cumplimiento_Add]
@UidTarea uniqueidentifier,
@UidDepartamento uniqueidentifier = null,
@UidArea uniqueidentifier = null,
@UidUsuario uniqueidentifier,
@DtFechaHora datetimeoffset(2) = null,
@DtFechaProgramada date,
@UidEstadoCumplimiento uniqueidentifier,
@VchObservacion nvarchar(200) = null,
@UrlFoto nvarchar(50) = null,
@BitValor bit = null,
@DcValor1 decimal(18, 4) = null,
@DcValor2 decimal(18, 4) = null,
@UidOpcion uniqueidentifier = null,
@UidTurno uniqueidentifier = null,
@UidCumplimientoAntecesor uniqueidentifier = null,
@UidCumplimiento uniqueidentifier = null OUTPUT
AS

BEGIN

SET NOCOUNT ON

SET @UidCumplimiento = NEWID()

INSERT INTO Cumplimiento (UidCumplimiento,
						  UidTarea,
						  UidDepartamento,
						  UidArea,
						  UidUsuario,
						  DtFechaHora,
						  DtFechaProgramada,
						  UidEstadoCumplimiento,
						  VchObservacion, 
						  UrlFoto,
						  BitValor,
						  DcValor1,
						  DcValor2,
						  UidOpcion,
						  UidTurno,
						  UidCumplimientoAntecesor)
				  VALUES (@UidCumplimiento,
						  @UidTarea,
						  @UidDepartamento,
						  @UidArea,
						  @UidUsuario,
						  @DtFechaHora,
						  @DtFechaProgramada,
						  @UidEstadoCumplimiento,
						  @VchObservacion,
						  @UrlFoto,
						  @BitValor,
						  @DcValor1,
						  @DcValor2,
						  @UidOpcion,
						  @UidTurno,
						  @UidCumplimientoAntecesor)

EXECUTE usp_AuxFolioCumpl_UpdateCumplimiento @UidCumplimiento = @UidCumplimiento

END