﻿CREATE PROCEDURE usp_TurnoSupervisor_CrearTurno
@UidUsuario uniqueidentifier,
@UidSucursal uniqueidentifier,
@DtFechaInicio datetimeoffset(7)
AS

SET NOCOUNT ON

DECLARE @UidTurnoSupervisor uniqueidentifier = NEWID(),
		@UidEstadoTurno uniqueidentifier

SELECT @UidEstadoTurno = UidEstadoTurno FROM EstadoTurno WHERE VchEstadoTurno = 'Abierto'

INSERT INTO TurnoSupervisor (UidTurnoSupervisor, UidUsuario, UidSucursal, DtFechaInicio, UidEstadoTurno)
	VALUES (@UidTurnoSupervisor, @UidUsuario, @UidSucursal, @DtFechaInicio, @UidEstadoTurno)

EXECUTE usp_AuxFolioTurno_UpdateTurnoSupervisor @UidTurnoSupervisor = @UidTurnoSupervisor