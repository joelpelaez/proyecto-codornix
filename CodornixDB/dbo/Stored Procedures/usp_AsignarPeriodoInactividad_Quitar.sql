﻿CREATE  PROCEDURE usp_AsignarPeriodoInactividad_Quitar
@UidPeriodoInactividad uniqueidentifier,
@DtFecha date
AS

BEGIN

SET NOCOUNT ON

DELETE FROM Periodo WHERE UidPeriodoInactividad = @UidPeriodoInactividad AND DtFechaInicio = @DtFecha AND DtFechaFin = @DtFecha

END