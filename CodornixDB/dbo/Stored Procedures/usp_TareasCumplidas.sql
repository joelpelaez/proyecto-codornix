﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TareasCumplidas]
	-- Add the parameters for the stored procedure here
	@UidDepartamento uniqueidentifier,
	@UidUsuario uniqueidentifier,
	@DtFecha date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select c.*,ec.*, t.*,es.*, tf.*,tt.* from Tarea as t
INNER JOIN TipoTarea tt ON t.UidTipoTarea = tt.UidTipoTarea 
INNER JOIN Estatus es ON t.UidStatus = es.UidStatus 
LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
LEFT JOIN Area a ON ta.UidArea = a.UidArea
LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
INNER JOIN Departamento d ON d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = dt.UidDepartamento
INNER JOIN Periodo p ON d.UidDepartamento = p.UidDepartamento
INNER JOIN Usuario u ON u.UidUsuario = p.UidUsuario
INNER JOIN Turno AS tu ON p.UidTurno = tu.UidTurno
LEFT JOIN Cumplimiento c ON t.UidTarea = c.UidTarea AND (c.UidTurno = tu.UidTurno) AND (c.UidDepartamento = dt.UidDepartamento OR c.UidArea = ta.UidArea) 
LEFT JOIN EstadoCumplimiento ec ON ec.UidEstadoCumplimiento = c.UidEstadoCumplimiento
INNER JOIN Estatus as e on e.UidStatus=t.UidStatus
INNER JOIN Periodicidad as pe on pe.UidPeriodicidad=t.UidPeriodicidad
INNER JOIN TipoFrecuencia as tf on tf.UidTipoFrecuencia=pe.UidTipoFrecuencia
	where c.UidDepartamento=@UidDepartamento
	and c.UidUsuario=@UidUsuario
	and 
	ec.vchTipoCumplimiento = 'Completo' AND CAST(c.DtFechaProgramada AS date) = @DtFecha
	and 
	(p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha) 
ORDER BY t.IntFolio ASC
END