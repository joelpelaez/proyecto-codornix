﻿CREATE PROCEDURE usp_Supervisor_ListaDepartamentos
	@UidUsuario uniqueidentifier,
	@DtFecha date
AS
BEGIN

SET NOCOUNT ON 

SELECT d.UidDepartamento
FROM Departamento d
INNER JOIN AsignacionSupervision a ON a.UidDepartamento = d.UidDepartamento

WHERE
	a.UidUsuario = @UidUsuario AND
	a.DtFechaInicio <= @DtFecha AND a.DtFechaFin >= @DtFecha

END