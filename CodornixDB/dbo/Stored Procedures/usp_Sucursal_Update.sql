﻿CREATE PROCEDURE [dbo].[usp_Sucursal_Update]
@UidSucursal uniqueidentifier,
@VchNombre nvarchar(50),
@UidTipoSucursal uniqueidentifier,
@DtFechaRegistro date,
@VchZonaHoraria nvarchar(50),
@VchRutaImagen nvarchar(200)

AS

SET NOCOUNT ON

UPDATE Sucursal SET
VchNombre = @VchNombre,
DtFechaRegistro = @DtFechaRegistro,
UidTipoSucursal = @UidTipoSucursal,
VchRutaImagen=@VchRutaImagen,
VchZonaHoraria = @VchZonaHoraria
WHERE
UidSucursal = @UidSucursal
