﻿CREATE PROCEDURE [dbo].[usp_AuxFolioTurno_UpdateTurnoSupervisor]
@UidTurnoSupervisor uniqueidentifier
AS
BEGIN

	DECLARE @IntCount int,
			@UidSucursal uniqueidentifier;

	SELECT @UidSucursal = UidSucursal FROM TurnoSupervisor
		WHERE UidTurnoSupervisor = @UidTurnoSupervisor
	BEGIN TRANSACTION
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	SELECT @IntCount = IntCount FROM AuxFolioSupr WHERE UidSucursal = @UidSucursal

	IF @IntCount IS NULL
	BEGIN
		-- El primer elemento será 1 pero se guardará 2 como el siguiente disponible
		INSERT INTO AuxFolioSupr(UidSucursal, IntCount) VALUES (@UidSucursal, 2);
		SET @IntCount = 1;
	END
	ELSE
		UPDATE AuxFolioSupr SET IntCount = @IntCount + 1 WHERE UidSucursal = @UidSucursal;

	COMMIT TRANSACTION

	UPDATE TurnoSupervisor SET IntFolio = @IntCount WHERE UidTurnoSupervisor = @UidTurnoSupervisor;

END