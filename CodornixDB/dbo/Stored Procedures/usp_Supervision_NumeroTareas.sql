﻿CREATE PROCEDURE [dbo].[usp_Supervision_NumeroTareas]
@UidUsuario uniqueidentifier,
@UidSucursal uniqueidentifier,
@DtFecha date
AS

SET NOCOUNT ON

SELECT
	CASE WHEN it.UidInicioTurno IS NOT NULL THEN it.IntFolio ELSE 0 END as IntFolio,
	it.UidInicioTurno,
	d.UidDepartamento,
	d.VchNombre AS VchDepartamento,
	u.UidUsuario AS UidUsuario,
	CASE WHEN dbo.fn_RevisarInactividad(p.UidPeriodo, @DtFecha) = 0 THEN u.VchNombre + ' ' + u.VchApellidoPaterno + ' ' + u.VchApellidoMaterno ELSE 'No asignado' END AS VchUsuario, 
	COUNT(CASE WHEN ec.VchTipoCumplimiento = 'Completo' THEN 1 END) AS NumTareasCumplidas,
	COUNT(CASE WHEN ec.VchTipoCumplimiento <> 'Completo' OR ec.UidEstadoCumplimiento IS NULL THEN 1 END) AS NumTareasNoCumplidas,
	COUNT(CASE WHEN (ec.VchTipoCumplimiento <> 'Completo' OR ec.UidEstadoCumplimiento IS NULL ) AND tt.VchTipoTarea = 'Requerida' THEN 1 END) AS NumTareasRequeridasNoCumplidas,
	@DtFecha AS DtFecha,
	tu.VchTurno AS VchTurno,
	it.DtFechaHoraInicio,
	it.DtFechaHoraFin,
	et.VchEstadoTurno,
	et.UidEstadoTurno,
	p.UidPeriodo
FROM Tarea t
INNER JOIN TipoTarea tt ON t.UidTipoTarea = tt.UidTipoTarea 
INNER JOIN Estatus es ON t.UidStatus = es.UidStatus 
LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
LEFT JOIN Area a ON ta.UidArea = a.UidArea
LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
INNER JOIN Departamento d ON d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = dt.UidDepartamento
INNER JOIN Periodo p ON d.UidDepartamento = p.UidDepartamento
INNER JOIN Usuario u ON u.UidUsuario = p.UidUsuario
INNER JOIN AsignacionSupervision s ON s.UidDepartamento = p.UidDepartamento
INNER JOIN Usuario sp ON sp.UidUsuario = s.UidUsuario
INNER JOIN Turno AS tu ON p.UidTurno = tu.UidTurno
LEFT JOIN Cumplimiento c ON t.UidTarea = c.UidTarea AND (c.UidTurno = tu.UidTurno OR c.UidTurno IS NULL) AND (c.UidDepartamento = dt.UidDepartamento OR c.UidArea = ta.UidArea) 
LEFT JOIN EstadoCumplimiento ec ON ec.UidEstadoCumplimiento = c.UidEstadoCumplimiento
LEFT JOIN InicioTurno it ON it.UidPeriodo = p.UidPeriodo AND CAST(it.DtFechaHoraInicio as date) = @DtFecha 
LEFT JOIN EstadoTurno et ON et.UidEstadoTurno = it.UidEstadoTurno
WHERE 
	t.BitCaducado = 0 AND
	(dbo.fn_RevisarInactividad(p.UidPeriodo, @DtFecha) = 0 OR dbo.fn_VerificarSucesor(p.UidPeriodo, @DtFecha) = 0) AND
	es.VchStatus = 'Activo' AND
	sp.UidUsuario = @UidUsuario AND
	d.UidSucursal = @UidSucursal AND
	(s.DtFechaInicio <= @DtFecha AND s.DtFechaFin >= @DtFecha) AND
	(p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha) AND
	(c.UidCumplimiento IS NULL OR (DtFechaProgramada = @DtFecha AND (c.UidTurno = tu.UidTurno OR c.UidTurno IS NULL)))
GROUP BY it.UidInicioTurno, d.UidDepartamento, d.VchNombre, u.UidUsuario, u.VchNombre, u.VchApellidoPaterno,
	u.VchApellidoMaterno, tu.VchTurno, et.VchEstadoTurno, it.DtFechaHoraInicio, it.DtFechaHoraFin, et.UidEstadoTurno,
	p.UidPeriodo, it.IntFolio