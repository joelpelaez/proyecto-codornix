﻿CREATE   PROCEDURE [dbo].[usp_Cumplimiento_Do]
@UidTarea uniqueidentifier,
@UidDepartamento uniqueidentifier = null,
@UidArea uniqueidentifier = null,
@UidUsuario uniqueidentifier = null,
@UidCumplimiento uniqueidentifier = null,
@DtFechaHora datetimeoffset(2) = null,
@VchObservacion nvarchar(200) = null,
@UrlFoto nvarchar(50) = null,
@BitValor bit = null,
@DcValor1 decimal(18, 4) = null,
@DcValor2 decimal(18, 4) = null,
@UidOpcion uniqueidentifier = null,
@UidTurno uniqueidentifier = null,
@VchEstado nvarchar(50) = 'Completo',
@DtFechaNueva date = null,
@Estado tinyint = null OUTPUT,
@NuevoCumplimiento uniqueidentifier = null OUTPUT
AS

SET NOCOUNT ON

DECLARE @UidEC uniqueidentifier,
		@UidNO uniqueidentifier,
		@DtSiguiente date,
		@UidPeriodicidad uniqueidentifier,
		@Requerido nvarchar(50),
		@UidCumplimientoNuevo uniqueidentifier,
		@UidAntecesor uniqueidentifier;

SET @Estado = 0

SELECT @UidEC = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = @VchEstado;
SELECT @UidNO = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = 'No Realizado';

SELECT @UidPeriodicidad = UidPeriodicidad, @UidAntecesor = UidAntecesorTarea FROM Tarea WHERE UidTarea = @UidTarea;
SELECT @DtSiguiente = dbo.f_ObtenerPeriodicidad(@UidPeriodicidad, @DtFechaHora)
SELECT @Requerido = VchTipoTarea FROM TipoTarea tt JOIN Tarea t ON tt.UidTipoTarea = t.UidTipoTarea WHERE t.UidTarea = @UidTarea;



--IF (@VchEstado = 'Cancelado' OR @vchEstado = 'Pospuesto') AND @Requerido = 'Requerida'
--BEGIN
--	SET @Estado = 2
--	RETURN
--END

-- Si la fecha de aplazo es mayor a la de la siguiente tarea, se considera
-- como cancelado de forma automática.

IF @UidCumplimiento IS NULL
BEGIN
	-- Create the current clumplieminto
	EXECUTE usp_Cumplimiento_Add @UidTarea = @UidTarea, @UidDepartamento = @UidDepartamento,
		@UidArea = @UidArea, @UidUsuario = @UidUsuario, @DtFechaHora = @DtFechaHora,
		@DtFechaProgramada = @DtFechaHora,
		@UidEstadoCumplimiento = @UidEC, @VchObservacion = @VchObservacion,
		@UrlFoto = @UrlFoto, @BitValor = @BitValor, @DcValor1 = @DcValor1, @DcValor2 = @DcValor2,
		@UidOpcion = @UidOpcion, @UidTurno = @UidTurno, @UidCumplimiento = @UidCumplimiento OUTPUT;

END
ELSE
	UPDATE Cumplimiento
	SET
		UidEstadoCumplimiento = @UidEC,
		UidUsuario = @UidUsuario,
		DtFechaHora = @DtFechaHora,
		VchObservacion = @VchObservacion,
		BitValor = @BitValor,
		DcValor1 = @DcValor1,
		DcValor2 = @DcValor2,
		UidOpcion = @UidOpcion,
		UidTurno = @UidTurno
	WHERE UidCumplimiento = @UidCumplimiento

-- Ejecutar Notificacion
EXECUTE usp_Notificacion_CrearNotificacion @UidCumplimiento = @UidCumplimiento
-- Create the next

-- No generar el siguiente cumplimiento si no tiene periodicidad o tiene antecesora
IF (@DtSiguiente IS NULL) OR (@UidAntecesor IS NOT NULL AND @VchEstado = 'Cancelado')
	RETURN

EXECUTE usp_Cumplimiento_Add @UidTarea = @UidTarea, @UidDepartamento = @UidDepartamento, @UidArea = @UidArea,
	@UidUsuario = null, @DtFechaHora = null, @DtFechaProgramada = @DtSiguiente, @UidEstadoCumplimiento = @UidNO,
	@VchObservacion = null, @DcValor1 = null, @DcValor2 = null, @UidOpcion = null, @UrlFoto = null, @UidTurno = null,
	@UidCumplimiento = @UidCumplimientoNuevo OUTPUT;

UPDATE Cumplimiento
SET
	UidCumplimientoNuevo = @UidCumplimientoNuevo
WHERE UidCumplimiento = @UidCumplimiento

SET @NuevoCumplimiento = @UidCumplimiento