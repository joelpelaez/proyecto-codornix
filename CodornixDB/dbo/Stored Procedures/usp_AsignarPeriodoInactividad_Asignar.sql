﻿CREATE   PROCEDURE [dbo].[usp_AsignarPeriodoInactividad_Asignar]
	@DtFecha date,
	@UidUsuario uniqueidentifier,
	@UidPeriodoInactividad uniqueidentifier,
	@IntResult int = null OUTPUT
AS

/*
 * Define result constants:
 *     (0) Successful adding without modyfing anything.
 *     (1) Successful adding with date updates on undefined end asignations.
 *     (2) Successful update of already asignation (same person, same depto. or area, same turn, other end date).
 *    (-1) Failure on adding, same asignation alreayd exists (same all fields).
 *    (-2) Failure on adding, number of max asignations already reached.
 *    (-3) Failure on adding, the start date isn't the next day to last assignement.
 */

DECLARE @maxAsigns tinyint,
		@ExistsEntry uniqueidentifier,
		@Status tinyint,
		@LastDate date,
		@currentAsigns tinyint,
		@UidDepartamento uniqueidentifier,
		@UidTurno uniqueidentifier,
		@DtFechaInicio date = @DtFecha,
		@DtFechaFin date = @DtFecha;

SET NOCOUNT OFF -- Enable counting for this procedure


SELECT @UidDepartamento = UidDepartamento, @UidTurno = UidTurno FROM Periodo
	WHERE UidPeriodo = (SELECT UidPeriodo FROM PeriodoInactividad WHERE UidPeriodoInactividad = @UidPeriodoInactividad)

SELECT @maxAsigns = IntMaxAsignaciones FROM Encargado WHERE UidUsuario = @UidUsuario;

IF @maxAsigns IS NULL -- Only one asignation by default.
	SET @maxAsigns = 1;
	
-- Don't check overlapping assignations

SELECT @currentAsigns = COUNT(UidUsuario) FROM Periodo WHERE UidUsuario = @UidUsuario AND (DtFechaFin > GETDATE() OR DtFechaFin IS NULL);

-- They are already four assignations, cancel operation.
IF (@currentAsigns >= @maxAsigns) 
BEGIN
	SET @IntResult = -2;
	RETURN
END;

-- If the new assignation produces leakes between dates, cancel it.
--SELECT TOP(1) @LastDate = DtFechaFin FROM Periodo WHERE UidDepartamento = @UidDepartamento AND UidTurno = @UidTurno AND 

ELSE
BEGIN
	INSERT INTO Periodo (UidPeriodo, DtFechaInicio, DtFechaFin, UidUsuario, UidDepartamento, UidTurno, BlEspecial, UidPeriodoInactividad)
		VALUES  (NEWID(), @DtFechaInicio, @DtFechaFin, @UidUsuario, @UidDepartamento, @UidTurno, 1, @UidPeriodoInactividad);
	SET @IntResult = 0;
	RETURN
END