﻿CREATE PROCEDURE [dbo].[usp_Antecesor_Set]
@UidTarea uniqueidentifier,
@UidTareaAnterior uniqueidentifier,
@IntRepeticion int = null,
@BitUsarNotificacion bit,
@IntDiasDespues int
AS
BEGIN

SET NOCOUNT ON

DECLARE @found uniqueidentifier

SELECT @found = UidAntecesor FROM Antecesor WHERE UidTarea = @UidTarea

IF @found IS NOT NULL
	UPDATE Antecesor SET UidTareaAnterior = @UidTareaAnterior, IntRepeticion = @IntRepeticion,
		IntDiasDespues = @IntDiasDespues, BitUsarNotificacion = @BitUsarNotificacion WHERE UidTarea = @UidTarea
ELSE
	INSERT INTO Antecesor (UidAntecesor, UidTarea, UidTareaAnterior, IntRepeticion, IntDiasDespues, BitUsarNotificacion)
		VALUES (NEWID(), @UidTarea, @UidTareaAnterior, @IntRepeticion, @IntDiasDespues, @BitUsarNotificacion)

END