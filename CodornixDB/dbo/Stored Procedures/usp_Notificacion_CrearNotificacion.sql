﻿CREATE PROCEDURE [dbo].[usp_Notificacion_CrearNotificacion]
@UidCumplimiento uniqueidentifier
AS
BEGIN

DECLARE @UidTarea uniqueidentifier,
		@VchTipoMedicion nvarchar(50),
		@UidNotificacion uniqueidentifier,
		@DtFechaOriginal date,
		@UidTareaNueva uniqueidentifier,
		@DtFechaNueva date,
		@IntDias int,
		@IntMax int,
		@UidDepartamento uniqueidentifier,
		@UidArea uniqueidentifier,
		@IntConteoNotif int,
		@UidNO uniqueidentifier,
		@ActivaNoti bit,
		@BitUsarNotificacion bit;

SELECT @UidNO = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = 'No Realizado';

SELECT @UidTarea = UidTarea, @DtFechaOriginal = CAST(DtFechaHora AS date) FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento;

SELECT @UidNotificacion = UidNotificacion FROM Notificacion WHERE UidTarea = @UidTarea;

SET @ActivaNoti = 0

IF @UidNotificacion IS NOT NULL
BEGIN
	SELECT @VchTipoMedicion = m.VchTipoMedicion FROM Medicion m 
		INNER JOIN Tarea t ON t.UidTipoMedicion = m.UidTipoMedicion
		WHERE t.UidTarea = @UidTarea;

	-- Actualizar contador de notificaciones
	SELECT @IntConteoNotif = IntMaxNotificaciones FROM Notificacion WHERE UidTarea = @UidTarea;
	IF @IntConteoNotif IS NULL
	BEGIN
		SET @IntConteoNotif = 1;
		UPDATE Notificacion SET IntMaxNotificaciones = 2 WHERE UidTarea = @UidTarea; 
	END
	ELSE
		SET @IntConteoNotif = @IntConteoNotif + 1;
		UPDATE Notificacion SET IntMaxNotificaciones = @IntConteoNotif WHERE UidTarea = @UidTarea; 
		

	IF @VchTipoMedicion = 'Sin medición'
		SET @ActivaNoti = 1 -- Siempre activar notificaciones virtuales para los  sucesore
	
	ELSE IF @VchTipoMedicion = 'Numerico'
	BEGIN
		DECLARE @DcMayor decimal(18, 4),
				@DcMenor decimal(18, 4),
				@BitMenorIgual bit,
				@BitMayorIgual bit,
				@DcOrig decimal(18,4);

		SELECT @DcOrig = DcValor1 FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento;
		SELECT @DcMayor = DcMayorQue, @DcMenor = DcMenorQue, @BitMayorIgual = BitMayorIgual, @BitMenorIgual = BitMenorIgual FROM Notificacion WHERE UidTarea = @UidTarea;

		IF (@DcMayor IS NULL OR (@BitMayorIgual = 1 AND @DcOrig >= @DcMayor) OR @DcOrig > @DcMayor) AND
			(@DcMenor IS NULL OR (@BitMenorIgual = 1 AND @DcOrig <= @DcMenor) OR @DcOrig < @DcMenor)
			SET @ActivaNoti = 1
		ELSE -- Ya no cumple el requerimeinto se reinicia el contador
			UPDATE Notificacion SET IntMaxNotificaciones = 0 WHERE UidTarea = @UidTarea;

	END

	ELSE IF @VchTipoMedicion = 'Verdadero/Falso'
	BEGIN

		DECLARE @BlOrig bit,
				@BlDeseado bit;

		SELECT @BlOrig = BitValor FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento;
		SELECT @BlDeseado = BlValor FROM Notificacion WHERE UidTarea = @UidTarea;

		IF @BlOrig = @BlDeseado
			SET @ActivaNoti = 1
		ELSE -- Ya no cumple el requerimeinto se reinicia el contador
			UPDATE Notificacion SET IntMaxNotificaciones = 0 WHERE UidTarea = @UidTarea;
	END

	ELSE IF @VchTipoMedicion = 'Seleccionable'
	BEGIN
		DECLARE @UidOpcion uniqueidentifier,
				@VchOpciones nvarchar(2000),
				@Found int;

		SELECT @UidOpcion = UidOpcion FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento;
		SELECT @VchOpciones = VchOpciones FROM Notificacion WHERE UidTarea = @UidTarea;

		IF @UidOpcion IN (SELECT * FROM dbo.CSVtoTable(@VchOpciones, ','))
			SET @ActivaNoti = 1
		ELSE
			UPDATE Notificacion SET IntMaxNotificaciones = 0 WHERE UidTarea = @UidTarea;
	END

	IF @ActivaNoti = 1
	BEGIN
		EXECUTE usp_MensajeNotificacion_Generar @UidCumplimiento = @UidCumplimiento
	END
END
ELSE
	SET @ActivaNoti = 1 -- Establecer que todas la tareas tienen notificacion general 
create_cumplimiento:
-- Crear los cumplimientos acordes

DECLARE notif CURSOR LOCAL FORWARD_ONLY READ_ONLY
		FOR SELECT UidTarea, IntDiasDespues, IntRepeticion, BitUsarNotificacion FROM Antecesor WHERE UidTareaAnterior = @UidTarea

OPEN notif
FETCH NEXT FROM notif INTO @UidTareaNueva, @IntDias, @IntMax, @BitUsarNotificacion

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @IntConteoNotif > @IntMax
		GOTO siguiente -- Se ha ejecutado mas veces del limite establecido 

	IF @BitUsarNotificacion = 1 AND @ActivaNoti = 0
		GOTO siguiente

	SELECT @UidArea = ta.UidArea FROM TareaArea ta WHERE ta.UidTarea = @UidTareaNueva;

	IF @UidArea IS NULL
		SELECT @UidDepartamento = dt.UidDepartamento FROM DepartamentoTarea dt WHERE dt.UidTarea = @UidTareaNueva

	SET @DtFechaNueva = DATEADD(day, @IntDias, @DtFechaOriginal)

	EXECUTE usp_Cumplimiento_Add @UidTarea = @UidTareaNueva, @UidDepartamento = @UidDepartamento,
		@UidArea = @UidArea, @UidUsuario = null, @DtFechaHora = null, @DtFechaProgramada = @DtFechaNueva,
		@UidEstadoCumplimiento = @UidNO, @VchObservacion = null, @UrlFoto = null,
		@BitValor = null, @DcValor1 = null, @DcValor2 = null, @UidOpcion = null,
		@UidTurno = null, @UidCumplimientoAntecesor = @UidCumplimiento, @UidCumplimiento = null;

	siguiente:
	FETCH NEXT FROM notif INTO @UidTareaNueva, @IntDias, @IntMax, @BitUsarNotificacion
END

CLOSE notif
DEALLOCATE notif

END