﻿CREATE PROCEDURE [dbo].[usp_Revision_Sucesores]
@UidCumplimiento uniqueidentifier
AS

SET NOCOUNT ON

SELECT
	t.UidTarea,
	t.VchNombre as VchTarea,
	CASE WHEN cd.UidCumplimiento IS NULL THEN 0 ELSE 1 END AS bitCreado,
	cd.UidCumplimiento,
	cd.UidEstadoCumplimiento,
	CASE WHEN ec.VchTipoCumplimiento IS NULL THEN 'No Creado' ELSE ec.VchTipoCumplimiento END as VchEstadoCumplimiento
FROM Cumplimiento c
INNER JOIN Tarea ta ON c.UidTarea = ta.UidTarea
INNER JOIN Antecesor a ON a.UidTareaAnterior = ta.UidTarea
INNER JOIN Tarea t ON t.UidTarea = a.UidTarea
LEFT JOIN Cumplimiento cd ON cd.UidTarea = t.UidTarea AND cd.UidCumplimientoAntecesor = @UidCumplimiento
LEFT JOIN EstadoCumplimiento ec ON cd.UidEstadoCumplimiento = ec.UidEstadoCumplimiento
WHERE c.UidCumplimiento = @UidCumplimiento