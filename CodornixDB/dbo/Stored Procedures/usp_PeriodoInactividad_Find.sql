﻿CREATE PROCEDURE [dbo].[usp_PeriodoInactividad_Find]
@UidPeriodoInactividad uniqueidentifier
AS

SET NOCOUNT ON

SELECT
	p.*
FROM PeriodoInactividad p
WHERE p.UidPeriodoInactividad = @UidPeriodoInactividad