﻿CREATE TABLE [dbo].[MensajeNotificacion] (
    [UidMensajeNotificacion] UNIQUEIDENTIFIER NOT NULL,
    [UidCumplimiento]        UNIQUEIDENTIFIER NOT NULL,
    [UidEstadoNotificacion]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_MensajeNotificacion] PRIMARY KEY CLUSTERED ([UidMensajeNotificacion] ASC),
    CONSTRAINT [FK_MensajeNotificacion_Cumplimiento] FOREIGN KEY ([UidCumplimiento]) REFERENCES [dbo].[Cumplimiento] ([UidCumplimiento]),
    CONSTRAINT [FK_MensajeNotificacion_EstadoNotificacion] FOREIGN KEY ([UidEstadoNotificacion]) REFERENCES [dbo].[EstadoNotificacion] ([UidEstadoNotificacion])
);



