﻿CREATE TABLE [dbo].[TurnoSupervisor] (
    [UidTurnoSupervisor] UNIQUEIDENTIFIER   NOT NULL,
    [UidUsuario]         UNIQUEIDENTIFIER   NOT NULL,
    [UidSucursal]        UNIQUEIDENTIFIER   NOT NULL,
    [DtFechaInicio]      DATETIMEOFFSET (7) NOT NULL,
    [DtFechaFin]         DATETIMEOFFSET (7) NULL,
    [UidEstadoTurno]     UNIQUEIDENTIFIER   NOT NULL,
    [IntFolio]           INT                NULL,
    CONSTRAINT [PK_TurnoSupervisor] PRIMARY KEY CLUSTERED ([UidTurnoSupervisor] ASC),
    CONSTRAINT [FK_TurnoSupervisor_EstadoTurno] FOREIGN KEY ([UidEstadoTurno]) REFERENCES [dbo].[EstadoTurno] ([UidEstadoTurno]),
    CONSTRAINT [FK_TurnoSupervisor_Usuario] FOREIGN KEY ([UidUsuario]) REFERENCES [dbo].[Usuario] ([UidUsuario])
);

