﻿CREATE TABLE [dbo].[InicioTurno] (
    [UidInicioTurno]    UNIQUEIDENTIFIER   NOT NULL,
    [UidUsuario]        UNIQUEIDENTIFIER   NULL,
    [IntFolio]          INT                NULL,
    [DtFechaHoraInicio] DATETIMEOFFSET (2) NULL,
    [DtFechaHoraFin]    DATETIMEOFFSET (2) NULL,
    [IntNoCompletado]   INT                NULL,
    [UidPeriodo]        UNIQUEIDENTIFIER   NULL,
    [UidEstadoTurno]    UNIQUEIDENTIFIER   NULL,
    [UidCreador]        UNIQUEIDENTIFIER   NOT NULL,
    CONSTRAINT [PK_InicioTurno] PRIMARY KEY CLUSTERED ([UidInicioTurno] ASC),
    CONSTRAINT [FK_InicioTurno_Periodo] FOREIGN KEY ([UidPeriodo]) REFERENCES [dbo].[Periodo] ([UidPeriodo]),
    CONSTRAINT [FK_InicioTurno_Usuario] FOREIGN KEY ([UidUsuario]) REFERENCES [dbo].[Usuario] ([UidUsuario]),
    CONSTRAINT [FK_InicioTurno_Usuario_Creador] FOREIGN KEY ([UidCreador]) REFERENCES [dbo].[Usuario] ([UidUsuario])
);
















GO
CREATE NONCLUSTERED INDEX [IX_InicioTurno_Usuario]
    ON [dbo].[InicioTurno]([UidUsuario] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InicioTurno_Periodo]
    ON [dbo].[InicioTurno]([UidPeriodo] ASC);

