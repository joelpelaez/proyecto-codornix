﻿CREATE TABLE [dbo].[AuxFolioSupr] (
    [UidSucursal] UNIQUEIDENTIFIER NOT NULL,
    [IntCount]    INT              NOT NULL,
    CONSTRAINT [PK_AuxFolioSupr] PRIMARY KEY CLUSTERED ([UidSucursal] ASC)
);

