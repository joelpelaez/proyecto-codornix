﻿CREATE TABLE [dbo].[Antecesor] (
    [UidAntecesor]        UNIQUEIDENTIFIER NOT NULL,
    [UidTarea]            UNIQUEIDENTIFIER NOT NULL,
    [UidTareaAnterior]    UNIQUEIDENTIFIER NOT NULL,
    [IntRepeticion]       INT              NULL,
    [IntDiasDespues]      INT              NOT NULL,
    [BitUsarNotificacion] BIT              NULL,
    CONSTRAINT [PK_Antecesor] PRIMARY KEY CLUSTERED ([UidAntecesor] ASC),
    CONSTRAINT [FK_Antecesor_Tarea] FOREIGN KEY ([UidTarea]) REFERENCES [dbo].[Tarea] ([UidTarea]),
    CONSTRAINT [FK_Antecesor_TareaAnterior] FOREIGN KEY ([UidTareaAnterior]) REFERENCES [dbo].[Tarea] ([UidTarea])
);



