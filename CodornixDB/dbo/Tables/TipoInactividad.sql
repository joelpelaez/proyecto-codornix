﻿CREATE TABLE [dbo].[TipoInactividad] (
    [UidTipoInactividad] UNIQUEIDENTIFIER NOT NULL,
    [VchTipoInactividad] NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_TipoInactividad] PRIMARY KEY CLUSTERED ([UidTipoInactividad] ASC)
);

