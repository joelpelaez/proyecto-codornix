﻿CREATE TABLE [dbo].[EstadoNotificacion] (
    [UidEstadoNotificacion] UNIQUEIDENTIFIER NOT NULL,
    [VchEstadoNotificacion] NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_EstadoNotificacion] PRIMARY KEY CLUSTERED ([UidEstadoNotificacion] ASC)
);

