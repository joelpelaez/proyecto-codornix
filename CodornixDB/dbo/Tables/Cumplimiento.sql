﻿CREATE TABLE [dbo].[Cumplimiento] (
    [UidCumplimiento]          UNIQUEIDENTIFIER   CONSTRAINT [DF_Cumplimiento_UidCumplimiento] DEFAULT (newid()) NOT NULL,
    [UidTarea]                 UNIQUEIDENTIFIER   NOT NULL,
    [UidDepartamento]          UNIQUEIDENTIFIER   NULL,
    [UidArea]                  UNIQUEIDENTIFIER   NULL,
    [UidUsuario]               UNIQUEIDENTIFIER   NULL,
    [IntFolio]                 INT                NULL,
    [DtFechaProgramada]        DATE               NOT NULL,
    [DtFechaHora]              DATETIMEOFFSET (2) NULL,
    [UrlFoto]                  NVARCHAR (50)      NULL,
    [VchObservacion]           NVARCHAR (200)     NULL,
    [UidEstadoCumplimiento]    UNIQUEIDENTIFIER   NOT NULL,
    [BitValor]                 BIT                NULL,
    [DcValor1]                 DECIMAL (18, 4)    NULL,
    [DcValor2]                 DECIMAL (18, 4)    NULL,
    [UidOpcion]                UNIQUEIDENTIFIER   NULL,
    [UidTurno]                 UNIQUEIDENTIFIER   NULL,
    [BitPuedeCerrar]           BIT                NULL,
    [UidCumplimientoNuevo]     UNIQUEIDENTIFIER   NULL,
    [UidCumplimientoAntecesor] UNIQUEIDENTIFIER   NULL,
    CONSTRAINT [PK_Cumplimiento] PRIMARY KEY CLUSTERED ([UidCumplimiento] ASC),
    CONSTRAINT [FK_Cumplimiento_Area] FOREIGN KEY ([UidArea]) REFERENCES [dbo].[Area] ([UidArea]),
    CONSTRAINT [FK_Cumplimiento_Departamento] FOREIGN KEY ([UidDepartamento]) REFERENCES [dbo].[Departamento] ([UidDepartamento]),
    CONSTRAINT [FK_Cumplimiento_EstadoCumplimiento] FOREIGN KEY ([UidEstadoCumplimiento]) REFERENCES [dbo].[EstadoCumplimiento] ([UidEstadoCumplimiento]),
    CONSTRAINT [FK_Cumplimiento_Opciones] FOREIGN KEY ([UidOpcion]) REFERENCES [dbo].[Opciones] ([UidOpciones]),
    CONSTRAINT [FK_Cumplimiento_Tarea] FOREIGN KEY ([UidTarea]) REFERENCES [dbo].[Tarea] ([UidTarea]),
    CONSTRAINT [FK_Cumplimiento_Turno] FOREIGN KEY ([UidTurno]) REFERENCES [dbo].[Turno] ([UidTurno]),
    CONSTRAINT [FK_Cumplimiento_Usuario] FOREIGN KEY ([UidUsuario]) REFERENCES [dbo].[Usuario] ([UidUsuario])
);
























GO
CREATE NONCLUSTERED INDEX [IX_Cumplimiento_Usuario]
    ON [dbo].[Cumplimiento]([UidUsuario] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cumplimiento_Tarea]
    ON [dbo].[Cumplimiento]([UidTarea] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cumplimiento_EstadoCumplimiento]
    ON [dbo].[Cumplimiento]([UidEstadoCumplimiento] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cumplimiento_Departamento]
    ON [dbo].[Cumplimiento]([UidDepartamento] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cumplimiento_Area]
    ON [dbo].[Cumplimiento]([UidArea] ASC);

