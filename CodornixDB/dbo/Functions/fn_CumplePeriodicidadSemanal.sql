﻿CREATE  FUNCTION [dbo].[fn_CumplePeriodicidadSemanal] (@UidPeriodicidad uniqueidentifier, @DtFecha date)
RETURNS bit
AS
BEGIN
	DECLARE @frecuencia int,
			@diff int,
			@DtFechaInicio date,
			@DtResult date,
			@BitLunes bit,
			@BitMartes bit,
			@BitMiercoles bit,
			@BitJueves bit,
			@BitViernes bit,
			@BitSabado bit,
			@BitDomingo bit,
			@IntDayofWeek int,
			@EndDate date;

	SELECT
			@BitLunes = BitLunes, 
			@BitMartes = BitMartes,
			@BitMiercoles = BitMiercoles,
			@BitJueves = BitJueves,
			@BitViernes = BitViernes,
			@BitSabado = BitSabado,
			@BitDomingo = BitDomingo
		FROM PeriodicidadSemanal WHERE UidPeriodicidad = @UidPeriodicidad
	SELECT @frecuencia = IntFrecuencia, @DtFechaInicio = DtFechaInicio FROM Periodicidad WHERE UidPeriodicidad = @UidPeriodicidad

	SET @diff = DATEDIFF(day, @DtFechaInicio, @DtFecha) % @frecuencia

	IF @diff = 0
	BEGIN
		SET @IntDayofWeek = DATEPART(dw, @DtFecha)
		
		-- Check if the date's day of week with the selected day.
		IF (@IntDayofWeek = 1 AND @BitDomingo = 1) OR (@IntDayofWeek = 2 AND @BitLunes = 1) OR
			(@IntDayofWeek = 3 AND @BitMartes = 1) OR (@IntDayofWeek = 4 AND @BitMiercoles = 1) OR
			(@IntDayofWeek = 5 AND @BitJueves = 1) OR (@IntDayofWeek = 6 AND @BitViernes = 1) OR
			(@IntDayofWeek = 7 AND @BitSabado = 1)
			
			RETURN 1

	END

	RETURN 0
END