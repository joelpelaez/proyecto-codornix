﻿CREATE   FUNCTION [dbo].[fn_ObtenerNumeroFechasPeriodicidad](@UidPeriodicidad uniqueidentifier, @DtFechaInicio date, @DtFechaFin date)
RETURNS int

BEGIN
	DECLARE @tmp TABLE (DtDate date);
	DECLARE @DtFecha date,
			@count int = 0,
			@tipo nvarchar(50);

	SELECT @tipo = VchTipoFrecuencia FROM TipoFrecuencia tf INNER JOIN Periodicidad p ON p.UidTipoFrecuencia = tf.UidTipoFrecuencia WHERE p.UidPeriodicidad = @UidPeriodicidad
	
	SET @DtFecha = @DtFechaInicio

	IF @tipo = 'Sin periodicidad' OR @UidPeriodicidad IS NULL
	BEGIN
		WHILE @DtFecha <= @DtFechaFin
		BEGIN
			SET @count = @count + 1
			
			SET @DtFecha = DATEADD(day, 1, @DtFecha)
		END
	END

	ELSE
	BEGIN

		WHILE @DtFecha <= @DtFechaFin 
		BEGIN
			SET @DtFecha = dbo.f_ObtenerPeriodicidad(@UidPeriodicidad, @DtFecha)

			IF @DtFecha <= @DtFechaFin
				SET @count = @count + 1;

		END
	END

	RETURN @count
END