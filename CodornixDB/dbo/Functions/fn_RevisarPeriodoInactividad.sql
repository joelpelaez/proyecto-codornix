﻿CREATE FUNCTION [dbo].[fn_RevisarPeriodoInactividad](@UidPeriodoInactividad uniqueidentifier, @DtFecha date)
RETURNS bit
BEGIN

	DECLARE @UidPeriodicidad uniqueidentifier,
			@DtFechaInicio date,
			@DtFechaFin date;

	SELECT @UidPeriodicidad = UidPeriodicidad, @DtFechaInicio = DtFechaInicio, @DtFechaFin = DtFechaFin
	FROM PeriodoInactividad WHERE UidPeriodoInactividad = @UidPeriodoInactividad;

	IF @DtFecha >= @DtFechaInicio AND @DtFecha <= @DtFechaFin
		RETURN dbo.f_CumplePeriodicidad(@UidPeriodicidad, @DtFecha);

	RETURN 0;
END