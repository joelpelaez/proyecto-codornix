﻿CREATE   FUNCTION fn_VerificarSucesor(@UidPeriodo uniqueidentifier, @DtFecha date)
RETURNS bit

BEGIN

DECLARE @uid uniqueidentifier;

SELECT @uid = p.UidPeriodo FROM Periodo p INNER JOIN PeriodoInactividad pp ON p.UidPeriodoInactividad = pp.UidPeriodoInactividad
WHERE p.DtFechaInicio = @DtFecha AND p.DtFechaFin = @DtFecha AND p.BlEspecial = 1 AND pp.UidPeriodo = @UidPeriodo

IF @uid IS NOT NULL
	RETURN 1

RETURN 0

END