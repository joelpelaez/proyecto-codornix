﻿CREATE FUNCTION [dbo].[fn_RevisarInactividad](@UidPeriodo uniqueidentifier, @DtFecha date)
RETURNS bit
BEGIN

	DECLARE @UidPeriodoInactividad uniqueidentifier

	DECLARE pCursor CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR 
		SELECT UidPeriodoInactividad FROM PeriodoInactividad
		WHERE UidPeriodo = @UidPeriodo AND DtFechaInicio <= @DtFecha AND DtFechaFin >= @DtFecha;

	OPEN pCursor

	FETCH NEXT FROM pCursor INTO @UidPeriodoInactividad

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (dbo.fn_RevisarPeriodoInactividad(@UidPeriodoInactividad, @DtFecha) = 1)
			RETURN 1;
		FETCH NEXT FROM pCursor INTO @UidPeriodoInactividad
	END

	RETURN 0
END