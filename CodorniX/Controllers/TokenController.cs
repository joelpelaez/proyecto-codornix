﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace CodorniX.Controllers
{
    public class TokenController : ApiController
    {
        [AllowAnonymous]
        public TokenInfo Post(LoginInfo info)
        {
            if (CheckUser(info.Username, info.Password))
            {
                TokenInfo token = new TokenInfo();
                token.Token = JwtManager.GenerateToken(info.Username,1440);
                return token;
            }

            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }

        public bool CheckUser(string username, string password)
        {
            Usuario usuario = new Usuario.Repository().FindByName(username);

            if (usuario == null)
                return false;

            if (usuario.STRPASSWORD == password)
                return true;

            return false;
        }

        public class LoginInfo
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class TokenInfo
        {
            public string Token { get; set; }
        }
    }
}