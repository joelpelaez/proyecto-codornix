﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace CodorniX.Util
{
    public class Correo
    {
        private static string Origen = "info@datosmedix.com";
        private static string Pass = "Support@24h";
        private static string Dominio = "mail.datosmedix.com";
        private static string Host = "smtp.gmail.com";
        private static int Port = 2525;

        private SmtpClient smtpClient;

        public Correo()
        {
            smtpClient = new SmtpClient();
            smtpClient.Host = Host;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            smtpClient.DeliveryFormat = SmtpDeliveryFormat.International;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(Origen, Pass);
            smtpClient.Port = Port;
        }

        public bool EnviarCorreo(string destinatario, string asunto, string contenido, Attachment[] attachments, bool html = false)
        {
            MailMessage mailMessage = new MailMessage(Origen, destinatario);
            mailMessage.Subject = asunto;
            mailMessage.Body = contenido;
            mailMessage.IsBodyHtml = html;
            foreach (var attachment in attachments)
                mailMessage.Attachments.Add(attachment);
            smtpClient.Send(mailMessage);

            return true;
        }

        public void SendEmail(string to, string subject, string body, Attachment[] attachments = null, bool isHtml = true)
        {
            SendEmail(new string[] { to }, subject, body, attachments, isHtml);
        }

        public void SendEmail(string[] recipients, string subject, string body, Attachment[] attachments = null, bool isHtml = true)
        {
            MailMessage mailMessage = new MailMessage();
            
            foreach (string to in recipients)
            {
                mailMessage.To.Add(to);
            }
            mailMessage.From = new MailAddress(Origen);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = isHtml;

            if (attachments != null)
            {
                foreach (Attachment attachment in attachments)
                {
                    mailMessage.Attachments.Add(attachment);
                }
            }

            smtpClient.Send(mailMessage);
        }

        public void SendEmail(MailAddressCollection to, string subject, string body, Attachment[] attachments = null, bool isHtml = true)
        {
            MailMessage mailMessage = new MailMessage();

            foreach (var i in to)
            {
                mailMessage.To.Add(i);
            }
            mailMessage.From = new MailAddress(Origen);
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = isHtml;

            if (attachments != null)
            {
                foreach (Attachment attachment in attachments)
                {
                    mailMessage.Attachments.Add(attachment);
                }
            }

            smtpClient.Send(mailMessage);
        }
    }
}