﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Util
{
    public static class SesionExtensions
    {
        public static DateTime GetDateTime(this Sesion sesion)
        {
            DateTimeOffset time = Hora.ObtenerHoraServidor();
            DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(sesion.uidSucursalActual.Value));
            var local = horaLocal.DateTime;
            return local;
        }

        public static DateTimeOffset GetDateTimeOffset(this Sesion sesion)
        {
            DateTimeOffset time = Hora.ObtenerHoraServidor();
            if (!sesion.uidSucursalActual.HasValue)
                return time;

            DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(sesion.uidSucursalActual.Value));
            return horaLocal;
        }
    }
}