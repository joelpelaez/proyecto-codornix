﻿using System;
using System.Web;
using System.Web.Hosting;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace CodorniX.Util
{
    public class PageRender
    {
        private static PageRender _instance;
        private IRazorEngineService Razor;

        private PageRender()
        {
            if (_instance != null)
                throw new Exception("Cannot create a second instance");

            PrepareEngine();
        }

        public static PageRender Instance() => _instance ?? (_instance = new PageRender());

        private void PrepareEngine()
        {
            var config = new TemplateServiceConfiguration();
            config.TemplateManager = new ResolvePathTemplateManager(new []{ HostingEnvironment.MapPath("~/Views") });
            Razor = RazorEngineService.Create(config);
        }

        public virtual string RenderViewToString(string viewPath, object model)
        {
            return Razor.RunCompile(viewPath, null, model);
        }
    }
}