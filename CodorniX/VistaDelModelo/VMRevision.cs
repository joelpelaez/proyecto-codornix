﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMRevision
	{
		Revision.Repository revisionRepository = new Revision.Repository();
		Tarea.Repositorio tareaRepository = new Tarea.Repositorio();
		Cumplimiento.Repository cumplimientoRepository = new Cumplimiento.Repository();
		Area.Repository areaRepository = new Area.Repository();
		Departamento.Repository departamentoRepository = new Departamento.Repository();
		TareaOpcion.Repositorio tareaOpcionRepository = new TareaOpcion.Repositorio();
		Sucesor.Repository sucesorRepository = new Sucesor.Repository();
		Calificacion.Repository calificacionRepository = new Calificacion.Repository();

		private List<Revision> _RevisionesPendientes;

		public List<Revision> RevisionesPendientes
		{
			get { return _RevisionesPendientes; }
			set { _RevisionesPendientes = value; }
		}


		private Revision _Revision;

		public Revision Revision
		{
			get { return _Revision; }
			set { _Revision = value; }
		}

		private Cumplimiento _Cumplimiento;

		public Cumplimiento Cumplimiento
		{
			get { return _Cumplimiento; }
			set { _Cumplimiento = value; }
		}

		private Tarea _Tarea;

		public Tarea Tarea
		{
			get { return _Tarea; }
			set { _Tarea = value; }
		}

		private Departamento _Departamento;

		public Departamento Departamento
		{
			get { return _Departamento; }
			set { _Departamento = value; }
		}

		private List<Departamento> _departamentos;

		public List<Departamento> Departamentos
		{
			get { return _departamentos; }
			set { _departamentos = value; }
		}

		private Area _Area;

		public Area Area
		{
			get { return _Area; }
			set { _Area = value; }
		}

		private List<Area> _areas;

		public List<Area> Areas
		{
			get { return _areas; }
			set { _areas = value; }
		}

		private List<TareaOpcion> _Opciones;

		public List<TareaOpcion> Opciones
		{
			get { return _Opciones; }
			set { _Opciones = value; }
		}

		private List<Sucesor> _Sucesores;

		public List<Sucesor> Sucesores
		{
			get { return _Sucesores; }
			set { _Sucesores = value; }
		}

		private List<Calificacion> _calificaciones;

		public List<Calificacion> Calificaciones
		{
			get => _calificaciones;
			set => _calificaciones = value;
		}

		public void ObtenerRevisionesPendientes(Guid uidUsuario, Guid uidSucursal, DateTime fecha, List<Guid> periodos, string nombre, Guid depto, Guid area, int estado)
		{
			string lpr = null;
			if (periodos != null && periodos.Count > 0)
			{
				lpr = periodos[0].ToString();
				for (int i = 1; i < periodos.Count; i++)
				{
					lpr += "," + periodos[i].ToString();
				}
			}
			_RevisionesPendientes = revisionRepository.FindByUser(uidUsuario, uidSucursal, fecha, lpr, nombre, depto, area, estado);
		}

		public void ObtenerRevision(Guid uid)
		{
			_Revision = revisionRepository.Find(uid);
		}

		public void ObtenerCumplimiento(Guid uid)
		{
			_Cumplimiento = cumplimientoRepository.Find(uid);
		}

		public void ObtenerTarea(Guid uid)
		{
			_Tarea = tareaRepository.Encontrar(uid);
		}

		public void ObtenerDepartamento(Guid uid)
		{
			_Departamento = departamentoRepository.Encontrar(uid);
		}

		public void ObtenerArea(Guid uid)
		{
			_Area = areaRepository.Find(uid);
		}

		public void ObtenerOpcionesDeTarea(Guid uidTarea)
		{
			_Opciones = tareaOpcionRepository.Buscar(uidTarea);
		}

		public void RegistrarRevision(Guid uidCumplimiento, Guid uidUsuario, DateTime fechaRevision, bool? estado, decimal? valor1, decimal? valor2, Guid? uidOpcion, string observaciones, bool correcto, Guid uidCalificacion)
		{
			revisionRepository.RegistrarRevision(uidCumplimiento, uidUsuario, fechaRevision, estado, valor1, valor2, uidOpcion, observaciones, correcto, uidCalificacion);
		}

		public void ObtenerSucesores(Guid uidCumplimiento)
		{
			_Sucesores = sucesorRepository.FindAll(uidCumplimiento);
		}

		public void CrearSucesor(Guid uidCumplimiento, Guid uidTareaSucesor)
		{
			sucesorRepository.CrearSucesor(uidCumplimiento, uidTareaSucesor);
		}

		public void HabilitarSucesor(Guid uidCumplimiento)
		{
			sucesorRepository.Habilitar(uidCumplimiento);
		}

		public void DeshabilitarSucesor(Guid uidCumplimiento)
		{
			sucesorRepository.Deshabilitar(uidCumplimiento);
		}

		public void ObtenerAreas(Guid uid)
		{
			_areas = areaRepository.FindAll(uid);
		}

		public void ObtenerDepartamentos(List<Guid> periodos)
		{
			string lpr = null;
			if (periodos.Count > 0)
			{
				lpr = periodos[0].ToString();
				for (int i = 1; i < periodos.Count; i++)
				{
					lpr += "," + periodos[i];
				}
			}
			_departamentos = departamentoRepository.EncontrarPorListaDePeriodos(lpr);
		}
		public void ObtenerDepartamentosAsignados(List<Guid> lsDepartamentos)
		{
			_departamentos = new List<Departamento>();
			foreach (var item in lsDepartamentos)
			{
				_Departamento = new Departamento();
				_Departamento = departamentoRepository.Encontrar(item);
				_departamentos.Add(_Departamento);
			}
		}

		public void ObtenerCalificaciones()
		{
			_calificaciones = calificacionRepository.FindAll();
		}
	}
}