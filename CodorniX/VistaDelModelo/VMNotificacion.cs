﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMNotificacion
    {
        EstadoNotificacion.Repository estadoRepository = new EstadoNotificacion.Repository();
        MensajeNotificacion.Repository notificacionRepository = new MensajeNotificacion.Repository();

        private List<EstadoNotificacion> _Estados;

        public List<EstadoNotificacion> Estados
        {
            get { return _Estados; }
            set { _Estados = value; }
        }

        private List<MensajeNotificacion> _Mensajes;

        public List<MensajeNotificacion> Mensajes
        {
            get { return _Mensajes; }
            set { _Mensajes = value; }
        }

        private MensajeNotificacion _Notificacion;

        public MensajeNotificacion Notificacion
        {
            get { return _Notificacion; }
            set { _Notificacion = value; }
        }

        private List<Departamento> _Departamentos;

        public List<Departamento> Departamentos
        {
            get { return _Departamentos; }
            set { _Departamentos = value; }
        }


        public void ObtenerEstados()
        {
            _Estados = estadoRepository.FindAll();
        }

        public void ObtenerNotificaciones(Guid uidSucursal, string uidDepartamentos, string uidDepartamentosBusqueda,
            string estados, DateTime? fechaInicio, DateTime? fechaFin)
        {
            _Mensajes = notificacionRepository.Search(uidSucursal, uidDepartamentos, uidDepartamentosBusqueda, estados, fechaInicio, fechaFin);
        }

        public void ObtenerNotificacion(Guid uid)
        {
            _Notificacion = notificacionRepository.Find(uid);
        }

        public void ObtenerDepartamentos(Guid uidUsuario, DateTime fecha)
        {
            _Departamentos = notificacionRepository.GetDepartamentos(uidUsuario, fecha);
        }

        public void CambiarEstado(Guid uidNotificacion, string estado)
        {
            notificacionRepository.ChangeState(uidNotificacion, estado);
        }
    }
}