﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMAsignacionInactividad
    {
        Encargado.Repository encargadoRepository = new Encargado.Repository();
        Departamento.Repository departamentoRepository = new Departamento.Repository();
        PeriodoInactividad.Repository inactividadRepository = new PeriodoInactividad.Repository();

        private List<Departamento> _departamentos;

        public List<Departamento> Departamentos
        {
            get { return _departamentos; }
            set { _departamentos = value; }
        }

        private List<Encargado> _encargados;

        public List<Encargado> Encargados
        {
            get { return _encargados; }
            set { _encargados = value; }
        }

        private List<PeriodoInactividad> _periodos;

        public List<PeriodoInactividad> Periodos
        {
            get { return _periodos; }
            set { _periodos = value; }
        }

        private List<PeriodoInactividad> _dias;

        public List<PeriodoInactividad> Dias
        {
            get { return _dias; }
            set { _dias = value; }
        }

        public void ObtenerDepartamentos(Guid uidSucursal)
        {
            _departamentos = departamentoRepository.EncontrarTodos(uidSucursal);
        }

        public void ObtenerEncargado(Guid uidSucursal, string nombre)
        {
            _encargados = encargadoRepository.FindByName(nombre, nombre, nombre, uidSucursal.ToString());
        }

        public void ObtenerPeriodos(Guid uidSucursal, DateTime fechaInicio, DateTime fechaFin, string departamentos)
        {
            _periodos = inactividadRepository.SearchByDepartament(uidSucursal, fechaInicio, fechaFin, departamentos);
        }

        public void ObtenerDias(Guid uidPeriodoInactividad, DateTime fechaInicio, DateTime fechaFin)
        {
            _dias = inactividadRepository.getInactivedDays(uidPeriodoInactividad, fechaInicio, fechaFin);
        }

        public int AsignarDia(Guid uidPeriodoInactividad, DateTime fecha, Guid uidUsuario)
        {
            return inactividadRepository.AsignarPeriodo(uidPeriodoInactividad, fecha, uidUsuario);
        }

        public void QuitarDia(Guid uidPeriodoInactividad, DateTime fecha)
        {
            inactividadRepository.QuitarPeriodo(uidPeriodoInactividad, fecha);
        }

        public void ObtenerDepartamentosPorLista(List<Guid> departamentos)
        {
            string lpr = null;
            if (departamentos.Count > 0)
            {
                lpr = departamentos[0].ToString();
                for (int i = 1; i < departamentos.Count; i++)
                {
                    lpr += "," + departamentos[i];
                }
            }
            _departamentos = departamentoRepository.EncontrarPorLista(lpr);
        }
    }
}