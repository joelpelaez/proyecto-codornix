﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;
using CodorniX.ConexionDB;
using System.Web.UI.WebControls;
using System.IO;

namespace CodorniX.VistaDelModelo
{
    public class VMImagenes
    {
        DBImagen conexion =new DBImagen();
        private Guid _UidImagen;

        public Guid ID
        {
            get { return _UidImagen; }
            set { _UidImagen = value; }
        }

        private string _Ruta;

        public string STRRUTA
        {
            get { return _Ruta; }
            set { _Ruta = value; }
        }
        private string _descripcion;

        public string STRDESCRIPCION
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        public bool ValidarExtencionImagen(FileUpload Imagen)
        {
            bool correcto = false;
            string extencion = Path.GetExtension(Imagen.FileName).ToLower();
            string[] extensionePerfmitidas = { ".png", ".jpg", ".jpeg" };

            for (int i = 0; i < extensionePerfmitidas.Length; i++)
            {
                if (extencion == extensionePerfmitidas[i])
                {
                    correcto = true;
                }
            }
            return correcto;
        }
    }
}