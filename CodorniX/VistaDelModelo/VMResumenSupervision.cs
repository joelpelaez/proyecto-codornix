﻿using CodorniX.Modelo;
using CodorniX.Modelo.Model;
using CodorniX.Modelo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMResumenSupervision
    {
        Usuario.Repository usuarioRepository = new Usuario.Repository();
        Empresa.Repository empresaRepository = new Empresa.Repository();
        Sucursal.Repository sucursalRepository = new Sucursal.Repository();
        ResumenSupervisionRepository reporteSupervisionRepository = new ResumenSupervisionRepository();

        public Usuario Usuario { get; private set; }
        public Empresa Empresa { get; private set; }
        public Sucursal Sucursal { get; private set; }
        public List<ResumenSupervision> ReportesSupervision { get; private set; }

        public void ObtenerUsuario(Guid uid)
        {
            Usuario = usuarioRepository.Find(uid);
        }

        public void ObtenerEmpresa(Guid uid)
        {
            Empresa = empresaRepository.Find(uid);
        }

        public void ObtenerSucursal(Guid uid)
        {
            Sucursal = sucursalRepository.Find(uid);
        }

        public void ObtenerReportesSupervision(Guid uidUsuario, Guid uidSucursal, DateTimeOffset fecha)
        {
            ReportesSupervision = reporteSupervisionRepository.FindBySupervisor(uidUsuario, uidSucursal, fecha);
        }
    }
}