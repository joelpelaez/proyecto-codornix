﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMInicioSupervisor
    {
        private TurnoSupervisor.Repository turnoRepository = new TurnoSupervisor.Repository();
        private EstadoTurno.Repository estadoRepository = new EstadoTurno.Repository();

        public TurnoSupervisor TurnoSupervisor { get; set; }
        public EstadoTurno EstadoTurno { get; set; }

        public void ObtenerTurnoDeHoy(Guid uidUsuario, Guid uidSucursal, DateTime fecha)
        {
            TurnoSupervisor = turnoRepository.ObtenerTurnoHoy(uidUsuario, uidSucursal, fecha);
        }

        public void ObtenerTurno(Guid uid)
        {
            TurnoSupervisor = turnoRepository.Find(uid);
        }

        public void CrearTurno(Guid uidUsuario, Guid uidSucursal, DateTimeOffset fechaInicio)
        {
            turnoRepository.CrearTurno(uidUsuario, uidSucursal, fechaInicio);
        }

        public void ModificarFechaFin(Guid uidTurnoSupervisor, DateTimeOffset fechafin)
        {
            turnoRepository.ModificarFechaFin(uidTurnoSupervisor, fechafin);
        }

        public void IniciarTurno(Guid uidTurnoSupervisor)
        {
            turnoRepository.ModificarEstado(uidTurnoSupervisor, "Abierto");
        }

        public void CerrarTurno(Guid uidTurnoSupervisor)
        {
            turnoRepository.ModificarEstado(uidTurnoSupervisor, "Cerrado");
        }

        public void ObtenerEstadoTurno(Guid uid)
        {
            EstadoTurno = estadoRepository.Find(uid);
        }
    }
}