﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMSucursales
    {
        private Sucursal.Repository repository = new Sucursal.Repository();
        private SucursalTelefono.Repository telefonoRepository = new SucursalTelefono.Repository();
        private SucursalDireccion.Repository direccionRepository = new SucursalDireccion.Repository();
        private EmpresaDireccion.Repository empresaDireccionRepository = new EmpresaDireccion.Repository();
        private Pais.Repository paisRepository = new Pais.Repository();
        private Estado.Repository estadoRepository = new Estado.Repository();
        private TipoTelefono.Repository tipoTelefonoRepository = new TipoTelefono.Repository();
        private TipoSucursal.Repository tipoSucursalRepository = new TipoSucursal.Repository();

        private List<Sucursal> _Sucursales;

        public List<Sucursal> Sucursales
        {
            get { return _Sucursales; }
            set { _Sucursales = value; }
        }

        private Sucursal _Sucursal;

        public Sucursal Sucursal
        {
            get { return _Sucursal; }
            set { _Sucursal = value; }
        }

        private List<SucursalDireccion> _direcciones;

        public List<SucursalDireccion> Direcciones
        {
            get { return _direcciones; }
            set { _direcciones = value; }
        }

        private Direccion _direccion;

        public Direccion Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        private List<SucursalTelefono> _telefonos;

        public List<SucursalTelefono> Telefonos
        {
            get { return _telefonos; }
            set { _telefonos = value; }
        }

        private Telefono _telefono;

        public Telefono Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        private List<Pais> _paises;

        public List<Pais> Paises
        {
            get { return _paises; }
            set { _paises = value; }
        }

        private List<Estado> _estados;

        public List<Estado> Estados
        {
            get { return _estados; }
            set { _estados = value; }
        }

        private IList<TipoTelefono> _TipoTelefonos;

        public IList<TipoTelefono> TipoTelefonos
        {
            get { return _TipoTelefonos; }
            set { _TipoTelefonos = value; }
        }

        private List<TipoSucursal> _TipoSucursales;

        public List<TipoSucursal> TipoSucursales
        {
            get { return _TipoSucursales; }
            set { _TipoSucursales = value; }
        }

        private List<EmpresaDireccion> _EmpresaDirecciones;

        public List<EmpresaDireccion> EmpresaDirecciones
        {
            get { return _EmpresaDirecciones; }
            set { _EmpresaDirecciones = value; }
        }

        public void ObtenerSucursales(Guid uidEmpresa)
        {
            _Sucursales = repository.FindAll(uidEmpresa);
        }

        public void ObtenerSucursal(Guid uid)
        {
            _Sucursal = repository.Find(uid);
        }

        public void ObtenerDirecciones()
        {
            _direcciones = direccionRepository.FindAll(_Sucursal.UidSucursal);
        }

        public void ObtenerDireccion(Guid uid)
        {
            _direccion = direccionRepository.Find(uid);
        }

        public void ObtenerTelefonos()
        {
            _telefonos = telefonoRepository.FindAll(_Sucursal.UidSucursal);
        }

        public void ObtenerTelefono(Guid uid)
        {
            _telefono = telefonoRepository.Find(uid);
        }

        public void ObtenerPaises()
        {
            _paises = paisRepository.FindAll();
        }

        public void ObtenerEstados(Guid uidPais)
        {
            _estados = estadoRepository.FindAll(uidPais);
        }

        public void BuscarSucursales(string nombre, DateTime? registradoDespues, DateTime? registradoAntes, string uids, Guid empresa)
        {
            Sucursal.Criteria criteria = new Sucursal.Criteria()
            {
                Nombre = nombre,
                FechaRegistroDespues = registradoDespues,
                FechaRegistroAntes = registradoAntes,
                Tipos = uids,
                Empresa = empresa,
                
            };
            _Sucursales = repository.FindBy(criteria);
        }

        public void GuardarSucursal(Sucursal Sucursal)
        {
            repository.Save(Sucursal);
        }

        public void GuardarDirecciones(List<SucursalDireccion> direcciones, Guid uidSucursal)
        {
            foreach (SucursalDireccion direccion in direcciones)
            {
                direccion.UidSucursal = uidSucursal;
                direccionRepository.Save(direccion);
            }
        }

        public void EliminarDirecciones(List<SucursalDireccion> direcciones)
        {
            foreach (SucursalDireccion direccion in direcciones)
            {
                direccionRepository.Remove(direccion);
            }
        }

        public void GuardarTelefonos(List<SucursalTelefono> telefonos, Guid uidSucursal)
        {
            foreach (SucursalTelefono telefono in telefonos)
            {
                telefono.UidSucursal = uidSucursal;
                telefonoRepository.Save(telefono);
            }
        }

        public void EliminarTelefonos(List<SucursalTelefono> telefonos)
        {
            foreach (SucursalTelefono telefono in telefonos)
            {
                telefonoRepository.Remove(telefono);
            }
        }

        public void ObtenerTipoTelefonos()
        {
            _TipoTelefonos = tipoTelefonoRepository.FindAll();
        }

        public void ObtenerTipoSucursales()
        {
            _TipoSucursales = tipoSucursalRepository.FindAll();
        }

        public void ObtenerEmpresaDirecciones(Guid uid)
        {
            _EmpresaDirecciones = empresaDireccionRepository.FindAll(uid);
        }

        public void ObtenerEmpresaDireccion(Guid uid)
        {
            _direccion = empresaDireccionRepository.Find(uid);
        }
    }
}