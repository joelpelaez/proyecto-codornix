﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
    public class VMBienvenido
    {
        private TareasNoCumplidas.Repositorio TareasNoCumplidasRepositorio = new TareasNoCumplidas.Repositorio();
        private IniciarTurno.Repositorio IniciarTurnoRepositorio = new IniciarTurno.Repositorio();
        private ResumenTarea.Repositorio ResumenTareaRepositorio = new ResumenTarea.Repositorio();

        private List<TareasNoCumplidas> _ltsDepartamento;

        public List<TareasNoCumplidas> ltsDepartamento
        {
            get { return _ltsDepartamento; }
            set { _ltsDepartamento = value; }
        }


        private TareasNoCumplidas _CDepartamento;

        public TareasNoCumplidas CDepartamento
        {
            get { return _CDepartamento; }
            set { _CDepartamento = value; }
        }

        private TareasNoCumplidas _CTareasNoCumplidas;

        public TareasNoCumplidas CTareasNoCumplidas
        {
            get { return _CTareasNoCumplidas; }
            set { _CTareasNoCumplidas = value; }
        }

        private IniciarTurno _CIniciarTurno;

        public IniciarTurno CIniciarTurno
        {
            get { return _CIniciarTurno; }
            set { _CIniciarTurno = value; }
        }

        private IniciarTurno _CIniciarTurno2;

        public IniciarTurno CIniciarTurno2
        {
            get { return _CIniciarTurno2; }
            set { _CIniciarTurno2 = value; }
        }

        private List<ResumenTarea> _ltsTareasCumplidas;

        public List<ResumenTarea> ltsTareasCumplidas
        {
            get { return _ltsTareasCumplidas; }
            set { _ltsTareasCumplidas = value; }
        }

        private List<ResumenTarea> _ltsTareasNoCumplidas;

        public List<ResumenTarea> ltsTareasNoCumplidas
        {
            get { return _ltsTareasNoCumplidas; }
            set { _ltsTareasNoCumplidas = value; }
        }

        private List<ResumenTarea> _ltsTareasRequeridasNoCumplidas;

        public List<ResumenTarea> ltsTareasRequeridasNoCumplidas
        {
            get { return _ltsTareasRequeridasNoCumplidas; }
            set { _ltsTareasRequeridasNoCumplidas = value; }
        }

        private List<ResumenTarea> _TareasRequeridas;

        public List<ResumenTarea> TareasRequeridas
        {
            get { return _TareasRequeridas; }
            set { _TareasRequeridas = value; }
        }

        private List<ResumenTarea> _TareasPospuestas;

        public List<ResumenTarea> TareasPospuestas
        {
            get { return _TareasPospuestas; }
            set { _TareasPospuestas = value; }
        }

        private List<ResumenTarea> _TareasCanceladas;

        public List<ResumenTarea> TareasCanceladas
        {
            get { return _TareasCanceladas; }
            set { _TareasCanceladas = value; }
        }


        public void ObtenerCumplimiento(Guid UidSucursal, Guid UidUsuario, DateTime Fecha)
        {
            _ltsDepartamento = TareasNoCumplidasRepositorio.ConsultarCumplimiento(UidUsuario, UidSucursal, Fecha);
        }


        public void ObtenerTareas(Guid UidSucursal, Guid UidUsuario, DateTime Fecha)
        {
            _CDepartamento = TareasNoCumplidasRepositorio.ObtenerTareas(UidUsuario, UidSucursal, Fecha);
        }
        public void ObtenerDepartamento(Guid UidDepartamento, Guid UidUsuario, DateTime fecha, Guid UidSucursal)
        {
            CTareasNoCumplidas = TareasNoCumplidasRepositorio.DepartamentoSeleccionado(UidDepartamento, UidUsuario, fecha, UidSucursal);
        }
        public void ObtenerHora(DateTime fecha, Guid UidUsuario, Guid UidPeriodo)
        {
            _CIniciarTurno = IniciarTurnoRepositorio.ObtenerHora(fecha, UidUsuario, UidPeriodo);
        }

        public void ObtenerTareasCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha)
        {
            _ltsTareasCumplidas = ResumenTareaRepositorio.TareasCumplidas(UidDepartamento, UidUsuario, Fecha);
        }

        public void ObtenerTareasNoCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            ltsTareasNoCumplidas = ResumenTareaRepositorio.TareasNoCumplidas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasRequeridasNoCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            ltsTareasRequeridasNoCumplidas = ResumenTareaRepositorio.TareasRequeridasNoCumplidas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasRequeridas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            TareasRequeridas = ResumenTareaRepositorio.TareasRequeridas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasPospuestas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            TareasPospuestas = ResumenTareaRepositorio.TareasPospuestas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasCanceladas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            TareasCanceladas = ResumenTareaRepositorio.TareasCanceladas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

		/* 2018 08 03*/
		public bool ActualizarEstatusInicioTurnoEncargado(Guid UIDInicioTurno)
		{
			bool r = false;

			IniciarTurno.Repositorio repositorio = new IniciarTurno.Repositorio();
			r = repositorio.ActualizarEstatusTurnoEncargado(UIDInicioTurno);

			return r;
		}
	}
}