﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMAsignacionSupervision
    {
        AsignacionSupervision.Repository AsignacionSupervisionRepository = new AsignacionSupervision.Repository();
        Encargado.Repository usuarioRepository = new Encargado.Repository();
        Departamento.Repository departamentoRepository = new Departamento.Repository();
        Turno.Repository turnoRepository = new Turno.Repository();

        private List<AsignacionSupervision> _AsignacionSupervisions;

        public List<AsignacionSupervision> AsignacionesSupervision
        {
            get { return _AsignacionSupervisions; }
            set { _AsignacionSupervisions = value; }
        }

        private AsignacionSupervision _AsignacionSupervision;

        public AsignacionSupervision AsignacionSupervision
        {
            get { return _AsignacionSupervision; }
            set { _AsignacionSupervision = value; }
        }

        private List<Encargado> _Encargados;

        public List<Encargado> Encargados
        {
            get { return _Encargados; }
            set { _Encargados = value; }
        }

        private Usuario _Usuario;

        public Usuario Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private List<Departamento> _Departamentos;

        public List<Departamento> Departamentos
        {
            get { return _Departamentos; }
            set { _Departamentos = value; }
        }

        private List<Turno> _Turnos;

        public List<Turno> Turnos
        {
            get { return _Turnos; }
            set { _Turnos = value; }
        }

        public void ObtenerAsignacion(Guid uid)
        {
            _AsignacionSupervision = AsignacionSupervisionRepository.Find(uid);
        }

        public void ObtenerUltimaAsignacion(Guid departamento, Guid turno)
        {
            _AsignacionSupervision = AsignacionSupervisionRepository.Last(departamento, turno);
        }

        public void ObtenerAsignacionesActuales()
        {
            AsignacionSupervision.Criteria criteria = new AsignacionSupervision.Criteria()
            {
                FechaInicioDespuesDe = DateTime.Today,
            };
            _AsignacionSupervisions = AsignacionSupervisionRepository.FindBy(criteria, true);
        }

        public void BuscarAsignaciones(DateTime? fechaInicioDespuesDe, DateTime? fechaInicioAntesDe,
                                       DateTime? fechaFinDespuesDe, DateTime? fechaFinAntesDe,
                                       string usuario, String turno, String departamento, Guid sucursal)
        {
            AsignacionSupervision.Criteria criteria = new AsignacionSupervision.Criteria()
            {
                FechaInicioDespuesDe = fechaInicioDespuesDe,
                FechaInicioAntesDe = fechaInicioAntesDe,
                FechaFinDespuesDe = fechaFinDespuesDe,
                FechaFinAntesDe = fechaFinAntesDe,
                Usuarios = usuario,
                Turnos = turno,
                Departamentos = departamento,
                Sucursal = sucursal,
            };
            _AsignacionSupervisions = AsignacionSupervisionRepository.FindBy(criteria, true);
        }

        public AsignacionSupervision.ReturnCode GuardarAsignacion(AsignacionSupervision AsignacionSupervision)
        {
            return AsignacionSupervisionRepository.Save(AsignacionSupervision);
        }

        public void BuscarUsuario(string patron, Guid sucursal)
        {
			/*Modificacion 2018 07 25*/
			List<Encargado> lsTemp = usuarioRepository.FindByName(patron, patron, patron, sucursal.ToString());
			_Encargados = new List<Encargado>();
			foreach (Encargado item in lsTemp)
			{
				if (item.StrPerfil.ToUpperInvariant().Contains("SUPERVISOR"))
					_Encargados.Add(item);
			}
		}

        public void ObtenerUsuario(Guid uid)
        {
            _Usuario = usuarioRepository.Find(uid);
        }

        public void ObtenerDepartamentos(Guid sucursal)
        {
            _Departamentos = departamentoRepository.EncontrarTodos(sucursal);
        }

        public void ObtenerTurnos()
        {
            _Turnos = turnoRepository.FindAll();
        }

        public void ObtenerDepartamentosPorLista(List<Guid> departamentos)
        {
            string lpr = null;
            if (departamentos.Count > 0)
            {
                lpr = departamentos[0].ToString();
                for (int i = 1; i < departamentos.Count; i++)
                {
                    lpr += "," + departamentos[i];
                }
            }
            _Departamentos = departamentoRepository.EncontrarPorLista(lpr);
        }
    }
}