﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMAsignacion
    {
        Periodo.Repository periodoRepository = new Periodo.Repository();
        Encargado.Repository usuarioRepository = new Encargado.Repository();
        Departamento.Repository departamentoRepository = new Departamento.Repository();
        Turno.Repository turnoRepository = new Turno.Repository();
        PeriodoInactividad.Repository inactividadRepository = new PeriodoInactividad.Repository();
        TipoInactividad.Repository tiposRepository = new TipoInactividad.Repository();

        Dias.Repositorio diasRepository = new Dias.Repositorio();
        Ordinal.Repositorio ordinalRepository = new Ordinal.Repositorio();

        Periodicidad.Repositorio periodicidadRepository = new Periodicidad.Repositorio();
        PeriodicidadSemanal.Repositorio semanalRepository = new PeriodicidadSemanal.Repositorio();
        PeriodicidadMensual.Repositorio mensualRepository = new PeriodicidadMensual.Repositorio();
        PeriodicidadAnual.Repositorio anualRepository = new PeriodicidadAnual.Repositorio();
        TipoFrecuencia.Repositorio tipoFrecuenciaRepo = new TipoFrecuencia.Repositorio();


        private List<Periodo> _Periodos;

        public List<Periodo> Periodos
        {
            get { return _Periodos; }
            set { _Periodos = value; }
        }

        private Periodo _Periodo;

        public Periodo Periodo
        {
            get { return _Periodo; }
            set { _Periodo = value; }
        }

        private List<Encargado> _Encargados;

        public List<Encargado> Encargados
        {
            get { return _Encargados; }
            set { _Encargados = value; }
        }

        private Usuario _Usuario;

        public Usuario Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private List<Departamento> _Departamentos;

        public List<Departamento> Departamentos
        {
            get { return _Departamentos; }
            set { _Departamentos = value; }
        }

        private List<Turno> _Turnos;

        public List<Turno> Turnos
        {
            get { return _Turnos; }
            set { _Turnos = value; }
        }

        private List<Dias> _Dias;

        public List<Dias> Dias
        {
            get { return _Dias; }
            set { _Dias = value; }
        }

        private List<Ordinal> _Ordinales;

        public List<Ordinal> Ordinales
        {
            get { return _Ordinales; }
            set { _Ordinales = value; }
        }

        private Periodicidad _Periodicidad;

        public Periodicidad Periodicidad
        {
            get { return _Periodicidad; }
            set { _Periodicidad = value; }
        }

        private PeriodicidadSemanal _PeriodicidadSemanal;

        public PeriodicidadSemanal PeriodicidadSemanal
        {
            get { return _PeriodicidadSemanal; }
            set { _PeriodicidadSemanal = value; }
        }

        private PeriodicidadMensual _PeriodicidadMensual;

        public PeriodicidadMensual PeriodicidadMensual
        {
            get { return _PeriodicidadMensual; }
            set { _PeriodicidadMensual = value; }
        }


        public void ObtenerAsignacion(Guid uid)
        {
            _Periodo = periodoRepository.Find(uid);
        }

        public void ObtenerUltimaAsignacion(Guid departamento, Guid turno)
        {
            _Periodo = periodoRepository.Last(departamento, turno);
        }

        public void ObtenerAsignacionesActuales()
        {
            Periodo.Criteria criteria = new Periodo.Criteria()
            {
                FechaInicioDespuesDe = DateTime.Today,
            };
            _Periodos = periodoRepository.FindBy(criteria, true);
        }

        public void BuscarAsignaciones(DateTime? fechaInicioDespuesDe, DateTime? fechaInicioAntesDe,
                                       DateTime? fechaFinDespuesDe, DateTime? fechaFinAntesDe,
                                       string usuario, String turno, String departamento, Guid sucursal)
        {
            Periodo.Criteria criteria = new Periodo.Criteria()
            {
                FechaInicioDespuesDe = fechaInicioDespuesDe,
                FechaInicioAntesDe = fechaInicioAntesDe,
                FechaFinDespuesDe = fechaFinDespuesDe,
                FechaFinAntesDe = fechaFinAntesDe,
                Usuarios = usuario,
                Turnos = turno,
                Departamentos = departamento,
                Sucursal = sucursal,
            };
            _Periodos = periodoRepository.FindBy(criteria, true);
        }

        public Periodo.ReturnCode GuardarAsignacion(Periodo periodo)
        {
            return periodoRepository.Save(periodo);
        }
        
        public void BuscarUsuario(string patron, Guid sucursal)
        {
			/*Modificacion 2018 07 25*/
			List<Encargado> lsTemp = usuarioRepository.FindByName(patron, patron, patron, sucursal.ToString());
			_Encargados = new List<Encargado>();
			foreach (Encargado item in lsTemp)
			{
				if (!item.StrPerfil.ToUpperInvariant().Contains("SUPERVISOR"))
					_Encargados.Add(item);
			}
        }

        public void ObtenerUsuario(Guid uid)
        {
            _Usuario = usuarioRepository.Find(uid);
        }

        public void ObtenerDepartamentos(Guid sucursal)
        {
            _Departamentos = departamentoRepository.EncontrarTodos(sucursal);
        }

        public void ObtenerDepartamentosPorLista(List<Guid> departamentos)
        {
            string lpr = null;
            if (departamentos.Count > 0)
            {
                lpr = departamentos[0].ToString();
                for (int i = 1; i < departamentos.Count; i++)
                {
                    lpr += "," + departamentos[i];
                }
            }
            _Departamentos = departamentoRepository.EncontrarPorLista(lpr);
        }

        public void ObtenerTurnos()
        {
            _Turnos = turnoRepository.FindAll();
        }

        // Submodulo de Inactividad

        private List<PeriodoInactividad> _PeriodosInactividad;

        public List<PeriodoInactividad> PeriodosInactividad
        {
            get { return _PeriodosInactividad; }
            set { _PeriodosInactividad = value; }
        }

        private PeriodoInactividad _PeriodoInactividad;

        public PeriodoInactividad PeriodoInactividad
        {
            get { return _PeriodoInactividad; }
            set { _PeriodoInactividad = value; }
        }

        private List<TipoInactividad> _TiposInactividad;

        public List<TipoInactividad> TiposInactividad
        {
            get { return _TiposInactividad; }
            set { _TiposInactividad = value; }
        }

        private TipoFrecuencia _TipoFrecuencia;

        public TipoFrecuencia TipoFrecuencia
        {
            get { return _TipoFrecuencia; }
            set { _TipoFrecuencia = value; }
        }


        public void ObtenerInactividades(Guid uidPeriodo, DateTime? fechaInicio1, DateTime? fechaInicio2,
            DateTime? fechaFin1, DateTime? fechaFin2, string uidTipos)
        {
            PeriodosInactividad = inactividadRepository.Search(uidPeriodo, fechaInicio1, fechaFin1, uidTipos);
        }

        public void ObtenerInactividad(Guid uidInactividad)
        {
            PeriodoInactividad = inactividadRepository.Find(uidInactividad);
        }

        public void EliminarInactividad(Guid uidInactividad, DateTime fecha)
        {
            inactividadRepository.Delete(uidInactividad, fecha);
        }

        public void GuardarInactividad(PeriodoInactividad periodo)
        {
            inactividadRepository.Save(periodo);
        }

        public void ObtenerTiposInactividad()
        {
            _TiposInactividad = tiposRepository.FindAll();
        }

        public void ObtenerDias()
        {
            _Dias = diasRepository.ConsultarDias();
        }

        public void ObtenerOrdinales()
        {
            _Ordinales = ordinalRepository.ConsultarOrdinal();
        }

        public void ObtenerPeriodicidad(Guid periodicidad)
        {
            _Periodicidad = periodicidadRepository.ConsultarPeriodicidad(periodicidad);
        }

        public void ObtenerPeriodicidadSemanal(Guid periodicidad)
        {
            _PeriodicidadSemanal = semanalRepository.ConsultarPeriodicidadSemanal(periodicidad);
        }

        public void ObtenerPeriodicidadMensual(Guid periodicidad)
        {
            _PeriodicidadMensual = mensualRepository.ConsultarPeriodicidadMensual(periodicidad);
        }
        
        public void ObtenerTipoFrecuencia(Guid tipo)
        {
            TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
            {
                UidTipoFrecuencia = tipo,
            };

            _TipoFrecuencia = tipoFrecuenciaRepo.Buscar(criterio);
        }

        public void BuscarTipoFrecuencia(string nombre)
        {
            TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
            {
                TipoFrecuencia = nombre,
            };

            _TipoFrecuencia = tipoFrecuenciaRepo.Buscar(criterio);
        }
    }
}