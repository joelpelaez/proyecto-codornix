﻿using CodorniX.Modelo;
using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMCumplimiento
    {
        public enum ModoCambio
        {
            Cancelar = 1,
            Posponer = 2
        }

        private Tarea.Repositorio tareaRepository = new Tarea.Repositorio();
        private Cumplimiento.Repository cumplimientoRepository = new Cumplimiento.Repository();
        private Departamento.Repository departamentoRepository = new Departamento.Repository();
        private Area.Repository areaRepository = new Area.Repository();
        private TareaOpcion.Repositorio opcionRepository = new TareaOpcion.Repositorio();
        private TipoTarea.Repositorio tipoRepository = new TipoTarea.Repositorio();
        private Periodo.Repository periodoRepository = new Periodo.Repository();
        private EstadoCumplimiento.Repository estadoRepository = new EstadoCumplimiento.Repository();
        private Periodicidad.Repositorio periodicidadRepository = new Periodicidad.Repositorio();
        private TipoFrecuencia.Repositorio frecuenciaRepository = new TipoFrecuencia.Repositorio();
        private Notificacion.Repository notificacionRepository = new Notificacion.Repository();
        private MensajeNotificacion.Repository mensajeNotifiacionRepository = new MensajeNotificacion.Repository();
        private Sucursal.Repository sucursalRepository = new Sucursal.Repository();
        private Empresa.Repository empresaRepository = new Empresa.Repository();
        private Usuario.Repository usuarioRepository = new Usuario.Repository();

        private List<Cumplimiento> _CumplimientosPendientes;

        public List<Cumplimiento> CumplimientosPendientes
        {
            get { return _CumplimientosPendientes; }
            set { _CumplimientosPendientes = value; }
        }

        private List<Cumplimiento> _ltsDepartamentos;

        public List<Cumplimiento> ltsDepartamentos
        {
            get { return _ltsDepartamentos; }
            set { _ltsDepartamentos = value; }
        }

        private List<TipoTarea> _tiposTarea;

        public List<TipoTarea> TiposTarea
        {
            get { return _tiposTarea; }
            set { _tiposTarea = value; }
        }

        private Tarea _Tarea;

        public Tarea Tarea
        {
            get { return _Tarea; }
            set { _Tarea = value; }
        }

        private Departamento _Departamento;

        public Departamento Departamento
        {
            get { return _Departamento; }
            set { _Departamento = value; }
        }

        private List<Departamento> _departamentos;

        public List<Departamento> Departamentos
        {
            get { return _departamentos; }
            set { _departamentos = value; }
        }

        private Area _Area;

        public Area Area
        {
            get { return _Area; }
            set { _Area = value; }
        }

        private List<Area> _areas;

        public List<Area> Areas
        {
            get { return _areas; }
            set { _areas = value; }
        }

        private List<TareaOpcion> _Opciones;

        public List<TareaOpcion> Opciones
        {
            get { return _Opciones; }
            set { _Opciones = value; }
        }

        private Cumplimiento _Cumplimiento;

        public Cumplimiento Cumplimiento
        {
            get { return _Cumplimiento; }
            set { _Cumplimiento = value; }
        }

        private Cumplimiento _CCumplimiento;

        public Cumplimiento CCumplimiento
        {
            get { return _CCumplimiento; }
            set { _CCumplimiento = value; }
        }

        private List<EstadoCumplimiento> _Estados;

        public List<EstadoCumplimiento> Estados
        {
            get { return _Estados; }
            set { _Estados = value; }
        }

        private Periodo _Periodo;

        public Periodo Periodo
        {
            get { return _Periodo; }
            set { _Periodo = value; }
        }

        private Periodicidad _periodicidad;

        public Periodicidad Periodicidad
        {
            get { return _periodicidad; }
            set { _periodicidad = value; }
        }

        private TipoFrecuencia _tipoFrecuencia;

        public TipoFrecuencia TipoFrecuencia
        {
            get { return _tipoFrecuencia; }
            set { _tipoFrecuencia = value; }
        }

        public void ObtenerTareasDeHoy(Guid uidUsuario, Guid uidPeriodo, List<Guid> periodos, string nombre, string estados, Guid departamento, Guid area, Guid tipo, DateTime fecha)
        {
            string lpr = null;
            if (periodos.Count > 0)
            {
                lpr = periodos[0].ToString();
                for (int i = 1; i < periodos.Count; i++)
                {
                    lpr += "," + periodos[i].ToString();
                }
            }
            _CumplimientosPendientes = cumplimientoRepository.FindByUser(uidUsuario, uidPeriodo, fecha, lpr, nombre, estados, departamento, area, tipo);
        }

        public void ObtenerTarea(Guid uid)
        {
            _Tarea = tareaRepository.Encontrar(uid);
        }

        public void ObtenerTareaCumplimiento(Guid uid, Guid UidUsuario)
        {
            _CCumplimiento = cumplimientoRepository.ObtenerTareaCumplimiento(uid,  UidUsuario);
        }

        public void ObtenerDepartamento(Guid uid)
        {
            _Departamento = departamentoRepository.Encontrar(uid);
        }

        public void ObtenerDepartamentos(List<Guid> periodos)
        {
            string lpr = null;
            if (periodos.Count > 0)
            {
                lpr = periodos[0].ToString();
                for (int i = 1; i < periodos.Count; i++)
                {
                    lpr += "," + periodos[i];
                }
            }
            _departamentos = departamentoRepository.EncontrarPorListaDePeriodos(lpr);
        }

        public void ObtenerArea(Guid uid)
        {
            _Area = areaRepository.Find(uid);
        }

        public void ObtenerAreas(Guid uid)
        {
            _areas = areaRepository.FindAll(uid);
        }

        public void ObtenerOpcionesDeTarea(Guid uidTarea)
        {
            _Opciones = opcionRepository.Buscar(uidTarea);
        }

        public void ObtenerCumplimiento(Guid uid)
        {
            _Cumplimiento = cumplimientoRepository.Find(uid);
        }

        public void RegistrarCumplimiento(ref Guid? uidCumplimiento, Guid uidTarea, Guid? uidDepartamento,
            Guid? uidArea, Guid uidUsuario, DateTimeOffset fechaCumplimiento, bool? estado, decimal? valor1,
            decimal? valor2, Guid? uidOpcion, string observaciones, string urlFoto, Guid turno)
        {
            cumplimientoRepository.RegistrarCumplimiento(ref uidCumplimiento, uidTarea, uidDepartamento,
                uidArea, uidUsuario, fechaCumplimiento, estado, valor1, valor2, uidOpcion, observaciones, urlFoto, turno);
        }

        public int CambiarCumplimiento(Guid? uidCumplimiento, Guid uidTarea, Guid? uidDepartamento, Guid? uidArea, Guid? uidUsuario,
            DateTime? fechaNueva, ModoCambio modo, DateTimeOffset fecha)
        {
            string operation = modo == ModoCambio.Cancelar ? "Cancelado" : "Pospuesto";

            int result = cumplimientoRepository.CambiarCumplimiento(uidCumplimiento, uidTarea, uidDepartamento, uidArea, uidUsuario, fechaNueva, operation, fecha);

            return result;
        }

        public void ObtenerPeriodo(Guid uid)
        {
            _Periodo = periodoRepository.Find(uid);
        }
        public void Buscar(string Fecha, string Fecha2, Guid uidusurio)
        {
            _ltsDepartamentos = cumplimientoRepository.Buscar(Fecha, Fecha2,uidusurio);
        }

        public void ActualizarCumplimiento(Guid uidCumplimiento, DateTimeOffset fechaCumplimiento, bool? estado, decimal? valor1,
            decimal? valor2, Guid? uidOpcion, string observaciones, string urlFoto)
        {
            cumplimientoRepository.ActualizarCumplimiento(uidCumplimiento, fechaCumplimiento, estado, valor1, valor2, uidOpcion, observaciones, urlFoto);
        }


        public DateTime? ObtenerFechaSiguienteTarea(Guid uidTarea, DateTime fecha)
        {
            return cumplimientoRepository.ObtenerSiguienteFecha(uidTarea, fecha);
        }

        public void DeshacerCumplimiento(Guid uidCumplimiento)
        {
            cumplimientoRepository.Deshacer(uidCumplimiento);
        }

        public void ObtenerEstados()
        {
            _Estados = estadoRepository.FindAll();
        }

        public void ObtenerTiposTarea()
        {
            _tiposTarea = tipoRepository.ConsultarTipoTarea();
        }

        public void ObtenerPeriocidad(Guid uid)
        {
            _periodicidad = periodicidadRepository.ConsultarPeriodicidad(uid);
        }

        public void ObtenerTipoFrecuencia(Guid uid)
        {
            TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
            {
                UidTipoFrecuencia = uid
            };

            _tipoFrecuencia = frecuenciaRepository.Buscar(criterio);
        }

        public void EnviarDeshacerNotificacion(Guid uidCumplimiento)
        {
            MensajeNotificacion mensajeNotificacion = mensajeNotifiacionRepository.FindByCumplimiento(uidCumplimiento);
            if (mensajeNotificacion != null)
            {
                string areaNombre = "General";
                Cumplimiento cumplimiento = cumplimientoRepository.Find(mensajeNotificacion.UidCumplimiento);
                Tarea tarea = tareaRepository.Encontrar(cumplimiento.UidTarea);
                Departamento departamento = null;
                Area area = null;
                if (cumplimiento.UidArea.HasValue)
                {
                    area = areaRepository.Find(cumplimiento.UidArea.Value);
                    areaNombre = area.StrNombre;
                    departamento = departamentoRepository.Encontrar(area.UidArea);
                }
                else
                {
                    departamento = departamentoRepository.Encontrar(cumplimiento.UidDepartamento.Value);
                }
                Sucursal sucursal = sucursalRepository.Find(departamento.UidSucursal);
                Empresa empresa = empresaRepository.Find(sucursal.UidEmpresa);
                DateTimeOffset time = Hora.ObtenerHoraServidor();
                DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(sucursal.UidSucursal));
                var local = horaLocal.DateTime;

                string asunto = "Notificacion de Cancelación de Cumplimiento - " + empresa.StrNombreComercial + " - " + sucursal.StrNombre;

                var modelo = new
                {
                    Asunto = asunto,
                    Empresa = empresa.StrNombreComercial,
                    Sucursal = sucursal.StrNombre,
                    Departamento = departamento.StrNombre,
                    Area = areaNombre,
                    Tarea = tarea.StrNombre,
                    Folio = tarea.IntFolio.ToString("0000") + "-" + cumplimiento.IntFolio.ToString("0000"),
                    FechaCancelacion = local,
                    Valor = mensajeNotificacion.StrResultado,
                    Observaciones = cumplimiento.StrObservacion,
                };

                mensajeNotifiacionRepository.ChangeState(mensajeNotificacion.UidMensajeNotificacion, "Cancelado");

                List<Usuario> usuarios = usuarioRepository.ObtenerCorreos(cumplimiento.UidCumplimiento);
                if (usuarios.Count == 0)
                    return;

                List<MailAddress> mails = usuarios.Where(x => IsValidEmail(x.STRCORREO)).Select(x => new MailAddress(x.STRCORREO, x.StrNombreCompleto)).ToList();
                MailAddressCollection mailAddresses = new MailAddressCollection();
                foreach (var mail in mails)
                {
                    mailAddresses.Add(mail);
                }

                PageRender pageRender = PageRender.Instance();
                string content = pageRender.RenderViewToString("NotificacionCancelada.cshtml", modelo);

                Correo correo = new Correo();

                correo.SendEmail(mailAddresses, asunto, content);
            }
        } 

        public void EnviarNotificacion(Guid uidCumplimiento)
        {
            MensajeNotificacion mensajeNotificacion = mensajeNotifiacionRepository.FindByCumplimiento(uidCumplimiento);
            if (mensajeNotificacion != null)
            {
                string areaNombre = "General";
                Cumplimiento cumplimiento = cumplimientoRepository.Find(mensajeNotificacion.UidCumplimiento);
                Tarea tarea = tareaRepository.Encontrar(cumplimiento.UidTarea);
                Departamento departamento = null;
                Area area = null;
                if (cumplimiento.UidArea.HasValue)
                {
                    area = areaRepository.Find(cumplimiento.UidArea.Value);
                    areaNombre = area.StrNombre;
                    departamento = departamentoRepository.Encontrar(area.UidDepartamento);
                }
                else
                {
                    departamento = departamentoRepository.Encontrar(cumplimiento.UidDepartamento.Value);
                }
                Sucursal sucursal = sucursalRepository.Find(departamento.UidSucursal);
                Empresa empresa = empresaRepository.Find(sucursal.UidEmpresa);
                
                string asunto = "Notificacion de Cumplimiento - " + empresa.StrNombreComercial + " - " + sucursal.StrNombre;

                var modelo = new
                {
                    Asunto = asunto,
                    Empresa = empresa.StrNombreComercial,
                    Sucursal = sucursal.StrNombre,
                    Departamento = departamento.StrNombre,
                    Area = areaNombre,
                    Tarea = tarea.StrNombre,
                    Folio = tarea.IntFolio.ToString("0000") + "-" + cumplimiento.IntFolio.ToString("0000"),
                    FechaCumplimiento = cumplimiento.DtFechaHora.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    Valor = mensajeNotificacion.StrResultado,
                    Observaciones = cumplimiento.StrObservacion,
                };

                List<Usuario> usuarios = usuarioRepository.ObtenerCorreos(cumplimiento.UidCumplimiento);
                if (usuarios.Count == 0)
                    return;

                List<MailAddress> mails = usuarios.Where(x => IsValidEmail(x.STRCORREO)).Select(x => new MailAddress(x.STRCORREO, x.StrNombreCompleto)).ToList();
                MailAddressCollection mailAddresses = new MailAddressCollection();
                foreach (var mail in mails)
                {
                    mailAddresses.Add(mail);
                }

                PageRender pageRender = PageRender.Instance();
                string content = pageRender.RenderViewToString("Notificacion.cshtml", modelo);

                Correo correo = new Correo();
                
                correo.SendEmail(mailAddresses, asunto, content);
            }
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}