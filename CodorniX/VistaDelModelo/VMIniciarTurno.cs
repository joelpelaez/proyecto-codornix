﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMIniciarTurno
	{
		private IniciarTurno.Repositorio IniciarTunoRepositorio = new IniciarTurno.Repositorio();
		private TareasNoCumplidas.Repositorio TareasNoCumplidasRepositorio = new TareasNoCumplidas.Repositorio();
		private Periodo.Repository PeriodoRepositorio = new Periodo.Repository();
		private Cumplimiento.Repository cumplimientoRepository = new Cumplimiento.Repository();
		private Usuario.Repository usuarioRepository = new Usuario.Repository();
		private Perfil.Repositorio perfilRepository = new Perfil.Repositorio();

		private EstadoTurno.Repository estadoRepository = new EstadoTurno.Repository();

		private IniciarTurno _CIniciarTurno;
		public IniciarTurno CIniciarTurno
		{
			get { return _CIniciarTurno; }
			set { _CIniciarTurno = value; }
		}

		private IniciarTurno _CInicioTurno;
		public IniciarTurno CInicioTurno
		{
			get { return _CInicioTurno; }
			set { _CInicioTurno = value; }
		}

		private IniciarTurno _CIniciarTurno2;
		public IniciarTurno CIniciarTurno2
		{
			get { return _CIniciarTurno2; }
			set { _CIniciarTurno2 = value; }
		}

		private Periodo _CPeriodo;
		public Periodo CPeriodo
		{
			get { return _CPeriodo; }
			set { _CPeriodo = value; }
		}


		private TareasNoCumplidas _CTareasNoCumplidas;

		public TareasNoCumplidas CTareasNoCumplidas
		{
			get { return _CTareasNoCumplidas; }
			set { _CTareasNoCumplidas = value; }
		}

		private List<TareasNoCumplidas> _ltsDepartamento;

		public List<TareasNoCumplidas> ltsDepartamento
		{
			get { return _ltsDepartamento; }
			set { _ltsDepartamento = value; }
		}

		private List<Cumplimiento> _Cumplimientos;

		public List<Cumplimiento> Cumplimientos
		{
			get { return _Cumplimientos; }
			set { _Cumplimientos = value; }
		}

		private Usuario _Encargado;

		public Usuario Encargado
		{
			get { return _Encargado; }
			set { _Encargado = value; }
		}

		private Perfil _perfil;

		public Perfil Perfil
		{
			get { return _perfil; }
			set { _perfil = value; }
		}

		private EstadoTurno _EstadoTurno;

		public EstadoTurno EstadoTurno
		{
			get { return _EstadoTurno; }
			set { _EstadoTurno = value; }
		}


		public bool Guardar(Guid uidusuario, DateTimeOffset fecha, Guid UidPeriodo, bool isEncargado, bool isSupervisor)
		{
			CIniciarTurno = new IniciarTurno()
			{
				UidUsuario = uidusuario,
				DtFechaHoraInicio = fecha,
				UidPeriodo = UidPeriodo,
				UidCreador = uidusuario,

			};
			bool resultado = false;
			try
			{
				resultado = CIniciarTurno.GuardarDatos(isEncargado, isSupervisor);
			}
			catch (Exception)
			{

				throw;
			}
			return resultado;
		}

		public bool Modificar(Guid UidInicioTurno, DateTimeOffset? fechaFin, string nocompletado)
		{

			CIniciarTurno = new IniciarTurno()
			{
				UidInicioTurno = UidInicioTurno,
				IntNoCompleto = 0,
				DtFechaHoraFin = fechaFin.HasValue ? fechaFin.Value : (DateTimeOffset?)null,
			};
			bool Resultado = false;
			try
			{
				Resultado = CIniciarTurno.ModificarDatos();
			}
			catch (Exception)
			{

				throw;
			}
			return Resultado;
		}

		public void ObtenerInicioTurno(Guid inicioturno)
		{
			_CIniciarTurno = IniciarTunoRepositorio.ObtenerInicioTurno(inicioturno);
		}

		public void ObtenerNoCumplidos(Guid UidSucursal, Guid UidUsuario, string Fecha)
		{
			_CTareasNoCumplidas = TareasNoCumplidasRepositorio.ObtenerNoCumplimiento(UidSucursal, UidUsuario, Convert.ToDateTime(Fecha));
		}

		public void ObtenerCumplimiento(Guid UidSucursal, Guid UidUsuario, DateTime Fecha)
		{
			_ltsDepartamento = TareasNoCumplidasRepositorio.ConsultarCumplimiento(UidUsuario, UidSucursal, Fecha);
		}

		public void ObtenerInicioPorPeriodo(Guid usuario, Guid periodo, DateTime fecha)
		{
			_CIniciarTurno2 = IniciarTunoRepositorio.ObtenerInicioPorPeriodo(usuario, periodo, fecha);
		}
		public void ObtenerAsignacion(Guid UidUsuario)
		{
			_CPeriodo = PeriodoRepositorio.BuscarPeriodo(UidUsuario);
		}

		public void ObtenerCumplimientos(Guid uidUsuario, Guid uidSucursal, DateTime fecha, string periodos)
		{
			_Cumplimientos = cumplimientoRepository.FindByUser(uidUsuario, uidSucursal, fecha, periodos, null, null, Guid.Empty, Guid.Empty, Guid.Empty);
		}

		public void ObtenerUsuario(Guid uid)
		{
			_Encargado = usuarioRepository.Find(uid);
		}
		public void ObtenerPeriodoTurno(Guid UidPeriodo)
		{
			_CPeriodo = PeriodoRepositorio.ObtenerPeriodoTurno(UidPeriodo);
		}

		public void ObtenerTurnoUsuario(Guid UidPeriodo, string Fecha)
		{
			_CIniciarTurno = IniciarTunoRepositorio.ObtenerTurnoUsuario(UidPeriodo, Convert.ToDateTime(DateTime.ParseExact(Fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture)));
		}

		public void ModificarEstadoTurno(Guid uidTurno, string estado)
		{
			IniciarTunoRepositorio.ModificarEstado(uidTurno, estado);
		}

		public void ObtenerPerfil(Guid uidPerfil)
		{
			_perfil = perfilRepository.CargarDatos(uidPerfil);
		}

		public int ObtenerNumeroTareasSinEstado(Guid UidUsuario, Guid UidPeriodo, DateTime fecha)
		{
			return TareasNoCumplidasRepositorio.NumeroTareasSinEstado(UidPeriodo, UidUsuario, fecha);
		}

		public void ObtenerEstadoTurno(Guid uid)
		{
			_EstadoTurno = estadoRepository.Find(uid);
		}

		public bool ValidarCirreInicioTurnoAnteriorDepartamento(Guid? uidInicioTurno, Guid uidDepartamento)
		{
			IniciarTurno Aux = new IniciarTurno();
			Aux = IniciarTunoRepositorio.ObtenerUltimoPorDepartamento(uidInicioTurno, uidDepartamento);

			if (Aux.DtFechaHoraFin == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
}