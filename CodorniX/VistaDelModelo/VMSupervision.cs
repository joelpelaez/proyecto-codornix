﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMSupervision
    {
        private TareasNoCumplidas.Repositorio TareasNoCumplidasRepositorio = new TareasNoCumplidas.Repositorio();
        private IniciarTurno.Repositorio IniciarTurnoRepositorio = new IniciarTurno.Repositorio();
        private ResumenTarea.Repositorio ResumenTareaRepositorio = new ResumenTarea.Repositorio();
        private EstadoTurno.Repository estadoRepository = new EstadoTurno.Repository();
        private List<TareasNoCumplidas> _ltsDepartamento;

        public List<TareasNoCumplidas> ltsDepartamento
        {
            get { return _ltsDepartamento; }
            set { _ltsDepartamento = value; }
        }


        private TareasNoCumplidas _CDepartamento;

        public TareasNoCumplidas CDepartamento
        {
            get { return _CDepartamento; }
            set { _CDepartamento = value; }
        }

        private TareasNoCumplidas _CTareasNoCumplidas;

        public TareasNoCumplidas CTareasNoCumplidas
        {
            get { return _CTareasNoCumplidas; }
            set { _CTareasNoCumplidas = value; }
        }

        private List<ResumenTarea> _ltsTareasCumplidas;

        public List<ResumenTarea> ltsTareasCumplidas
        {
            get { return _ltsTareasCumplidas; }
            set { _ltsTareasCumplidas = value; }
        }

        private List<ResumenTarea> _ltsTareasNoCumplidas;

        public List<ResumenTarea> ltsTareasNoCumplidas
        {
            get { return _ltsTareasNoCumplidas; }
            set { _ltsTareasNoCumplidas = value; }
        }

        private List<ResumenTarea> _ltsTareasRequeridasNoCumplidas;

        public List<ResumenTarea> ltsTareasRequeridasNoCumplidas
        {
            get { return _ltsTareasRequeridasNoCumplidas; }
            set { _ltsTareasRequeridasNoCumplidas = value; }
        }

        private List<ResumenTarea> _TareasRequeridas;

        public List<ResumenTarea> TareasRequeridas
        {
            get { return _TareasRequeridas; }
            set { _TareasRequeridas = value; }
        }

        private List<ResumenTarea> _TareasPospuestas;

        public List<ResumenTarea> TareasPospuestas
        {
            get { return _TareasPospuestas; }
            set { _TareasPospuestas = value; }
        }

        private List<ResumenTarea> _TareasCanceladas;

        public List<ResumenTarea> TareasCanceladas
        {
            get { return _TareasCanceladas; }
            set { _TareasCanceladas = value; }
        }

        private IniciarTurno _InicioTurno;

        public IniciarTurno InicioTurno
        {
            get { return _InicioTurno; }
            set { _InicioTurno = value; }
        }

        private EstadoTurno _EstadoTurno;

        public EstadoTurno EstadoTurno
        {
            get { return _EstadoTurno; }
            set { _EstadoTurno = value; }
        }


        public void ObtenerCumplimiento(Guid UidSucursal, Guid UidUsuario, DateTime Fecha)
        {
            _ltsDepartamento = TareasNoCumplidasRepositorio.ConsultarSupervision(UidUsuario, UidSucursal, Fecha);
        }


        public void ObtenerTareas(Guid UidSucursal, Guid UidUsuario, DateTime Fecha)
        {
            _CDepartamento = TareasNoCumplidasRepositorio.ObtenerTareas(UidUsuario, UidSucursal, Fecha);
        }
        public void ObtenerDepartamento(Guid UidDepartamento, Guid UidUsuario, DateTime fecha, Guid UidSucursal)
        {
            CTareasNoCumplidas = TareasNoCumplidasRepositorio.DepartamentoSeleccionado(UidDepartamento, UidUsuario, fecha, UidSucursal);
        }
        

        public void ObtenerTareasCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha)
        {
            _ltsTareasCumplidas = ResumenTareaRepositorio.TareasCumplidas(UidDepartamento, UidUsuario, Fecha);
        }

        public void ObtenerTareasNoCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            ltsTareasNoCumplidas = ResumenTareaRepositorio.TareasNoCumplidas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasRequeridasNoCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            ltsTareasRequeridasNoCumplidas = ResumenTareaRepositorio.TareasRequeridasNoCumplidas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasRequeridas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            TareasRequeridas = ResumenTareaRepositorio.TareasRequeridas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasPospuestas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            TareasPospuestas = ResumenTareaRepositorio.TareasPospuestas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void ObtenerTareasCanceladas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
        {
            TareasCanceladas = ResumenTareaRepositorio.TareasCanceladas(UidDepartamento, UidUsuario, Fecha, uidsucursal);
        }

        public void CrearTurno(Guid uidPeriodo, Guid uidUsuario, Guid uidCreador, DateTimeOffset fecha, string estadoInicial,bool isEncargado, bool isSupervisor)
        {
            IniciarTurno turno = new IniciarTurno();
            turno.UidPeriodo = uidPeriodo;
            turno.UidUsuario = uidUsuario;
            turno.DtFechaHoraInicio = fecha;
            turno.UidCreador = uidCreador;
            turno.GuardarDatos(isEncargado,isSupervisor);
            IniciarTurnoRepositorio.ModificarEstado(turno.UidInicioTurno, estadoInicial);

            _InicioTurno = turno;
        }

        public void ObtenerTurno(Guid uidInicioTurno)
        {
            _InicioTurno = IniciarTurnoRepositorio.Find(uidInicioTurno);
        }

        public void ModificarEstadoTurno(Guid uidInicioTurno, string estado)
        {
            IniciarTurnoRepositorio.ModificarEstado(uidInicioTurno, estado);
        }

        public void CerrarTurno(Guid uidInicioTurno, DateTimeOffset fechaHora)
        {
            IniciarTurno turno = IniciarTurnoRepositorio.Find(uidInicioTurno);
            turno.DtFechaHoraFin = fechaHora;
            turno.ModificarDatos();
            IniciarTurnoRepositorio.ModificarEstado(turno.UidInicioTurno, "Cerrado");
        }

		public void ReabrirTurno(Guid uidInicioTurno, string estado)
		{
			IniciarTurnoRepositorio.ReAbrirTurno(uidInicioTurno,estado);
		}
        public void ObtenerEstadoTurno(Guid uid)
        {
            _EstadoTurno = estadoRepository.Find(uid);
        }
    }
}