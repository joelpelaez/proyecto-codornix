﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMDepartamentos
    {
        private Departamento.Repository repository = new Departamento.Repository();
        private Area.Repository areasRepository = new Area.Repository();
        private Status.Repository statusRepository = new Status.Repository();

        private List<Departamento> _departamentos = null;
        public List<Departamento> Departamentos { get { return _departamentos; } }

        private Departamento _departamento;

        public Departamento Departamento
        {
            get { return _departamento; }
            set { _departamento = value; }
        }

        private List<Area> _areas;

        public List<Area> Areas { get { return _areas; } }

        private Area _area;

        public Area Area
        {
            get { return _area; }
            set { _area = value; }
        }

        private List<Status> _status;

        public List<Status> Status
        {
            get { return _status; }
            set { _status = value; }
        }


        public void ObtenerDepartamentos(Guid uid)
        {
            _departamentos = repository.EncontrarTodos(uid);
        }

        public void ObtenerDepartamento(Guid uid)
        {
            _departamento = repository.Encontrar(uid);
        }

        public void GuardarDepartamento(Departamento departamento)
        {
            repository.Guardar(departamento);
        }

        public void BusquedaDepartamento(string nombre, string descripcion, Guid sucursal)
        {
            Departamento.Criterio criteria = new Departamento.Criterio()
            {
                Nombre = nombre,
                Descripcion = descripcion,
                Sucursal = sucursal,
            };
            _departamentos = repository.EncontrarPor(criteria);
        }
        

        public void ObtenerAreas(Guid uidDepto)
        {
            _areas = areasRepository.FindAll(uidDepto);
        }

        public void ObtenerArea(Guid uid)
        {
            _area = areasRepository.Find(uid);
        }

        public void BusquedaArea(string nombre, string descripcion, Guid depto)
        {
            Area.Criteria criteria = new Area.Criteria()
            {
                Nombre = nombre,
                Descripcion = descripcion,
                Departamento = depto,
            };
            _areas = areasRepository.FindBy(criteria);
        }

        public void GuardarArea(Area area)
        {
            areasRepository.Save(area);
        }

        public void ObtenerStatus()
        {
            _status = statusRepository.FindAll();
        }
    }
}