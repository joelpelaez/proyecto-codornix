﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class RevisionType : ObjectGraphType<Revision>
	{
		public RevisionType()
		{
			Field(x => x.strIdRevision).Name("id").Description("Identificador de la revision");
			Field(x => x.IntFolio).Name("folio").Description("Folio");
			Field(x => x.strIdCumplimiento).Name("idCumplimiento").Description("Identificador del cumplimiento");
			Field(x => x.strIdUsuario).Name("idUsuario").Description("Identificador del usuario");
			Field(x => x.strFechaHora).Name("fechaHora").Description("Fecha Hora de la revision de la tarea");
			Field(x => x.StrNotas).Name("notas").Description("Notas de la tarea");
			Field(x => x.BlCorrecto).Name("esCorrecto").Description("Correcto");
			Field(x => x.strBlValor).Name("valorBoolean").Description("Valor del resultado de la tarea ");
			Field(x => x.strDcValor1).Name("valorUnoDecimal").Description("Valor de resultado de la tarea 1");
			Field(x => x.strDcValor2).Name("valorDosDecimal").Description("Valor de resultado de la tarea 2");
			Field(x => x.strIdOpcion).Name("idOpcion").Description("Identificador de la opcion");
			Field(x => x.StrOpcion).Name("opcion").Description("Opcion de la tarea");
			Field(x => x.strIdCalificacion).Name("idCalificacion").Description("Identificador de la calificacion");
			Field(x => x.StrEstado).Name("estado").Description("Estado de la actividad");
			Field(x => x.StrNombreUsuario).Name("nombreUsuario").Description("Nombre del usuario");
			Field(x => x.StrApellidoPaterno).Name("apellidoPaterno").Description("Apellido Paterno");
			Field(x => x.StrTarea).Name("tarea").Description("Titulo de tarea");
			Field(x => x.StrDepartamento).Name("departamento").Description("Departamento");
			Field(x => x.StrArea).Name("area").Description("Area de la tarea");
			Field(x => x.strHora).Name("hora").Description("Hora programada para realizar tarea");
			Field(x => x.StrValor).Name("valor").Description("Valor en texto");
			Field(x => x.StrTipoMedicion).Name("tipoMedicion").Description("Tipo de Medicion");
			Field(x => x.StrTipoUnidad).Name("tipoUnidad").Description("Tipo de unidad");
			Field(x => x.BlValorOriginal).Name("valorOriginalBoolean").Description("");
			Field(x => x.DcValor1Original).Name("valorUnoOriginalDecimal").Description("");
			Field(x => x.DcValor2Original).Name("valorDosOriginalDecimal").Description("");
			Field(x => x.strIdOpcionOriginal).Name("idOpcionOriginal").Description("Identificador de la opcion original");
			Field(x => x.StrOpcionOriginal).Name("opcionOrignial").Description("Opcion original");
			Field(x => x.StrObservacion).Name("observacion").Description("Observaciones");				
		}
	}
}