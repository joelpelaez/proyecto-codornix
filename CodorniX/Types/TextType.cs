﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GraphQL.Types;
using CodorniX.Modelo;

namespace CodorniX.Types
{
	public class TextType : ObjectGraphType<ServerMessage>
	{
		public TextType() {
			Field(x => x.Value).Name("value").Description("Value");
		}
	}
}