﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class ResumenTareaType : ObjectGraphType<ResumenTarea>
	{
		public ResumenTareaType() {
			Field(x => x.strIdTareaCumplida).Name("idTareaCumplida").Description("Identificador tarea cumplida");
			Field(x => x.strIdTareaNoCumplida).Name("idTareaNoCumplida").Description("Identificador tarea no cumplida");
			Field(x => x.strIdTareaRequeridaNoCumplida).Name("idTareaRequeridaNoCumplida").Description("Identificador tarea requerida no cumplida");

			Field(x => x.StrNombre).Name("nombre").Description("Nombre");
			Field(x => x.StrTipoFrecuencia).Name("tipoFrecuencia").Description("Tipo de frecuencia");
			Field(x => x.StrTipoTarea).Name("tipoTarea").Description("Tipo de tarea");
			Field(x => x.StrStatus).Name("estatus").Description("Estatus");
			Field(x => x.IntFolio).Name("folio").Description("Folio");
			Field(x => x.StrEstadoCumplimiento).Name("estadoCumplimiento").Description("Estado de cumplimiento");

		}
	}
}