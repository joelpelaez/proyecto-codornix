﻿using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class AsignacionSupervisionType : ObjectGraphType<AsignacionSupervision>
	{
		public AsignacionSupervisionType()
		{
			Field(x => x.strIdAsignacion).Name("id").Description("Identificador de la asignacion");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identificador del departamento");
			Field(x => x.strIdTurno).Name("idTurno").Description("Identificador del turno");
			Field(x => x.strIdUsuario).Name("idUsuario").Description("Identificador del usuario");
			Field(x => x.strDtFechaInicio).Name("fechaInicio").Description("Fecha de inicio");
			Field(x => x.strDtFechaFin).Name("fechaFin").Description("Fecha Fin");
			Field(x => x.StrNombreDepto).Name("departamento").Description("Nombre del departamento");
			Field(x => x.StrNombreUsuario).Name("usuario").Description("Nombre del usuario");
			Field(x => x.StrTurno).Name("turno").Description("Turno");
		}
	}
}