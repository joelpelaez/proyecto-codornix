﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class TareaNoCumplidaType
		: ObjectGraphType<TareasNoCumplidas>
	{
		public TareaNoCumplidaType()
		{
			Field(x => x.IntFolio).Name("folio").Description("Folio");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identificador del departamento");
			Field(x => x.strIdArea).Name("idArea").Description("Identificador del area");
			Field(x => x.strIdUsuario).Name("idUsuario").Description("");
			Field(x => x.IntNumTareasRequeridasdNoCumplidas).Name("totalTareasRNC").Description("Cantidad de tareas no cumplidas");
			Field(x => x.StrDepartamento).Name("departamento").Description("Departamento");
			Field(x => x.StrArea).Name("area").Description("Area");
			Field(x => x.IntTareasCumplidas).Name("totalTareasC").Description("Cantidad de tareas cumplidas");
			Field(x => x.IntTareasNoCumplidas).Name("totalTareasNC").Description("Cantidad de tareas no cumplidas");
			Field(x => x.strDtFecha).Name("fecha").Description("Fecha");
			Field(x => x.StrTurno).Name("turno").Description("Turno");
			Field(x => x.strIdPeriodo).Name("idPeriodo").Description("Identificador del periodo");
			Field(x => x.StrEstadoTurno).Name("estadoTurno").Description("Estado del turno");
			Field(x => x.strDtFechaHoraInicio).Name("fechaInicio").Description("Fecha de inicio");
			Field(x => x.strDtFechaHoraFin).Name("fechaFin").Description("Fecha de finalizacion");
			Field(x => x.StrUsuario).Name("usuario").Description("Nombre del usuario");
			Field(x => x.strIdInicioTurno).Name("idInicioTurno").Description("Identificador inicio del turno");
			Field(x => x.strIdEstadoTurno).Name("idEstadoTurno").Description("Identificador estado del turno");

			Field<ListGraphType<ResumenTareaType>>("lsTareasC", "Tareas completadas", null, resolve: context =>
			{
				var idDepartamento = context.Source.strIdDepartamento;
				var idUsuario = context.Source.strIdUsuario;
				var dtFecha = context.Source.strDtFecha;

				DateTime dateTime = Convert.ToDateTime(DateTime.ParseExact(dtFecha, "dd-MM-yyyy", CultureInfo.InvariantCulture));
				var repos = new ResumenTarea.Repositorio();

				var result = repos.TareasCumplidas(new Guid(idDepartamento), new Guid(idUsuario), dateTime);

				return result;
			});


			Field<ListGraphType<ResumenTareaType>>("lsTareasNC", "Tareas completadas", null, resolve: context =>
			{
				var idDepartamento = context.Source.strIdDepartamento;
				var idUsuario = context.Source.strIdUsuario;
				var dtFecha = context.Source.strDtFecha;
				var idSucursal = context.Source.StrIdSucursal;

				DateTime dateTime = Convert.ToDateTime(DateTime.ParseExact(dtFecha, "dd-MM-yyyy", CultureInfo.InvariantCulture));
				var repos = new ResumenTarea.Repositorio();

				var result = repos.TareasNoCumplidas(new Guid(idDepartamento), new Guid(idUsuario), dateTime,new Guid(idSucursal));

				return result;
			});

			Field<ListGraphType<ResumenTareaType>>("lsTareasRNC", "Tareas completadas", null, resolve: context =>
			{
				var idDepartamento = context.Source.strIdDepartamento;
				var idUsuario = context.Source.strIdUsuario;
				var dtFecha = context.Source.strDtFecha;
				var idSucursal = context.Source.StrIdSucursal;

				DateTime dateTime = Convert.ToDateTime(DateTime.ParseExact(dtFecha, "dd-MM-yyyy", CultureInfo.InvariantCulture));
				var repos = new ResumenTarea.Repositorio();

				var result = repos.TareasRequeridas(new Guid(idDepartamento), new Guid(idUsuario), dateTime,new Guid(idSucursal));

				return result;
			});
			
		}
	}
}