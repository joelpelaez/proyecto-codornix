﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class CumplimientoType : ObjectGraphType<Cumplimiento>
	{
		public CumplimientoType()
		{
			Field(x => x.strIdCumplimiento).Name("id").Description("Identificador único del Cumplimiento");
			Field(x => x.strIdTarea).Name("idTarea").Description("Identificador de la tarea");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identificador del departamento");
			Field(x => x.strIdArea).Name("idArea").Description("Identificador del area");
			Field(x => x.strIdPeriodo).Name("idPeriodo").Description("Identificador del periodo");
			Field(x => x.strIdUsuario).Name("idUsuario").Description("Identificador del usuario");
			Field(x => x.StrObservacion).Name("observacion").Description("Observaciones del cumplimiento");

			Field(x => x.IntFolio).Name("folio").Description("Folio del Cumplimiento");
			Field(x => x.IntFolioTarea).Name("folioTarea").Description("Folio de Tarea");
			Field(x => x.strBlValor).Name("valorBooleano").Description("Valor booleano del Cumplimiento (si aplica)");
			Field(x => x.strDcValor1).Name("valorNumerico").Description("Valor numerico del Cumplimiento (si aplica)");
			Field(x => x.strIdOpcion).Name("valorOpcion").Description("Valor de la opción seleccionada");
			Field(x => x.strDtFechaProgramada).Name("fechaPrograma").Description("Fecha de ejecución programada de la Tarea");
			Field(x => x.strDtFechaHora).Name("fechaEjecucion").Description("Fecha y hora de realización del Cumplimiento");
			Field(x => x.strTmHora).Name("tmHora").Description("Hora cumplimiento");
			Field(x => x.StrEstadoCumplimiento).Name("estadoCumplimiento").Description("Descripcion del cumplimienti");

			Field(x => x.StrTarea).Name("tarea").Description("Tarea");
			Field(x => x.StrDepartamento).Name("departamento").Description("Departamento");
			Field(x => x.StrArea).Name("area").Description("Area");
			Field(x => x.StrTipoTarea).Name("tipoTarea").Description("Tipo de Tarea");

			Field(x => x.strProximo).Name("dtProximo").Description("");
			Field(x => x.isDateToday).Name("isTodayProximo").Description("");

			//Field(x => x.).Name("").Description("T");
			/*
			Field<TareaType>("tarea", "Tarea del Cumplimiento", arguments: null, resolve: context =>
            {
                var id = context.Source.UidTarea;
                var repo = new Tarea.Repositorio();
                var result = repo.Encontrar(id);
                return result;
            });
            Field<UsuarioType>("usuario", "Usuario quien ejecuto el Cumplimiento", arguments: null, resolve: context =>
            {
                var id = context.Source.UidUsuario;
                if (!id.HasValue)
                    return null;

                var repo = new Usuario.Repository();
                var result = repo.Find(id.Value);
                return result;
            });
			*/
			Field<TareaType>("objTarea", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idTTarea" }
			), resolve: context =>
			 {
				 var id = context.GetArgument<string>("idTTarea");
				 var repos = new Tarea.Repositorio();
				 var result = repos.Encontrar(new Guid(id));
				 return result;
			 });
			Field<TareaType>("objCTarea", arguments: null, resolve: context =>
			  {
				  Guid id = context.Source.UidTarea;
				  var repos = new Tarea.Repositorio();
				  var result = repos.Encontrar(id);
				  return result;
			  });

			Field<DepartamentoType>("objDepartamento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idDDepto" }
			), resolve: context =>
			{
				var id = context.GetArgument<string>("idDDepto");
				var repos = new Departamento.Repository();
				var result = repos.Encontrar(new Guid(id));
				return result;
			});
			Field<DepartamentoType>("objCDepartamento", arguments: null, resolve: context =>
			 {
				 var id = context.Source.UidDepartamento == null ? Guid.Empty : context.Source.UidDepartamento;
				 var repos = new Departamento.Repository();
				 var result = repos.Encontrar(id.Value);
				 return result;
			 });

			Field<AreaType>("objArea", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idAArea" }
			), resolve: context =>
			 {
				 var id = context.GetArgument<string>("idAArea");
				 var repos = new Area.Repository();
				 var result = repos.Find(new Guid(id));
				 return result;
			 });
			Field<AreaType>("objCArea", arguments: null, resolve: context =>
			{
				var id = context.Source.UidArea == null ? Guid.Empty : context.Source.UidArea;
				var repos = new Area.Repository();
				var result = repos.Find(id.Value);
				return result;
			});

			Field<ListGraphType<SucesorType>>("sucesores", null, resolve: context =>
			{
				Guid uidCumplimiento = context.Source.UidCumplimiento;
				Sucesor.Repository repository = new Sucesor.Repository();

				var result = repository.FindAll(uidCumplimiento);

				return result;
			});
		}
	}
}