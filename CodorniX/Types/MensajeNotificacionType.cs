﻿using GraphQL.Types;
using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class MensajeNotificacionType : ObjectGraphType<MensajeNotificacion>
	{
		public MensajeNotificacionType() {
			Field(x => x.strIdMensajeNotificacion).Name("id").Description("Identificador del mensaje");
			Field(x => x.strIdEstadoNotificacion).Name("idEstadoNotificacion").Description("Identificador del estado de notificacion");
			Field(x => x.strIdCumplimiento).Name("idCumplimiento").Description("Identificador del cumplimiento");

			Field(x => x.StrTarea).Name("tarea").Description("Tarea");
			Field(x => x.StrDepartamento).Name("departamento").Description("Departamento");
			Field(x => x.StrEstadoNotificacion).Name("estadoNotificacion").Description("Estado Notificacion");
			Field(x => x.StrResultado).Name("resultado").Description("Resultado");
			Field(x => x.StrArea).Name("area").Description("Area");
		}
	}
}