﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace CodorniX.Types
{
    internal class CodorniXSchema : Schema
    {
        public CodorniXSchema(CodorniXQuery query, CodorniXMutation mutation)
        {
            Query = query;
            Mutation = mutation;
        }
    }
}