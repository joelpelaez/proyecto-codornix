﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using CodorniX.Types;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace CodorniX.Types
{
	public class CodorniXMutation : ObjectGraphType
	{
		public CodorniXMutation()
		{
			/// <summary>
			/// Turnos Supervisor
			/// </summary>
			Field<ListGraphType<TextType>>("iniciarTurnoSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsuario" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "idTurnoSupervisor" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var idUsuario = context.GetArgument<string>("idUsuario");
					var idTurnoSupervisor = context.GetArgument<string>("idTurnoSupervisor");

					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));
					VMInicioSupervisor vMInicioSupervisor = new VMInicioSupervisor();

					if (new Guid(idTurnoSupervisor) == Guid.Empty)
					{
						vMInicioSupervisor.CrearTurno(new Guid(idUsuario), new Guid(idSucursal), horaLocal);
					}
					else
					{
						vMInicioSupervisor.IniciarTurno(new Guid(idTurnoSupervisor));
					}

					serverMessage.Value = "true";
					return serverMessage;
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message;
					return serverMessage;
				}
			});
			Field<ListGraphType<TextType>>("cerrarTurnoSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idTurnoSupervisor" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var idTurnoSupervisor = context.GetArgument<string>("idTurnoSupervisor");

					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));
					var local = horaLocal.DateTime;
					DateTime date = local;

					VMInicioSupervisor vMInicioSupervisor = new VMInicioSupervisor();
					vMInicioSupervisor.CerrarTurno(new Guid(idTurnoSupervisor));
					vMInicioSupervisor.ModificarFechaFin(new Guid(idTurnoSupervisor), date);
					serverMessage.Value = "true";
					return serverMessage;
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message;
					return serverMessage;
				}
			});

			/// <summary>
			/// Asignacion de Supervision
			/// </summary>
			Field<TextType>("guardarAsignacionSupervision", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsr" },
				new QueryArgument<StringGraphType> { Name = "idDepto" },
				new QueryArgument<StringGraphType> { Name = "idTurno" },
				new QueryArgument<StringGraphType> { Name = "dtInicio" },
				new QueryArgument<StringGraphType> { Name = "dtFin" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				try
				{
					var idUsuario = context.GetArgument<string>("idUsr");
					var idDepartamento = context.GetArgument<string>("idDepto");
					var idTurno = context.GetArgument<string>("idTurno");
					var fechaInicio = context.GetArgument<string>("dtInicio");
					var fechaFin = context.GetArgument<string>("dtFin");

					if (string.IsNullOrEmpty(idUsuario))
					{
						serverMessage.Value = "Debe seleccionar un encargado para el area";
						return serverMessage;
					}
					if (string.IsNullOrEmpty(fechaInicio))
					{
						serverMessage.Value = "Debe seleccionar una fecha de inicio válida";
						return serverMessage;
					}

					Guid uidUsuario = new Guid(idUsuario);
					Guid uidTurno = new Guid(idTurno);
					Guid uidDepartamento = new Guid(idDepartamento);
					DateTime DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture)); ;
					DateTime? DtFechaFin = null;
					if (!string.IsNullOrEmpty(fechaFin))
					{
						DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(fechaFin, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}

					VMAsignacionSupervision vMAsignacionSupervision = new VMAsignacionSupervision();
					AsignacionSupervision AsignacionSupervision = new AsignacionSupervision()
					{
						UidUsuario = uidUsuario,
						DtFechaInicio = DtFechaInicio,
						DtFechaFin = DtFechaFin,
						UidTurno = uidTurno,
						UidDepartamento = uidDepartamento,
					};

					AsignacionSupervision.ReturnCode code = vMAsignacionSupervision.GuardarAsignacion(AsignacionSupervision);

					switch (code)
					{
						case AsignacionSupervision.ReturnCode.Success:
							serverMessage.Value = "Se ha asignado el departamento exitosamente.";
							break;
						case AsignacionSupervision.ReturnCode.SuccessWithChanges:
							serverMessage.Value = "Se ha asignado el departamento, la asignación anterior ha sido modificada.";
							break;
						case AsignacionSupervision.ReturnCode.SuccessUpdate:
							serverMessage.Value = "Se ha actualizado la asignación.";
							break;
						case AsignacionSupervision.ReturnCode.FailureSame:
							serverMessage.Value = "No se ha podido insertar, existe una asignación similar.";
							break;
						case AsignacionSupervision.ReturnCode.FailureMax:
							serverMessage.Value = "No se ha podido asignar el departamento al supervisor, no tiene espacio para manejar otra.";
							break;
						case AsignacionSupervision.ReturnCode.Undefined:
							serverMessage.Value = "Error al procesar la solicitud, intentelo nuevamente.";
							break;
						default:
							break;
					}

					return serverMessage;
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message;
					return serverMessage;
				}
			});


			/* Incumplimiento (Tarea no realizada) */
			Field<ListGraphType<TareaAtrasadaType>>("posponerTareaAtrasada", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idTarea" },
					new QueryArgument<StringGraphType> { Name = "idDepartamento" },
					new QueryArgument<StringGraphType> { Name = "idArea" },
					new QueryArgument<StringGraphType> { Name = "idCumplimiento" },
					new QueryArgument<StringGraphType> { Name = "nuevaFecha" }
				), resolve: context =>
			  {
				  var idUsuario = context.GetArgument<string>("idUsuario");
				  var idSucursal = context.GetArgument<string>("idSucursal");
				  var idTarea = context.GetArgument<string>("idTarea");
				  var idDepartamento = context.GetArgument<string>("idDepartamento");
				  var idArea = context.GetArgument<string>("idArea");
				  var idCumplimiento = context.GetArgument<string>("idCumplimiento");
				  var dtNuevaFecha = context.GetArgument<string>("nuevaFecha");

				  string serverMessage = "";
				  List<TareaAtrasada> tareaAtrasadas = new List<TareaAtrasada>();
				  bool errorAux = false;

				  Guid? uidTarea, uidDepto, uidArea = null, uidCumpl = null;
				  uidTarea = new Guid(idTarea);
				  uidDepto = new Guid(idDepartamento);

				  if (idArea != string.Empty)
					  uidArea = new Guid(idArea);
				  if (idCumplimiento != string.Empty)
					  uidCumpl = new Guid(idCumplimiento);

				  DateTime fechaNueva = default(DateTime);
				  try
				  {
					  fechaNueva = Convert.ToDateTime(DateTime.ParseExact(dtNuevaFecha, "dd-MM-yyyy", CultureInfo.InvariantCulture));
				  }
				  catch
				  {
					  serverMessage = "La fecha no es correcta";
					  errorAux = true;
				  }

				  VMTareasAtrasadas Vm = new VMTareasAtrasadas();
				  if (!errorAux)
				  {
					  int result = Vm.PosponerTarea(uidTarea.Value, uidDepto, uidArea, uidCumpl, DateTime.Today, fechaNueva);
					  if (result == 2)
					  {
						  serverMessage = "La tarea fue completada";
					  }
					  else if (result == 1)
					  {
						  serverMessage = "La tarea no puede posponerse más allá de su siguiente cumplimiento.";
					  }
				  }

				  Vm.ObtenerTareasAtrasadas(new Guid(idUsuario), new Guid(idSucursal), DateTime.Today, null, null, null);
				  tareaAtrasadas = Vm.TareasAtrasadas;

				  if (tareaAtrasadas.Count == 0)
				  {
					  TareaAtrasada tareaAtrasada = new TareaAtrasada();
					  tareaAtrasada.strFechaProxima = "";
					  tareaAtrasada.strFechaProgramada = "";
					  tareaAtrasada.strTipo = "";
					  tareaAtrasada.StrArea = "";
					  tareaAtrasada.StrTurno = "";
					  tareaAtrasada.StrTarea = "";
					  tareaAtrasada.StrDepartamento = "";
					  tareaAtrasada.strServerMessage = serverMessage;
					  tareaAtrasadas.Add(tareaAtrasada);
				  }
				  else
				  {
					  tareaAtrasadas[0].strServerMessage = serverMessage;
				  }
				  return tareaAtrasadas;
			  });

			Field<ListGraphType<TareaAtrasadaType>>("cancelarTareaAtrasada", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idTarea" },
					new QueryArgument<StringGraphType> { Name = "idDepartamento" },
					new QueryArgument<StringGraphType> { Name = "idArea" },
					new QueryArgument<StringGraphType> { Name = "idCumplimiento" }
				), resolve: context =>
			{
				var idUsuario = context.GetArgument<string>("idUsuario");
				var idSucursal = context.GetArgument<string>("idSucursal");
				var idTarea = context.GetArgument<string>("idTarea");
				var idDepartamento = context.GetArgument<string>("idDepartamento");
				var idArea = context.GetArgument<string>("idArea");
				var idCumplimiento = context.GetArgument<string>("idCumplimiento");

				string serverMessage = "";

				List<TareaAtrasada> tareaAtrasadas = new List<TareaAtrasada>();
				VMTareasAtrasadas Vm = new VMTareasAtrasadas();
				Guid? uidTarea, uidDepto, uidArea = null, uidCumpl = null;
				uidTarea = new Guid(idTarea);
				uidDepto = new Guid(idDepartamento);

				if (idArea != string.Empty)
					uidArea = new Guid(idArea);
				if (idCumplimiento != string.Empty)
					uidCumpl = new Guid(idCumplimiento);

				int result = Vm.CancelarTarea(uidTarea.Value, uidDepto, uidArea, uidCumpl, DateTime.Today);

				if (result == 2)
				{
					serverMessage = "La tarea fue completada";
				}

				Vm.ObtenerTareasAtrasadas(new Guid(idUsuario), new Guid(idSucursal), DateTime.Today, null, null, null);
				tareaAtrasadas = Vm.TareasAtrasadas;

				if (tareaAtrasadas.Count == 0)
				{
					TareaAtrasada tareaAtrasada = new TareaAtrasada();
					tareaAtrasada.strFechaProxima = "";
					tareaAtrasada.strFechaProgramada = "";
					tareaAtrasada.strTipo = "";
					tareaAtrasada.StrArea = "";
					tareaAtrasada.StrTurno = "";
					tareaAtrasada.StrTarea = "";
					tareaAtrasada.StrDepartamento = "";
					tareaAtrasada.strServerMessage = serverMessage;
					tareaAtrasadas.Add(tareaAtrasada);
				}
				else
				{
					tareaAtrasadas[0].strServerMessage = serverMessage;
				}

				return tareaAtrasadas;
			});

			/* Supervision */
			Field<TextType>("iniciarTurno", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idInicioTurno" },
					new QueryArgument<StringGraphType> { Name = "idPeriodo" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var idUsuario = context.GetArgument<string>("idUsuario");
					var idInicioTurno = context.GetArgument<string>("idInicioTurno");
					var idPeriodo = context.GetArgument<string>("idPeriodo");

					VMSupervision Vm = new VMSupervision();
					Guid uidInicioTurno = Guid.Empty;
					Guid uidSucursal = new Guid(idSucursal);
					if (!string.IsNullOrEmpty(idInicioTurno)) { uidInicioTurno = new Guid(idInicioTurno); }

					if (uidInicioTurno == Guid.Empty)
					{
						DateTimeOffset time = Hora.ObtenerHoraServidor();
						if (!string.IsNullOrEmpty(idSucursal))
							time = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));

						Vm.CrearTurno(new Guid(idPeriodo), new Guid(idUsuario), new Guid(idUsuario), time, "Abierto por Supervisor", false, true);
					}
					else
					{
						Vm.ModificarEstadoTurno(uidInicioTurno, "Abierto por Supervisor");
					}
					serverMessage.Value = "true";
				}
				catch (Exception e)
				{
					serverMessage.Value = e.Message;
				}
				return serverMessage;
			});
			Field<TextType>("tomarTurno", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idPeriodo" },
					new QueryArgument<StringGraphType> { Name = "idInicioTurno" }
			), resolve: context =>
			 {
				 ServerMessage serverMessage = new ServerMessage();
				 serverMessage.Value = "";
				 try
				 {
					 var idSucursal = context.GetArgument<string>("idSucursal");
					 var idUsuario = context.GetArgument<string>("idUsuario");
					 var idPeriodo = context.GetArgument<string>("idPeriodo");
					 var idInicioTurno = context.GetArgument<string>("idInicioTurno");

					 VMSupervision Vm = new VMSupervision();

					 if (string.IsNullOrEmpty(idInicioTurno))
					 {
						 DateTimeOffset time = Hora.ObtenerHoraServidor();
						 if (!string.IsNullOrEmpty(idSucursal))
							 time = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));

						 Vm.CrearTurno(new Guid(idPeriodo), new Guid(idUsuario), new Guid(idUsuario), time,
							 "Abierto (Controlado)", false, true);
					 }
					 else
					 {
						 var uidInicioTurno = new Guid(idInicioTurno);
						 Vm.ObtenerTurno(uidInicioTurno);
						 Vm.ModificarEstadoTurno(uidInicioTurno, "Abierto (Controlado)");
					 }
					 serverMessage.Value = "true";
				 }
				 catch (Exception e)
				 {
					 serverMessage.Value = e.Message;
				 }
				 return serverMessage;

			 });
			Field<TextType>("cederTurno", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idInicioTurno" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var idUsuario = context.GetArgument<string>("idUsuario");
					var idInicioTurno = context.GetArgument<string>("idInicioTurno");

					VMSupervision Vm = new VMSupervision();
					Vm.ModificarEstadoTurno(new Guid(idInicioTurno), "Abierto");
					serverMessage.Value = "true";
				}
				catch (Exception e)
				{
					serverMessage.Value = e.Message;
				}
				return serverMessage;
			});
			Field<TextType>("bloquearTurno", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idPeriodo" },
					new QueryArgument<StringGraphType> { Name = "idInicioTurno" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var idUsuario = context.GetArgument<string>("idUsuario");
					var idInicioTurno = context.GetArgument<string>("idInicioTurno");
					var idPeriodo = context.GetArgument<string>("idPeriodo");

					VMSupervision Vm = new VMSupervision();
					Guid uidInicioTurno = Guid.Empty;
					if (!string.IsNullOrEmpty(idInicioTurno)) { uidInicioTurno = new Guid(idInicioTurno); }

					if (uidInicioTurno == Guid.Empty)
					{
						DateTimeOffset time = Hora.ObtenerHoraServidor();
						if (!string.IsNullOrEmpty(idSucursal))
							time = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));

						Vm.CrearTurno(new Guid(idPeriodo), new Guid(idUsuario), new Guid(idUsuario), time, "Bloqueado", false, true);
					}
					else
					{
						Vm.ModificarEstadoTurno(uidInicioTurno, "Bloqueado");
					}

					serverMessage.Value = "true";
				}
				catch (Exception e)
				{
					serverMessage.Value = e.Message;
				}
				return serverMessage;
			});
			Field<TextType>("desbloquearTurno", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idInicioTurno" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var idUsuario = context.GetArgument<string>("idUsuario");
					var idInicioTurno = context.GetArgument<string>("idInicioTurno");

					VMSupervision Vm = new VMSupervision();
					Guid uidInicioTurno = new Guid(idInicioTurno);

					Vm.ModificarEstadoTurno(uidInicioTurno, "Abierto (Controlado)");
					serverMessage.Value = "true";
				}
				catch (Exception e)
				{
					serverMessage.Value = e.Message;
				}
				return serverMessage;
			});
			Field<TextType>("cerrarTurno", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idInicioTurno" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var idUsuario = context.GetArgument<string>("idUsuario");
					var idInicioTurno = context.GetArgument<string>("idInicioTurno");

					VMSupervision Vm = new VMSupervision();
					Guid uidInicioTurno = new Guid(idInicioTurno);
					DateTimeOffset time = Hora.ObtenerHoraServidor();
					if (!string.IsNullOrEmpty(idSucursal))
						time = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));

					Vm.CerrarTurno(uidInicioTurno, time);

					serverMessage.Value = "true";
				}
				catch (Exception e)
				{
					serverMessage.Value = e.Message;
				}

				return serverMessage;
			});
			Field<TextType>("reabrirTurno", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idInicioTurno" }
			), resolve: context =>
			{
				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					var idInicioTurno = context.GetArgument<string>("idInicioTurno");

					VMSupervision Vm = new VMSupervision();
					Guid uidInicioTurno = new Guid(idInicioTurno);

					Vm.ReabrirTurno(uidInicioTurno, "Abierto por Supervisor");

					serverMessage.Value = "true";
				}
				catch (Exception e)
				{
					serverMessage.Value = e.Message;
				}

				return serverMessage;
			});

			/*Notificaciones*/
			Field<ListGraphType<MensajeNotificacionType>>("actualizarEstadoNotificacion", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idDepartamentos" },
					new QueryArgument<StringGraphType> { Name = "idNotificacion" },
					new QueryArgument<StringGraphType> { Name = "estadoActual" }
			), resolve: context =>
			{
				var idSucursal = context.GetArgument<string>("idSucursal");
				var idDepartamentos = context.GetArgument<string>("idDepartamentos");
				var idNotificacion = context.GetArgument<string>("idNotificacion");
				var estadoActual = context.GetArgument<string>("estadoActual");

				VMNotificacion Vm = new VMNotificacion();
				Vm.CambiarEstado(new Guid(idNotificacion), estadoActual);

				Vm.ObtenerNotificaciones(new Guid(idSucursal), idDepartamentos, null, null, null, null);

				return Vm.Mensajes;
			});

			/* Cumplimiento */
			Field<TextType>("registrarCumplimiento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idCumpl" },
				new QueryArgument<StringGraphType> { Name = "idSuc" },
				new QueryArgument<StringGraphType> { Name = "idTarea" },
				new QueryArgument<StringGraphType> { Name = "idUsr" },
				new QueryArgument<StringGraphType> { Name = "idDepto" },
				new QueryArgument<StringGraphType> { Name = "idArea" },
				new QueryArgument<StringGraphType> { Name = "idPeriodo" },
				new QueryArgument<StringGraphType> { Name = "tMedicion" },
				new QueryArgument<StringGraphType> { Name = "blEstado" },
				new QueryArgument<StringGraphType> { Name = "dcVUno" },
				new QueryArgument<StringGraphType> { Name = "idOpc" },
				new QueryArgument<StringGraphType> { Name = "observaciones" },
				new QueryArgument<StringGraphType> { Name = "isSupervisor" }
			), resolve: context =>
			 {

				 var idCumplimiento = context.GetArgument<string>("idCumpl");
				 var idSucursal = context.GetArgument<string>("idSuc");
				 var idTarea = context.GetArgument<string>("idTarea");
				 var idUsuario = context.GetArgument<string>("idUsr");
				 var idDepartamento = context.GetArgument<string>("idDepto");
				 var idArea = context.GetArgument<string>("idArea");
				 var idPeriodo = context.GetArgument<string>("idPeriodo");

				 var strTipoMedicion = context.GetArgument<string>("tMedicion");
				 var strBlEstado = context.GetArgument<string>("blEstado");
				 var strDcValor1 = context.GetArgument<string>("dcVUno");
				 var idOpcion = context.GetArgument<string>("idOpc");
				 var strObservaciones = context.GetArgument<string>("observaciones");
				 var isSupervisor = context.GetArgument<string>("isSupervisor");

				 ServerMessage serverMessage = new ServerMessage();
				 serverMessage.Value = "";

				 try
				 {
					 Guid guidCumplimiento = new Guid(idCumplimiento);
					 Guid? realCumplimiento = guidCumplimiento == Guid.Empty ? (Guid?)null : guidCumplimiento;
					 Guid guidDepartamento = new Guid(idDepartamento);
					 Guid? realDepartamento = guidDepartamento == Guid.Empty ? (Guid?)null : guidDepartamento;
					 Guid guidArea = new Guid(idArea);
					 Guid? realArea = guidArea == Guid.Empty ? (Guid?)null : guidArea;

					 bool? blEstado = null;
					 decimal? valor1 = null;
					 decimal? valor2 = null;
					 Guid? opcion = null;

					 switch (strTipoMedicion)
					 {
						 case "Verdadero/Falso":
							 if (strBlEstado.Equals("true"))
								 blEstado = true;
							 if (strBlEstado.Equals("false"))
								 blEstado = false;
							 else
								 blEstado = false;
							 break;
						 case "Numerico":
							 try
							 {
								 valor1 = Convert.ToDecimal(strDcValor1);
							 }
							 catch (FormatException)
							 {
								 serverMessage.Value = "Solo se aceptan valores numéricos";
								 return serverMessage;
							 }
							 break;
						 case "Seleccionable":
							 opcion = new Guid(idOpcion);
							 break;
					 }

					 VMCumplimiento Vm = new VMCumplimiento();
					 DateTimeOffset time = Hora.ObtenerHoraServidor();
					 DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));

					 Guid uidUsuario = new Guid(idUsuario);
					 Vm.ObtenerPeriodo(new Guid(idPeriodo));

					 if (isSupervisor.Equals("false"))
					 {
						 uidUsuario = Vm.Periodo.UidUsuario;
					 }

					 Vm.RegistrarCumplimiento(ref realCumplimiento, new Guid(idTarea), realDepartamento, realArea, uidUsuario, horaLocal, blEstado, valor1, valor2, opcion, strObservaciones, null, Vm.Periodo.UidTurno);
					 serverMessage.Value = "true";
					 Vm.EnviarNotificacion(realCumplimiento.Value);

				 }
				 catch (Exception e)
				 {
					 serverMessage.Value += e.Message.ToString();
				 }

				 return serverMessage;
			 });
			Field<TextType>("actualizarCumplimiento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idCumpl" },
				new QueryArgument<StringGraphType> { Name = "idSuc" },
				new QueryArgument<StringGraphType> { Name = "tMedicion" },
				new QueryArgument<StringGraphType> { Name = "blEstado" },
				new QueryArgument<StringGraphType> { Name = "dcVUno" },
				new QueryArgument<StringGraphType> { Name = "idOpc" },
				new QueryArgument<StringGraphType> { Name = "observaciones" }
			), resolve: context =>
			{
				var idCumplimiento = context.GetArgument<string>("idCumpl");
				var strTipoMedicion = context.GetArgument<string>("tMedicion");
				var idSucursal = context.GetArgument<string>("idSuc");

				var strBlEstado = context.GetArgument<string>("blEstado");
				var strDcValor1 = context.GetArgument<string>("dcVUno");
				var idOpcion = context.GetArgument<string>("idOpc");
				var observaciones = context.GetArgument<string>("observaciones");

				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";

				try
				{
					bool? blEstado = null;
					decimal? valor1 = null;
					decimal? valor2 = null;
					Guid? opcion = null;
					Guid guidCumplimiento = new Guid(idCumplimiento);
					Guid? realCumplimiento = guidCumplimiento == Guid.Empty ? (Guid?)null : guidCumplimiento;

					switch (strTipoMedicion)
					{
						case "Verdadero/Falso":
							if (strBlEstado.Equals("true"))
								blEstado = true;
							if (strBlEstado.Equals("false"))
								blEstado = false;
							else
								blEstado = false;
							break;
						case "Numerico":
							try
							{
								valor1 = Convert.ToDecimal(strDcValor1);
							}
							catch (FormatException)
							{
								serverMessage.Value = "Solo se aceptan valores numéricos";
								return serverMessage;
							}
							break;
						case "Seleccionable":
							opcion = new Guid(idOpcion);
							break;
					}

					VMCumplimiento Vm = new VMCumplimiento();
					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));

					Vm.ActualizarCumplimiento(new Guid(idCumplimiento), horaLocal, blEstado, valor1, valor2, opcion, observaciones, null);
					serverMessage.Value = "true";
				}
				catch (Exception e)
				{
					serverMessage.Value = e.Message.ToString();
				}

				return serverMessage;
			});
			Field<TextType>("posponerCumplimiento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsr" },
				new QueryArgument<StringGraphType> { Name = "idSuc" },
				new QueryArgument<StringGraphType> { Name = "idCumpl" },
				new QueryArgument<StringGraphType> { Name = "idTarea" },
				new QueryArgument<StringGraphType> { Name = "idDepto" },
				new QueryArgument<StringGraphType> { Name = "idArea" },
				new QueryArgument<StringGraphType> { Name = "dtNuevo" }
			), resolve: context =>
			  {
				  var idUsuario = context.GetArgument<string>("idUsr");
				  var idSucursal = context.GetArgument<string>("idSuc");
				  var idCumplimiento = context.GetArgument<string>("idCumpl");
				  var idTarea = context.GetArgument<string>("idTarea");
				  var idDepartamento = context.GetArgument<string>("idDepto");
				  var idArea = context.GetArgument<string>("idArea");
				  var dtNuevaFecha = context.GetArgument<string>("dtNuevo");

				  ServerMessage serverMessage = new ServerMessage();
				  serverMessage.Value = "";

				  try
				  {
					  Guid uidUsuario = new Guid(idUsuario);
					  Guid uidCumplimiento = new Guid(idCumplimiento);
					  Guid uidTarea = new Guid(idTarea);
					  Guid uidDepartamento = new Guid(idDepartamento);
					  Guid uidArea = new Guid(idArea);

					  Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
					  Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
					  Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;

					  DateTimeOffset time = Hora.ObtenerHoraServidor();
					  DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));
					  DateTime real = horaLocal.DateTime;
					  DateTime nFecha = Convert.ToDateTime(DateTime.ParseExact(dtNuevaFecha, "dd-MM-yyyy", CultureInfo.InvariantCulture));

					  VMCumplimiento Vm = new VMCumplimiento();
					  int result = Vm.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea, uidUsuario, nFecha, VMCumplimiento.ModoCambio.Posponer, real);

					  if (result == 1)
					  {
						  serverMessage.Value = "La fecha nueva no corresponde a un periodo del usuario actual";
						  return serverMessage;
					  }
					  else if (result == 2)
					  {
						  serverMessage.Value = "La tarea es requerida y no puede posponerse";
						  return serverMessage;
					  }

					  serverMessage.Value = "true";
				  }
				  catch (Exception e)
				  {
					  serverMessage.Value = e.Message.ToString();
				  }

				  return serverMessage;
			  });
			Field<TextType>("cancelarCumplimiento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsr" },
				new QueryArgument<StringGraphType> { Name = "idCumpl" },
				new QueryArgument<StringGraphType> { Name = "idTarea" },
				new QueryArgument<StringGraphType> { Name = "idDepto" },
				new QueryArgument<StringGraphType> { Name = "idArea" },
				new QueryArgument<StringGraphType> { Name = "idSuc" }
			), resolve: context =>
			{
				var idUsuario = context.GetArgument<string>("idUsr");
				var idCumplimiento = context.GetArgument<string>("idCumpl");
				var idTarea = context.GetArgument<string>("idTarea");
				var idDepartamento = context.GetArgument<string>("idDepto");
				var idArea = context.GetArgument<string>("idArea");
				var idSucursal = context.GetArgument<string>("idSuc");

				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					Guid uidUsuario = new Guid(idUsuario);
					Guid uidCumplimiento = new Guid(idCumplimiento);
					Guid uidTarea = new Guid(idTarea);
					Guid uidDepartamento = new Guid(idDepartamento);
					Guid uidArea = new Guid(idArea);

					Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
					Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
					Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;

					VMCumplimiento Vm = new VMCumplimiento();
					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));

					int result = Vm.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea, uidUsuario, null, VMCumplimiento.ModoCambio.Cancelar, horaLocal);
					if (result == 1)
					{
						serverMessage.Value = "La fecha nueva no corresponde a un periodo del usuario actual";
						return serverMessage;
					}
					else if (result == 2)
					{
						serverMessage.Value = "La tarea es requerida y no puede cancelarse";
						return serverMessage;
					}

					serverMessage.Value = "true";
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message.ToString();
				}
				return serverMessage;
			});
			Field<TextType>("deshacerCumplimiento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "id" }
			), resolve: context =>
			{
				var idCumplimiento = context.GetArgument<string>("id");
				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					Guid uidCumplimiento = new Guid(idCumplimiento);

					VMCumplimiento Vm = new VMCumplimiento();
					Vm.EnviarDeshacerNotificacion(uidCumplimiento);
					Vm.DeshacerCumplimiento(uidCumplimiento);

					serverMessage.Value = "true";
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message.ToString();
				}
				return serverMessage;
			});

			/*Revision*/
			Field<TextType>("registrarRevisionDetallada", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idCumplimiento" },
				new QueryArgument<StringGraphType> { Name = "idTarea" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "idUsuario" },
				new QueryArgument<StringGraphType> { Name = "idCalificacion" },
				new QueryArgument<StringGraphType> { Name = "notas" },
				new QueryArgument<StringGraphType> { Name = "dcValor" },
				new QueryArgument<StringGraphType> { Name = "idOpcion" },
				new QueryArgument<StringGraphType> { Name = "blValor" }
			), resolve: context =>
			{
				var idCumplimiento = context.GetArgument<string>("idCumplimiento");
				var idTarea = context.GetArgument<string>("idTarea");
				var idSucursal = context.GetArgument<string>("idSucursal");
				var idUsuario = context.GetArgument<string>("idUsuario");
				var idCalificacion = context.GetArgument<string>("idCalificacion");
				var strNotas = context.GetArgument<string>("notas");
				var dcValor = context.GetArgument<string>("dcValor");
				var idOpcion = context.GetArgument<string>("idOpcion");
				var blValor = context.GetArgument<string>("blValor");

				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";
				try
				{
					Guid uidCumplimiento = new Guid(idCumplimiento);
					Guid uidTarea = new Guid(idTarea);
					Guid uidUsuario = new Guid(idUsuario);
					Guid uidSucursal = new Guid(idSucursal);
					Guid uidCalificacion = new Guid(idCalificacion);
					Guid? realCumplimiento = uidCumplimiento;
					bool? estado = null;
					decimal? valor1 = null;
					decimal? valor2 = null;
					Guid? opcion = null;

					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(uidSucursal));
					var local = horaLocal.DateTime;

					VMRevision vMRevision = new VMRevision();
					vMRevision.ObtenerTarea(uidTarea);
					vMRevision.ObtenerCumplimiento(uidCumplimiento);

					switch (vMRevision.Tarea.StrTipoMedicion)
					{
						case "Verdadero/Falso":
							if (blValor.Equals("true"))
							{
								if (!vMRevision.Cumplimiento.BlValor.Value)
									estado = true;
							}
							else if (blValor.Equals("false"))
							{
								if (vMRevision.Cumplimiento.BlValor.Value)
									estado = false;
							}
							else
							{
								estado = false;
							}
							break;
						case "Numerico":
							try
							{
								decimal tmp;
								tmp = Convert.ToDecimal(dcValor);

								if (vMRevision.Cumplimiento.DcValor1.Value.ToString() != tmp.ToString())
									valor1 = tmp;
							}
							catch (Exception)
							{
								serverMessage.Value = "Solo se aceptan valores numéricos";
								return serverMessage;
							}
							break;
						case "Seleccionable":
							if (vMRevision.Cumplimiento.UidOpcion.ToString() != idOpcion)
								opcion = new Guid(idOpcion);
							break;
						default:
							break;
					}

					vMRevision.RegistrarRevision(realCumplimiento.Value, uidUsuario,
					local, estado, valor1, valor2, opcion, strNotas, false, uidCalificacion);
					serverMessage.Value = "true";
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message;
				}

				return serverMessage;
			});
			Field<TextType>("registrarRevisionRapida", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idCumplimiento" },
				new QueryArgument<StringGraphType> { Name = "idUsuario" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "idCalificacion" },
				new QueryArgument<StringGraphType> { Name = "notas" }
			), resolve: context =>
			{
				var idCumplimiento = context.GetArgument<string>("idCumplimiento");
				var idUsuario = context.GetArgument<string>("idUsuario");
				var idSucursal = context.GetArgument<string>("idSucursal");
				var idCalificacion = context.GetArgument<string>("idCalificacion");
				var strNotas = context.GetArgument<string>("notas");

				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";

				try
				{
					Guid uidUsuario = new Guid(idUsuario);
					Guid uidSucursal = new Guid(idSucursal);
					Guid uidCumplimiento = new Guid(idCumplimiento);
					Guid uidCalificacion = new Guid(idCalificacion);

					Guid? realCumplimiento = uidCumplimiento;
					bool? estado = null;
					decimal? valor1 = null;
					decimal? valor2 = null;
					Guid? opcion = null;
					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(uidSucursal));
					var local = horaLocal.DateTime;

					VMRevision vMRevision = new VMRevision();
					vMRevision.RegistrarRevision(realCumplimiento.Value, uidUsuario,
					local, estado, valor1, valor2, opcion, strNotas, false, uidCalificacion);
					serverMessage.Value = "true";
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message.ToString();
				}

				return serverMessage;
			});
			Field<TextType>("crearSucesor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idTarea" },
				new QueryArgument<StringGraphType> { Name = "idRCumpl" }
			), resolve: context =>
			{
				var idTarea = context.GetArgument<string>("idTarea");
				var idCumplimiento = context.GetArgument<string>("idRCumpl");

				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";

				try
				{
					if (idTarea == string.Empty) { idTarea = Guid.Empty.ToString(); }
					if (idCumplimiento == string.Empty) { idCumplimiento = Guid.Empty.ToString(); }
					VMRevision vMRevision = new VMRevision();
					vMRevision.CrearSucesor(new Guid(idCumplimiento), new Guid(idTarea));
					serverMessage.Value = "true";
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message;
				}

				return serverMessage;
			});
			Field<TextType>("habilitarSucesor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idSCumpl" }
			), resolve: context =>
			{
				var idCumplimiento = context.GetArgument<string>("idSCumpl");

				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";

				try
				{
					if (idCumplimiento == string.Empty) { idCumplimiento = Guid.Empty.ToString(); }
					VMRevision vMRevision = new VMRevision();
					vMRevision.HabilitarSucesor(new Guid(idCumplimiento));
					serverMessage.Value = "true";
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message;
				}

				return serverMessage;
			});
			Field<TextType>("deshabilitarSucesor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idSCumpl" }
			), resolve: context =>
			{
				var idCumplimiento = context.GetArgument<string>("idSCumpl");

				ServerMessage serverMessage = new ServerMessage();
				serverMessage.Value = "";

				try
				{
					if (idCumplimiento == string.Empty) { idCumplimiento = Guid.Empty.ToString(); }
					VMRevision vMRevision = new VMRevision();
					vMRevision.DeshabilitarSucesor(new Guid(idCumplimiento));
					serverMessage.Value = "true";
				}
				catch (Exception ex)
				{
					serverMessage.Value = ex.Message;
				}

				return serverMessage;
			});
		}
	}
}