﻿using CodorniX.Modelo;
using CodorniX.Types;
using CodorniX.VistaDelModelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CodorniX.Types
{
	internal class CodorniXQuery : ObjectGraphType
	{
		/// <summary>
		/// Class to create query's to use un graphql
		/// </summary>
		public CodorniXQuery()
		{
			Field<UsuarioType>("usuario", arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id" }), resolve: context =>
			{
				try
				{
					var id = context.GetArgument<string>("id");
					var result = new Usuario.Repository().Find(new Guid(id));
					return result;
				}
				catch (Exception)
				{
					Usuario Aux = new Usuario();
					return Aux;
				}
			});

			Field<UsuarioType>("me", arguments: null, resolve: context =>
			{
				var name = context.UserContext as string;
				var repo = new Usuario.Repository();
				var result = repo.FindByName(name);
				return result;
			});

			Field<TareaType>("tarea", arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id" }), resolve: context =>
				 {
					 var idTarea = context.GetArgument<string>("id");
					 var repository = new Tarea.Repositorio();
					 var result = repository.Encontrar(new Guid(idTarea));
					 return result;
				 });

			/// <summary>
			/// Turnos Supervisor
			/// </summary>
			Field<ListGraphType<TurnoSupervisorType>>("turnosupervisor", arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "idUsuario" }, new QueryArgument<StringGraphType> { Name = "idSucursal" }), resolve: context =>
			{
				var idSucursal = context.GetArgument<string>("idSucursal");
				var idUsuario = context.GetArgument<string>("idUsuario");

				VMInicioSupervisor vMInicioSupervisor = new VMInicioSupervisor();
				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));
				var local = horaLocal.DateTime;
				DateTime date = local;
				vMInicioSupervisor.ObtenerTurnoDeHoy(new Guid(idUsuario), new Guid(idSucursal), date);
				var results = new List<TurnoSupervisor> { vMInicioSupervisor.TurnoSupervisor };
				return results;
			});

			Field<UserSessionType>("loginSessionData", arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "idUsuario" }), resolve: context =>
				 {
					 var idUsuario = context.GetArgument<string>("idUsuario");
					 Sesion sesion = new Sesion();
					 sesion.perfil = "";
					 sesion.Messsage = "";
					 sesion.appWeb = "";

					 Status.Repository statusRepository = new Status.Repository();
					 Perfil.Repositorio perfilRepository = new Perfil.Repositorio();
					 Empresa.Repository empresaRepository = new Empresa.Repository();

					 /* Obtener datos del usuario */
					 Usuario usuario = new Usuario.Repository().Find(new Guid(idUsuario));
					 sesion.uidUsuario = usuario.UIDUSUARIO;

					 /*Obtener Estatus del Usuario */
					 Status status = statusRepository.Find(usuario.UidStatus);
					 if (!status.strStatus.Equals("Activo"))
					 {
						 sesion.Messsage = "Usuario inactivo";
						 return sesion;
					 }

					 /* Obtener Empresas Pertenecientes al usuario */
					 List<UsuarioPerfilEmpresa> ep = new UsuarioPerfilEmpresa.Repository().FindAll(new Guid(idUsuario));
					 if (ep.Count > 0)
					 {
						 List<Guid> uidEmpresas = (from em in ep select em.UidEmpresa).ToList();
						 List<Guid> uidPerfiles = (from pf in ep select pf.UidPerfil).ToList();
						 sesion.uidEmpresasPerfiles = uidEmpresas;
						 sesion.uidEmpresaActual = uidEmpresas[0];
						 sesion.uidPerfilActual = uidPerfiles[0];
						 sesion.uidNivelAccesoActual = perfilRepository.CargarDatos(sesion.uidPerfilActual.Value).UidNivelAcceso;
					 }
					 else
					 {
						 /* Obtener sucursales Pertenecientes al usuario */
						 List<UsuarioPerfilSucursal> sp = new UsuarioPerfilSucursal.Repository().FindAll(new Guid(idUsuario));
						 if (sp.Count > 0)
						 {
							 List<Guid> uidSucursales = (from su in sp select su.UidSucursal).ToList();
							 List<Guid> uidPerfiles = (from pf in sp select pf.UidPerfil).ToList();
							 sesion.uidSucursalesPerfiles = uidSucursales;
							 sesion.uidSucursalActual = uidSucursales[0];
							 sesion.uidEmpresaActual = new Sucursal.Repository().Find(uidSucursales[0]).UidEmpresa;
							 sesion.uidPerfilActual = uidPerfiles[0];
						 }
						 else
						 {
							 sesion.Messsage = "El usuario no tiene empresa ni sucursal asignados";
							 return sesion;
						 }
					 }

					 Empresa empresa = empresaRepository.Find(sesion.uidEmpresaActual.Value);
					 if (empresa != null && empresa.UidStatus != Guid.Empty)
					 {
						 status = statusRepository.Find(empresa.UidStatus);

						 if (status.strStatus != "Activo")
						 {
							 sesion.Messsage = "La empresa a la que pertenece se encuentra desactivada";
							 return sesion;
						 }
					 }

					 Perfil perfil = perfilRepository.CargarDatos(sesion.uidPerfilActual.Value);
					 sesion.perfil = perfil.strPerfil;
					 if (sesion.perfil == "Supervisor")
					 {
						 /* Obtener listado Id departamentos */
						 sesion.UidDepartamentos = AsignacionSupervision.Repository.ObtenerDepartamentosSupervisados(usuario.UIDUSUARIO,
								 DateTime.Today);
					 }

					 string nivel = Acceso.ObtenerAppWeb(sesion.uidPerfilActual.Value);
					 sesion.appWeb = nivel;
					 if (nivel == "Frontend")
					 {
						 if (!Acceso.TienePeriodo(usuario.UIDUSUARIO, sesion.uidSucursalActual.Value, DateTime.Now))
						 {
							 sesion = new Sesion();
							 sesion.perfil = "";
							 sesion.Messsage = "No tiene ningun turno el día de hoy";
							 sesion.appWeb = "";
							 return sesion;
						 }

						 sesion.UidDepartamentos = Acceso.ObtenerDepartamentosSupervisor(usuario.UIDUSUARIO, DateTime.Today);
					 }

					 return sesion;
				 });

			/// <summary>
			/// Tipos Sucursal
			/// </summary>
			Field<ListGraphType<TipoSucursalType>>("tipoSucursal", null, resolve: context =>
			  {
				  try
				  {
					  TipoSucursal.Repository repository = new TipoSucursal.Repository();

					  var results = repository.FindAll();

					  return results;
				  }
				  catch (Exception)
				  {
					  List<TipoSucursal> lsAux = new List<TipoSucursal>();
					  return lsAux;
				  }
			  });

			/// <summary>
			/// Departamentos
			/// </summary>
			Field<DepartamentoType>("departamento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "id" }
			), resolve: context =>
			{
				try
				{
					var id = context.GetArgument<string>("id");

					Departamento.Repository repository = new Departamento.Repository();
					var result = repository.Encontrar(new Guid(id));
					return result;
				}
				catch (Exception)
				{
					Departamento Aux = new Departamento();
					return Aux;
				}
			});
			Field<ListGraphType<DepartamentoType>>("departamentoBySucursal", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "id" }
			), resolve: context =>
			{
				try
				{
					var idSucursal = context.GetArgument<string>("id");
					Guid uidSucursal = new Guid(idSucursal);
					Departamento.Repository repository = new Departamento.Repository();

					var results = repository.EncontrarTodos(uidSucursal);
					return results;
				}
				catch (Exception)
				{
					List<Departamento> lsAux = new List<Departamento>();
					return lsAux;
				}
			});
			Field<ListGraphType<DepartamentoType>>("departamentosByIds", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "ids" }
			), resolve: context =>
			{
				try
				{
					var idsDepartamentos = context.GetArgument<string>("ids");

					List<Guid> lsDepartamentos = new List<Guid>();
					if (!string.IsNullOrEmpty(idsDepartamentos))
					{
						List<string> ls = idsDepartamentos.Split(',').ToList();

						foreach (var item in ls)
						{
							lsDepartamentos.Add(new Guid(item));
						}
					}

					VMRevision vMRevision = new VMRevision();
					vMRevision.ObtenerDepartamentosAsignados(lsDepartamentos);

					return vMRevision.Departamentos;

				}
				catch (Exception)
				{
					List<Departamento> lsAux = new List<Departamento>();
					return lsAux;
				}
			});

			/// <summary>
			/// Turnos
			/// </summary>
			Field<ListGraphType<TurnoType>>("turnos", null, resolve: context =>
			 {
				 try
				 {
					 Turno.Repository repository = new Turno.Repository();
					 var results = repository.FindAll();

					 return results;
				 }
				 catch (Exception)
				 {
					 List<Turno> lsAux = new List<Turno>();
					 return lsAux;
				 }
			 });

			/// <summary>
			/// Encargados
			/// </summary>		
			Field<ListGraphType<EncargadoType>>("encargadosBySucursal", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "nombre" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" }
			), resolve: context =>
			{
				try
				{
					var nombre = context.GetArgument<string>("nombre");
					var idSucursal = context.GetArgument<string>("idSucursal");

					Guid uidSucursal = new Guid(idSucursal);
					VMAsignacionSupervision vMAsignacionSupervision = new VMAsignacionSupervision();
					vMAsignacionSupervision.BuscarUsuario(nombre, uidSucursal);

					return vMAsignacionSupervision.Encargados;
				}
				catch (Exception)
				{
					List<Encargado> lsAux = new List<Encargado>();
					return lsAux;
				}
			});

			/// <summary>
			/// Get Sucursales by Identifier of Empresa
			/// </summary>
			Field<ListGraphType<SucursalType>>("sucursales", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "nombre" },
				new QueryArgument<StringGraphType> { Name = "idTipoSucursal" },
				new QueryArgument<StringGraphType> { Name = "idEmpresa" },
				new QueryArgument<StringGraphType> { Name = "fechaRAntes" },
				new QueryArgument<StringGraphType> { Name = "fechaRDespues" }
			), resolve: context =>
			{
				try
				{
					var nombreComercial = context.GetArgument<string>("nombre");
					var idTipoSucursal = context.GetArgument<string>("idTipoSucursal");
					var idEmpresa = context.GetArgument<string>("idEmpresa");
					var registradoAntes = context.GetArgument<string>("fechaRAntes");
					var registradoDespues = context.GetArgument<string>("fechaRDespues");

					string strNombreComercial = string.IsNullOrEmpty(nombreComercial) ? string.Empty : nombreComercial;
					string strIdTipoSucursal = string.IsNullOrEmpty(idTipoSucursal) ? string.Empty : idTipoSucursal;
					Guid uidEmpresa = string.IsNullOrEmpty(idEmpresa) ? Guid.Empty : new Guid(idEmpresa);
					DateTime? dtRegistradoDespues = null;
					if (!string.IsNullOrEmpty(registradoDespues))
					{
						dtRegistradoDespues = Convert.ToDateTime(DateTime.ParseExact(registradoDespues, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}

					DateTime? dtRegistradoAntes = null;
					if (!string.IsNullOrEmpty(registradoAntes))
					{
						dtRegistradoAntes = Convert.ToDateTime(DateTime.ParseExact(registradoAntes, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}

					VMSucursales vMSucursales = new VMSucursales();

					vMSucursales.BuscarSucursales(strNombreComercial, dtRegistradoDespues, dtRegistradoAntes, idTipoSucursal, uidEmpresa);
					return vMSucursales.Sucursales;
				}
				catch (Exception)
				{
					List<Sucursal> lsAux = new List<Sucursal>();
					return lsAux;
				}
			});
			Field<SucursalType>("sucursal", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "id" }
			), resolve: context =>
			{
				try
				{
					var id = context.GetArgument<string>("id");

					Sucursal.Repository repository = new Sucursal.Repository();
					return repository.Find(new Guid(id));
				}
				catch (Exception)
				{
					Sucursal Aux = new Sucursal();
					return Aux;
				}
			});

			/// <summary>
			/// Query's to Get Asignaciones de supervisor
			/// <summary>
			Field<ListGraphType<AsignacionSupervisionType>>("asignacionesSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "usuario" },
				new QueryArgument<StringGraphType> { Name = "turno" },
				new QueryArgument<StringGraphType> { Name = "departamentos" },
				new QueryArgument<StringGraphType> { Name = "fechaInicio" },
				new QueryArgument<StringGraphType> { Name = "fechaFin" }
			), resolve: context =>
			{
				try
				{
					var idSucursal = context.GetArgument<string>("idSucursal");
					var usuario = context.GetArgument<string>("usuario");
					var turno = context.GetArgument<string>("turno");
					var departamentos = context.GetArgument<string>("departamentos");
					var fechaInicio = context.GetArgument<string>("fechaInicio");
					var fechaFin = context.GetArgument<string>("fechaFin");

					Guid uidSucursal = new Guid(idSucursal);
					DateTime? DtFechaInicio = null;
					if (!string.IsNullOrEmpty(fechaInicio))
					{
						DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					DateTime? DtFechaFin = null;
					if (!string.IsNullOrEmpty(fechaFin))
					{
						DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(fechaFin, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}

					VMAsignacionSupervision vMAsignacionSupervision = new VMAsignacionSupervision();
					vMAsignacionSupervision.BuscarAsignaciones(
						DtFechaInicio,
						null,
						null,
						DtFechaFin,
						usuario,
						turno,
						departamentos,
						uidSucursal);

					return vMAsignacionSupervision.AsignacionesSupervision;
				}
				catch (Exception)
				{
					List<AsignacionSupervision> lsAux = new List<AsignacionSupervision>();
					return lsAux;
				}
			});

			/* Incumplimiento (Tareas no realizadas) */
			Field<ListGraphType<DepartamentoType>>("departamentosSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsuario" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" }
				), resolve: context =>
				{
					var idUsuario = context.GetArgument<string>("idUsuario");
					var idSucursal = context.GetArgument<string>("idSucursal");
					VMTareasAtrasadas Vm = new VMTareasAtrasadas();
					Vm.ObtenerDepartamentosDeSupervision(new Guid(idUsuario), new Guid(idSucursal), DateTime.Today);
					var results = Vm.Departamentos;
					return results;
				});
			Field<ListGraphType<TareaAtrasadaType>>("getTareasAtrasadasSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsuario" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "fechaInicio" },
				new QueryArgument<StringGraphType> { Name = "fechaFin" },
				new QueryArgument<StringGraphType> { Name = "departamentos" }
				), resolve: context =>
			  {
				  var idUsuario = context.GetArgument<string>("idUsuario");
				  var idSucursal = context.GetArgument<string>("idSucursal");
				  var startDate = context.GetArgument<string>("fechaInicio");
				  var endDate = context.GetArgument<string>("fechaFin");
				  string departamentos = context.GetArgument<string>("departamentos");

				  /* DD/MM/YYYY */
				  DateTime? fechaInicio = null, fechaFin = null;
				  if (startDate != string.Empty) { fechaInicio = Convert.ToDateTime(DateTime.ParseExact(startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)); }
				  if (endDate != string.Empty) { fechaFin = Convert.ToDateTime(DateTime.ParseExact(endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)); }
				  if (departamentos == string.Empty) { departamentos = null; }

				  VMTareasAtrasadas Vm = new VMTareasAtrasadas();
				  Vm.ObtenerTareasAtrasadas(new Guid(idUsuario), new Guid(idSucursal), DateTime.Today, fechaInicio, fechaFin, departamentos);
				  var results = Vm.TareasAtrasadas;
				  return results;
			  });
			Field<TareaAtrasadaType>("getTareaAtrasada", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idTarea" },
					new QueryArgument<StringGraphType> { Name = "idDepartamento" },
					new QueryArgument<StringGraphType> { Name = "idArea" },
					new QueryArgument<StringGraphType> { Name = "idCumplimiento" }
				), resolve: context =>
			  {
				  var idTarea = context.GetArgument<string>("idTarea");
				  var idDepartamento = context.GetArgument<string>("idDepartamento");
				  var idArea = context.GetArgument<string>("idArea");
				  var idCumplimiento = context.GetArgument<string>("idCumplimiento");

				  Guid uidTarea, uidDepto, uidArea = default(Guid), uidCumpl = default(Guid);
				  uidTarea = new Guid(idTarea);
				  uidDepto = new Guid(idDepartamento);
				  if (!string.IsNullOrEmpty(idArea))
					  uidArea = new Guid(idArea);
				  if (!string.IsNullOrEmpty(idCumplimiento))
					  uidCumpl = new Guid(idCumplimiento);

				  VMTareasAtrasadas Vm = new VMTareasAtrasadas();

				  TareaAtrasada tareaAtrasada = new TareaAtrasada();
				  tareaAtrasada.strFechaProxima = "";
				  tareaAtrasada.strFechaProgramada = "";
				  tareaAtrasada.strTipo = "";
				  tareaAtrasada.StrArea = "";
				  tareaAtrasada.StrTurno = "";
				  tareaAtrasada.strServerMessage = "";

				  Vm.ObtenerTarea(uidTarea);
				  tareaAtrasada.UidTarea = Vm.Tarea.UidTarea;
				  tareaAtrasada.StrTarea = Vm.Tarea.StrNombre;
				  tareaAtrasada.strTipo = Vm.Tarea.StrTipoTarea;


				  Vm.ObtenerDepartamento(uidDepto);
				  tareaAtrasada.UidDepartamento = Vm.Departamento.UidDepartamento;
				  tareaAtrasada.StrDepartamento = Vm.Departamento.StrNombre;

				  if (uidArea != default(Guid))
				  {
					  Vm.ObtenerArea(uidArea);
					  tareaAtrasada.UidArea = Vm.Area.UidArea;
				  }
				  if (uidCumpl != default(Guid))
				  {
					  Vm.ObtenerCumplimiento(uidCumpl);
					  tareaAtrasada.strFechaProgramada = Vm.Cumplimiento?.DtFechaProgramada.ToString("dd/MM/yyyy");
				  }

				  tareaAtrasada.StrArea = Vm.Area != null ? Vm.Area.StrNombre : "(global)";
				  DateTime? proximo = Vm.ObtenerFechaSiguienteTarea(uidTarea, DateTime.Now.AddDays(-1.0));
				  tareaAtrasada.strFechaProxima = proximo.HasValue ? proximo?.ToString("dd/MM/yyyy") : "(ninguno)";

				  var result = tareaAtrasada;
				  return result;
			  });


			/*Revisiones*/
			Field<ListGraphType<CalificacionType>>("getCalificaciones", null, resolve: context =>
			  {
				  VMRevision Vm = new VMRevision();
				  Vm.ObtenerCalificaciones();
				  var results = Vm.Calificaciones;
				  return results;
			  });
			Field<ListGraphType<DepartamentoType>>("getDepartamentosPeriodo", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idPeriodo" }
			), resolve: context =>
			{
				var idPeriodo = context.GetArgument<string>("idPeriodo");

				List<Guid> lsPeriodos = new List<Guid>();
				if (!string.IsNullOrEmpty(idPeriodo))
				{
					List<string> ls = idPeriodo.Split(',').ToList();

					foreach (var item in ls)
					{
						lsPeriodos.Add(new Guid(item));
					}
				}

				Departamento departamento = new Departamento();
				departamento.UidDepartamento = Guid.Empty;
				departamento.StrNombre = "Todos";

				VMRevision Vm = new VMRevision();
				Vm.ObtenerDepartamentos(lsPeriodos);

				Vm.Departamentos.Insert(0, departamento);

				var results = Vm.Departamentos;
				return results;
			});

			Field<ListGraphType<RevisionType>>("getRevisionesPendientesSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsuario" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "idDepartamento" },
				new QueryArgument<StringGraphType> { Name = "idArea" },
				new QueryArgument<StringGraphType> { Name = "estado" },
				new QueryArgument<StringGraphType> { Name = "nombre" }
			), resolve: context =>
			 {
				 var idUsuario = context.GetArgument<string>("idUsuario");
				 var idSucursal = context.GetArgument<string>("idSucursal");
				 var idDepartamento = context.GetArgument<string>("idDepartamento");
				 var idArea = context.GetArgument<string>("idArea");
				 var estado = context.GetArgument<string>("estado");
				 var nombre = context.GetArgument<string>("nombre");

				 DateTimeOffset time = Hora.ObtenerHoraServidor();
				 DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));
				 var local = horaLocal.DateTime;

				 int value = Convert.ToInt32(estado);

				 VMRevision Vm = new VMRevision();
				 Vm.ObtenerRevisionesPendientes(new Guid(idUsuario), new Guid(idSucursal), local, null,
					 nombre, new Guid(idDepartamento), new Guid(idArea), value);

				 var results = Vm.RevisionesPendientes;
				 return results;
			 });
			Field<RevisionType>("getDatosRevision", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idRevision" }
			), resolve: context =>
			{
				var idRevision = context.GetArgument<string>("idRevision");

				Guid uidRevision;

				if (idRevision == string.Empty)
					uidRevision = Guid.Empty;
				else
					uidRevision = new Guid(idRevision);

				VMRevision vMRevision = new VMRevision();
				vMRevision.ObtenerRevision(uidRevision);

				return vMRevision.Revision;
			});

			/* Supervision */
			Field<ListGraphType<TareaNoCumplidaType>>("getCumplimientoSupervisor", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idUsuario" }
				), resolve: context =>
			 {
				 var idSucursal = context.GetArgument<string>("idSucursal");
				 var idUsuario = context.GetArgument<string>("idUsuario");
				 VMSupervision Vm = new VMSupervision();
				 Vm.ObtenerCumplimiento(new Guid(idSucursal), new Guid(idUsuario), DateTime.Today);
				 var results = Vm.ltsDepartamento;
				 return results;
			 });
			Field<TareaNoCumplidaType>("getDepartamentoSupervision", arguments: new QueryArguments(
					new QueryArgument<StringGraphType> { Name = "idDepartamento" },
					new QueryArgument<StringGraphType> { Name = "idUsuario" },
					new QueryArgument<StringGraphType> { Name = "idSucursal" },
					new QueryArgument<StringGraphType> { Name = "idInicioTurno" },
					new QueryArgument<StringGraphType> { Name = "fecha" }
				), resolve: context =>
			  {
				  var idDepartamento = context.GetArgument<string>("idDepartamento");
				  var idUsuario = context.GetArgument<string>("idUsuario");
				  var idSucursal = context.GetArgument<string>("idSucursal");
				  var idInicioTurno = context.GetArgument<string>("idInicioTurno");
				  var dtFecha = context.GetArgument<string>("fecha");

				  DateTime dateTime = Convert.ToDateTime(DateTime.ParseExact(dtFecha, "dd-MM-yyyy", CultureInfo.InvariantCulture));
				  VMSupervision Vm = new VMSupervision();
				  Vm.ObtenerDepartamento(new Guid(idDepartamento), new Guid(idUsuario), dateTime, new Guid(idSucursal));

				  TareasNoCumplidas tnc = new TareasNoCumplidas();
				  tnc.StrIdSucursal = idSucursal;
				  tnc.StrEstadoTurno = "";
				  tnc.DtFecha = dateTime;
				  tnc.UidUsuario = new Guid(idUsuario);

				  if (Vm.CTareasNoCumplidas != null)
				  {
					  tnc.UidDepartamento = Vm.CTareasNoCumplidas.UidDepartamento;
					  tnc.UidArea = Vm.CTareasNoCumplidas.UidArea;
					  tnc.StrDepartamento = Vm.CTareasNoCumplidas.StrDepartamento;
					  tnc.StrTurno = Vm.CTareasNoCumplidas.StrTurno;
					  tnc.IntTareasCumplidas = Vm.CTareasNoCumplidas.IntTareasCumplidas;
					  tnc.IntTareasNoCumplidas = Vm.CTareasNoCumplidas.IntTareasNoCumplidas;
					  tnc.IntNumTareasRequeridasdNoCumplidas = Vm.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas;
					  tnc.UidPeriodo = Vm.CTareasNoCumplidas.UidPeriodo;

					  if (!string.IsNullOrEmpty(idInicioTurno))
					  {
						  Vm.ObtenerTurno(new Guid(idInicioTurno));

						  if (Vm.InicioTurno != null)
						  {
							  string dtInicio = Vm.InicioTurno.DtFechaHoraInicio.ToString("dd/MM/yyyy");
							  if (dtInicio != string.Empty) { tnc.DtFechaHoraInicio = Convert.ToDateTime(DateTime.ParseExact(dtInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture)); }

							  string dtFin = Vm.InicioTurno.DtFechaHoraFin == null ? "" : Vm.InicioTurno.DtFechaHoraFin?.ToString("dd/MM/yyyy");
							  if (dtFin.Length > 0) { tnc.DtFechaHoraFin = Convert.ToDateTime(DateTime.ParseExact(dtFin, "dd/MM/yyyy", CultureInfo.InvariantCulture)); }
							  Vm.ObtenerEstadoTurno(Vm.InicioTurno.UidEstadoTurno);

							  if (Vm.EstadoTurno != null)
							  {
								  tnc.UidEstadoTurno = Vm.EstadoTurno.UidEstadoTurno;
								  tnc.StrEstadoTurno = Vm.EstadoTurno.StrEstadoTurno;
							  }
						  }
					  }
				  }

				  return tnc;
			  });

			/* Notificaciones */
			Field<ListGraphType<MensajeNotificacionType>>("getNotificacionesSucursal", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "idDepartamento" },
				new QueryArgument<StringGraphType> { Name = "estado" },
				new QueryArgument<StringGraphType> { Name = "startDate" },
				new QueryArgument<StringGraphType> { Name = "endDate" }
				), resolve: context =>
			{
				var idSucursal = context.GetArgument<string>("idSucursal");
				var idDepartamentos = context.GetArgument<string>("idDepartamento");
				var strEstado = context.GetArgument<string>("estado");
				var startDate = context.GetArgument<string>("startDate");
				var endDate = context.GetArgument<string>("endDate");

				DateTime? fechaInicio = null, fechaFin = null;
				if (startDate != string.Empty) { fechaInicio = Convert.ToDateTime(DateTime.ParseExact(startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)); }
				if (endDate != string.Empty) { fechaFin = Convert.ToDateTime(DateTime.ParseExact(endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)); }

				VMNotificacion Vm = new VMNotificacion();
				if (idDepartamentos == string.Empty) { idDepartamentos = null; }
				if (strEstado == string.Empty) { strEstado = null; }

				Vm.ObtenerNotificaciones(new Guid(idSucursal), idDepartamentos, null, strEstado, fechaInicio, fechaFin);
				var result = Vm.Mensajes;
				return result;
			});
			Field<MensajeNotificacionType>("getDatosNotificacion", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "id" }
			), resolve: context =>
			{
				var idNotificacion = context.GetArgument<string>("id");
				VMNotificacion Vm = new VMNotificacion();
				Vm.ObtenerNotificacion(new Guid(idNotificacion));
				return Vm.Notificacion;
			});

			/*Cumplimiento*/
			Field<ListGraphType<EstadoCumplimientoType>>("getEstadosCumplimiento", null, resolve: context =>
			{
				VMCumplimiento Vm = new VMCumplimiento();
				Vm.ObtenerEstados();
				return Vm.Estados;
			});
			Field<ListGraphType<CumplimientoType>>("getCumplimientos", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsuario" },
				new QueryArgument<StringGraphType> { Name = "idPeriodo" },
				new QueryArgument<StringGraphType> { Name = "idSucursal" },
				new QueryArgument<StringGraphType> { Name = "idDepartamento" },
				new QueryArgument<StringGraphType> { Name = "idArea" },
				new QueryArgument<StringGraphType> { Name = "periodos" },
				new QueryArgument<StringGraphType> { Name = "nombre" },
				new QueryArgument<StringGraphType> { Name = "estado" },
				new QueryArgument<StringGraphType> { Name = "idTipo" }
			), resolve: context =>
			{
				var idUsuario = context.GetArgument<string>("idUsuario");
				var idPeriodo = context.GetArgument<string>("idPeriodo");
				var idSucursal = context.GetArgument<string>("idSucursal");
				var idDepartamento = context.GetArgument<string>("idDepartamento");
				var idArea = context.GetArgument<string>("idArea");
				var strPeriodos = context.GetArgument<string>("periodos");
				var strNombre = context.GetArgument<string>("nombre");
				var strEstados = context.GetArgument<string>("estado");
				var idTipo = context.GetArgument<string>("idTipo");

				List<string> lsStrPeriodos = new List<string>();
				if (!string.IsNullOrEmpty(strPeriodos))
				{
					lsStrPeriodos = strPeriodos.Split(',').ToList();
				}

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(new Guid(idSucursal)));
				var local = horaLocal.DateTime;

				List<Guid> lsPeriodos = new List<Guid>();
				foreach (string item in lsStrPeriodos)
				{
					lsPeriodos.Add(new Guid(item));
				}

				VMCumplimiento Vm = new VMCumplimiento();
				if (idPeriodo == string.Empty)
					idPeriodo = "00000000-0000-0000-0000-000000000000";

				if (strNombre == string.Empty)
					strNombre = null;

				if (strEstados == string.Empty)
					strEstados = null;

				if (idDepartamento == string.Empty)
					idDepartamento = "00000000-0000-0000-0000-000000000000";

				if (idArea == string.Empty)
					idArea = "00000000-0000-0000-0000-000000000000";

				if (idTipo == string.Empty)
					idTipo = "00000000-0000-0000-0000-000000000000";

				Vm.ObtenerTareasDeHoy(new Guid(idUsuario), new Guid(idPeriodo), lsPeriodos, strNombre, strEstados, new Guid(idDepartamento), new Guid(idArea), new Guid(idTipo), local);
				var results = Vm.CumplimientosPendientes;
				return results;
			});
			Field<CumplimientoType>("getDatosCumplimiento", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idTarea" },
				new QueryArgument<StringGraphType> { Name = "idDepto" },
				new QueryArgument<StringGraphType> { Name = "idArea" },
				new QueryArgument<StringGraphType> { Name = "idCumplimiento" },
				new QueryArgument<StringGraphType> { Name = "idPeriodo" }
			), resolve: context =>
			{
				var id = context.GetArgument<string>("idCumplimiento");
				var idTarea = context.GetArgument<string>("idTarea");
				VMCumplimiento Vm = new VMCumplimiento();
				Vm.ObtenerCumplimiento(new Guid(id));

				if (Vm.Cumplimiento == null)
				{
					Vm.Cumplimiento = new Cumplimiento
					{
						StrEstadoCumplimiento = "",
						strProximo = "",
						isDateToday = 0,
						StrObservacion = ""
					};
				}

				DateTime? proximo = Vm.ObtenerFechaSiguienteTarea(new Guid(idTarea), DateTime.Now);
				if (proximo.HasValue)
				{
					Vm.Cumplimiento.strProximo = proximo?.ToString("dd-MM-yyyy");
					Vm.Cumplimiento.isDateToday = proximo.Value.Subtract(DateTime.Today).TotalDays;
				}

				var result = Vm.Cumplimiento;
				return result;
			});
			Field<CumplimientoType>("getCumplimiento", arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "id" }),
			resolve: context =>
			{
				var id = context.GetArgument<string>("id");

				VMCumplimiento Vm = new VMCumplimiento();

				Vm.ObtenerCumplimiento(new Guid(id));
				if (Vm.Cumplimiento != null)
				{
					Vm.Cumplimiento.strProximo = "";
					Vm.Cumplimiento.isDateToday = 0;
				}

				var result = Vm.Cumplimiento;
				return result;
			});
		}
	}
}