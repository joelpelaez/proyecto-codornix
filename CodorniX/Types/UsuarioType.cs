﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
    public class UsuarioType : ObjectGraphType<Usuario>
    {
        public UsuarioType()
        {
            Field(x => x.Id).Description("Identificador del Usuario");
			Field(x => x.STRNOMBRE).Name("nombre").Description("Nombre");
			Field(x => x.STRAPELLIDOPATERNO).Name("apellidoPaterno").Description("Apellido Paterno");
			Field(x => x.STRAPELLIDOMATERNO).Name("apellidoMaterno").Description("Apellido Materno");
			Field(x => x.strDtFechaNacimiento).Name("fechaNacimiento").Description("Fecha de nacimiento");
			Field(x => x.STRCORREO).Name("correo").Description("Direccion de correo electronico");
			Field(x => x.STRUSUARIO).Name("usuario").Description("Nombre de usuario");
			Field(x => x.strIdPerfil).Name("idPerfil").Description("Identificador del perfil");
			Field(x => x.StrEmpresa).Name("empresa").Description("Nombre de la empresa a la que pertenece");
			Field(x => x.IdEstatus).Description("Identificador Estatus del Usuario").Name("uidEstatus");			
        }
    }
}