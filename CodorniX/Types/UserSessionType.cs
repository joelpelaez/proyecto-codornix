﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class UserSessionType : ObjectGraphType<Sesion>
	{
		public UserSessionType()
		{
			Field(x => x.strIdUsuario).Name("idUsuario").Description("Identificador del usuario");
			Field(x => x.strIdEmpresa).Name("idEmpresa").Description("Identificador de la empresa");
			Field(x => x.strIdSucursal).Name("idSucursal").Description("Identificador de la sucursal");
			Field(x => x.strIdPerfil).Name("idPerfil").Description("Identificador del perfil");
			Field(x => x.strIdNivelAcceso).Name("idNivelAcceso").Description("Identificador del nivel de acceso");
			Field(x => x.strIdPeriodo).Name("idPeriodo").Description("Identificador del periodo");
			Field(x => x.strIdTurno).Name("idTurno").Description("Identificador del turno");
			Field(x => x.strIdTurnoSupervisor).Name("idTurnoSupervisor").Description("Identificador del turno del supervisor");
			Field(x => x.appWeb).Name("appWeb").Description("Nivel de la Aplicacion");
			Field(x => x.perfil).Name("perfil").Description("Perfil del usuario");
			Field(x => x.Messsage).Name("message").Description("Mensaje del servidor");

			Field<ListGraphType<StringGraphType>>("uidDepartamentos", "Identificadores de los departamentos", null, resolve: context =>
			{
				List<Guid> lsGuid = new List<Guid>();
				lsGuid = context.Source.UidDepartamentos;

				List<string> listString = new List<string>();

				foreach (var item in lsGuid)
				{
					listString.Add(item.ToString());
				}

				return listString;
			});
		}
	}
}