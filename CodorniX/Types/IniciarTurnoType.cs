﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class IniciarTurnoType : ObjectGraphType<IniciarTurno>
	{
		public IniciarTurnoType() {
			Field(x => x.IntFolio).Name("folio").Description("Folio");
			Field(x => x.strIdInicioTurno).Name("idInicioTurno").Description("Identificador del Inicio de turno");
			Field(x => x.strIdUsuario).Name("idUsuario").Description("Identificador del usuario");
			Field(x => x.strDtFechaHoraInicio).Name("dtInicio").Description("Fecha y Hora de inicio");
			Field(x => x.strDtFechaHoraFin).Name("dtFin").Description("Fecha y Hora de cierre");
			Field(x => x.strIntNoCompleto).Name("noCompleto").Description("Int numero completo");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identificador del departamento");
			Field(x => x.strIdPeriodo).Name("idPeriodo").Description("Identificador del periodo");
			Field(x => x.strIdTurno).Name("idTurno").Description("Identificador del turno");
			Field(x => x.strIdEstadoTurno).Name("idEstadoTurno").Description("Identificador del estado del turno");
			Field(x => x.strIdCreador).Name("idCreador").Description("Identificador del creador");
		}
	}
}