﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class AreaType : ObjectGraphType<Area>
	{
		public AreaType()
		{
			Field(x => x.strIdArea).Name("id").Description("Identidicador");
			Field(x => x.StrNombre).Name("nombre").Description("Nombre del area");
			Field(x => x.StrDescripcion).Name("descripcion").Description("Descripcion del area");
			Field(x => x.StrURL).Name("url").Description("Url de la imagen");
			Field(x => x.strIdStatus).Name("idStatus").Description("Identidicador del estatus");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identidicador del departamento");
		}
	}
}