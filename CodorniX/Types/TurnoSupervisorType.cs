﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class TurnoSupervisorType : ObjectGraphType<TurnoSupervisor>
	{
		public TurnoSupervisorType()
		{
			Field(x => x.strIdTurnoSupervisor).Name("idTurnoSupervisor").Description("Identificador del turno del supervisor");
			Field(x => x.strIdUsuario).Name("idUsuario").Description("Identificador del usuario");
			Field(x => x.strIdSucursal).Name("idSucursal").Description("Identificador de la sucursal");
			Field(x => x.strIdEstadoTurno).Name("idEstadoTurno").Description("Identificador de estado del turno");
			Field(x => x.IntFolio).Name("folio").Description("Folio del turno del supervisor");

			/* DATE */
			Field(x => x.FechaInicio).Name("fechaInicio").Description("Fecha inicio del turno");
			Field(x => x.FechaFin).Name("fechaFin").Description("Fecha de finalizacion del turno");

			/* EXTRA */
			Field(x => x.StrNombre).Name("nombre").Description("Nombre");
			Field(x => x.StrApellidoPaterno).Name("apellidoPaterno").Description("Apellido Paterno");
			Field(x => x.StrApellidoMaterno).Name("apellidoMaterno").Description("Apellido Materno");
			Field(x => x.StrEstadoTurno).Name("estadoTurno").Description("Estado del turno");
		}
	}
}