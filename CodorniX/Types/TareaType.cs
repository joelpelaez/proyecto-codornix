﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class TareaType : ObjectGraphType<Tarea>
	{
		public TareaType()
		{
			Field(x => x.Id).Name("id").Description("Identificador único de la Tarea");
			Field(x => x.strIdTareaCumplida).Name("idTareaCumplida").Description("Identificador de la tarea cumplida");
			Field(x => x.StrNombre).Name("nombre").Description("Nombre de la Tarea");
			Field(x => x.StrDescripcion).Name("descripcion").Description("Descripción de la Tarea");
			Field(x => x.strIdAntecesor).Name("idAntecesor").Description("Identificador del antecesor");
			Field(x => x.strIdUnidadMedida).Name("idUnidadMedida").Description("Identificador de la unidad de medida");
			Field(x => x.strIdPeriodicidad).Name("idPeriodicidad").Description("Identificador periodicida");
			Field(x => x.StrAntecesor).Name("antecesor").Description("Nombre del antecesor");
			Field(x => x.StrTipoFrecuencia).Name("tipoFrecuencia").Description("Tipo de frecuencia");
			Field(x => x.StrPeriodicidad).Name("periodicidad").Description("Periodicidad");
			Field(x => x.IntFolio).Name("folio").Description("Folio de la Tarea (Único por Sucursal)");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identificador del departamento");
			Field(x => x.StrDepartamento).Name("departamento").Description("Descripcion del departamento");
			Field(x => x.strIdTurno).Name("idTurno").Description("Identificador del turno");
			Field(x => x.StrTurno).Name("turno").Description("Descripcion del turno");
			Field(x => x.strIdMedicion).Name("idMedicion").Description("Identificador del tipo de medicion");
			Field(x => x.strTmHora).Name("hora").Description("Hora");
			Field(x => x.strToleranciaInt).Name("tolerancia").Description("Tolerancia");
			Field(x => x.strIdStatus).Name("idStatus").Description("Identificador del estatus");
			Field(x => x.strIdTipoTarea).Name("idTipoTarea").Description("Identificador del tipo de tarea");
			Field(x => x.strFechaInicio).Name("fechaInicio").Description("Fecha de inicio de la tarea");
			Field(x => x.StrUsuario).Name("usuario").Description("Nombre del usuario");
			Field(x => x.strBlFoto).Name("blFoto").Description("Valida si requiere una foto");			
			Field(x => x.StrTipoTarea).Name("tipo").Description("Tipo de Tarea");
			Field(x => x.StrStatus).Name("estatus").Description("Estatus");
			Field(x => x.StrTipoMedicion).Name("tipoMedicion").Description("Tipo de medicion");
			Field(x => x.StrUnidadMedida).Name("unidadMedida").Description("Unidad de medida");
			Field(x => x.BlCaducado).Name("caducado").Description("Boleano que devuelve si la tarea ha caducado");

			Field<ListGraphType<OpcionType>>("opciones", "Lista de Opciones", arguments: null, resolve: context =>
			{
				if (context.Source.StrTipoMedicion.Equals("Seleccionable")) {
					var id = context.Source.UidTarea;
					var repo = new TareaOpcion.Repositorio();
					var results = repo.Buscar(id);
					return results;
				}
				else {
					return null;
				}
			});

			Field<PeriodicidadType>("objPeriodicidad", null, resolve: context =>
			{
				var id = context.Source.UidPeriodicidad;

				var repos = new Periodicidad.Repositorio();
				var result = repos.ConsultarPeriodicidad(id);
				return result;
			});
		}
	}
}