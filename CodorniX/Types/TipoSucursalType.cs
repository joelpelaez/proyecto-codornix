﻿using CodorniX.Modelo;
using GraphQL.Types;
namespace CodorniX.Types
{
	public class TipoSucursalType : ObjectGraphType<TipoSucursal>
	{
		public TipoSucursalType()
		{
			Field(x => x.strIdTipoSucursal).Name("id").Description("Identificador");
			Field(x => x.StrTipoSucursal).Name("tipo").Description("Tipo del sucursal");
		}

	}
}