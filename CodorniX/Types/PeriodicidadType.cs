﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GraphQL.Types;
using CodorniX.Modelo;

namespace CodorniX.Types
{
	public class PeriodicidadType:ObjectGraphType<Periodicidad>
	{
		public PeriodicidadType()
		{
			Field(x => x.strIdPeriodicidad).Name("id").Description("Identificador de la periodicidad");
			Field(x => x.IntFrecuencia).Name("numFrecuencia").Description("Numero de frecuencia");
			Field(x => x.strIdTipoFrecuencia).Name("idTipoFrecuencia").Description("Identificador");
			Field(x => x.strDtFechaInicio).Name("fechaInicio").Description("Fecha de inicio");
			Field(x => x.strDtFechaFin).Name("fechaFin").Description("Fecha fin");

			Field<TipoFrecuenciaType>("tipo",null,resolve: context => 
			{
				var id = context.Source.UidTipoFrecuencia;

				TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio();
				criterio.UidTipoFrecuencia = id;

				var repos = new TipoFrecuencia.Repositorio();
				var result = repos.Buscar(criterio);
				return result;
			});
		}
	}
}