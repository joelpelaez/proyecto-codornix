﻿using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class TurnoType : ObjectGraphType<Turno>
	{
		public TurnoType()
		{
			Field(x => x.strIdTurno).Name("id").Description("Identificador del turno");
			Field(x => x.StrTurno).Name("nombre").Description("Nombre del turno");
		}
	}
}