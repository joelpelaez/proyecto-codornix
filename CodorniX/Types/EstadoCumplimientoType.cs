﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class EstadoCumplimientoType : ObjectGraphType<EstadoCumplimiento>
	{
		public EstadoCumplimientoType()
		{
			Field(x => x.strIdEstadoCumplimiento).Name("id").Description("");
			Field(x => x.StrEstadoCumplimiento).Name("estadoCumplimiento").Description("");
		}
	}
}