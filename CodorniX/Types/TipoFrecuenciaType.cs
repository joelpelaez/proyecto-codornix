﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class TipoFrecuenciaType : ObjectGraphType<TipoFrecuencia>
	{
		public TipoFrecuenciaType()
		{
			Field(x => x.strIdTipoFrecuencia).Name("id").Description("Identificador del tipo de frecuencia");
			Field(x => x.StrTipoFrecuencia).Name("tipo").Description("Tipo de frecuencia");
		}
	}
}