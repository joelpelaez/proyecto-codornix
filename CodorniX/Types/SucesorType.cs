﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class SucesorType : ObjectGraphType<Sucesor>
	{
		public SucesorType() {
			Field(x => x.strIdTarea).Name("idTarea").Description("Identificador");
			Field(x => x.strIdCumplimiento).Name("idCumplimiento").Description("Identificador");
			Field(x => x.strIdEstadoCumplimiento).Name("idEstadoCumplimiento").Description("Identificador");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identificador");
			Field(x => x.StrTarea).Name("tarea").Description("Descripcion de la tarea");
			Field(x => x.StrEstadoCumplimiento).Name("estadoCumplimiento").Description("Estado de cumplimiento");
			Field(x => x.StrDepartamento).Name("departamento").Description("Departamento");
		}
	}
}