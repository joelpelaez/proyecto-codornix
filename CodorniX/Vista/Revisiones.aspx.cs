﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class Revisiones : System.Web.UI.Page
    {
        private VMRevision VM = new VMRevision();
        private Sesion SesionActual
        {
            get
            {
                return (Sesion)Session["Sesion"];
            }
        }

        private List<Revision> VSRevisiones
        {
            get => ViewState["Revisiones"] as List<Revision>;
            set => ViewState["Revisiones"] = value;
        } 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;

            if (!SesionActual.uidSucursalActual.HasValue)
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!Acceso.TieneAccesoAModulo("Revisiones", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!IsPostBack)
            {
                btnRealizar.Disable();
                btnHecho.Disable();
                frmSiNo.Visible = false;
                frmValue.Visible = false;
                frmTwoValues.Visible = false;
                frmOptions.Visible = false;

                VM.ObtenerCalificaciones();
                ddCalificacion.DataSource = VM.Calificaciones;
                ddCalificacion.DataTextField = "StrCalificacion";
                ddCalificacion.DataValueField = "UidCalificacion";
                ddCalificacion.DataBind();
                ddCalificacion.SelectedIndex = 0;
                ddCalificacion.Disable();

                VM.ObtenerDepartamentosAsignados(SesionActual.UidDepartamentos);
                Departamento departamento = new Departamento();
                departamento.UidDepartamento = Guid.Empty;
                departamento.StrNombre = "Todos";
                VM.Departamentos.Insert(0, departamento);
                ddDepartamentos.DataSource = VM.Departamentos;
                ddDepartamentos.DataTextField = "StrNombre";
                ddDepartamentos.DataValueField = "UidDepartamento";
                ddDepartamentos.SelectedIndex = 0;
                ddDepartamentos.DataBind();

                List<Area> areas = new List<Area>();
                Area area = new Area();
                area.UidArea = Guid.Empty; // Todos
                area.StrNombre = "Todos";
                areas.Add(area);
                ddAreas.DataSource = areas;
                ddAreas.DataTextField = "StrNombre";
                ddAreas.DataValueField = "UidArea";
                ddAreas.SelectedIndex = 0;
                ddAreas.DataBind();

                DateTimeOffset time = Hora.ObtenerHoraServidor();
                DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
                var local = horaLocal.DateTime;
                
                int value = Convert.ToInt32(ddEstado.SelectedValue);
                VM.ObtenerRevisionesPendientes(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, local,null,
                    txtNombre.Text, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), value);
                VSRevisiones = VM.RevisionesPendientes;
                dgvTareasPendientes.DataSource = VSRevisiones;
                dgvTareasPendientes.DataBind();
            }
        }

        protected void dgvTareasPendientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uidCumplimiento = new Guid(dgvTareasPendientes.SelectedDataKey.Value.ToString());
            Guid uidRevision = new Guid(dgvTareasPendientes.SelectedDataKey.Values[1].ToString());

            VM.ObtenerCumplimiento(uidCumplimiento);
            VM.ObtenerTarea(VM.Cumplimiento.UidTarea);
            VM.ObtenerRevision(uidRevision);
            fldUidTarea.Value = VM.Cumplimiento.UidTarea.ToString();

            if (VM.Cumplimiento.UidArea.HasValue)
            {
                VM.ObtenerArea(VM.Cumplimiento.UidArea.Value);
                VM.ObtenerDepartamento(VM.Area.UidDepartamento);
                fldUidArea.Value = VM.Area.UidArea.ToString();
            }
            else
            {
                VM.ObtenerDepartamento(VM.Cumplimiento.UidDepartamento.Value);
            }

            fldUidCumplimiento.Value = uidCumplimiento.ToString();
            fldUidDepartamento.Value = VM.Departamento.UidDepartamento.ToString();

            btnActualizar.Enable();
            btnOK.Visible = false;
            btnCancelar.Visible = false;

            lblTarea.Text = VM.Tarea.StrNombre;
            lblDepto.Text = VM.Departamento.StrNombre;
            lblArea.Text = VM.Area == null ? "(global)" : VM.Area.StrNombre;

            frmSiNo.Visible = false;
            frmValue.Visible = false;
            frmTwoValues.Visible = false;
            frmOptions.Visible = false;

            rbYes.Disable();
            rbNo.Disable();
            txtValor.Disable();
            ddOpciones.Disable();
            txtObservaciones.Disable();
            ddCalificacion.Disable();

            switch (VM.Tarea.StrTipoMedicion)
            {
                case "Verdadero/Falso":
                    frmSiNo.Visible = true;
                    if (VM.Cumplimiento != null)
                    {
                        if (VM.Cumplimiento.BlValor.HasValue)
                        {
                            if (VM.Cumplimiento.BlValor.Value)
                            {
                                rbYes.Checked = true;
                                rbNo.Checked = false;
                                lblValorOriginal.Text = "Sí";
                            }
                            else
                            {
                                rbNo.Checked = true;
                                rbYes.Checked = false;
                                lblValorOriginal.Text = "No";
                            }

                            if (VM.Revision != null && VM.Revision.BlValor.HasValue)
                            {
                                if (VM.Revision.BlValor.Value)
                                {
                                    rbYes.Checked = true;
                                    rbNo.Checked = false;
                                }
                                else
                                {
                                    rbNo.Checked = true;
                                    rbYes.Checked = false;
                                }
                            }
                        }
                    }
                    break;
                case "Numerico":
                    frmValue.Visible = true;
                    lblUnidad.Text = VM.Tarea.StrUnidadMedida;
                    if (VM.Revision != null)
                    {
                        if (VM.Revision.DcValor1.HasValue)
                        {
                            lblValorOriginal.Text = VM.Cumplimiento.DcValor1.Value.ToString();
                            txtValor.Text = VM.Revision.DcValor1.Value.ToString();
                            if (string.IsNullOrWhiteSpace(txtValor.Text))
                                txtValor.Text = VM.Cumplimiento.DcValor1.Value.ToString();
                        }
                        else
                        {
                            txtValor.Text = VM.Cumplimiento.DcValor1.Value.ToString();
                        }
                    }
                    else
                    {
                        if (VM.Cumplimiento.DcValor1.HasValue)
                        {
                            lblValorOriginal.Text = VM.Cumplimiento.DcValor1.Value.ToString();

                            txtValor.Text = VM.Cumplimiento.DcValor1.Value.ToString();
                        }

                        else
                            txtValor.Text = "";
                    }
                    break;
                case "Seleccionable":
                    VM.ObtenerOpcionesDeTarea(VM.Tarea.UidTarea);
                    frmOptions.Visible = true;
                    ddOpciones.DataSource = VM.Opciones;
                    ddOpciones.DataValueField = "UidOpciones";
                    ddOpciones.DataTextField = "StrOpciones";
                    ddOpciones.DataBind();
                    if (VM.Cumplimiento != null)
                    {           
                        if (VM.Revision != null && VM.Revision.UidOpcion.HasValue)
                        {
                            lblValorOriginal.Text = VM.Opciones.Where(x => x.UidOpciones == VM.Cumplimiento.UidOpcion.Value).Select(x => x.StrOpciones).FirstOrDefault();
                            if (VM.Revision.UidOpcion.HasValue)
                                ddOpciones.SelectedValue = VM.Revision.UidOpcion.Value.ToString();
                        }
                        else
                        {
							if (VM.Cumplimiento.UidOpcion.HasValue)
								ddOpciones.SelectedValue = VM.Cumplimiento.UidOpcion.Value.ToString();
						}
                    }
                    break;
            }
            txtObservaciones.Text = VM.Cumplimiento.StrObservacion;
            if (VM.Revision != null)
            {
                btnRealizar.Disable();
                btnHecho.Disable();
                ddCalificacion.SelectedValue = VM.Revision.UidCalificacion.ToString();
                if (!string.IsNullOrWhiteSpace(VM.Revision.StrNotas))
                    txtNotas.Text = VM.Revision.StrNotas;
                else
                    txtNotas.Text = string.Empty;
            }
            else
            {
                btnRealizar.Enable();
                btnHecho.Enable();
                txtNotas.Text = string.Empty;
                ddCalificacion.SelectedIndex = 0;
            }

            int pos = -1;
            if (ViewState["CumplPreviousRow"] != null)
            {
                pos = (int)ViewState["CumplPreviousRow"];
                GridViewRow previousRow = dgvTareasPendientes.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["CumplPreviousRow"] = dgvTareasPendientes.SelectedIndex;
            dgvTareasPendientes.SelectedRow.AddCssClass("success");

            lblError.Text = string.Empty;
            valorUnico.RemoveCssClass("has-error");

            VM.ObtenerSucesores(uidCumplimiento);

            dgvSucesores.DataSource = VM.Sucesores;
            dgvSucesores.DataBind();
        }

        protected void dgvTareasPendientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTareasPendientes, "Select$" + e.Row.RowIndex);

                if (e.Row.Cells[3].Text == "&nbsp;")
                {
                    e.Row.Cells[3].Text = "(global)";
                }
                if (e.Row.Cells[4].Text == "&nbsp;")
                {
                    e.Row.Cells[4].Text = "N/A";
                }
                if (e.Row.Cells[5].Text == "&nbsp;")
                {
                    e.Row.Cells[5].Text = "No Realizada";
                }
            }
        }

        protected void btnRealizar_Click(object sender, EventArgs e)
        {
            rbYes.Enable();
            rbNo.Enable();
            txtValor.Enable();
            ddOpciones.Enable();
            txtNotas.Enable();
            btnRealizar.Disable();
            btnHecho.Disable();
            ddCalificacion.Enable();
            btnOK.Visible = true;
            btnCancelar.Visible = true;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
            Guid uidTarea = new Guid(fldUidTarea.Value);

            Guid? realCumplimiento = uidCumplimiento;
            bool? estado = null;
            decimal? valor1 = null;
            decimal? valor2 = null;
            Guid? opcion = null;

            lblError.Text = string.Empty;
            valorUnico.RemoveCssClass("has-error");

            VM.ObtenerTarea(uidTarea);
            VM.ObtenerCumplimiento(uidCumplimiento);
            switch (VM.Tarea.StrTipoMedicion)
            {
                case "Verdadero/Falso":
                    if (rbYes.Checked)
                    {
                        if (!VM.Cumplimiento.BlValor.Value)
                            estado = true;
                    }
                    else if (rbNo.Checked)
                    {
                        if (VM.Cumplimiento.BlValor.Value)
                            estado = false;
                    }
                    else
                    {
                        estado = false;
                    }
                    break;
                case "Numerico":
                    try
                    {
                        decimal tmp;
                        tmp = Convert.ToDecimal(txtValor.Text);

                        if (VM.Cumplimiento.DcValor1.Value.ToString() != tmp.ToString())
                            valor1 = tmp;
                    }
                    catch (FormatException ex)
                    {
                        lblError.Text = "Solo se aceptan valores numéricos";
                        valorUnico.AddCssClass("has-error");
                        return;
                    }
                    break;
                case "Seleccionable":
                    if (VM.Cumplimiento.UidOpcion.ToString() != ddOpciones.SelectedValue.ToString())
                        opcion = new Guid(ddOpciones.SelectedValue.ToString());
                    break;
            }

            Guid selected = new Guid(ddCalificacion.SelectedValue);

            DateTimeOffset time = Hora.ObtenerHoraServidor();
            DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
            var local = horaLocal.DateTime;
            VM.RegistrarRevision(realCumplimiento.Value, SesionActual.uidUsuario,
                local, estado, valor1, valor2, opcion, txtNotas.Text, false, selected);

            rbYes.Disable();
            rbNo.Disable();
            txtValor.Disable();
            ddOpciones.Disable();
            txtObservaciones.Disable();
            ddCalificacion.Disable();
            btnHecho.Enable();
            btnRealizar.Enable();
            btnOK.Visible = false;
            btnCancelar.Visible = false;
            
            int value = Convert.ToInt32(ddEstado.SelectedValue);
            VM.ObtenerRevisionesPendientes(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, local,
                null, txtNombre.Text, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), value);
            VSRevisiones = VM.RevisionesPendientes;
            dgvTareasPendientes.DataSource = VSRevisiones;
            dgvTareasPendientes.DataBind();

            ViewState["CumplPreviousRow"] = null;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            rbYes.Disable();
            rbNo.Disable();
            txtValor.Disable();
            ddOpciones.Disable();
            btnRealizar.Enable();
            btnHecho.Enable();
            btnOK.Visible = false;
            btnCancelar.Visible = false;
            lblError.Text = string.Empty;
            valorUnico.RemoveCssClass("has-error");
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            frmSiNo.Visible = false;
            frmValue.Visible = false;
            frmTwoValues.Visible = false;
            frmOptions.Visible = false;
            DateTimeOffset time = Hora.ObtenerHoraServidor();
            DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
            var local = horaLocal.DateTime;
            
            int value = Convert.ToInt32(ddEstado.SelectedValue);
            VM.ObtenerRevisionesPendientes(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, local,
                null, txtNombre.Text, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), value);
            VSRevisiones = VM.RevisionesPendientes;
            dgvTareasPendientes.DataSource = VSRevisiones;
            dgvTareasPendientes.DataBind();

            lblError.Text = string.Empty;
            valorUnico.RemoveCssClass("has-error");

            ViewState["CumplPreviousRow"] = null;

            
            btnMostrar.Text = "Ocultar";
            panelBusqueda.Visible = false;
            panelResultados.Visible = true;
            btnLimpiar.Visible = false;
            btnActualizar.Visible = false;
        }

        protected void btnHecho_Click(object sender, EventArgs e)
        {
            Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
            Guid uidTarea = new Guid(fldUidTarea.Value);

            Guid? realCumplimiento = uidCumplimiento;
            bool? estado = null;
            decimal? valor1 = null;
            decimal? valor2 = null;
            Guid? opcion = null;
            DateTimeOffset time = Hora.ObtenerHoraServidor();
            DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
            var local = horaLocal.DateTime;

            Guid selected = new Guid(ddCalificacion.SelectedValue);
            VM.RegistrarRevision(realCumplimiento.Value, SesionActual.uidUsuario, local, estado,
                valor1, valor2, opcion, txtNotas.Text, true, selected);

            rbYes.Disable();
            rbNo.Disable();
            txtValor.Disable();
            ddOpciones.Disable();
            txtObservaciones.Disable();
            btnHecho.Disable();
            btnRealizar.Disable();
            btnOK.Visible = false;
            btnCancelar.Visible = false;
            int value = Convert.ToInt32(ddEstado.SelectedValue);
            VM.ObtenerRevisionesPendientes(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, local,
                null, txtNombre.Text, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), value);
            VSRevisiones = VM.RevisionesPendientes;
            dgvTareasPendientes.DataSource = VSRevisiones;
            dgvTareasPendientes.DataBind();

            ViewState["CumplPreviousRow"] = null;
        }

        protected void dgvSucesores_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Guid uidCumpl = new Guid(fldUidCumplimiento.Value);

            if (e.CommandName == "Crear")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Guid uidTarea = new Guid(dgvSucesores.DataKeys[i].Value.ToString());
                VM.CrearSucesor(uidCumpl, uidTarea);
            }
            else if (e.CommandName == "Habilitar")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Guid uidCumplNuevo = new Guid(dgvSucesores.DataKeys[i].Values[1].ToString());
                VM.HabilitarSucesor(uidCumplNuevo);
            }
            else if (e.CommandName == "Deshabilitar")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Guid uidCumplNuevo = new Guid(dgvSucesores.DataKeys[i].Values[1].ToString());
                VM.DeshabilitarSucesor(uidCumplNuevo);
            }
            else
            {
                return;
            }

            VM.ObtenerSucesores(uidCumpl);
            dgvSucesores.DataSource = VM.Sucesores;
            dgvSucesores.DataBind();
        }

        protected void btnCumplimiento_Click(object sender, EventArgs e)
        {
            activeCumplimiento.AddCssClass("active");
            activeRevision.RemoveCssClass("active");
            activeSucesores.RemoveCssClass("active");
            panelCumplimiento.Visible = true;
            panelRevision.Visible = false;
            panelSucesores.Visible = false;
        }

        protected void tabRevision_Click(object sender, EventArgs e)
        {
            activeCumplimiento.RemoveCssClass("active");
            activeRevision.AddCssClass("active");
            activeSucesores.RemoveCssClass("active");
            panelCumplimiento.Visible = false;
            panelRevision.Visible = true;
            panelSucesores.Visible = false;
        }

        protected void tabSucesores_Click(object sender, EventArgs e)
        {
            activeCumplimiento.RemoveCssClass("active");
            activeRevision.RemoveCssClass("active");
            activeSucesores.AddCssClass("active");
            panelCumplimiento.Visible = false;
            panelRevision.Visible = false;
            panelSucesores.Visible = true;
        }

        protected void dgvSucesores_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton crear = e.Row.FindControl("CrearButton") as LinkButton;
                LinkButton habilitar = e.Row.FindControl("HabilitarButton") as LinkButton;
                LinkButton deshabilitar = e.Row.FindControl("DeshabilitarButton") as LinkButton;

                string estado = e.Row.Cells[1].Text;

                if (estado == "No Creado")
                {
                    crear.Enable();
                    habilitar.Disable();
                    deshabilitar.Disable();
                }
                else if (estado == "Deshabilitado")
                {
                    crear.Disable();
                    habilitar.Enable();
                    deshabilitar.Disable();
                }
                else if (estado == "No Realizado")
                {
                    crear.Disable();
                    habilitar.Disable();
                    deshabilitar.Enable();
                }
                else
                {
                    crear.Disable();
                    habilitar.Disable();
                    deshabilitar.Disable();
                }
            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            if (btnMostrar.Text == "Mostrar")
            {
                btnMostrar.Text = "Ocultar";
                panelBusqueda.Visible = false;
                panelResultados.Visible = true;
                btnLimpiar.Visible = false;
                btnActualizar.Visible = false;
            }
            else
            {
                btnMostrar.Text = "Mostrar";
                panelBusqueda.Visible = true;
                panelResultados.Visible = false;
                btnLimpiar.Visible = true;
                btnActualizar.Visible = true;
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "";
            ddDepartamentos.SelectedIndex = 0;
            ddAreas.SelectedIndex = 0;
        }

        protected void ddDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid selected = new Guid(ddDepartamentos.SelectedValue);
            if (selected != Guid.Empty)
            {
                VM.ObtenerAreas(selected);
                Area area = new Area();
                area.UidArea = new Guid("00000000-0000-0000-0000-000000000001"); // General
                area.StrNombre = "General";
                VM.Areas.Insert(0, area);
                area = new Area();
                area.UidArea = new Guid("00000000-0000-0000-0000-000000000000"); // Todos
                area.StrNombre = "Todos";
                VM.Areas.Insert(0, area);
                ddAreas.DataSource = VM.Areas;
                ddAreas.DataTextField = "StrNombre";
                ddAreas.DataValueField = "UidArea";
                ddAreas.DataBind();

                ddAreas.SelectedIndex = 0;
            }
            else
            {
                List<Area> areas = new List<Area>();
                Area area = new Area();
                area.UidArea = Guid.Empty; // Todos
                area.StrNombre = "Todos";
                areas.Add(area);
                ddAreas.DataSource = areas;
                ddAreas.DataTextField = "StrNombre";
                ddAreas.DataValueField = "UidArea";
                ddAreas.SelectedIndex = 0;
                ddAreas.DataBind();
            }
        }

        protected void dgvTareasPendientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["CumplPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["SortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortRevisiones(SortExpression, SortDirection, true);
            }
            else
            {
                dgvTareasPendientes.DataSource = VSRevisiones;
            }
            dgvTareasPendientes.PageIndex = e.NewPageIndex;
            dgvTareasPendientes.DataBind();
        }

        protected void dgvTareasPendientes_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["CumplPreviousRow"] = null;
            SortRevisiones(e.SortExpression, e.SortDirection, false);
            dgvTareasPendientes.DataBind();
        }

        private void SortRevisiones(string sortExpression, SortDirection sortDirection, bool same = false)
        {
            if (sortExpression == (string)ViewState["SortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                sortDirection =
                    ((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }

            if (sortExpression == "Folio")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                    VSRevisiones = VSRevisiones.OrderBy(x => x.IntFolio).ToList();
                }
                else
                {
                    VSRevisiones = VSRevisiones.OrderByDescending(x => x.IntFolio).ToList();
                }
            }
            
            if (sortExpression == "Tarea")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                     VSRevisiones = VSRevisiones.OrderBy(x => x.StrTarea).ToList();
                }
                else
                {
                     VSRevisiones = VSRevisiones.OrderByDescending(x => x.StrTarea).ToList();
                }
            }
            else if (sortExpression == "Departamento")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                     VSRevisiones = VSRevisiones.OrderBy(x => x.StrDepartamento).ToList();
                }
                else
                {
                     VSRevisiones = VSRevisiones.OrderByDescending(x => x.StrDepartamento).ToList();
                }
            }
            else if (sortExpression == "Area")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                     VSRevisiones = VSRevisiones.OrderBy(x => x.StrArea).ToList();
                }
                else
                {
                     VSRevisiones = VSRevisiones.OrderByDescending(x => x.StrArea).ToList();
                }
            }
            else if (sortExpression == "Completo")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                     VSRevisiones = VSRevisiones.OrderBy((x => x.StrEstado), new EstadoComparator()).ToList();
                }
                else
                {
                     VSRevisiones = VSRevisiones.OrderByDescending((x => x.StrEstado), new EstadoComparator()).ToList();
                }
            }

            dgvTareasPendientes.DataSource = VSRevisiones;
            ViewState["SortColumn"] = sortExpression;
            ViewState["SortColumnDirection"] = sortDirection;
        }

        private class EstadoComparator : IComparer<string>
        {
            private int GetValue(string s)
            {
                if (s == "No Revisado")
                    return -1;
                else if (s == "Revisado")
                    return 3;
                else
                    return 10;
            }
            public int Compare(string x, string y)
            {
                return GetValue(x) - GetValue(y);
            }
        }

    }
}