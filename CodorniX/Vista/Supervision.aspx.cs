﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class Supervision : Page
	{
		private readonly VMSupervision VM = new VMSupervision();

		private Sesion SesionActual => (Sesion)Session["Sesion"];

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("Bienvenido", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!SesionActual.uidTurnoSupervisor.HasValue)
			{
				Response.Redirect("InicioSupervisor.aspx", false);
				return;
			}

			if (!IsPostBack)
			{
				divCerrarturno.Visible = false;
				dvgDepartamentos.Visible = false;
				dvgDepartamentos.DataSource = null;
				dvgDepartamentos.DataBind();
				PanelCompletadas.Visible = false;
				PanelNoCompletadas.Visible = false;
				panelRequeridas.Visible = false;
				btnInicioTurno.Disable();
				btnCerrarTurno.Disable();
				btnActualizar_Click(sender, e);
			}
		}

		protected void DVGResumenTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
		}

		protected void DVGResumenTareas_Sorting(object sender, GridViewSortEventArgs e)
		{
		}

		protected void dvgDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				lblAceptarCerrarTurno.Text = string.Empty;
				lblnoiniciarturno.Text = string.Empty;
				var date = Convert.ToDateTime(dvgDepartamentos.SelectedDataKey.Values[2].ToString());
				var usuario = new Guid(dvgDepartamentos.SelectedDataKey.Values[1].ToString());
				var content = dvgDepartamentos.SelectedDataKey.Values[3]?.ToString();

				VM.ObtenerDepartamento(new Guid(dvgDepartamentos.SelectedDataKey.Value.ToString()), usuario, date,
					SesionActual.uidSucursalActual.Value);

				if (VM.CTareasNoCumplidas == null)
				{
					lblmensaje.Visible = true;
					lblmensaje.Text = "Error al obtener datos";
				}

				txtUidDepartamento.Text = VM.CTareasNoCumplidas.UidDepartamento.ToString();
				txtUidArea.Text = VM.CTareasNoCumplidas.UidArea.ToString();
				var uidDepartamento = VM.CTareasNoCumplidas.UidDepartamento;
				lblDepartamento.Text = VM.CTareasNoCumplidas.StrDepartamento.ToString();
				lblTurno.Text = VM.CTareasNoCumplidas.StrTurno;
				lblCumplidas.Text = VM.CTareasNoCumplidas.IntTareasCumplidas.ToString();
				lblNoCumplidas.Text = VM.CTareasNoCumplidas.IntTareasNoCumplidas.ToString();
				lblRequeridasNoCumplidas.Text = VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas.ToString();
				SesionActual.UidPeriodo = VM.CTareasNoCumplidas.UidPeriodo;
				fldUidPeriodo.Value = VM.CTareasNoCumplidas.UidPeriodo.ToString();

				if (!string.IsNullOrEmpty(content))
				{
					fldUidInicioTurno.Value = content;
					VM.ObtenerTurno(new Guid(fldUidInicioTurno.Value));
					btnCerrarTurno.Enable();
				}
				else
				{
					fldUidInicioTurno.Value = string.Empty;
					btnCerrarTurno.Disable();
				}

				fldUidUsuario.Value = usuario.ToString();

				if (VM.InicioTurno != null)
				{
					lblHoraInicio.Text = VM.InicioTurno.DtFechaHoraInicio.ToString("hh\\:mm tt");
					lblHoraFin.Text = VM.InicioTurno.DtFechaHoraFin.HasValue
						? VM.InicioTurno.DtFechaHoraFin.Value.ToString("hh\\:mm tt")
						: "";
					if (lblHoraFin.Text.Length == 0)
						lblEstadoDelTurno.Text = "Abierto";
					else
						lblEstadoDelTurno.Text = "Cerrado";
				}
				else
				{
					lblHoraInicio.Text = string.Empty;
					lblHoraFin.Text = string.Empty;
					if (lblHoraInicio.Text.Length == 0 && lblHoraFin.Text.Length == 0) lblEstadoDelTurno.Text = "No creado";
				}

				VM.ObtenerTareasCumplidas(uidDepartamento, usuario, date);
				DVGTareasCumplidas.DataSource = VM.ltsTareasCumplidas;
				DVGTareasCumplidas.DataBind();

				VM.ObtenerTareasNoCumplidas(uidDepartamento, usuario, date, SesionActual.uidSucursalActual.Value);
				DvgTareasNoCumplidas.DataSource = VM.ltsTareasNoCumplidas;
				DvgTareasNoCumplidas.DataBind();

				VM.ObtenerTareasRequeridas(uidDepartamento, usuario, date, SesionActual.uidSucursalActual.Value);
				dgvTareasRequeridas.DataSource = VM.TareasRequeridas;
				dgvTareasRequeridas.DataBind();

				btnIniciarTurno.Disable();
				btnInicioTurno.Disable();
				btnCerrarTurno.Disable();
				btnBloquearTurno.Disable();

				if (VM.InicioTurno != null)
				{
					VM.ObtenerEstadoTurno(VM.InicioTurno.UidEstadoTurno);

					if (VM.EstadoTurno == null)
					{
						btnIniciarTurno.Text = "Iniciar";
						btnIniciarTurno.Enable();
					}
					else
						switch (VM.EstadoTurno.StrEstadoTurno)
						{
							case "Abierto":
								btnInicioTurno.Text = "Tomar";
								btnInicioTurno.Enable();
								btnBloquearTurno.Enable();
								btnCerrarTurno.Enable();
								break;
							case "Abierto (Controlado)":
							case "Abierto por Supervisor":
								btnInicioTurno.Text = "Ceder";
								btnInicioTurno.Enable();
								btnBloquearTurno.Enable();
								btnCerrarTurno.Enable();
								break;
							case "Bloqueado":
								btnInicioTurno.Text = "Tomar";
								btnBloquearTurno.Text = "Desbloquear";
								btnBloquearTurno.Enable();
								break;
							case "Cerrado":
								btnIniciarTurno.Text = "Reabrir";
								btnIniciarTurno.Enable();
								break;
							default:
								btnIniciarTurno.Text = "Iniciar";
								btnIniciarTurno.Enable();
								break;
						}
					//if (!VM.InicioTurno._blAbiertoEncargado) { btnInicioTurno.Disable(); }
				}
				else
				{
					btnIniciarTurno.Text = "Iniciar";
					btnIniciarTurno.Enable();
				}

				if (ViewState["DepartamentoPreviousRow"] != null)
				{
					var pos = (int)ViewState["DepartamentoPreviousRow"];
					var previousRow = dvgDepartamentos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}

				ViewState["DepartamentoPreviousRow"] = dvgDepartamentos.SelectedIndex;
				dvgDepartamentos.SelectedRow.AddCssClass("success");
			}
			catch (Exception)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error al obtener datos";
			}
		}

		protected void dvgDepartamentos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
				e.Row.Attributes["onclick"] =
					ClientScript.GetPostBackClientHyperlink(dvgDepartamentos, "Select$" + e.Row.RowIndex);
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			btnActualizar.Enabled = false;
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				VM.ObtenerCumplimiento(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario, DateTime.Today);

				dvgDepartamentos.Visible = true;
				dvgDepartamentos.DataSource = VM.ltsDepartamento;
				dvgDepartamentos.DataBind();

				var master = (Site1)Page.Master;
				master.Noiniciarturno();
				master.NoTurno();
				btnCancelarCerrarTurno_Click(null, null);

				// Eliminar los elementos procesados con anterioridad para evitar accesos innecesarios.
				SesionActual.UidPeriodos?.Clear();

				// Procesar turnos para encontrar si existe alguna que tenga acceso el supervisor.
				foreach (var turno in VM.ltsDepartamento)
					if (turno.StrEstadoTurno.Contains("Abierto"))
						if (!SesionActual.UidPeriodos.Contains(turno.UidPeriodo))
							SesionActual.UidPeriodos.Add(turno.UidPeriodo);

				ViewState["DepartamentoPreviousRow"] = null;
			}
			catch (Exception)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error al actualizar";
			}
			finally { btnActualizar.Enabled = true; }
		}

		protected void tabResumen_Click(object sender, EventArgs e)
		{
			PanelResumen.Visible = true;
			PanelCompletadas.Visible = false;
			PanelNoCompletadas.Visible = false;
			panelRequeridas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeResumen.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasCumplidas.Visible = false;
			divTareasNoCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void tabCompletadas_Click(object sender, EventArgs e)
		{
			PanelCompletadas.Visible = true;
			PanelResumen.Visible = false;
			PanelNoCompletadas.Visible = false;
			panelRequeridas.Visible = false;

			activeResumen.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasNoCumplidas.Visible = false;
			divTareasCumplidas.Visible = true;
			PanelBusqueda.Visible = true;
		}

		protected void tabNoCompletadas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = true;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			panelRequeridas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasNoCumplidas.Visible = true;
			divTareasCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void btnReporte_Click(object sender, EventArgs e)
		{
			var uidInicioTurno = default(Guid);
			var uidPeriodo = new Guid(fldUidPeriodo.Value);
			if (!string.IsNullOrEmpty(fldUidInicioTurno.Value))
			{
				uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ObtenerTurno(uidInicioTurno);
				VM.ObtenerEstadoTurno(VM.InicioTurno.UidEstadoTurno);
				if (/*VM.EstadoTurno.StrEstadoTurno == "Cerrado"*/ true)
				{
					Session["Periodo"] = uidPeriodo;
					Session["Fecha"] = DateTime.Today;
					Session["HoraInicio"] = lblHoraInicio.Text;
					Session["HoraFin"] = lblHoraFin.Text.Length == 0 ? "Sin Cerrar Turno" : lblHoraFin.Text;
					ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
						"window.open('ReporteTareas.aspx', '_blank')", true);
					return;
				}
			}

			lblError.Text = "El turno no se ha cerrado y no puede obtener su reporte.";
			panelAlert.Visible = true;
		}

		protected void btnAceptarCerrarTurno_Click(object sender, EventArgs e)
		{
			var master = (Site1)Page.Master;
			master.AceptarCerrarTurno();
		}

		public void btnCancelarCerrarTurno_Click(object sender, EventArgs e)
		{
			divCerrarturno.Visible = false;
			lblAceptarCerrarTurno.Text = string.Empty;
		}

		protected void tabRequeridas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = false;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			panelRequeridas.Visible = true;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "";
			activeRequeridas.AddCssClass("active");

			divTareasNoCumplidas.Visible = false;
			divTareasCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void btnCloseAlert_Click(object sender, EventArgs e)
		{
			panelAlert.Visible = false;
			lblError.Text = string.Empty;
		}

		protected void btnLista_Click(object sender, EventArgs e)
		{
			var uidInicioTurno = default(Guid);
			var uidPeriodo = new Guid(fldUidPeriodo.Value);
			if (!string.IsNullOrEmpty(fldUidInicioTurno.Value))
			{
				uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ObtenerTurno(uidInicioTurno);
				VM.ObtenerEstadoTurno(VM.InicioTurno.UidEstadoTurno);

				Session["Usuario"] = new Guid(fldUidUsuario.Value);
				Session["Periodo"] = uidPeriodo;
				Session["Fecha"] = DateTime.Today;
				Session["HoraInicio"] = lblHoraInicio.Text;
				Session["HoraFin"] = lblHoraFin.Text.Length == 0 ? "Sin Cerrar Turno" : lblHoraFin.Text;
				ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
					"window.open('ListaTareas.aspx', '_blank')", true);
			}
		}

		protected void btnIniciarTurno_Click(object sender, EventArgs e)
		{
			string control = btnIniciarTurno.Text;
			var uidPeriodo = new Guid(fldUidPeriodo.Value);

			if (control.Equals("Iniciar")) {
				
				var uidUsuario = new Guid(fldUidUsuario.Value);

				if (string.IsNullOrEmpty(fldUidInicioTurno.Value))
				{
					VM.CrearTurno(uidPeriodo, SesionActual.uidUsuario, SesionActual.uidUsuario, SesionActual.GetDateTimeOffset(), "Abierto por Supervisor", false, true);
				}
				else
				{
					var uidInicioTurno = new Guid(fldUidInicioTurno.Value);
					//VM.ObtenerTurno(uidInicioTurno);
					VM.ModificarEstadoTurno(uidInicioTurno, "Abierto por Supervisor");
				}				
			}
			else if (control.Equals("Reabrir"))
			{
				var uidInicioTurno = (fldUidInicioTurno.Value);
				VM.ReabrirTurno(new Guid(uidInicioTurno), "Abierto por Supervisor");
			}

			// Administrar el turno
			SesionActual.UidPeriodos.Add(uidPeriodo);
			btnInicioTurno.Disable();
			btnActualizar_Click(null, null);
		}
		protected void btnInicioTurno_Click(object sender, EventArgs e)
		{
			if (btnInicioTurno.Text == "Tomar")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);

				if (string.IsNullOrEmpty(fldUidInicioTurno.Value))
				{
					VM.CrearTurno(uidPeriodo, SesionActual.uidUsuario, SesionActual.uidUsuario, SesionActual.GetDateTimeOffset(),
						"Abierto (Controlado)", false, true);
				}
				else
				{
					var uidInicioTurno = new Guid(fldUidInicioTurno.Value);
					VM.ObtenerTurno(uidInicioTurno);
					VM.ModificarEstadoTurno(uidInicioTurno, "Abierto (Controlado)");
				}

				SesionActual.UidPeriodos.Add(uidPeriodo);
				btnInicioTurno.Text = "Ceder";
			}
			else if (btnInicioTurno.Text == "Ceder")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);
				var uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ModificarEstadoTurno(uidInicioTurno, "Abierto");

				SesionActual.UidPeriodos.Remove(uidPeriodo);

				btnInicioTurno.Text = "Tomar";
			}

			btnActualizar_Click(sender, e);
			var site = (Site1)Master;
			site.UpdateNavbar();
		}
		protected void btnBloquearTurno_Click(object sender, EventArgs e)
		{
			if (btnBloquearTurno.Text == "Bloquear")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);

				if (string.IsNullOrEmpty(fldUidInicioTurno.Value))
				{
					VM.CrearTurno(uidPeriodo, SesionActual.uidUsuario, SesionActual.uidUsuario, SesionActual.GetDateTimeOffset(),
						"Bloqueado", false, true);
				}
				else
				{
					var uidInicioTurno = new Guid(fldUidInicioTurno.Value);
					VM.ObtenerTurno(uidInicioTurno);
					VM.ModificarEstadoTurno(uidInicioTurno, "Bloqueado");
				}

				SesionActual.UidPeriodos.Add(uidPeriodo);
				btnBloquearTurno.Text = "Desbloquear";
			}
			else if (btnBloquearTurno.Text == "Desbloquear")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);
				var uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ModificarEstadoTurno(uidInicioTurno, "Abierto (Controlado)");

				SesionActual.UidPeriodos.Remove(uidPeriodo);

				btnBloquearTurno.Text = "Bloquear";
			}

			btnActualizar_Click(sender, e);
			var site = (Site1)Master;
			site.UpdateNavbar();
		}
		protected void btnCerrarTurno_Click(object sender, EventArgs e)
		{
			var uidPeriodo = new Guid(fldUidPeriodo.Value);
			var uidInicioTurno = new Guid(fldUidInicioTurno.Value);

			VM.CerrarTurno(uidInicioTurno, SesionActual.GetDateTimeOffset());

			SesionActual.UidPeriodos.Remove(uidPeriodo);

			btnActualizar_Click(sender, e);
		}
		/*Iniciado por supervisor, Tomar, Bloquear*/
	}
}