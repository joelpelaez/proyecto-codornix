﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Bienvenido.aspx.cs" Inherits="CodorniX.Vista.Bienvenido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<asp:PlaceHolder runat="server" ID="PanelBusqueda">
				<div class="panel panel-primary">
					<div class="panel-heading text-center">Turnos</div>
					<div class="pull-right">
						<asp:LinkButton ID="btnActualizar" OnClick="btnActualizar_Click" CssClass="btn btn-sm btn-default" runat="server">
                            <span class="glyphicon glyphicon-search"></span>
                            Buscar
						</asp:LinkButton>
					</div>
					<div class="panel-body">
						<asp:PlaceHolder ID="PanelBusquedas" runat="server">
							<div class="col-md-4">
								<h6>Fecha</h6>
								<div class="input-group date extra">
									<asp:TextBox ID="txtFecha" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-4">
								<h6>Fecha</h6>
								<div class="input-group date extra">
									<asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
						</asp:PlaceHolder>
						<div class="col-md-12" style="padding-top: 15px;">
							<asp:Label runat="server" ID="lblmensaje" />
							<asp:GridView ID="dvgDepartamentos" runat="server" CssClass="table table-bordered table-sm table-condensed" OnRowDataBound="dvgDepartamentos_RowDataBound" OnSelectedIndexChanged="dvgDepartamentos_SelectedIndexChanged" AutoGenerateColumns="false" DataKeyNames="UidDepartamento,DtFecha">
								<EmptyDataTemplate>No hay turnos disponibles.</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="StrTurno" HeaderText="Turno" />
									<asp:BoundField DataField="DtFecha" DataFormatString="{0:d}" HeaderText="Fecha" />
									<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" />
									<asp:BoundField DataField="DtFechaHoraInicio" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Fecha y hora Inicio" />
									<asp:BoundField DataField="DtFechaHoraFin" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Fecha y hora Fin" />
									<asp:BoundField DataField="StrEstadoTurno" HeaderText="Estado" />
								</Columns>
							</asp:GridView>
						</div>
					</div>
				</div>
			</asp:PlaceHolder>
		</div>

		<asp:Label ID="lblnoiniciarturno" runat="server" />

		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Tareas</div>
				<div class="row">
					<div class="col-xs-6 text-left">
						<asp:LinkButton ID="btnReporte" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnReporte_Click" OnClientClick="document.forms[0].target ='_blank';">
                            <span class="glyphicon glyphicon-file"></span>
                            Reporte
						</asp:LinkButton>
					</div>
					<div class="col-xs-6 text-right">
						<div class="btn-group">
							<asp:LinkButton ID="btnIniciarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnIniciarTurno_Click">
                                Inicar turno
							</asp:LinkButton>
							<asp:LinkButton ID="btnCerrarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnCerrarTurno_Click">
                                Cerrar turno
							</asp:LinkButton>
						</div>
					</div>
					<div class="col-md-12">
						<asp:Label Text="Mensaje de error" CssClass="text-danger" runat="server" Visible="false" ID="lblErrorGeneral" />
						<asp:HiddenField ID="hfUidInicioTurno" runat="server" />
					</div>
				</div>
				<asp:TextBox CssClass="hide" ID="txtUidDepartamento" runat="server"></asp:TextBox>
				<asp:TextBox CssClass="hide" ID="txtUidArea" runat="server"></asp:TextBox>

				<div id="divCerrarturno" runat="server" class="alert alert-default">
					<asp:Label ID="lblAceptarCerrarTurno" runat="server" />

					<asp:LinkButton ID="btnAceptarCerrarTurno" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnAceptarCerrarTurno_Click">
						<asp:Label ID="Label2" CssClass="glyphicon glyphicon-ok" runat="server" />
					</asp:LinkButton>
					<asp:LinkButton ID="btnCancelarCerrarTurno" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnCancelarCerrarTurno_Click">
                        <span class="glyphicon glyphicon-remove"></span>
					</asp:LinkButton>
				</div>
				<asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
					<div class="alert alert-danger">
						<asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">x</asp:LinkButton>
						<strong>Error: </strong>
						<asp:Label ID="lblError" runat="server" />
					</div>
				</asp:PlaceHolder>
				<div class="panel-body">

					<ul class="nav nav-tabs">
						<li class="active" id="activeResumen" runat="server">
							<asp:LinkButton ID="tabResumen" runat="server" Text="Resumen" OnClick="tabResumen_Click" /></li>
						<li id="activeCompletadas" runat="server">
							<asp:LinkButton ID="tabCompletadas" runat="server" Text="Completadas" OnClick="tabCompletadas_Click" />
						</li>
						<li id="activeNoCompletadas" runat="server">
							<asp:LinkButton ID="tabNoCompletadas" runat="server" Text="No Completadas" OnClick="tabNoCompletadas_Click" /></li>
						<li id="activeRequeridas" runat="server">
							<asp:LinkButton ID="tabRequeridas" runat="server" Text="Requeridas" OnClick="tabRequeridas_Click" />
						</li>
					</ul>
					<asp:PlaceHolder ID="PanelResumen" runat="server">
						<div style="padding-top: 25px" class="col-md-12">
							<h5>Departamento</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblDepartamento" />
						</div>
						<div class="col-md-4">
							<h5>Turno</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblTurno" />
						</div>
						<div class="col-md-4">
							<h5>Hora Inicio</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraInicio"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Hora Fin</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraFin"></asp:TextBox>
						</div>

						<div class="col-md-4">
							<h5>Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>No Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblNoCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Requeridas No Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblRequeridasNoCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Estado</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblEstadoDelTurno"></asp:TextBox>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelCompletadas" runat="server">
						<div id="divTareasCumplidas" runat="server" style="padding-top: 25px">
							<asp:Label runat="server">Tareas Cumplidas</asp:Label>
							<asp:GridView ID="DVGTareasCumplidas" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false" DataKeyNames="UidTareaCumplida">
								<EmptyDataTemplate>No hay Tareas Cumplidas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelNoCompletadas" runat="server">
						<div id="divTareasNoCumplidas" runat="server" style="padding-top: 25px">
							<asp:Label runat="server"> No cumplidas</asp:Label>
							<asp:GridView ID="DvgTareasNoCumplidas" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false" DataKeyNames="UidTareaNoCumplida">
								<EmptyDataTemplate>No hay Tareas No Cumplidas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrEstadoCumplimiento" HeaderText="Estado" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="panelRequeridas" runat="server">
						<div style="padding-top: 25px;">
							<asp:Label runat="server">Requeridas</asp:Label>
							<asp:GridView ID="dgvTareasRequeridas" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false">
								<EmptyDataTemplate>No hay Tareas Requeridas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrEstadoCumplimiento" HeaderText="Estado" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>

	<script>
		//<![CDATA[
		function pageLoad() {
			prepareFechaAll();
		}
        //]]>
	</script>
</asp:Content>
