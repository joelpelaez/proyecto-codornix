﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="AsignacionInactividad.aspx.cs" Inherits="CodorniX.Vista.AsignacionInactividad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Asignación de Inactidad</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                Lista de Inactividades
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-right">
                            <div class="btn-group">
                                <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnBuscar_Click">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Buscar
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnLimpiar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    Limpiar
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnMostrar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click" Text="Mostrar" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="panelBusqueda" runat="server">
                        <div class="row">
                            <div class="col-xs-6">
                                <h6>Fecha inicio</h6>
                                <div class="date input-group start-date">
                                    <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar" />
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h6>Fecha fin</h6>
                                <div class="date input-group end-date">
                                    <asp:TextBox ID="txtFechaFin" runat="server" CssClass="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar" />
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <h6>Departamentos</h6>
                                <asp:ListBox ID="lbDepartamentos" runat="server" CssClass="form-control" Rows="6" SelectionMode="Multiple" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelResultados" runat="server">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:GridView ID="dgvListaInactividades" runat="server" AutoGenerateColumns="false" CssClass="table table-sm table-bordered" DataKeyNames="UidPeriodoInactividad,DtFechaInicio,DtFechaFin" OnSelectedIndexChanged="dgvListaInactividades_SelectedIndexChanged" OnRowDataBound="dgvListaInactividades_RowDataBound">
                                    <EmptyDataTemplate>
                                        No existen días sin asignar durante el periodo especificado.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
                                        <asp:BoundField DataField="DtFechaInicio" HeaderText="Fecha Inicio" SortExpression="FechaInicio" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="DtFechaFin" HeaderText="Fecha Fin" SortExpression="FechaFin" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="IntDiasInactividad" HeaderText="Días inactivos" SortExpression="Dias" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                Días de inactividad
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="text-right">
                                        <asp:LinkButton ID="btnQuitarTodos" runat="server" CssClass="btn btn-danger btn-sm" Text="Quitar Seleccionados" OnClick="btnQuitarTodos_Click" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div style="height: 400px; overflow-y: scroll;">
                                        <asp:GridView ID="dgvDiasInactividad" runat="server" AutoGenerateColumns="false" CssClass="table table-sm table-bordered table-responsive" DataKeyNames="DtFecha" OnSelectedIndexChanged="dgvDiasInactividad_SelectedIndexChanged" OnRowDataBound="dgvDiasInactividad_RowDataBound" OnRowCommand="dgvDiasInactividad_RowCommand">
                                            <EmptyDataTemplate>
                                                No hay días inactivos próximos del periodo seleccionado.
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <span>
                                                            <asp:CheckBox ID="cbTodos" runat="server" AutoPostBack="true" OnCheckedChanged="cbTodos_CheckedChanged" /></span>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <span>
                                                            <asp:CheckBox ID="cbSeleccionado" runat="server" AutoPostBack="true" OnCheckedChanged="cbSeleccionado_CheckedChanged" /></span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DtFecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="QuitarButton" runat="server" CommandName="Quitar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" CssClass="btn btn-danger btn-sm" Text="Quitar" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" DataField="BlAsignado" HeaderText="Asignado" ReadOnly="true" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h6>Nombre del encargado</h6>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtNombreEncargado" runat="server" CssClass="form-control" />
                                        <span class="input-group-btn">
                                            <asp:LinkButton ID="btnBuscarEncargado" runat="server" CssClass="btn btn-default" OnClick="btnBuscarEncargado_Click">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="height: 350px; overflow-y: scroll;">
                                        <asp:GridView ID="dgvEncargados" runat="server" DataKeyNames="UidUsuario" AutoGenerateColumns="false" CssClass="table table-sm table-bordered table-responsive" OnSelectedIndexChanged="dgvEncargados_SelectedIndexChanged" OnRowDataBound="dgvEncargados_RowDataBound" OnRowCommand="dgvEncargados_RowCommand">
                                            <EmptyDataTemplate>
                                                Realize una búsqueda.
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:BoundField DataField="StrNombreCompleto" HeaderText="Nombre Completo" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="AsignarButton" runat="server" CommandName="Asignar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" CssClass="btn btn-success btn-sm" Text="Asignar" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        //<![CDATA[
        var startDateReady = false;
        var endDateReady = false;

        function enableDatapicker() {
            if (!startDateReady) {
                $(".input-group.date.start-date").datepicker({
                    todayBtn: true,
                    clearBtn: true,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'es',
                }).on('changeDate', function (e) {
                    setEndDateLimit(e.format());
                });
            }
            if (!endDateReady) {
                $(".input-group.date.end-date").datepicker({
                    todayBtn: true,
                    clearBtn: true,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'es',
                });
            }
        }

        function setStartDateLimit(date) {
            $(".input-group.date.start-date").datepicker('remove');
            $(".input-group.date.start-date").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: date,
            }).on('changeDate', function (e) {
                setEndDateLimit(e.format());
            });
            setEndDateLimit(date);
            $(".input-group.date.start-date").datepicker('update', date);
            startDateReady = true;
        }

        function setEndDateLimit(date) {
            $(".input-group.date.end-date").datepicker('remove');
            $(".input-group.date.end-date").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: date,
            });
            $(".input-group.date.end-date").datepicker('update', date);
            endDateReady = true;
        }

        function pageLoad() {
            enableDatapicker();
            $(".input-group.date.n-n").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            })
            $(".input-group.date.start-inactivity").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            })
            $(".input-group.date.end-inactivity").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            })
        }
        //]]>
    </script>
</asp:Content>
