﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class Incumplimiento : System.Web.UI.Page
    {
        private VMTareasAtrasadas VM = new VMTareasAtrasadas();
        private Sesion SesionActual
        {
            get
            {
                return (Sesion)Session["Sesion"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;

            if (!SesionActual.uidSucursalActual.HasValue)
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!Acceso.TieneAccesoAModulo("Incumplimiento", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!IsPostBack)
            {
                btnPosponer.Disable();
                btnCancelar.Disable();

                VM.ObtenerDepartamentosDeSupervision(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, DateTime.Today);
                lbDepartamentos.DataSource = VM.Departamentos;
                lbDepartamentos.DataTextField = "StrNombre";
                lbDepartamentos.DataValueField = "UidDepartamento";
                lbDepartamentos.DataBind();

                dgvTareasIncompletas.DataSource = null;
                dgvTareasIncompletas.DataBind();

                btnBuscar_Click(sender, e);
            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            if (btnMostrar.Text == "Mostrar")
            {
                panelGridTareas.Visible = true;
                panelBusqueda.Visible = false;
                btnLimpiar.Visible = false;
                btnBuscar.Visible = false;
                btnMostrar.Text = "Ocultar";
            }
            else
            {
                panelGridTareas.Visible = false;
                panelBusqueda.Visible = true;
                btnLimpiar.Visible = true;
                btnBuscar.Visible = true;
                btnMostrar.Text = "Mostrar";
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            lbDepartamentos.ClearSelection();
            txtFechaInicio.Text = string.Empty;
            txtFechaFin.Text = string.Empty;

        }

		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			ViewState["TareasPreviousRow"] = null;
			DateTime? fechaInicio = null, fechaFin = null;
			string departamentos = "";
			if (txtFechaInicio.Text != string.Empty)
			{
				fechaInicio = Convert.ToDateTime(txtFechaInicio.Text);
			}
			if (txtFechaFin.Text != string.Empty)
			{
				fechaFin = Convert.ToDateTime(txtFechaFin.Text);
			}
			int[] i = lbDepartamentos.GetSelectedIndices();

			foreach (int j in i)
			{
				string value = lbDepartamentos.Items[j].Value;
				if (departamentos.Count() == 0)
					departamentos += value;
				else
					departamentos += "," + value;
			}

			if (departamentos == string.Empty)
				departamentos = null;

			VM.ObtenerTareasAtrasadas(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, DateTime.Today, fechaInicio, fechaFin, departamentos);

			dgvTareasIncompletas.DataSource = VM.TareasAtrasadas;
			dgvTareasIncompletas.DataBind();

			string[] dBusqueda = new string[3];
			dBusqueda[0] = fechaInicio == null ? "" : fechaInicio.ToString();
			dBusqueda[1] = fechaFin == null ? "" : fechaFin.ToString();
			dBusqueda[2] = departamentos == "" ? "" : departamentos;
			Session["dBusqueda"] = dBusqueda;

			btnMostrar.Text = "Mostrar";
            btnMostrar_Click(sender, e);
        }

        protected void dgvTareasIncompletas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTareasIncompletas, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvTareasIncompletas_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnCancel_Click(sender, e);

            string sUidTarea = dgvTareasIncompletas.SelectedDataKey.Value.ToString();
            string sUidDepto = dgvTareasIncompletas.SelectedDataKey.Values[1].ToString();
            string sUidArea = dgvTareasIncompletas.SelectedDataKey.Values[2]?.ToString();
            string sUidCumpl = dgvTareasIncompletas.SelectedDataKey.Values[4]?.ToString();

            fldUidTarea.Value = sUidTarea;
            fldUidDepartamento.Value = sUidDepto;
            fldUidArea.Value = sUidArea;
            fldUidCumplimiento.Value = sUidCumpl;

            Guid uidTarea, uidDepto, uidArea = default(Guid), uidCumpl = default(Guid);
            uidTarea = new Guid(sUidTarea);
            uidDepto = new Guid(sUidDepto);
            if (!string.IsNullOrEmpty(sUidArea))
                uidArea = new Guid(sUidArea);
            if (!string.IsNullOrEmpty(sUidCumpl))
                uidCumpl = new Guid(sUidCumpl);

            VM.ObtenerTarea(uidTarea);
            VM.ObtenerDepartamento(uidDepto);
            if (uidArea != default(Guid))
                VM.ObtenerArea(uidArea);
            if (uidCumpl != default(Guid))
                VM.ObtenerCumplimiento(uidCumpl);
            
            DateTime? proximo = VM.ObtenerFechaSiguienteTarea(new Guid(fldUidTarea.Value), DateTime.Now.AddDays(-1.0));

            if (proximo.HasValue && proximo.Value > DateTime.Today)
                btnPosponer.Enable();
            else
                btnPosponer.Disable();
            btnCancelar.Enable();

            lblTarea.Text = VM.Tarea.StrNombre;
            lblDepto.Text = VM.Departamento.StrNombre;
            lblArea.Text = VM.Area != null ? VM.Area.StrNombre : "(global)";
            lblTipoTarea.Text = VM.Tarea.StrTipoTarea;
            lblFechaPrograda.Text = VM.Cumplimiento?.DtFechaProgramada.ToString("dd/MM/yyyy");
            lblFechaSiguiente.Text = proximo.HasValue ? proximo?.ToString("dd/MM/yyyy") : "(ninguno)";
            lblErrorTarea.Text = string.Empty;
            fldEditing.Value = string.Empty;
            posponerCampos.Visible = false;

            int pos = -1;
            if (ViewState["TareasPreviousRow"] != null)
            {
                pos = (int)ViewState["TareasPreviousRow"];
                GridViewRow previousRow = dgvTareasIncompletas.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["TareasPreviousRow"] = dgvTareasIncompletas.SelectedIndex;
            dgvTareasIncompletas.SelectedRow.AddCssClass("success");
        }

		protected void dgvTareasIncompletas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ViewState["TareasPreviousRow"] = null;
			string[] dBusqueda = new string[3];
			dBusqueda = (string[])Session["dBusqueda"];
			DateTime? fechaInicio = null, fechaFin = null;
			if (dBusqueda[0] != string.Empty) { fechaInicio = Convert.ToDateTime(dBusqueda[0]); }
			if (dBusqueda[1] != string.Empty) { fechaFin = Convert.ToDateTime(dBusqueda[1]); }

			VM.ObtenerTareasAtrasadas(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, DateTime.Today, fechaInicio, fechaFin, dBusqueda[2]);

			dgvTareasIncompletas.DataSource = VM.TareasAtrasadas;
			dgvTareasIncompletas.PageIndex = e.NewPageIndex;
			dgvTareasIncompletas.DataBind();
		}

		protected void btnPosponer_Click(object sender, EventArgs e)
        {
            btnPosponer.Disable();
            btnCancelar.Disable();
            btnOK.Enable();
            btnOK.Visible = true;
            btnCancel.Enable();
            btnCancel.Visible = true;

            posponerCampos.Visible = true;
            fldEditing.Value = "Posponer";

            DateTime? proximo = VM.ObtenerFechaSiguienteTarea(new Guid(fldUidTarea.Value), DateTime.Now);
            string script = "enablePosponerDatepicker('" + DateTime.Today.ToString("dd/MM/yyyy") + "', '" + proximo?.AddDays(-1.0).ToString("dd/MM/yyyy") + "')";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateStartDate", script, true);
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            btnPosponer.Disable();
            btnCancelar.Disable();
            btnOK.Enable();
            btnOK.Visible = true;
            btnCancel.Enable();
            btnCancel.Visible = true;
            posponerCampos.Visible = false;
            fldEditing.Value = "Cancelar";
            lblErrorTarea.Text = string.Empty;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Guid? uidTarea, uidDepto, uidArea = null, uidCumpl = null;
            uidTarea = new Guid(fldUidTarea.Value);
            uidDepto = new Guid(fldUidDepartamento.Value);
            if (fldUidArea.Value != string.Empty)
                uidArea = new Guid(fldUidArea.Value);
            if (fldUidCumplimiento.Value != string.Empty)
                uidCumpl = new Guid(fldUidCumplimiento.Value);

            if (fldEditing.Value == "Posponer")
            {
                DateTime fechaNueva = default(DateTime);
                try
                {
                    fechaNueva = Convert.ToDateTime(txtFecha.Text);
                }
                catch
                {
                    lblErrorTarea.Text = "La fecha no es correcta";
                    panelAlert.Visible = true;
                    return;
                }

                int result = VM.PosponerTarea(uidTarea.Value, uidDepto, uidArea, uidCumpl, DateTime.Today, fechaNueva);

                if (result == 2)
                {
                    lblErrorTarea.Text = "La tarea fue completada";
                    panelAlert.Visible = true;
                }
                else if (result == 1)
                {
                    lblErrorTarea.Text = "La tarea no puede posponerse más allá de su siguiente cumplimiento.";
                    panelAlert.Visible = true;
                    // Salir inmediatamente para revisar.
                    return;
                }

                // Llamar nuevamente a la busqueda
                btnBuscar_Click(sender, e);

                lblTarea.Text = "(ninguno)";
                lblDepto.Text = "(ninguno)";
                lblArea.Text = "(ninguno)";
                lblTipoTarea.Text = "(ninguno)";
                lblFechaPrograda.Text = "(ninguno)";
                lblFechaSiguiente.Text = "(ninguno)";
                lblErrorTarea.Text = string.Empty;
                fldEditing.Value = string.Empty;
                btnOK.Disable();
                btnOK.Visible = false;
                btnCancel.Disable();
                btnCancel.Visible = false;
                txtFecha.Disable();
                txtFecha.Text = "";
                return;
            }
            else if (fldEditing.Value == "Cancelar")
            {
                int result = VM.CancelarTarea(uidTarea.Value, uidDepto, uidArea, uidCumpl, DateTime.Today);

                if (result == 2)
                {
                    lblErrorTarea.Text = "La tarea fue completada";
                    panelAlert.Visible = true;
                }

                // Llamar nuevamente a la busqueda
                btnBuscar_Click(sender, e);

                lblTarea.Text = "(ninguno)";
                lblDepto.Text = "(ninguno)";
                lblArea.Text = "(ninguno)";
                lblTipoTarea.Text = "(ninguno)";
                lblFechaPrograda.Text = "(ninguno)";
                lblFechaSiguiente.Text = "(ninguno)";
                lblErrorTarea.Text = string.Empty;
                fldEditing.Value = string.Empty;
                btnOK.Disable();
                btnOK.Visible = false;
                btnCancel.Disable();
                btnCancel.Visible = false;
                txtFecha.Disable();
                txtFecha.Text = "";
                return;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnPosponer.Enable();
            btnCancelar.Enable();
            btnOK.Disable();
            btnOK.Visible = false;
            btnCancel.Disable();
            btnCancel.Visible = false;

            fldEditing.Value = string.Empty;
        }
        
        protected void btnCloseAlert_Click(object sender, EventArgs e)
        {
            panelAlert.Visible = false;
            lblErrorTarea.Text = string.Empty;
        }

		
	}
}