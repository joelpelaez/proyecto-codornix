﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CodorniX.Vista.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Content/bootstrap-theme.css" rel="stylesheet" />
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <script src="../Scripts/jquery-3.2.1.js"></script>
    <script src="../Scripts/bootstrap.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label runat="server" CssClass="control-label input-lg">Login</asp:Label>
                        </div>
                        <div class="panel-body">
                            <div>
                                <asp:Label ID="lblMensaje" runat="server" />
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:Label ID="lblUsuario" runat="server" CssClass="control-label" Text="Usuario:" AssociatedControlID="lblUsuario"> </asp:Label>
                                <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:Label ID="lblPassword" runat="server" CssClass="control-label" Text="Contraseña:" AssociatedControlID="lblPassword"></asp:Label>
                                <asp:TextBox ID="txtPassword" CssClass="form-control" runat="server" TextMode="Password" />
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:Button ID="btnLogin"  OnClick="btnLogin_Click" CssClass="btn btn-sm btn-default pull-right" runat="server" Text="Iniciar Sessión" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
