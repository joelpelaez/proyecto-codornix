﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PruebarCorreo.aspx.cs" Inherits="CodorniX.Vista.PruebarCorreo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Destinatario: <asp:TextBox ID="txtCorreo" runat="server" />
            <br />
            Asunto: <asp:TextBox ID="txtAsunto" runat="server" />
            <br />
            Mensaje:
            <br />
            <asp:TextBox ID="txtMensaje" runat="server" TextMode="MultiLine" />
            <br />
            <asp:FileUpload runat="server" ID="fuAttachment" accept="application/pdf" />
            <br />
            <asp:LinkButton ID="btnEnviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" />
        </div>
    </form>
</body>
</html>
