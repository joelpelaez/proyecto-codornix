﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using CodorniX.Util;
using System.Globalization;

namespace CodorniX.Vista
{
	public partial class Bienvenido : System.Web.UI.Page
	{
		VMBienvenido VM = new VMBienvenido();
		VMIniciarTurno VMI = new VMIniciarTurno();
		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("Bienvenido", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				try
				{
					btnIniciarTurno.Disable();
					btnCerrarTurno.Disable();
					txtFecha.Text = SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy");
					txtfecha2.Text = SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy");
					divCerrarturno.Visible = false;
					dvgDepartamentos.Visible = false;
					dvgDepartamentos.DataSource = null;
					dvgDepartamentos.DataBind();
					PanelCompletadas.Visible = false;
					PanelNoCompletadas.Visible = false;
					panelRequeridas.Visible = false;
					btnActualizar_Click(sender, e);
				}
				catch (Exception)
				{

				}
			}
		}

		protected void DVGResumenTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{

		}
		protected void DVGResumenTareas_Sorting(object sender, GridViewSortEventArgs e)
		{

		}

		protected void dvgDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			Site1 master = (Site1)Page.Master;
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }
				string estadoTurno = null;
				lblAceptarCerrarTurno.Text = string.Empty;
				lblnoiniciarturno.Text = string.Empty;

				string strDate = dvgDepartamentos.SelectedDataKey.Values[1].ToString();
				DateTime date = Convert.ToDateTime(strDate);
				VM.ObtenerDepartamento(new Guid(dvgDepartamentos.SelectedDataKey.Value.ToString()), SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				txtUidDepartamento.Text = VM.CTareasNoCumplidas.UidDepartamento.ToString();
				txtUidArea.Text = VM.CTareasNoCumplidas.UidArea.ToString();
				Guid UidDepartamento = VM.CTareasNoCumplidas.UidDepartamento;
				lblDepartamento.Text = VM.CTareasNoCumplidas.StrDepartamento;
				lblTurno.Text = VM.CTareasNoCumplidas.StrTurno;
				lblCumplidas.Text = VM.CTareasNoCumplidas.IntTareasCumplidas.ToString();
				lblNoCumplidas.Text = VM.CTareasNoCumplidas.IntTareasNoCumplidas.ToString();
				lblRequeridasNoCumplidas.Text = VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas.ToString();

				Guid uidPeriodo = VM.CTareasNoCumplidas.UidPeriodo;
				bool abiertoEncargado = false;
				bool abiertoSupervisor = false;
				bool isClosed = false;

				VM.ObtenerHora(date, SesionActual.uidUsuario, uidPeriodo);

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				DateTime local = horaLocal.DateTime;

				VMI.ObtenerPeriodoTurno(uidPeriodo);
				VMI.ObtenerTurnoUsuario(uidPeriodo, local.ToString("dd/MM/yyyy"));

				if (VMI.CIniciarTurno != null)
				{
					hfUidInicioTurno.Value = VMI.CIniciarTurno.UidInicioTurno.ToString();
					VMI.ObtenerEstadoTurno(VMI.CIniciarTurno.UidEstadoTurno);
					estadoTurno = VMI.EstadoTurno.StrEstadoTurno;
					abiertoEncargado = VMI.CIniciarTurno._blAbiertoEncargado;
					abiertoSupervisor = VMI.CIniciarTurno._blAbiertoSupervisor;

					lblHoraInicio.Text = VMI.CIniciarTurno.DtFechaHoraInicio.ToString("t");
					lblHoraFin.Text = VMI.CIniciarTurno.DtFechaHoraFin == null ? "" : VMI.CIniciarTurno.DtFechaHoraFin?.ToString("t");
					if (lblHoraFin.Text.Length == 0)
					{
						lblEstadoDelTurno.Text = "Abierto";
					}
					else
					{
						lblEstadoDelTurno.Text = "Cerrado";
						isClosed = true;
					}
				}
				else
				{
					lblHoraInicio.Text = string.Empty;
					lblHoraFin.Text = string.Empty;
					if (lblHoraInicio.Text.Length == 0 && lblHoraFin.Text.Length == 0)
					{
						lblEstadoDelTurno.Text = "No Creado";
					}
				}

				VMI.ObtenerInicioTurno(SesionActual.uidUsuario);

				if (VMI.CIniciarTurno != null)
				{
					if (hfUidInicioTurno.Value != string.Empty) { hfUidInicioTurno.Value = VMI.CIniciarTurno.UidInicioTurno.ToString(); }

					if (abiertoEncargado)
					{
						if (lblHoraFin.Text.Length == 0 && lblHoraInicio.Text.Length != 0)
						{
							master.Noiniciarturno();
							btnIniciarTurno.Disable();
							master.SiCerrarTurno();
							btnCerrarTurno.Enable();
							master.siturno();
							master.SiCumplimiento();
							if (!abiertoEncargado) { master.siiniciarturno(); }
						}
						else
						{
							master.NoCumplimiento();
							master.NoTurno();
							isClosed = true;
						}
					}
				}
				else
				{
					if (VM.CTareasNoCumplidas.DtFecha.ToString("dd/MM/yyyy") != SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy"))
					{
						master.Noiniciarturno();
						btnIniciarTurno.Disable();
						master.NoTurno();
					}
					else
					{
						master.siturno();
						master.siiniciarturno();
						master.noCerrarTurno();
						master.NoCumplimiento();
						btnIniciarTurno.Enable();
						btnCerrarTurno.Disable();
					}
				}

				if (isClosed)
				{
					btnIniciarTurno.Disable();
					btnCerrarTurno.Disable();
					master.NoTurno();
				}
				else
				{
					Guid? uidInicioActual = null;
					if (!string.IsNullOrEmpty(hfUidInicioTurno.Value))
					{
						uidInicioActual = new Guid(hfUidInicioTurno.Value);
					}

					bool Result = VMI.ValidarCirreInicioTurnoAnteriorDepartamento(uidInicioActual,new Guid(txtUidDepartamento.Text));

					if (!Result)
					{
						master.NoTurno();
						btnIniciarTurno.Disable();
						btnCerrarTurno.Disable();

						lblmensaje.Visible = true;
						lblmensaje.Text = "Turno pendiente por cerrar";
					}
					if (estadoTurno != null)
					{
						if (estadoTurno == "Bloqueado")
						{
							SesionActual.UidPeriodo = null;
							master.NoTurno();
							btnIniciarTurno.Disable();
							btnCerrarTurno.Disable();
						}
						else if (estadoTurno == "Abierto por Supervisor")
						{
							SesionActual.UidPeriodo = null;
							master.NoTurno();
							btnIniciarTurno.Disable();
							btnCerrarTurno.Disable();
						}
						else if (estadoTurno == "Abierto (Controlado)")
						{
							master.noCerrarTurno();
							if (abiertoEncargado)
							{
								btnIniciarTurno.Disable();
							}
							else
							{
								btnIniciarTurno.Enable();
							}

							btnCerrarTurno.Disable();

						}
						else if (estadoTurno.Equals("Abierto") && !abiertoEncargado)
						{
							SesionActual.UidPeriodo = null;
							btnIniciarTurno.Enable();
						}
						else if (estadoTurno.Equals("Abierto") && abiertoEncargado)
						{
							btnIniciarTurno.Disable();
							btnCerrarTurno.Enable();
						}
					}
				}

				if (abiertoEncargado || (!abiertoEncargado && !abiertoSupervisor)) { SesionActual.UidPeriodo = uidPeriodo; }

				VM.ObtenerTareasCumplidas(UidDepartamento, SesionActual.uidUsuario, date);
				DVGTareasCumplidas.DataSource = VM.ltsTareasCumplidas;
				DVGTareasCumplidas.DataBind();

				VM.ObtenerTareasNoCumplidas(UidDepartamento, SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				DvgTareasNoCumplidas.DataSource = VM.ltsTareasNoCumplidas;
				DvgTareasNoCumplidas.DataBind();

				VM.ObtenerTareasRequeridas(UidDepartamento, SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				dgvTareasRequeridas.DataSource = VM.TareasRequeridas;
				dgvTareasRequeridas.DataBind();

				int pos = -1;
				if (ViewState["DepartamentoPreviousRow"] != null)
				{
					pos = (int)ViewState["DepartamentoPreviousRow"];
					GridViewRow previousRow = dvgDepartamentos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}
				ViewState["DepartamentoPreviousRow"] = dvgDepartamentos.SelectedIndex;
				dvgDepartamentos.SelectedRow.AddCssClass("success");
			}
			catch (Exception ex)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error al obtener datos: " + ex.Message;
			}

		}

		protected void dvgDepartamentos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgDepartamentos, "Select$" + e.Row.RowIndex);
			}
		}

		public void btnActualizar_Click(object sender, EventArgs e)
		{
			ActualizarGrid();
		}
		public void ActualizarGrid()
		{
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				string filtrofecha = txtFecha.Text;
				string filtrofecha2 = txtfecha2.Text;

				if (txtfecha2.Text.Length == 0)
				{
					filtrofecha2 = filtrofecha;
				}
				if (txtFecha.Text.Length == 0 && txtfecha2.Text.Length != 0)
				{
					filtrofecha = filtrofecha2;
				}
				if (txtFecha.Text.Length == 0 && txtfecha2.Text.Length == 0)
				{
					dvgDepartamentos.DataSource = null;
					dvgDepartamentos.DataBind();
				}
				else
				{
					List<TareasNoCumplidas> cumplimiento = new List<TareasNoCumplidas>();
					DateTime fecha1 = Convert.ToDateTime(DateTime.ParseExact(filtrofecha, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					DateTime fecha2 = Convert.ToDateTime(DateTime.ParseExact(filtrofecha2, "dd/MM/yyyy", CultureInfo.InvariantCulture));

					DateTime fechatemporal;
					fechatemporal = fecha1;

					for (; fechatemporal <= fecha2; fechatemporal = fechatemporal.AddDays(1))
					{
						VM.ObtenerCumplimiento(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario, fechatemporal);
						cumplimiento.AddRange(VM.ltsDepartamento);
					}

					dvgDepartamentos.Visible = true;
					dvgDepartamentos.DataSource = cumplimiento;
					dvgDepartamentos.DataBind();

					Site1 master = (Site1)Page.Master;
					master.Noiniciarturno();
					master.NoTurno();
					btnCancelarCerrarTurno_Click(null, null);
				}

				ViewState["DepartamentoPreviousRow"] = null;
			}
			catch (Exception er)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = er.Message;
			}
		}

		protected void tabResumen_Click(object sender, EventArgs e)
		{
			PanelResumen.Visible = true;
			PanelCompletadas.Visible = false;
			PanelNoCompletadas.Visible = false;
			panelRequeridas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeResumen.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasCumplidas.Visible = false;
			divTareasNoCumplidas.Visible = false;
			PanelBusqueda.Visible = true;

		}

		protected void tabCompletadas_Click(object sender, EventArgs e)
		{
			PanelCompletadas.Visible = true;
			PanelResumen.Visible = false;
			PanelNoCompletadas.Visible = false;
			panelRequeridas.Visible = false;

			activeResumen.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasNoCumplidas.Visible = false;
			divTareasCumplidas.Visible = true;
			PanelBusqueda.Visible = true;
		}

		protected void tabNoCompletadas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = true;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			panelRequeridas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasNoCumplidas.Visible = true;
			divTareasCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void btnReporte_Click(object sender, EventArgs e)
		{
			if (SesionActual.UidPeriodo != null)
			{
				if (lblErrorGeneral.Visible == true) { lblErrorGeneral.Visible = false; }
				Session["Periodo"] = SesionActual.UidPeriodo;
				Session["Fecha"] = Convert.ToDateTime(txtFecha.Text);
				Session["HoraInicio"] = lblHoraInicio.Text;
				Session["HoraFin"] = lblHoraFin.Text.Length == 0 ? "Sin Cerrar Turno" : lblHoraFin.Text;
				ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "open", "window.open('ReporteTareas.aspx', '_blank')", true);
			}
			else
			{
				lblErrorGeneral.Visible = true;
				lblErrorGeneral.Text = "Turno no iniciado";
			}
		}

		protected void btnAceptarCerrarTurno_Click(object sender, EventArgs e)
		{
			Site1 master = (Site1)Page.Master;
			master.AceptarCerrarTurno();
		}

		public void btnCancelarCerrarTurno_Click(object sender, EventArgs e)
		{
			divCerrarturno.Visible = false;
			lblAceptarCerrarTurno.Text = string.Empty;
		}

		public void cerrarturno()
		{
			divCerrarturno.Visible = true;
			lblAceptarCerrarTurno.Text = "¿Quieres cerrar Turno?";
		}

		public void NoIniciar()
		{
			divCerrarturno.Visible = true;
			lblAceptarCerrarTurno.Text = "No Puedes iniciar el mismo turno dos veces";
			lblAceptarCerrarTurno.Visible = true;
			btnAceptarCerrarTurno.Visible = false;
			btnCancelarCerrarTurno.Visible = false;
		}

		protected void DvgTareasNoCumplidas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(DvgTareasNoCumplidas, "Select$" + e.Row.RowIndex);
			}
		}
		public void llenarhora()
		{
			VM.ObtenerHora(Convert.ToDateTime(SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy")), SesionActual.uidUsuario, SesionActual.UidPeriodo.Value);
			if (VM.CIniciarTurno != null)
			{
				lblHoraInicio.Text = VM.CIniciarTurno.DtFechaHoraInicio.ToString("hh\\:mm tt");
				lblHoraFin.Text = VM.CIniciarTurno.DtFechaHoraFin.HasValue ? VM.CIniciarTurno.DtFechaHoraFin.Value.ToString("hh\\:mm tt") : "";
				if (lblHoraFin.Text.Length == 0)
				{
					lblEstadoDelTurno.Text = "Abierto";
				}
				else
				{
					lblEstadoDelTurno.Text = "Cerrado";
				}
			}
			else
			{
				lblHoraInicio.Text = "12:00 AM";
				lblHoraFin.Text = "12:00 AM";
				if (lblHoraInicio.Text == "12:00 AM" && lblHoraFin.Text == "12:00 AM")
				{
					lblEstadoDelTurno.Text = "No Creado";
				}
			}
		}

		public void validarfecha()
		{
			DateTime date = Convert.ToDateTime(dvgDepartamentos.SelectedDataKey.Values[1].ToString());
			VM.ObtenerDepartamento(new Guid(dvgDepartamentos.SelectedDataKey.Value.ToString()), SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);

			if (VM.CTareasNoCumplidas.DtFecha.ToString("dd/MM/yyyy") != SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy"))
			{
				Site1 master = (Site1)Page.Master;
				master.Noiniciarturno();
				lblError.Text = "No Puedes iniciar Turno con Fecha Diferente a hoy";
				panelAlert.Visible = true;
			}
			else
			{
				Site1 master = (Site1)Page.Master;
				master.siiniciarturno();
			}
		}
		public void requeridonocumplido()
		{
			lblError.Text = "Existen tareas sin estado, revisar antes de cerrar turno.";
			panelAlert.Visible = true;
		}
		public void requeridoSicumplido()
		{
			lblError.Text = "";
		}

		protected void tabRequeridas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = false;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			panelRequeridas.Visible = true;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "";
			activeRequeridas.AddCssClass("active");

			divTareasNoCumplidas.Visible = false;
			divTareasCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void btnCloseAlert_Click(object sender, EventArgs e)
		{
			panelAlert.Visible = false;
			lblError.Text = string.Empty;
		}

		protected void btnIniciarTurno_Click(object sender, EventArgs e)
		{
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				if (SesionActual.UidPeriodo == null)
				{
					Guid uidInicioTurno = new Guid(hfUidInicioTurno.Value.ToString());
					VM.ActualizarEstatusInicioTurnoEncargado(uidInicioTurno);
					ActualizarGrid();
					Site1 master = (Site1)Page.Master;
					master.UpdateNavbar();
				}
				else
				{
					Site1 master = (Site1)Page.Master;
					master.IniciarTurno();
				}
			}
			catch (Exception ex)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error: " + ex.Message;
			}
			finally { btnIniciarTurno.Disable(); }
		}
		protected void btnCerrarTurno_Click(object sender, EventArgs e)
		{
			btnCerrarTurno.Disable();
			Site1 master = (Site1)Page.Master;
			master.CerrarTurno();
		}
	}
}