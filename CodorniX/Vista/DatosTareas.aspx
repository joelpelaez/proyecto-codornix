﻿<%@ Page Language="C#" MasterPageFile="Site1.Master" AutoEventWireup="true" CodeBehind="DatosTareas.aspx.cs" Inherits="CodorniX.Vista.DatosTareas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6">
            <asp:PlaceHolder ID="PanelBusquedas" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center">
                        Buscar Tareas
                    </div>
                    <div class="pull-right">
                        <asp:Label ID="lblMensajeIzquierdo" runat="server" />
                        <asp:LinkButton ID="btnMostrar" OnClick="btnMostrar_Click" CssClass="btn btn-sm btn-default" runat="server">
                            <asp:Label ID="lblFiltros" class="glyphicon glyphicon-collapse-up" runat="server" />
                        </asp:LinkButton><asp:LinkButton ID="btnLimpiar" OnClick="btnLimpiar_Click" CssClass="btn btn-sm btn-default" runat="server">
                            <span class="glyphicon glyphicon-trash"></span>
                            Limpiar
                        </asp:LinkButton><asp:LinkButton ID="btnBuscar" OnClick="btnBuscar_Click" CssClass="btn btn-sm btn-default" runat="server">
                            <span class="glyphicon glyphicon-search"></span>
                            Buscar
                        </asp:LinkButton>
                    </div>

                    <div class="panel-body">
                        <asp:Panel ID="PanelFiltros" runat="server">

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <h6>Nombre</h6>
                                    <asp:TextBox CssClass="form-control" ID="FiltroNombre" runat="server" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Departamento</h6>
                                    <asp:ListBox ID="lblDepartamento" runat="server" SelectionMode="Multiple" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Unidad Medida</h6>
                                    <asp:ListBox ID="lblUnidad" runat="server" SelectionMode="Multiple" CssClass="form-control" />
                                </div>

                                <div class="col-md-4">
                                    <h6>Fecha Inicio</h6>
                                    <div class="input-group date extra">
                                        <asp:TextBox ID="filtrofechainicio1" CssClass="form-control" runat="server" />
                                        <span class="input-group-addon input-sm ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6>Fecha Inicio</h6>
                                    <div class="input-group date extra">
                                        <asp:TextBox ID="filtrofechainicio2" CssClass="form-control" runat="server" />
                                        <span class="input-group-addon input-sm ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h6>Encargado</h6>
                                    <asp:TextBox CssClass="form-control" ID="FiltroEncargado" runat="server" />
                                </div>
                            </div>

                        </asp:Panel>

                        <div style="padding-top: 25px">
                            <asp:GridView ID="DVGTareas" AllowPaging="true" PageSize="10" OnPageIndexChanging="DVGTareas_PageIndexChanging" OnSelectedIndexChanged="DVGTareas_SelectedIndexChanged" OnSorting="DVGTareas_Sorting" OnRowDataBound="DVGTareas_RowDataBound" AutoGenerateColumns="false" DataKeyNames="UidTarea" CssClass="table table-bordered table-condensed table-striped" AllowSorting="true" runat="server">
                                <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No hay Datos
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
                                    <asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
                                    <asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodicidad" SortExpression="Periodicidad" />
                                    <asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
                                    <asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:d}" HeaderText="FechaInicio" SortExpression="FechaInicio" />
                                    <asp:BoundField DataField="StrUsuario" HeaderText="Encargado" SortExpression="Encargado" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

            </asp:PlaceHolder>
        </div>


        <div class="col-md-6">
            <asp:PlaceHolder ID="PanelVisualizacion" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center">
                        Visualizacion de las tareas
                    </div>
                    <div class="panel-body">
                        <asp:PlaceHolder ID="Datos" runat="server">
                            <div class="row">
                                <asp:TextBox CssClass="hide" ID="txtUidTarea" runat="server" />
                                <asp:TextBox ID="txtUidPeriodicidad" CssClass="hide" runat="server" />
                                <asp:TextBox CssClass="hide" ID="txtUidAntecesor" runat="server" />
                                <div class="col-md-12">
                                    <h6>Nombre</h6>
                                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Fecha Inicio</h6>
                                    <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Fecha Fin</h6>
                                    <asp:TextBox ID="txtFechaFin" runat="server" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Tipo Tarea</h6>
                                    <asp:TextBox ID="txtTipoTarea" runat="server" CssClass="form-control" />
                                </div>

                                <div class="col-md-4">
                                    <h6>Estatus</h6>
                                    <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Foto</h6>
                                    <asp:TextBox ID="txtFoto" runat="server" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Antecesor</h6>
                                    <asp:TextBox ID="txtAntecesor" runat="server" CssClass="form-control" />
                                </div>
                                <div id="Departamentos" runat="server" class="col-md-4">
                                    <h6>Departamentos</h6>
                                    <asp:ListBox ID="lblDepartamentos" runat="server" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Tipo Medicion</h6>
                                    <asp:TextBox ID="txtTipoMedicion" runat="server" CssClass="form-control" />
                                </div>
                                <div id="divUnidadMedida" runat="server" class="col-md-4">
                                    <h6>Unidad Medida</h6>
                                    <asp:TextBox ID="txtunidadmedida" runat="server" CssClass="form-control" />
                                </div>
                                <div id="divgrid" runat="server" class="col-md-4">
                                    <h6>Opciones de la seleccion</h6>
                                    <asp:ListBox ID="lblOpciones" runat="server" CssClass="form-control" />
                                </div>
                                <div id="Hora" runat="server" class="col-md-4">
                                    <h6>Hora</h6>
                                    <div class="input-group clockpicker" data-placement="top" data-align="left" data-autoclose="true">
                                        <asp:TextBox ID="txtHora" runat="server" CssClass="form-control" />
                                        <span id="clock" runat="server" class="input-group-addon input-sm">
                                            <i class="glyphicon glyphicon-time"></i>
                                        </span>
                                    </div>
                                </div>
                                <div id="Tolerancia" runat="server" class="col-md-4">
                                    <h6>Tolerancia</h6>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtTolerancia" runat="server" CssClass="form-control" />
                                        <span id="min" runat="server" class="input-group-addon input-sm"><i>min</i> </span>
                                    </div>
                                </div>
                                <div id="divTipoFrecuencia" runat="server" class="col-md-4">
                                    <h6>Tipo Frecuencia</h6>
                                    <asp:TextBox ID="txtTipoFrecuencia" runat="server" CssClass="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <h6>Frecuencia</h6>
                                    <asp:TextBox ID="txtFrecuencia" runat="server" CssClass="form-control" />
                                </div>
                                <div id="divsemanal" runat="server" class="col-md-4">
                                    <h6>Departamentos</h6>
                                    <asp:ListBox ID="lblSemanal" runat="server" CssClass="form-control" />
                                </div>

                            </div>
                        </asp:PlaceHolder>

                        <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Tarea: 
                                    <asp:Label runat="server" ID="lblNombre"></asp:Label></h4>
                                </div>
                                <div class="col-md-12">
                                    <h5>Fecha: 
                                    <asp:Label runat="server" ID="lblFechainicio"></asp:Label></h5>
                                </div>
                                <div id="fechaFin" runat="server" class="col-md-12">
                                    <h5>Fecha Fin:
                                        <asp:Label runat="server" ID="lblFechaFin"></asp:Label></h5>
                                </div>
                                <div class="col-md-12">
                                    <h5>Nombre del Responsable: 
                                     <asp:Label runat="server" ID="lblEncargado"></asp:Label></h5>
                                </div>
                                <div id="antecesor" runat="server" class="col-md-4" style="margin-top: 25px">
                                    <h6>Antecesor: </h6>
                                    <asp:Label runat="server" ID="lblAntecesor"></asp:Label>
                                </div>
                                <div id="lhora" runat="server" class="col-md-4">
                                    <h5>Hora:
                                        <asp:Label runat="server" ID="lblhora"></asp:Label></h5>

                                </div>
                                <div id="ltolerancia" runat="server" class="col-md-4">
                                    <h5>Tolerancia:
                                        <asp:Label runat="server" ID="lblTolerancia"> </asp:Label>
                                        Minutos</h5>
                                </div>
                                <div class="col-md-12">
                                    <h5>Descripcion: 
                                     <asp:Label runat="server" ID="lblDescripcion"></asp:Label></h5>
                                </div>

                                <div class="col-md-4">
                                    <h5>Requerida: 
                                     <asp:Label runat="server" ID="lblTipoTarea"></asp:Label></h5>
                                </div>
                                <div class="col-md-5">
                                    <h5>Medicion: 
                                     <asp:Label runat="server" ID="lblTipoMedicion"></asp:Label></h5>
                                </div>

                                <div id="gridopciones" runat="server" class="col-md-3">
                                    <h5>Seleccion</h5>
                                    <asp:DropDownList ID="DdSeleccion" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                    <%--<asp:GridView  ID="dgvOpciones" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false" DataKeyNames="UidOpciones">
                                        <Columns>
                                            <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                            <asp:BoundField DataField="StrOpciones" HeaderText="Tipo" />
                                        </Columns>
                                    </asp:GridView> --%>
                                </div>
                                <div id="unidad" runat="server" class="col-md-3">
                                    <h5>Unidad Medida: 
                                     <asp:Label runat="server" ID="lblUnidadMedida"></asp:Label></h5>
                                </div>
                                <div class="col-md-4">
                                    <h6>Foto</h6>
                                    <asp:Image runat="server" CssClass="img img-thumbnail" ID="ImgUsuario" Width="200px" Height="160px" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
