﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class ListaTareas : System.Web.UI.Page
    {
        VMReporteTarea VM = new VMReporteTarea();

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;

            if (!Acceso.TieneAccesoAModulo("ReporteTareas", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Width = Unit.Percentage(100);
            ReportViewer1.Height = Unit.Percentage(100);
            ReportViewer1.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;

            if (!IsPostBack)
            {
                Guid periodo, uidUsuario;
                DateTime date;
                string horaInicio, horaFin;
                try
                {
                    uidUsuario = (Guid)Session["Usuario"];
                    periodo = (Guid)Session["Periodo"];
                    date = (DateTime)Session["Fecha"];
                    horaInicio = (string)Session["HoraInicio"];
                    horaFin = (string)Session["HoraFin"];
                }
                catch
                {
                    Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                    return;
                }

                VM.ObtenerPeriodo(periodo);
                VM.ObtenerTurno(VM.Periodo.UidTurno);
                VM.ObtenerDepartamento(VM.Periodo.UidDepartamento);
                VM.ObtenerSucursal(VM.Departamento.UidSucursal);
                VM.ObtenerEmpresa(VM.Sucursal.UidEmpresa);
                VM.ObtenerEncargado(uidUsuario);
                VM.ObtenerTareas(VM.Periodo.UidUsuario, periodo, date);

                ProcessingData(VM.Cumplimientos);

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ListaTareas2.rdlc");
                ReportParameter[] param = new ReportParameter[8];
                string nombreCompleto = VM.Encargado.STRNOMBRE + " " + VM.Encargado.STRAPELLIDOPATERNO + " " + VM.Encargado.STRAPELLIDOMATERNO;
                param[0] = new ReportParameter("NombreEncargado", nombreCompleto);
                param[1] = new ReportParameter("NombreDepartamento", VM.Departamento.StrNombre);
                param[2] = new ReportParameter("Fecha", DateTime.Today.ToString("dd/MM/yyyy"));
                param[3] = new ReportParameter("NombreEmpresa", VM.Empresa.StrNombreComercial);
                param[4] = new ReportParameter("NombreSucursal", VM.Sucursal.StrNombre);
                param[5] = new ReportParameter("Turno", VM.Turno.StrTurno);
                param[6] = new ReportParameter("HoraInicio", horaInicio);
                param[7] = new ReportParameter("HoraFin", horaFin);
                ReportViewer1.LocalReport.SetParameters(param);
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", VM.Cumplimientos));

                ReportViewer1.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
                ReportViewer1.LocalReport.Refresh();
            }
        }

        private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            Guid uid = new Guid(e.Parameters["ID"].Values[0]);

            string tipo = e.Parameters["TIPO"].Values[0];

            if (tipo == "Seleccionable")
            {
                VM.ObtenerOpciones(uid);
                e.DataSources.Add(new ReportDataSource("DataSet1", VM.Opciones));
            }
            else
            {
                // Crear un dataset falso para renderizar el subreporte.
                List<TareaOpcion> opciones = new List<TareaOpcion>();
                TareaOpcion opcion = new TareaOpcion();
                opciones.Add(opcion);
                e.DataSources.Add(new ReportDataSource("DataSet1", opciones));
            }
        }

        private List<Modelo.Cumplimiento> ProcessingData(List<Modelo.Cumplimiento> originalList)
        {
            List<Modelo.Cumplimiento> transformed = new List<Modelo.Cumplimiento>(originalList.Count);

            for (int i = 0; i < originalList.Count; i++)
            {
                var current = originalList[i];
                if (string.IsNullOrEmpty(current.StrArea))
                    originalList[i].StrArea = "General";
                
            }

            return transformed;
        }
    }
}