﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class InicioSupervisor : System.Web.UI.Page
	{
		private VMInicioSupervisor VM = new VMInicioSupervisor();
		private Sesion SesionActual => (Sesion)Session["Sesion"];

		private Guid UidTurnoSucursal
		{
			get => (Guid)(ViewState["UidTurnoSucursal"] ?? Guid.Empty);
			set => ViewState["UidTurnoSucursal"] = value;
		}

		private void UpdateList()
		{
			VM.ObtenerTurnoDeHoy(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, SesionActual.GetDateTime());
			dvgTurnos.DataSource = new List<TurnoSupervisor> { VM.TurnoSupervisor };
			dvgTurnos.DataBind();

			if (VM.TurnoSupervisor.StrEstadoTurno == "Abierto")
			{
				btnIniciarTurno.Disable();
				btnCerrarTurno.Enable();
				SesionActual.uidTurnoSupervisor = VM.TurnoSupervisor.UidTurnoSupervisor;
			}
			else
			{
				btnIniciarTurno.Enable();
				btnCerrarTurno.Disable();
				SesionActual.uidTurnoSupervisor = null;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("InicioSupervisor", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				UpdateList();
				btnCerrarTurno.Disable();
				btnIniciarTurno.Disable();
			}
		}

		protected void btnIniciarTurno_Click(object sender, EventArgs e)
		{
			Site1 master = (Site1)Page.Master;
			btnIniciarTurno.Disable();
			try
			{
				if (lblMessage.Visible == true) { lblMessage.Visible = false; }
				Guid uid = new Guid(dvgTurnos.SelectedDataKey.Value.ToString());

				if (uid == Guid.Empty)
				{
					VM.CrearTurno(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, SesionActual.GetDateTimeOffset());
				}
				else
				{
					VM.IniciarTurno(uid);
				}

				UpdateList();
				btnCerrarTurno.Enable();
				btnIniciarTurno.Disable();

				if (uid == Guid.Empty)
				{
					VM.ObtenerTurnoDeHoy(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, SesionActual.GetDateTime());
					uid = VM.TurnoSupervisor.UidTurnoSupervisor;
				}

				SesionActual.uidTurnoSupervisor = uid;

				master.UpdateNavbar();
			}
			catch (Exception)
			{
				lblMessage.Visible = true;
				lblMessage.Text = "Error al iniciar turno";
			}
		}

		protected void btnCerrarTurno_Click(object sender, EventArgs e)
		{
			btnCerrarTurno.Disable();
			try
			{
				if (lblMessage.Visible == true) { lblMessage.Visible = false; }
				Guid uid = new Guid(dvgTurnos.SelectedDataKey.Value.ToString());
				VM.CerrarTurno(uid);
				VM.ModificarFechaFin(uid, SesionActual.GetDateTimeOffset());

				UpdateList();
				btnIniciarTurno.Enable();
				btnCerrarTurno.Disable();

				SesionActual.uidTurnoSupervisor = null;
			}
			catch (Exception)
			{
				lblMessage.Visible = true;
				lblMessage.Text = "Error al cerrar turno";
			}
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			btnActualizar.Enabled = false;
			try
			{
				if (lblMessage.Visible == true) { lblMessage.Visible = false; }
				UpdateList();
				btnCerrarTurno.Disable();
				btnIniciarTurno.Disable();

				int pos = -1;
				if (ViewState["TurnoPreviousRow"] != null)
				{
					pos = (int)ViewState["TurnoPreviousRow"];
					GridViewRow previousRow = dvgTurnos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}

				ViewState["TurnoPreviousRow"] = null;

			}
			catch (Exception)
			{
				lblMessage.Text = "Error al obtener datos";
			}
			finally { btnActualizar.Enabled = true; }
		}

		protected void dvgTurnos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
				e.Row.Attributes["onclick"] =
					ClientScript.GetPostBackClientHyperlink(dvgTurnos, "Select$" + e.Row.RowIndex);
		}

		protected void dvgTurnos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (lblMessage.Visible == true) { lblMessage.Visible = false; }

				Guid uid = new Guid(dvgTurnos.SelectedDataKey.Value.ToString());
				VM.ObtenerTurno(uid);
				if (VM.TurnoSupervisor != null)
				{
					VM.ObtenerEstadoTurno(VM.TurnoSupervisor.UidEstadoTurno);

					if (VM.EstadoTurno.StrEstadoTurno == "Abierto")
					{
						btnIniciarTurno.Disable();
						btnCerrarTurno.Enable();
					}
					else
					{
						btnIniciarTurno.Enable();
						btnCerrarTurno.Disable();
					}

				}
				else
				{
					btnIniciarTurno.Enable();
					btnCerrarTurno.Disable();
				}
				int pos = -1;
				if (ViewState["TurnoPreviousRow"] != null)
				{
					pos = (int)ViewState["TurnoPreviousRow"];
					GridViewRow previousRow = dvgTurnos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}

				ViewState["TurnoPreviousRow"] = dvgTurnos.SelectedIndex;
				dvgTurnos.SelectedRow.AddCssClass("success");
			}
			catch (Exception)
			{
				lblMessage.Visible = true;
				lblMessage.Text = "Error al obtener datos";
			}
		}

		protected void btnReporteSupervisor_Click(object sender, EventArgs e)
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
				"window.open('ReporteSupervisor.aspx', '_blank')", true);
		}

		protected void btnResumenSupervision_Click(object sender, EventArgs e)
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
				"window.open('ResumenSupervision.aspx', '_blank')", true);
		}
	}
}