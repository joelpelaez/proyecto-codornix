﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using System.Data.SqlClient;
using CodorniX.Modelo;

namespace CodorniX.Vista
{
    public partial class Login : System.Web.UI.Page
    {
        VMLogin _CVMLogin = new VMLogin();
        private string _strperfil = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Sesion"] != null)
            {
                var SesionActual = (Sesion)Session["Sesion"];
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }
        }
        public bool loguear = false;

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Sesion sesion = new Sesion();
            if (txtUsuario.Text != string.Empty && txtPassword.Text != string.Empty)
            {
                Guid idusuario = _CVMLogin.IniciarSesion(txtUsuario.Text, txtPassword.Text);

                if (idusuario != Guid.Empty)
                {
					/* Obtener datos usuario */
                    Usuario usuario = new Usuario.Repository().Find(idusuario);

                    if (usuario.STRPASSWORD != txtPassword.Text)
                    {
                        lblMensaje.Text = "La contraseña es incorrecta";
                        return;
                    }

                    Status.Repository statusRepository = new Status.Repository();
                    Perfil.Repositorio perfilRepository = new Perfil.Repositorio();
                    Empresa.Repository empresaRepository = new Empresa.Repository();

					/* Obtener Estatus del Usuario */
                    Status status = statusRepository.Find(usuario.UidStatus);

					/* Validar si el usuario esta activo */
                    if (status.strStatus != "Activo")
                    {
                        lblMensaje.Text = "El usuario está inactivo";
                        return;
                    }

                    sesion.uidUsuario = idusuario;

					/* Obtener Empresas Pertenecientes al usuario */
                    List<UsuarioPerfilEmpresa> ep = new UsuarioPerfilEmpresa.Repository().FindAll(idusuario);
                    if (ep.Count > 0)
                    {
                        List<Guid> uidEmpresas = (from em in ep select em.UidEmpresa).ToList();
                        List<Guid> uidPerfiles = (from pf in ep select pf.UidPerfil).ToList();
                        sesion.uidEmpresasPerfiles = uidEmpresas;
                        sesion.uidEmpresaActual = uidEmpresas[0];
                        sesion.uidPerfilActual = uidPerfiles[0];
                        sesion.uidNivelAccesoActual = perfilRepository.CargarDatos(sesion.uidPerfilActual.Value).UidNivelAcceso;
                    }
                    else
                    {
						/* Obtener sucursales Pertenecientes al usuario */
                        List<UsuarioPerfilSucursal> sp = new UsuarioPerfilSucursal.Repository().FindAll(idusuario);
                        if (sp.Count > 0)
                        {
                            List<Guid> uidSucursales = (from su in sp select su.UidSucursal).ToList();
                            List<Guid> uidPerfiles = (from pf in sp select pf.UidPerfil).ToList();
                            sesion.uidSucursalesPerfiles = uidSucursales;
                            sesion.uidSucursalActual = uidSucursales[0];
                            sesion.uidEmpresaActual = new Sucursal.Repository().Find(uidSucursales[0]).UidEmpresa;
                            sesion.uidPerfilActual = uidPerfiles[0];
                        }
                        else
                        {
                            lblMensaje.Text = "El usuario no tiene empresa ni sucursal asignados";
                            return;
                        }
                    }

					/* Obtener datos Empresa */
                    Empresa empresa = empresaRepository.Find(sesion.uidEmpresaActual.Value);

                    if (empresa != null && empresa.UidStatus != Guid.Empty)
                    {
                        status = statusRepository.Find(empresa.UidStatus);

                        if (status.strStatus != "Activo")
                        {
                            lblMensaje.Text = "La empresa a la que pertenece se encuentra desactivada";
                            return;
                        }
                    }

                    Perfil perfil = perfilRepository.CargarDatos(sesion.uidPerfilActual.Value);
                    sesion.perfil = perfil.strPerfil;
                    if (sesion.perfil == "Supervisor")
                    {
						/* Obtener listado Id departamentos */
                        sesion.UidDepartamentos =
                            AsignacionSupervision.Repository.ObtenerDepartamentosSupervisados(usuario.UIDUSUARIO,
                                DateTime.Today);
                    }

                    _strperfil = Acceso.ObtenerAppWeb(sesion.uidPerfilActual.Value);
                    Session["Sesion"] = sesion;
                    
                    if (_strperfil == "Frontend")
                    {
                        if (!Acceso.TienePeriodo(usuario.UIDUSUARIO, sesion.uidSucursalActual.Value, DateTime.Now))
                        {
                            lblMensaje.Text = "No tiene ningun turno el día de hoy";
                            Session["Sesion"] = null;
                            return;
                        }

                        sesion.UidDepartamentos = Acceso.ObtenerDepartamentosSupervisor(usuario.UIDUSUARIO, DateTime.Today);
                        Session["Sesion"] = sesion;
                    }
                    Response.Redirect(Acceso.ObtenerHomePerfil(sesion.uidPerfilActual.Value), false);
                    return;
                }
                else
                {
                    lblMensaje.Text = "No estás registrado.";
                }
            }

        }
    }
}