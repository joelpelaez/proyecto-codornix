﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Cumplimiento.aspx.cs" Inherits="CodorniX.Vista.Cumplimiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Cumplimiento</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                Lista de Tareas del Día
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <div class="btn-group">
                        <asp:LinkButton runat="server" ID="btnMostrar" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click" Text="Mostrar" />
                        <asp:LinkButton runat="server" ID="btnLimpiar" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Limpiar
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnActualizar" CssClass="btn btn-sm btn-default" OnClick="btnActualizar_Click">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Buscar
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="panelBusqueda" runat="server">
                        <div class="row">
                            <div class="col-xs-6">
                                <h6>Tarea</h6>
                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Estado</h6>
                                <asp:ListBox ID="lbEstados" runat="server" CssClass="form-control" SelectionMode="Multiple" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Departamento</h6>
                                <asp:DropDownList ID="ddDepartamentos" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddDepartamentos_SelectedIndexChanged" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Área</h6>
                                <asp:DropDownList ID="ddAreas" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Tipo Tarea</h6>
                                <asp:DropDownList ID="ddTipo" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelResultados" runat="server">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:GridView runat="server" AllowPaging="true" AllowSorting="true" PageSize="10" ID="dgvTareasPendientes" CssClass="table table-bordered table-condensed table-striped" AutoGenerateColumns="false" DataKeyNames="UidTarea,UidDepartamento,UidArea,UidCumplimiento,UidPeriodo" OnSelectedIndexChanged="dgvTareasPendientes_SelectedIndexChanged" OnRowDataBound="dgvTareasPendientes_RowDataBound" OnPageIndexChanging="dgvTareasPendientes_PageIndexChanging" OnSorting="dgvTareasPendientes_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No tienes ninguna tarea asignada para el día de hoy.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField DataField="IntFolioTarea" HeaderText="Folio T." SortExpression="Folio" />
                                        <asp:BoundField DataField="IntFolio" HeaderText="Folio C." SortExpression="FolioCumpl" />
                                        <asp:BoundField DataField="StrTarea" HeaderText="Tarea" SortExpression="Tarea" />
                                        <asp:BoundField DataField="StrDepartamento" HeaderText="Depto." SortExpression="Departamento" />
                                        <asp:BoundField DataField="StrArea" HeaderText="Área" SortExpression="Area" />
                                        <asp:BoundField DataField="TmHora" HeaderText="Hora" SortExpression="Hora" HtmlEncode="false" DataFormatString="{0:hh\:mm}" />
                                        <asp:BoundField DataField="StrEstadoCumplimiento" HeaderText="Completado" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField DataField="StrTipoTarea" HeaderText="Requerido" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCompleto" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="text-center">
                        Tarea
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="btn-group">
                            <asp:LinkButton runat="server" ID="btnRealizar" CssClass="btn btn-sm btn-default" OnClick="btnRealizar_Click">
                                Realizar
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnDeshacer" CssClass="btn btn-sm btn-default" OnClick="btnDeshacer_Click">
                                Deshacer
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnOK" CssClass="btn btn-sm btn-success" Visible="false" OnClick="btnOK_Click">
                                <span class="glyphicon glyphicon-ok"></span>
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnCancelar" CssClass="btn btn-sm btn-danger" Visible="false" OnClick="btnCancelar_Click">
                                <span class="glyphicon glyphicon-remove"></span>
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-xs-6 text-right">
                        <div class="btn-group">
                            <asp:PlaceHolder ID="confirmarCancelar" runat="server" Visible="false">
                                <div class="btn-group">
                                    <asp:LinkButton runat="server" ID="btnOKCancelar" CssClass="btn btn-sm btn-success" OnClick="btnOKCancelar_Click">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnCancelarCancelar" CssClass="btn btn-sm btn-danger" OnClick="btnCancelarCancelar_Click">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </asp:LinkButton>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="confirmarPosponer" runat="server" Visible="false">
                                <div class="btn-group">
                                    <asp:LinkButton runat="server" ID="btnOKPosponer" CssClass="btn btn-sm btn-success" OnClick="btnOKPosponer_Click">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnCancelarPosponer" CssClass="btn btn-sm btn-danger" OnClick="btnCancelarPosponer_Click">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </asp:LinkButton>
                                </div>
                            </asp:PlaceHolder>
                            <asp:LinkButton runat="server" ID="btnCancelarTarea" CssClass="btn btn-sm btn-default" OnClick="btnCancelarTarea_Click">
                                Cancelar
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnPosponer" CssClass="btn btn-sm btn-default" OnClick="btnPosponer_Click">
                                Posponer
                            </asp:LinkButton>

                        </div>
                    </div>
                </div>
                <asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
                    <div class="alert alert-danger">
                        <asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">x</asp:LinkButton>
                        <strong>Error: </strong><asp:Label runat="server" ID="lblErrorTarea" />
                    </div>
                </asp:PlaceHolder>
                <div class="panel-body">
                    <asp:HiddenField runat="server" ID="fldUidTarea" />
                    <asp:HiddenField runat="server" ID="fldUidDepartamento" />
                    <asp:HiddenField runat="server" ID="fldUidArea" />
                    <asp:HiddenField runat="server" ID="fldUidCumplimiento" />
                    <asp:HiddenField runat="server" ID="fldUidPerido" />
                    <asp:HiddenField runat="server" ID="fldEditing" />
                    <div class="row">
                        <div class="col-xs-12">
                            <h6>Departamento:</h6>
                            <asp:TextBox runat="server" ID="lblDepto" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
                        </div>
                        <div class="col-xs-12">
                            <h6>Área:</h6>
                            <asp:TextBox runat="server" ID="lblArea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
                        </div>
                        <div class="col-xs-12">
                            <h6>Tarea:</h6>
                            <asp:TextBox runat="server" ID="lblTarea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
                        </div>
                        <div class="col-xs-6">
                            <h6>Tipo:</h6>
                            <asp:TextBox runat="server" ID="lblTipoTarea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
                        </div>
                        <div class="col-xs-6">
                            <h6>Periodicidad:</h6>
                            <asp:TextBox runat="server" ID="lblPeriodicidad" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
                        </div>
                    </div>
                    <asp:PlaceHolder runat="server" ID="cumplimientoCampos" Visible="true">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-inline">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:Label runat="server" ID="lblError" ForeColor="DarkRed" CssClass="has-error" />
                                        </div>
                                        <asp:PlaceHolder ID="panelUsuario" runat="server" Visible="false">
                                            <div class="col-xs-12">
                                                Realizado por: 
                                                <label class="radio-inline">
                                                    <asp:RadioButton runat="server" ID="rbEncargado" GroupName="rbgUsuario" Checked="false" />
                                                    Encargado
                                                </label>
                                                <label class="radio-inline">
                                                    <asp:RadioButton runat="server" ID="rbSupervisor" GroupName="rbgUsuario" Checked="false" />
                                                    Supervisor
                                                </label>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                    <asp:PlaceHolder runat="server" ID="frmSiNo">
                                        <label class="radio-inline">
                                            <asp:RadioButton runat="server" ID="rbYes" GroupName="rbgSiNo" />
                                            Sí
                                        </label>
                                        <label class="radio-inline">
                                            <asp:RadioButton runat="server" ID="rbNo" GroupName="rbgSiNo" />
                                            No
                                        </label>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder runat="server" ID="frmValue">
                                        <div class="form-group" id="valorUnico" runat="server">
                                            <div class="input-group">
                                                <asp:TextBox runat="server" ID="txtValor" CssClass="form-control" />
                                                <div class="input-group-addon">
                                                    <asp:Label runat="server" ID="lblUnidad" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder runat="server" ID="frmTwoValues">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                Desde:
                                            </div>
                                            <asp:TextBox runat="server" ID="txtValor1" CssClass="form-control" />
                                            <div class="input-group-addon">
                                                <asp:Label runat="server" ID="lblUnidad1" />
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                Hasta:
                                            </div>
                                            <asp:TextBox runat="server" ID="txtValor2" CssClass="form-control" />
                                            <div class="input-group-addon">
                                                <asp:Label runat="server" ID="lblUnidad2" />
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder runat="server" ID="frmOptions">
                                        <asp:DropDownList runat="server" ID="ddOpciones" CssClass="form-control" />
                                    </asp:PlaceHolder>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Observaciones:</label>
                                <asp:TextBox TextMode="MultiLine" ID="txtObservaciones" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="posponerCampos" Visible="false">
                        <div class="input-group date start-date">
                            <span class="input-group-addon input-sm">Fecha:
                            </span>
                            <asp:TextBox ID="txtFecha" CssClass="form-control" runat="server" />
                            <span class="input-group-addon input-sm ">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
    <script>
        //<![CDATA[
        var startDateReady = false;
        var endDateReady = false;

        function enableDatapicker() {
            if (!startDateReady) {
                $(".input-group.date.start-date").datepicker({
                    todayBtn: true,
                    clearBtn: true,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'es',
                }).on('changeDate', function (e) {
                    setEndDateLimit(e.format());
                });
            }
        }

        function setStartDateLimit(start, end) {
            $(".input-group.date.start-date").datepicker('remove');
            $(".input-group.date.start-date").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: start,
                endDate: end
            })
            startDateReady = true;
        }


        function pageLoad() {
            enableDatapicker();
        }
        //]]>
    </script>
</asp:Content>
