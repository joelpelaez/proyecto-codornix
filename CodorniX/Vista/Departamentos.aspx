﻿<%@ Page Title="" Language="C#" MasterPageFile="Site1.Master" AutoEventWireup="true" CodeBehind="Departamentos.aspx.cs" Inherits="CodorniX.Vista.Departamentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Departamentos y áreas</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">Departamentos y áreas</div>
                <div class="text-right">
                    <div class="btn-group">
                        <asp:LinkButton ID="btnMostrar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnMostrar_Click">
                            Mostrar
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnBorrar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnBorrar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Borrar
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnBuscar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnBuscar_Click">
                            <span class="glyphicon glyphicon-search"></span>
                            Buscar
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="panelBusqueda" runat="server">
                        <div class="row" style="padding-top: 10px;" id="Div1" runat="server">
                            <div class="col-md-6">
                                <asp:TextBox ID="busquedaNombre" runat="server" CssClass="form-control" placeholder="Nombre" />
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="busquedaDescripcion" runat="server" CssClass="form-control" placeholder="Descripción" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelResultados" runat="server">
                        <div class="row" style="padding-top: 10px;">
                            <div class="col-md-12">
                                <asp:GridView runat="server" PageSize="10" CssClass="table table-bordered table-responsive table-condensed" ID="dgvDepartamentos" DataKeyNames="UidDepartamento" AutoGenerateColumns="false" OnRowDataBound="dgvDepartamentos_RowDataBound" OnSelectedIndexChanged="dgvDepartamentos_SelectedIndexChanged" OnPageIndexChanging="dgvDepartamentos_PageIndexChanging" OnSorting="dgvDepartamentos_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No hay departamentos registrados
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                        <asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
                                        <asp:BoundField DataField="StrDescripcion" HeaderText="Descripción" SortExpression="Descripcion" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <asp:Label ID="title" runat="server" Text="Departamentos" />
                        </div>
                        <div class="text-left">
                            <div class="btn-group">
                                <asp:LinkButton ID="btnNuevoDatos" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnNuevoDatos_Click">
                                    <span class="glyphicon glyphicon-file"></span>
                                    Nuevo
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnEditarDatos" runat="server" CssClass="btn btn-default btn-sm disabled" OnClick="btnEditarDatos_Click">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Editar
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnOKDatos" runat="server" Visible="false" CssClass="btn btn-success btn-sm disabled" OnClick="btnOKDatos_Click">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnCancelarDatos" runat="server" Visible="false" CssClass="btn btn-danger btn-sm disabled" OnClick="btnCancelarDatos_Click">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row" style="color: red; padding-top: 10px;">
                                <div class="col-xs-12">
                                    <asp:Label ID="lblError" Text="" runat="server" />
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px;">
                                <div class="col-xs-12">
                                    <ul class="nav-tabs nav">
                                        <li id="tabDepartamentos" runat="server" class="active">
                                            <asp:LinkButton ID="btnDepartamentos" runat="server" Text="Departamentos" OnClick="btnDepartamentos_Click" />
                                        </li>
                                        <li id="tabAreas" runat="server">
                                            <asp:LinkButton ID="btnAreas" runat="server" Text="Áreas" OnClick="btnAreas_Click" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <asp:PlaceHolder ID="panelDepartamentos" runat="server" Visible="false">
                                <div class="paneles" id="panelDatos" runat="server">
                                    <div class="row">
                                        <asp:TextBox Visible="false" ID="uid" runat="server" CssClass="depto-image" />
                                        <div class="col-xs-12">
                                            <asp:Panel ID="frmGrpNombre" runat="server" CssClass="form-group">
                                                <h6>Nombre</h6>
                                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control disabled" placeholder="Nombre" />
                                            </asp:Panel>
                                        </div>
                                        <div class="col-md-4">
                                            <h6>Icono o foto</h6>
                                            <div class="square">
                                                <asp:Image ID="image" runat="server" CssClass="img img-responsive image-container" />
                                            </div>
                                            <asp:LinkButton CssClass="btn btn-sm btn-default btn-file padding disabled" ID="fuImagenButton" OnClientClick="openFileUpload(); return false;" runat="server">
                                                Subir archivo
                                            </asp:LinkButton>
                                            <asp:FileUpload ID="fuImagen" runat="server" CssClass="image-input hidden" accept="image/*" />
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <h6>Status</h6>
                                                    <asp:DropDownList ID="ddStatus" runat="server" CssClass="form-control disabled" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <h6>Posición</h6>
                                                    <asp:TextBox ID="txtPosicion" runat="server" CssClass="form-control disabled" />
                                                </div>
                                                <div class="col-xs-12">
                                                    <h6>Descripción</h6>
                                                    <asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control disabled" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="panelAreas" runat="server" Visible="false">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="btn-group">
                                            <asp:LinkButton ID="btnAreaNuevo" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnAreaNuevo_Click">
                                                <span class="glyphicon glyphicon-file"></span>
                                                Nuevo
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnAreaEditar" runat="server" CssClass="btn btn-default btn-sm disabled" OnClick="btnAreaEditar_Click">
                                                <span class="glyphicon glyphicon-edit"></span>
                                                Editar
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnAreaOK" runat="server" Visible="false" CssClass="btn btn-success btn-sm disabled" OnClick="btnAreaOK_Click">
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnAreaCancelar" runat="server" Visible="false" CssClass="btn btn-danger btn-sm disabled" OnClick="btnAreaCancelar_Click">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <asp:HiddenField ID="fldUidArea" runat="server" />
                                        <h6>Nombre</h6>
                                        <asp:TextBox runat="server" ID="txtAreaNombre" CssClass="form-control" />
                                    </div>
                                    <div class="col-xs-4">
                                        <h6>Status</h6>
                                        <asp:DropDownList ID="ddEstadoArea" runat="server" CssClass="form-control disabled" />
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 10px;">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" PageSize="10" CssClass="table table-bordered table-responsive table-condensed" ID="dgvAreas" DataKeyNames="UidArea" AutoGenerateColumns="false" OnRowDataBound="dgvAreas_RowDataBound" OnSelectedIndexChanged="dgvAreas_SelectedIndexChanged">
                                            <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                            <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                No hay áreas registradas
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                                <asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnOkDatos" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script>
        //<![CDATA[
        function openFileUpload() {
            console.log("Hola");
            $(".image-input").click();
            return false;
        }
        function pageLoad() {

            $(".image-input").change(function (e) {
                var files = e.target.files;
                if (FileReader && files) {
                    if (files[0] == null || !files[0].type.match("image.*") || files[0].size > (2 * 1024 * 1024)) {
                        if (files[0] != null) {
                            console.log(files[0].type);
                            console.log(files[0].size);
                        }
                        return;
                    }
                    var fr = new FileReader();
                    fr.onload = function () {
                        var e = $(".image-container").first();
                        e.attr('src', fr.result);
                    }
                    fr.readAsDataURL(files[0]);
                }
            });
        };
        //]]>
    </script>
    <style>
        .square {
            float: left;
            position: relative;
            width: 100%;
            padding-bottom: 100%; /* = width for a 1:1 aspect ratio */
            margin: 0;
            overflow: hidden;
            border: solid 1px #aaaaaa;
            border-radius: 5px;
        }

            .square img {
                position: absolute;
            }

        .padding {
            margin-top: 10px;
        }
    </style>
</asp:Content>
