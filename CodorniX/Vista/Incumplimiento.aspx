﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Incumplimiento.aspx.cs" Inherits="CodorniX.Vista.Incumplimiento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Tareas no Cumplidas</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-xs-6">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">
                    Tareas No Cumplidas
                </div>
                <div class="text-right">
                    <div class="btn-group">
                        <asp:LinkButton runat="server" ID="btnMostrar" Text="Mostrar" CssClass="btn btn-default btn-sm" OnClick="btnMostrar_Click" />
                        <asp:LinkButton runat="server" ID="btnLimpiar" Text="Limpiar" CssClass="btn btn-default btn-sm" OnClick="btnLimpiar_Click"/>
                        <asp:LinkButton runat="server" ID="btnBuscar" Text="Buscar" CssClass="btn btn-default btn-sm" OnClick="btnBuscar_Click"/>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:Placeholder runat="server" ID="panelBusqueda">
                        <div class="row">
                            <div class="col-xs-6">
                                <h6>Fecha Inicio</h6>
                                <div class="input-group date normal">
                                    <asp:TextBox ID="txtFechaInicio" CssClass="form-control" runat="server" />
                                    <span class="input-group-addon input-sm">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h6>Fecha Inicio</h6>
                                <div class="input-group date normal">
                                    <asp:TextBox ID="txtFechaFin" CssClass="form-control" runat="server" />
                                    <span class="input-group-addon input-sm">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <h6>Departamento</h6>
                                <asp:ListBox runat="server" ID="lbDepartamentos" CssClass="form-control" Rows="4" />
                            </div>
                        </div>
                    </asp:Placeholder>
                    <asp:PlaceHolder runat="server" ID="panelGridTareas">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:GridView runat="server" ID="dgvTareasIncompletas" DataKeyNames="UidTarea,UidDepartamento,UidArea,UidTurno,UidCumplimiento" AutoGenerateColumns="false" CssClass="table table-sm table-bordered table-striped" OnRowDataBound="dgvTareasIncompletas_RowDataBound" OnSelectedIndexChanged="dgvTareasIncompletas_SelectedIndexChanged" AllowPaging="true" OnPageIndexChanging="dgvTareasIncompletas_PageIndexChanging" PageSize="8">
                                    <EmptyDataTemplate>
                                        No hay tareas incompletas en el periodo
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" FooterStyle-CssClass="hidden" />
                                        <asp:BoundField DataField="StrTarea" HeaderText="Tarea" />
                                        <asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" />
                                        <asp:BoundField DataField="StrArea" HeaderText="Área" />
                                        <asp:BoundField DataField="StrTurno" HeaderText="Turno" />
                                    </Columns>
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">
                    Tareas No Realizadas
                </div>
                <div class="text-left">
                    <div class="btn-group">
                        <asp:LinkButton runat="server" ID="btnPosponer" CssClass="btn btn-default btn-sm" Text="Posponer" OnClick="btnPosponer_Click"/>
                        <asp:LinkButton runat="server" ID="btnCancelar" CssClass="btn btn-default btn-sm" Text="Cancelar" OnClick="btnCancelar_Click"/>
                        <asp:LinkButton runat="server" ID="btnOK" CssClass="btn btn-success btn-sm" OnClick="btnOK_Click" Visible="false">
                            <span class="glyphicon glyphicon-ok"></span>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnCancel" CssClass="btn btn-danger btn-sm" OnClick="btnCancel_Click" Visible="false">
                            <span class="glyphicon glyphicon-remove"></span>
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
                        <div class="alert alert-danger">
                            <asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">x</asp:LinkButton>
                            <strong>Error: </strong><asp:Label ID="lblErrorTarea" runat="server" />
                        </div>
                    </asp:PlaceHolder>

                    <asp:HiddenField runat="server" ID="fldUidTarea" />
                    <asp:HiddenField runat="server" ID="fldUidDepartamento" />
                    <asp:HiddenField runat="server" ID="fldUidArea" />
                    <asp:HiddenField runat="server" ID="fldUidCumplimiento" />
                    <asp:HiddenField runat="server" ID="fldEditing" />
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>
                                <asp:Label runat="server" ID="lblDepto" Text="(ninguna)" /></h3>
                        </div>
                        <div class="col-xs-12">
                            <h3>
                                <asp:Label runat="server" ID="lblArea" Text="(ninguna)" /></h3>
                        </div>
                        <div class="col-xs-12">
                            <h4>
                                <asp:Label runat="server" ID="lblTarea" Text="(ninguna)" /></h4>
                        </div>
                        <div class="col-xs-12">
                            <h5>
                                <asp:Label runat="server" ID="lblTipoTarea" Text="(ninguna)" /></h5>
                        </div>
                        <div class="col-xs-12">
                            <h5>
                                Fecha programada: <asp:Label runat="server" ID="lblFechaPrograda" Text="(ninguna)" /></h5>
                        </div>
                        <div class="col-xs-12">
                            <h5>
                                Fecha del próximo: <asp:Label runat="server" ID="lblFechaSiguiente" Text="(ninguna)" />
                            </h5>
                        </div>
                    </div>
                    <asp:PlaceHolder runat="server" ID="posponerCampos" Visible="false">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="input-group date extra">
                                    <span class="input-group-addon input-sm">Fecha:
                                    </span>
                                    <asp:TextBox ID="txtFecha" CssClass="form-control" runat="server" />
                                    <span class="input-group-addon input-sm ">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
    <script>
        //<![CDATA[
        var configured = false;
        var fechaInicio, fechaFin;

        function reenable() {
            if (configured == false)
                return;

            $(".input-group.date.extra").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: fechaInicio,
                endDate: fechaFin
            })
        }

        function enablePosponerDatepicker(start, end) {
            configured = true;
            fechaInicio = start;
            fechaFin = end;
            $(".input-group.date.extra").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: start,
                endDate: end
            })
        }
        function enableDatepicker() {
            $(".input-group.date.normal").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            })
        }
        function pageLoad() {
            enableDatepicker();
            reenable();
        }
        //]]>
    </script>
</asp:Content>
