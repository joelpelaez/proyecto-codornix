﻿<%@ Page Title="" Language="C#" MasterPageFile="Site1.Master" AutoEventWireup="true" CodeBehind="Asignacion.aspx.cs" Inherits="CodorniX.Vista.Asignacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Asignacion</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">Asiganciones</div>
                <div class="text-right">
                    <div class="btn-group">
                        <asp:LinkButton ID="btnMostrar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnMostrar_Click">
                            Mostrar
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnBorrar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnBorrar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Borrar
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnBuscar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnBuscar_Click">
                            <span class="glyphicon glyphicon-search"></span>
                            Buscar
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="panelBusqueda" runat="server">
                        <div class="row">
                            <asp:HiddenField ID="busquedaUidUsuario" runat="server" />
                            <div class="col-xs-12">
                                <h6>Nombre</h6>
                                <asp:TextBox ID="busquedaNombre" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-md-6">
                                <h6>Fecha Inicio</h6>
                                <div class="input-group date n-n">
                                    <asp:TextBox ID="busquedaFechaInicio" placeholder="Fecha Inicio" CssClass="form-control" runat="server" />
                                    <span class="input-group-addon input-sm">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h6>Fecha Fin</h6>
                                <div class="input-group date n-n">
                                    <asp:TextBox ID="busquedaFechaFin" placeholder="Fecha Fin" CssClass="form-control" runat="server" />
                                    <span class="input-group-addon input-sm">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h6>Turno</h6>
                                <asp:ListBox ID="lbTurno" runat="server" CssClass="form-control" SelectionMode="Multiple" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Departamentos</h6>
                                <asp:ListBox ID="lbDepartamentos" runat="server" CssClass="form-control" SelectionMode="Multiple" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelResultados" runat="server">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:GridView ID="dgvPeriodos" runat="server" PageSize="6" AllowSorting="true" AllowPaging="true" CssClass="table table-bordered table-responsive" OnPageIndexChanging="dgvPeriodos_PageIndexChanging" OnSorting="dgvPeriodos_Sorting" OnSelectedIndexChanged="dgvPeriodos_SelectedIndexChanged" OnRowDataBound="dgvPeriodos_RowDataBound" AutoGenerateColumns="false" DataKeyNames="UidPeriodo">
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No existen asignaciones que coincidan con la búsqueda.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                        <asp:BoundField DataField="StrNombreUsuario" HeaderText="Nombre del Encargado" SortExpression="NombreUsuario" />
                                        <asp:BoundField DataField="StrNombreDepto" HeaderText="Departamento" SortExpression="NombreDepto" />
                                        <asp:BoundField DataField="StrTurno" HeaderText="Turno" SortExpression="Turno" />
                                        <asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:d}" HtmlEncode="false" HeaderText="Fecha de inicio" SortExpression="FechaInicio" />
                                        <asp:BoundField DataField="DtFechaFin" DataFormatString="{0:d}" HtmlEncode="false" HeaderText="Fecha de finalización" SortExpression="FechaFin" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">Asiganaciones</div>
                <div class="text-left">
                    <div class="btn-group">
                        <asp:LinkButton ID="btnNuevo" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnNuevo_Click">
                            <span class="glyphicon glyphicon-file"></span>
                            Nuevo
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnEditar" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnEditar_Click">
                            <span class="glyphicon glyphicon-edit"></span>
                            Editar
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnOKDatos" runat="server" CssClass="btn btn-success btn-sm disabled" OnClick="btnOKDatos_Click">
                            <span class="glyphicon glyphicon-ok"></span>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnCancelarDatos" runat="server" CssClass="btn btn-danger btn-sm disabled" OnClick="btnCancelarDatos_Click">
                            <span class="glyphicon glyphicon-remove"></span>
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:TextBox ID="txtradio" runat="server" CssClass="hide" />
                    <asp:TextBox ID="txtUidTipoFrecuencia" CssClass="hide" runat="server" />
                    <asp:TextBox ID="txtUidPeriodicidad" CssClass="hide" runat="server" />
                    <asp:HiddenField ID="uidPeriodo" runat="server" />
                    <asp:HiddenField ID="uidPeriodoInactividad" runat="server" />
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:Label ID="lbMensaje" runat="server" />
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li runat="server" id="activoDatos" class="active">
                            <asp:LinkButton runat="server" ID="tabDatos" OnClick="tabDatos_Click">
                                Datos
                            </asp:LinkButton>
                        </li>
                        <li runat="server" id="activoInactividad">
                            <asp:LinkButton runat="server" ID="tabInactividad" OnClick="tabInactividad_Click">
                                Periodos de Inactividad
                            </asp:LinkButton>
                        </li>
                    </ul>
                    <asp:PlaceHolder runat="server" ID="panelDatos">
                        <asp:PlaceHolder ID="userFields" runat="server">
                            <div class="row">
                                <div class="col-xs-12 text-right">
                                    <asp:LinkButton ID="btnBuscarUsuario" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnBuscarUsuario_Click">
                                        Buscar Encargado
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div class="row">
                                <asp:HiddenField ID="uidUsuario" runat="server" />
                                <div class="col-xs-12">
                                    <asp:Panel ID="frmGrpEncargado" runat="server" CssClass="form-group">
                                        <h6>Nombre del Encargado</h6>
                                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
                                    </asp:Panel>
                                </div>
                                <div class="col-xs-6">
                                    <h6>Departamento</h6>
                                    <asp:DropDownList ID="ddDepartamentos" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddDepartamentos_SelectedIndexChanged" />
                                </div>
                                <div class="col-xs-6">
                                    <h6>Turno</h6>
                                    <asp:DropDownList ID="ddTurno" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddTurno_SelectedIndexChanged" />
                                </div>
                                <div class="col-md-6">
                                    <asp:Panel ID="frmGrpFechaInicio" runat="server" CssClass="form-group">
                                        <h6>Fecha Inicio</h6>
                                        <div class="input-group date start-date">
                                            <asp:TextBox ID="txtFechaInicio" placeholder="Fecha Inicio" CssClass="form-control" runat="server" />
                                            <span class="input-group-addon input-sm">
                                                <i class="glyphicon glyphicon-calendar"></i>
                                            </span>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="col-md-6">
                                    <h6>Feche de finalización</h6>
                                    <div class="input-group date end-date">
                                        <asp:TextBox ID="txtFechaFin" placeholder="Fecha Fin" CssClass="form-control" runat="server" />
                                        <span class="input-group-addon input-sm">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="userGrid" runat="server">
                            <asp:GridView ID="dgvUsuario" runat="server" AllowSorting="true" AllowPaging="true" CssClass="table table-bordered table-responsive" OnSorting="dgvUsuario_Sorting" OnPageIndexChanging="dgvUsuario_PageIndexChanging" OnSelectedIndexChanged="dgvUsuario_SelectedIndexChanged" OnRowDataBound="dgvUsuario_RowDataBound" DataKeyNames="UidUsuario" AutoGenerateColumns="false">
                                <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No existen encargados que coincidan con la búsqueda.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                    <asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
                                    <asp:BoundField DataField="StrApellidoPaterno" HeaderText="Apellido Paterno" SortExpression="ApellidoPaterno" />
                                    <asp:BoundField DataField="StrApellidoMaterno" HeaderText="Apellido Materno" SortExpression="ApellidoMaterno" />
                                </Columns>
                            </asp:GridView>
                        </asp:PlaceHolder>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="panelInactividad" Visible="false">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="btn-group">
                                    <asp:LinkButton runat="server" ID="btnAgregarInactividad" CssClass="btn btn-sm btn-default" OnClick="btnAgregarInactividad_Click" Enabled="false">
                                        Agregar
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnEditarInactividad" CssClass="btn btn-sm btn-default" OnClick="btnEditarInactividad_Click" Enabled="false">
                                        Editar
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnEliminarInactividad" CssClass="btn btn-sm btn-default" OnClick="btnEliminarInactividad_Click" Enabled="false">
                                        Eliminar
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnOKInactividad" CssClass="btn btn-sm btn-success" Visible="false" OnClick="btnOKInactividad_Click">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnCancelarInactividad" CssClass="btn btn-sm btn-danger" Visible="false" OnClick="btnCancelarInactividad_Click">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <asp:PlaceHolder runat="server" ID="panelGridInactividad">
                            <asp:GridView runat="server" ID="dgvPeriodosInactividad" AutoGenerateColumns="false" DataKeyNames="UidPeriodoInactividad" CssClass="table table-bordered table-condensed" OnRowDataBound="dgvPeriodosInactividad_RowDataBound" OnSelectedIndexChanged="dgvPeriodosInactividad_SelectedIndexChanged">
                                <EmptyDataTemplate>
                                    No hay ningun periodo de inactividad establecido.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                    <asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:d}" HtmlEncode="false" HeaderText="Fecha de inicio" SortExpression="FechaInicio" />
                                    <asp:BoundField DataField="DtFechaFin" DataFormatString="{0:d}" HtmlEncode="false" HeaderText="Fecha de finalización" SortExpression="FechaFin" />
                                    <asp:BoundField DataField="StrTipoInactividad" HeaderText="Tipo" SortExpression="Tipo" />
                                </Columns>
                            </asp:GridView>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="panelCamposInactividad" Visible="false">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <h6>Fecha Inicio</h6>
                                    <div class="input-group date start-inactivity">
                                        <asp:TextBox ID="txtFechaInactividadInicio" placeholder="Fecha Inicio" CssClass="form-control" runat="server" />
                                        <span class="input-group-addon input-sm">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <h6>Fecha Fin</h6>
                                    <div class="input-group date end-inactivity">
                                        <asp:TextBox ID="txtFechaInactividadFin" placeholder="Fecha Fin" CssClass="form-control" runat="server" />
                                        <span class="input-group-addon input-sm">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <h6>Tipo de Inactividad</h6>
                                    <asp:DropDownList runat="server" ID="ddTiposInactividad" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="row">
                                <h6></h6>
                                <div class="col-xs-12">
                                    <asp:TextBox runat="server" ID="txtNotas" TextMode="MultiLine" CssClass="form-control" Rows="4" />
                                </div>
                            </div>
                            <% // Importacion %>
                            <div class="row">

                                <div class="col-md-2" style="margin-top: 15px">
                                    <asp:PlaceHolder runat="server" ID="Frecuencia" Visible="true">
                                        <div class="row">
                                            <div class="col-xs-12">
                                            <label class="radio-inline">
                                                <asp:RadioButton ID="rdSinperiodicidad" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" Checked="true" />
                                                Sin Periodicidad</label>
                                        </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                            <label class="radio-inline">
                                                <asp:RadioButton ID="rdDiaro" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
                                                Diaria</label>
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                            <label class="radio-inline">

                                                <asp:RadioButton ID="rdSemanal" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
                                                Semanal</label>
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                            <label class="radio-inline">
                                                <asp:RadioButton ID="rdMensual" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
                                                Mensual</label>
                                                </div>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>

                                <div class=" col-md-10" style="margin-top: 15px">
                                    <asp:PlaceHolder runat="server" ID="PanelSemanal" Visible="false">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <asp:Label Text="Repetir Cada" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtFrecuenciaSemanal" runat="server"></asp:TextBox>

                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label Text="Semanas el:" runat="server"></asp:Label>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px;">
                                                <asp:Label runat="server"></asp:Label>
                                            </div>

                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox AutoPostBack="true" ID="CBLunes" runat="server" />Lunes</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="CBMartes" />Martes</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="CBMiercoles" />Miercoles</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="CBJueves" />Jueves</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="CBViernes" />Viernes</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="CBSabado" />Sabado</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="CBDomingo" />Domingo</label>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>

                                <div class="col-md-10" style="margin-top: 15px">
                                    <asp:PlaceHolder runat="server" ID="PanelDiario" Visible="false">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline">
                                                    <asp:RadioButton ID="rdCadaDiario" OnCheckedChanged="rdCadaDiario_CheckedChanged" GroupName="FrecuenciaDiaria" AutoPostBack="true" runat="server" />
                                                    Cada</label>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:TextBox CssClass="form-control" ID="txtcadaDiario" AutoPostBack="true" runat="server"></asp:TextBox>

                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label Text="Dias" runat="server"></asp:Label>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px;">
                                                <asp:Label runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                <label class="radio-inline">
                                                    <asp:RadioButton ID="rdtodoslosdias" OnCheckedChanged="rdCadaDiario_CheckedChanged" GroupName="FrecuenciaDiaria" AutoPostBack="true" runat="server" />
                                                    todos los dias de la semana</label>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>

                                <div class="col-md-10">
                                    <asp:PlaceHolder runat="server" ID="PanelMensual" Visible="false">
                                        <div class="row">

                                            <div class="col-md-2">
                                                <label class="radio-inline">
                                                    <asp:RadioButton OnCheckedChanged="rdElDiaMensual_CheckedChanged" GroupName="FrecuenciaMensual" AutoPostBack="true" ID="rdElDiaMensual" runat="server" />
                                                    El dia</label>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtDiasMes" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label Text="de Cada" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtCadames" runat="server"></asp:TextBox>
                                            </div>

                                            <div class="col-md-2">
                                                <asp:Label Text="Meses" runat="server"></asp:Label>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px;">
                                                <asp:Label runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-1">
                                                <label class="radio-inline">
                                                    <asp:RadioButton OnCheckedChanged="rdElDiaMensual_CheckedChanged" GroupName="FrecuenciaMensual" AutoPostBack="true" ID="rdElMeses" runat="server" />
                                                    El</label>
                                            </div>
                                            <div class="col-md-5">
                                                <asp:DropDownList ID="DdOrdinalMensual" CssClass="form-control" runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="col-md-5">
                                                <asp:DropDownList ID="DdDiasMensual" CssClass="form-control" runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px;">
                                                <asp:Label runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label Text="de cada" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtcadamensual" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:Label Text="Meses" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
    <script>
        //<![CDATA[
        var startDateReady = false;
        var endDateReady = false;

        function enableDatapicker() {
            if (!startDateReady) {
                $(".input-group.date.start-date").datepicker({
                    todayBtn: true,
                    clearBtn: true,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'es',
                }).on('changeDate', function (e) {
                    setEndDateLimit(e.format());
                });
            }
            if (!endDateReady) {
                $(".input-group.date.end-date").datepicker({
                    todayBtn: true,
                    clearBtn: true,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'es',
                });
            }
        }

        function setStartDateLimit(date) {
            $(".input-group.date.start-date").datepicker('remove');
            $(".input-group.date.start-date").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: date,
            }).on('changeDate', function (e) {
                setEndDateLimit(e.format());
            });
            setEndDateLimit(date);
            $(".input-group.date.start-date").datepicker('update', date);
            startDateReady = true;
        }

        function setEndDateLimit(date) {
            $(".input-group.date.end-date").datepicker('remove');
            $(".input-group.date.end-date").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: date,
            });
            $(".input-group.date.end-date").datepicker('update', date);
            endDateReady = true;
        }

        function pageLoad() {
            enableDatapicker();
            $(".input-group.date.n-n").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            })
            $(".input-group.date.start-inactivity").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            })
            $(".input-group.date.end-inactivity").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es'
            })
        }
        //]]>
    </script>
</asp:Content>
