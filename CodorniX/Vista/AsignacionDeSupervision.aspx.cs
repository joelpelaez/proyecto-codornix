﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class AsignacionDeSupervision : System.Web.UI.Page
    {
        private VMAsignacionSupervision VM = new VMAsignacionSupervision();

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }
        
        private bool ModoSupervisor = false;

        private List<AsignacionSupervision> AsignacionesSupervision
        {
            get
            {
                if (ViewState["AsignacionesSupervision"] == null)
                    ViewState["AsignacionesSupervision"] = new List<AsignacionSupervision>();

                return (List<AsignacionSupervision>)ViewState["AsignacionesSupervision"];
            }
            set { ViewState["AsignacionesSupervision"] = value; }
        }

        private List<Encargado> Encargados
        {
            get
            {
                if (ViewState["Usuarios"] == null)
                    ViewState["Usuarios"] = new List<Usuario>();

                return (List<Encargado>)ViewState["Usuario"];
            }
            set { ViewState["Usuario"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
            {
                return;
            }

            if (!SesionActual.uidSucursalActual.HasValue)
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!Acceso.TieneAccesoAModulo("Asignacion", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }
            
            if (SesionActual.perfil == "Supervisor")
            {

                // El supervisor no tiene acceso al cumplimiento si no ha tomado  algun turno para cumplir.
                if (SesionActual.UidDepartamentos.Count == 0)
                {
                    Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                    return;
                }

                ModoSupervisor = true;
            }

            if (!IsPostBack)
            {
                if (ModoSupervisor)
                    VM.ObtenerDepartamentosPorLista(SesionActual.UidDepartamentos);
                else
                    VM.ObtenerDepartamentos(SesionActual.uidSucursalActual.Value);

                lbDepartamentos.DataSource = VM.Departamentos;
                lbDepartamentos.DataValueField = "UidDepartamento";
                lbDepartamentos.DataTextField = "StrNombre";
                lbDepartamentos.DataBind();
                ddDepartamentos.DataSource = VM.Departamentos;
                ddDepartamentos.DataValueField = "UidDepartamento";
                ddDepartamentos.DataTextField = "StrNombre";
                ddDepartamentos.DataBind();

                VM.ObtenerTurnos();
                lbTurno.DataSource = VM.Turnos;
                lbTurno.DataValueField = "UidTurno";
                lbTurno.DataTextField = "StrTurno";
                lbTurno.DataBind();
                ddTurno.DataSource = VM.Turnos;
                ddTurno.DataValueField = "UidTurno";
                ddTurno.DataTextField = "StrTurno";
                ddTurno.DataBind();

                btnNuevo.Enable();
                btnEditar.Disable();
                btnOKDatos.Visible = false;
                btnCancelarDatos.Visible = false;

                txtNombre.Disable();
                btnBuscarUsuario.Disable();
                ddDepartamentos.Disable();
                ddTurno.Disable();
                txtFechaInicio.Disable();
                txtFechaFin.Disable();
            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            if (btnMostrar.Text.Contains("Mostrar"))
            {
                panelBusqueda.Visible = false;
                panelResultados.Visible = true;
                btnBorrar.Visible = false;
                btnBuscar.Visible = false;
                btnMostrar.Text = "Ocultar";
            }
            else
            {
                panelBusqueda.Visible = true;
                panelResultados.Visible = false;
                btnBorrar.Visible = true;
                btnBuscar.Visible = true;
                btnMostrar.Text = "Mostrar";
            }
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            busquedaNombre.Text = string.Empty;
            busquedaNombre.Enable();

            busquedaFechaInicio.Text = string.Empty;
            busquedaFechaFin.Text = string.Empty;
            lbDepartamentos.ClearSelection();
            lbTurno.ClearSelection();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            btnMostrar_Click(sender, e);

            string usuario = busquedaNombre.Text;

            string departamentos = "";
            int[] i = lbDepartamentos.GetSelectedIndices();
            foreach (int j in i)
            {
                string value = lbDepartamentos.Items[j].Value;
                if (departamentos.Count() == 0)
                    departamentos += value;
                else
                    departamentos += "," + value;
            }

            string turno = "";
            i = lbTurno.GetSelectedIndices();
            foreach (int j in i)
            {
                string value = lbDepartamentos.Items[j].Value;
                if (turno.Count() == 0)
                    turno += value;
                else
                    turno += "," + value;
            }

            DateTime? fechaInicio = null;
            if (!string.IsNullOrWhiteSpace(busquedaFechaInicio.Text))
            {
				fechaInicio = Convert.ToDateTime(DateTime.ParseExact(busquedaFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}

            DateTime? fechaFin = null;
            if (!string.IsNullOrWhiteSpace(busquedaFechaFin.Text))
            {
				fechaFin = Convert.ToDateTime(DateTime.ParseExact(busquedaFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}

            if (ModoSupervisor && departamentos == String.Empty)
            {
                foreach (var depto in SesionActual.UidDepartamentos)
                {
                    var value = depto.ToString();
                    if (!departamentos.Any())
                        departamentos += value;
                    else
                        departamentos += "," + value;
                }
            }

            VM.BuscarAsignaciones(fechaInicio, null, null, fechaFin, usuario, turno, departamentos, SesionActual.uidSucursalActual.Value);
            dgvPeriodos.DataSource = VM.AsignacionesSupervision;
            dgvPeriodos.DataBind();
            AsignacionesSupervision = VM.AsignacionesSupervision;
        }


        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            uidPeriodo.Value = string.Empty;
            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.Text = "Buscar Encargado";
            btnBuscarUsuario.Enable();
            uidUsuario.Value = string.Empty;
            txtNombre.Enable();
            txtNombre.Text = string.Empty;

            txtFechaInicio.Text = string.Empty;
            txtFechaInicio.Enable();
            txtFechaFin.Text = string.Empty;
            txtFechaFin.Enable();
            ddTurno.SelectedIndex = 0;
            ddTurno.Enable();
            ddDepartamentos.SelectedIndex = 0;
            ddDepartamentos.Enable();

            btnNuevo.Disable();
            btnEditar.Disable();
            btnOKDatos.Visible = true;
            btnOKDatos.Enable();
            btnCancelarDatos.Visible = true;
            btnCancelarDatos.Enable();
            btnBuscarUsuario.Text = "Buscar Encargado";

            UpdateStartDate();
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            userFields.Visible = true;
            userGrid.Visible = false;

            txtFechaFin.Enable();

            btnNuevo.Disable();
            btnEditar.Disable();
            btnOKDatos.Visible = true;
            btnOKDatos.Enable();
            btnCancelarDatos.Visible = true;
            btnCancelarDatos.Enable();
            btnBuscarUsuario.Text = "Seleccionar otro";
            //UpdateEndDate();
        }

        protected void btnOKDatos_Click(object sender, EventArgs e)
        {
            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.Text = "Buscar Encargado";

            if (string.IsNullOrWhiteSpace(uidUsuario.Value))
            {
                lbMensaje.Text = "Debe seleccionar un encargado para el area";
                btnBuscarUsuario.AddCssClass("btn-primary").RemoveCssClass("btn-default");
                btnBuscarUsuario.Focus();
                frmGrpEncargado.AddCssClass("has-error");
                return;
            }
            btnBuscarUsuario.RemoveCssClass("btn-primary").AddCssClass("btn-default");
            frmGrpEncargado.RemoveCssClass("has-error");
            Guid uidUsu = new Guid(uidUsuario.Value);

            if (string.IsNullOrWhiteSpace(txtFechaInicio.Text))
            {
                lbMensaje.Text = "Debe seleccionar una fecha de inicio válida";
                txtFechaInicio.Focus();
                frmGrpFechaInicio.AddCssClass("has-error");
                return;
            }
            frmGrpFechaInicio.RemoveCssClass("has-error");

            DateTime fechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)); ;

            DateTime? fechaFin = null;
            if (!string.IsNullOrWhiteSpace(txtFechaFin.Text))
            {
                fechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)); ;
            }

            Guid uidTurno = new Guid(ddTurno.SelectedValue);
            Guid uidDepartamento = new Guid(ddDepartamentos.SelectedValue);

            AsignacionSupervision AsignacionSupervision = new AsignacionSupervision()
            {
                UidUsuario = uidUsu,
                DtFechaInicio = fechaInicio,
                DtFechaFin = fechaFin,
                UidTurno = uidTurno,
                UidDepartamento = uidDepartamento,
            };

            AsignacionSupervision.ReturnCode code = VM.GuardarAsignacion(AsignacionSupervision);
            switch (code)
            {
                case AsignacionSupervision.ReturnCode.Success:
                    lbMensaje.Text = "Se ha asignado el departamento exitosamente.";
                    break;
                case AsignacionSupervision.ReturnCode.SuccessWithChanges:
                    lbMensaje.Text = "Se ha asignado el departamento, la asignación anterior ha sido modificada.";
                    break;
                case AsignacionSupervision.ReturnCode.SuccessUpdate:
                    lbMensaje.Text = "Se ha actualizado la asignación.";
                    break;
                case AsignacionSupervision.ReturnCode.FailureSame:
                    lbMensaje.Text = "No se ha podido insertar, existe una asignación similar.";
                    break;
                case AsignacionSupervision.ReturnCode.FailureMax:
                    lbMensaje.Text = "No se ha podido asignar el departamento al supervisor, no tiene espacio para manejar otra.";
                    break;
                case AsignacionSupervision.ReturnCode.Undefined:
                    lbMensaje.Text = "Error al procesar la solicitud, intentelo nuevamente.";
                    break;
                default:
                    break;
            }

            userFields.Visible = true;
            userGrid.Visible = false;
            uidUsuario.Value = string.Empty;
            txtNombre.Disable();
            txtNombre.Text = string.Empty;

            txtFechaInicio.Text = string.Empty;
            txtFechaInicio.Disable();
            txtFechaFin.Text = string.Empty;
            txtFechaFin.Disable();
            ddTurno.SelectedIndex = 0;
            ddTurno.Disable();
            ddDepartamentos.SelectedIndex = 0;
            ddDepartamentos.Disable();

            btnNuevo.Enable();
            btnOKDatos.Visible = false;
            btnOKDatos.Disable();
            btnCancelarDatos.Visible = false;
            btnCancelarDatos.Disable();

            btnMostrar.Text = "Mostrar";
            btnBuscar_Click(sender, e);
        }

        protected void btnCancelarDatos_Click(object sender, EventArgs e)
        {
            userFields.Visible = true;
            userGrid.Visible = false;

            if (!string.IsNullOrWhiteSpace(uidPeriodo.Value))
            {
                VM.ObtenerAsignacion(new Guid(uidPeriodo.Value));
                VM.ObtenerUsuario(VM.AsignacionSupervision.UidUsuario);

                uidUsuario.Value = VM.AsignacionSupervision.UidUsuario.ToString();
                txtNombre.Text = VM.Usuario.STRNOMBRE;
                ddDepartamentos.SelectedValue = VM.AsignacionSupervision.UidDepartamento.ToString();
                ddTurno.SelectedValue = VM.AsignacionSupervision.UidTurno.ToString();
                txtFechaInicio.Text = VM.AsignacionSupervision.DtFechaInicio.ToString("dd/MM/yyyy");
                txtFechaFin.Text = VM.AsignacionSupervision.DtFechaFin?.ToString("dd/MM/yyyy");

                btnEditar.Enable();
                btnBuscarUsuario.Text = "Seleccionar otro";
            }
            else
            {
                uidUsuario.Value = string.Empty;
                txtNombre.Text = string.Empty;

                txtFechaInicio.Text = string.Empty;
                txtFechaFin.Text = string.Empty;
                ddTurno.SelectedIndex = 0;
                ddDepartamentos.SelectedIndex = 0;

                btnBuscarUsuario.Text = "Buscar Encargado";
            }
            lbMensaje.Text = string.Empty;
            txtNombre.Disable();
            txtFechaInicio.Disable();
            txtFechaFin.Disable();
            ddTurno.Disable();
            ddDepartamentos.Disable();
            btnNuevo.Enable();
            btnOKDatos.Visible = false;
            btnOKDatos.Disable();
            btnCancelarDatos.Visible = false;
            btnCancelarDatos.Disable();
            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.RemoveCssClass("btn-primary").AddCssClass("btn-default");
            frmGrpEncargado.RemoveCssClass("has-error");
            frmGrpFechaInicio.RemoveCssClass("has-error");
        }

        protected void btnBuscarUsuario_Click(object sender, EventArgs e)
        {
            if (btnBuscarUsuario.Text.Contains("Buscar Encargado"))
            {
                string nombre = txtNombre.Text;

                VM.BuscarUsuario(nombre, SesionActual.uidSucursalActual.Value);

                if (VM.Encargados.Count == 0)
                {
                    // FIXME: add message
                    return;
                }

                Encargados = VM.Encargados;
                dgvUsuario.DataSource = VM.Encargados;
                dgvUsuario.DataBind();

                userFields.Visible = false;
                userGrid.Visible = true;
            }
            else
            {
                uidUsuario.Value = string.Empty;
                txtNombre.Text = string.Empty;
                txtNombre.Enable();
                btnBuscarUsuario.Text = "Buscar Encargado";
            }
        }

        protected void dgvUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            VM.ObtenerUsuario(new Guid(dgvUsuario.SelectedDataKey.Value.ToString()));
            uidUsuario.Value = VM.Usuario.UIDUSUARIO.ToString();
            txtNombre.Text = VM.Usuario.STRNOMBRE + ' ' + VM.Usuario.STRAPELLIDOPATERNO;
            txtNombre.Disable();

            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.Text = "Seleccionar otro";
            UpdateStartDate();
        }

        protected void dgvUsuario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvUsuario, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvPeriodos_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnCancelarDatos_Click(sender, e);

            Guid uidPeriodo = new Guid(dgvPeriodos.SelectedDataKey.Value.ToString());
            VM.ObtenerAsignacion(uidPeriodo);
            VM.ObtenerUsuario(VM.AsignacionSupervision.UidUsuario);

            this.uidPeriodo.Value = VM.AsignacionSupervision.UidAsignacion.ToString();
            uidUsuario.Value = VM.AsignacionSupervision.UidUsuario.ToString();
            txtNombre.Text = VM.Usuario.STRNOMBRE;
            ddDepartamentos.SelectedValue = VM.AsignacionSupervision.UidDepartamento.ToString();
            ddTurno.SelectedValue = VM.AsignacionSupervision.UidTurno.ToString();
            txtFechaInicio.Text = VM.AsignacionSupervision.DtFechaInicio.ToString("dd/MM/yyyy");
            txtFechaFin.Text = VM.AsignacionSupervision.DtFechaFin?.ToString("dd/MM/yyyy");

            btnEditar.Enable();

            int pos = -1;
            if (ViewState["AsignacionesSupervisionPreviousRow"] != null)
            {
                pos = (int)ViewState["AsignacionesSupervisionPreviousRow"];
                GridViewRow previousRow = dgvPeriodos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["AsignacionesSupervisionPreviousRow"] = dgvPeriodos.SelectedIndex;
            dgvPeriodos.SelectedRow.AddCssClass("success");
        }

        protected void dgvPeriodos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvPeriodos, "Select$" + e.Row.RowIndex);
            }
        }

        protected void ddDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateStartDate();
        }

        protected void ddTurno_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateStartDate();
        }

        private void UpdateStartDate()
        {
            DateTime newStart;
            Guid departamento = new Guid(ddDepartamentos.SelectedValue);
            Guid turno = new Guid(ddTurno.SelectedValue);
            VM.ObtenerUltimaAsignacion(departamento, turno);
            if (VM.AsignacionSupervision != null && VM.AsignacionSupervision.DtFechaFin.HasValue)
                newStart = VM.AsignacionSupervision.DtFechaFin.Value;
            else
                newStart = DateTime.Today;
            string script = "setStartDateLimit('" + newStart.ToString("dd/MM/yyyy") + "')";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateStartDate", script, true);
        }

        private void UpdateEndDate()
        {
            DateTime newStart = DateTime.Today;
            if (!string.IsNullOrWhiteSpace(uidPeriodo.Value))
            {
                Guid AsignacionSupervision = new Guid(uidPeriodo.Value);
                VM.ObtenerAsignacion(AsignacionSupervision);
                if (VM.AsignacionSupervision != null && VM.AsignacionSupervision.DtFechaFin.HasValue)
                    newStart = VM.AsignacionSupervision.DtFechaFin.Value.AddDays(1);
            }
            string script = "setEndDateLimit('" + newStart.ToString("dd/MM/yyyy") + "')";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateEndDate", script, true);
        }

        private void SortAsignacionSupervision(string SortExpression, SortDirection SortDirection, bool same = false)
        {
            if (SortExpression == (string)ViewState["SortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                SortDirection =
                    ((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }

            if (SortExpression == "NombreUsuario")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderBy(x => x.StrNombreUsuario).ToList();
                }
                else
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderByDescending(x => x.StrNombreUsuario).ToList();
                }
            }
            else if (SortExpression == "NombreDepto")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderBy(x => x.StrNombreDepto).ToList();
                }
                else
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderByDescending(x => x.StrNombreDepto).ToList();
                }
            }
            else if (SortExpression == "Turno")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderBy(x => x.StrTurno).ToList();
                }
                else
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderByDescending(x => x.StrTurno).ToList();
                }
            }
            else if (SortExpression == "FechaInicio")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderBy(x => x.DtFechaInicio).ToList();
                }
                else
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderByDescending(x => x.DtFechaInicio).ToList();
                }
            }
            else
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderBy(x => x.DtFechaFin).ToList();
                }
                else
                {
                    AsignacionesSupervision = AsignacionesSupervision.OrderByDescending(x => x.DtFechaFin).ToList();
                }
            }
            dgvPeriodos.DataSource = AsignacionesSupervision;
            ViewState["SortColumn"] = SortExpression;
            ViewState["SortColumnDirection"] = SortDirection;
        }

        protected void dgvPeriodos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["AsignacionesSupervisionPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["SortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortAsignacionSupervision(SortExpression, SortDirection, true);
            }
            else
            {
                dgvPeriodos.DataSource = AsignacionesSupervision;
            }
            dgvPeriodos.PageIndex = e.NewPageIndex;
            dgvPeriodos.DataBind();
        }

        protected void dgvPeriodos_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["AsignacionesSupervisionPreviousRow"] = null;
            SortAsignacionSupervision(e.SortExpression, e.SortDirection);
            dgvPeriodos.DataBind();
        }

        private void SortEncargado(string SortExpression, SortDirection SortDirection, bool same = false)
        {
            if (SortExpression == (string)ViewState["EncargadoSortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                SortDirection =
                    ((SortDirection)ViewState["EncargadoSortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }

            if (SortExpression == "Nombre")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    Encargados = Encargados.OrderBy(x => x.STRNOMBRE).ToList();
                }
                else
                {
                    Encargados = Encargados.OrderByDescending(x => x.STRNOMBRE).ToList();
                }
            }
            else if (SortExpression == "ApellidoPaterno")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    Encargados = Encargados.OrderBy(x => x.STRAPELLIDOPATERNO).ToList();
                }
                else
                {
                    Encargados = Encargados.OrderByDescending(x => x.STRAPELLIDOPATERNO).ToList();
                }
            }
            else if (SortExpression == "ApellidoMaterno")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    Encargados = Encargados.OrderBy(x => x.STRAPELLIDOMATERNO).ToList();
                }
                else
                {
                    Encargados = Encargados.OrderByDescending(x => x.STRAPELLIDOMATERNO).ToList();
                }
            }
            dgvUsuario.DataSource = Encargados;
            ViewState["EncargadoSortColumn"] = SortExpression;
            ViewState["EncargadoSortColumnDirection"] = SortDirection;
        }

        protected void dgvUsuario_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["EncargadoPreviousRow"] = null;
            SortEncargado(e.SortExpression, e.SortDirection);
            dgvUsuario.DataBind();
        }

        protected void dgvUsuario_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["EncargadoPreviousRow"] = null;
            if (ViewState["EncargadoSortColumn"] != null && ViewState["EncargadoSortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["EncargadoSortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["EncargadoSortColumnDirection"];
                SortEncargado(SortExpression, SortDirection, true);
            }
            else
            {
                dgvUsuario.DataSource = AsignacionesSupervision;
            }
            dgvUsuario.PageIndex = e.NewPageIndex;
            dgvUsuario.DataBind();
        }
    }
}