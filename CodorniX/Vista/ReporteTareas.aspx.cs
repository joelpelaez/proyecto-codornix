﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class ReporteTareas : System.Web.UI.Page
    {
        VMReporteTarea VM = new VMReporteTarea();

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;

            if (!Acceso.TieneAccesoAModulo("ReporteTareas", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!IsPostBack)
            {
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Width = Unit.Percentage(100);
                ReportViewer1.Height = Unit.Percentage(100);
                Guid periodo;
                DateTime date;
                string horaInicio, horaFin;
                try
                {
                    periodo = (Guid)Session["Periodo"];
                    date = (DateTime)Session["Fecha"];
                    horaInicio = (string)Session["HoraInicio"];
                    horaFin = (string)Session["HoraFin"];
                }
                catch
                {
                    Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                    return;
                }

                VM.ObtenerPeriodo(periodo);
                VM.ObtenerTurno(VM.Periodo.UidTurno);
                VM.ObtenerDepartamento(VM.Periodo.UidDepartamento);
                VM.ObtenerSucursal(VM.Departamento.UidSucursal);
                VM.ObtenerEmpresa(VM.Sucursal.UidEmpresa);
                VM.ObtenerEncargado(SesionActual.uidUsuario);
                VM.ObtenerTareas(SesionActual.uidUsuario, periodo, date);

                ProcessingData(VM.Cumplimientos);

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReporteTareas2.rdlc");
                ReportParameter[] param = new ReportParameter[8];
                string nombreCompleto = VM.Encargado.STRNOMBRE + " " + VM.Encargado.STRAPELLIDOPATERNO + " " + VM.Encargado.STRAPELLIDOMATERNO;
                param[0] = new ReportParameter("NombreEncargado", nombreCompleto);
                param[1] = new ReportParameter("NombreDepartamento", VM.Departamento.StrNombre);
                param[2] = new ReportParameter("Fecha", DateTime.Today.ToString("dd/MM/yyyy"));
                param[3] = new ReportParameter("NombreEmpresa", VM.Empresa.StrNombreComercial);
                param[4] = new ReportParameter("NombreSucursal", VM.Sucursal.StrNombre);
                param[5] = new ReportParameter("Turno", VM.Turno.StrTurno);
                param[6] = new ReportParameter("HoraInicio", horaInicio);
                param[7] = new ReportParameter("HoraFin", horaFin);
                ReportViewer1.LocalReport.SetParameters(param);
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", VM.Cumplimientos));
                ReportViewer1.LocalReport.Refresh();
            }
        }

        private List<Modelo.Cumplimiento> ProcessingData(List<Modelo.Cumplimiento> originalList)
        {
            List<Modelo.Cumplimiento> transformed = new List<Modelo.Cumplimiento>(originalList.Count);

            for (int i = 0; i < originalList.Count; i++)
            {
                var current = originalList[i];
                if (string.IsNullOrEmpty(current.StrArea))
                    originalList[i].StrArea = "General";

                if (current.StrEstadoCumplimiento != "Completo")
                    continue;

                switch (current.StrTipoMedicion)
                {
                    case "Verdadero/Falso":
                        originalList[i].StrValor = current.BlValor.Value ? "Sí" : "No";
                        break;
                    case "Numerico":
                        originalList[i].StrValor = current.DcValor1.Value.ToString() + " " + current.StrTipoUnidad;
                        break;
                    case "Seleccionable":
                        originalList[i].StrValor = current.StrOpcion;
                        break;
                    case "Sin medición":
                        originalList[i].StrValor = "N/A";
                        break;
                }
            }
            
            return transformed;
        }
    }
}