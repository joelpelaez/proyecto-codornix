﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using CodorniX.Modelo;
using System.Globalization;

namespace CodorniX.Vista
{
	public partial class Tareas : System.Web.UI.Page
	{
		#region Propiedades

		VMTareas VM = new VMTareas();
		VMPeriodicidad VMP = new VMPeriodicidad();
		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}
		private List<TareaOpcion> OpcionRemoved
		{
			get
			{
				if (ViewState["OpcionRemoved"] == null)
					ViewState["OpcionRemoved"] = new List<TareaOpcion>();

				return (List<TareaOpcion>)ViewState["OpcionRemoved"];
			}
		}
		private bool EditingMode
		{
			get
			{
				if (ViewState["EditingMode"] == null)
					return false;

				return (bool)ViewState["EditingMode"];
			}
			set
			{
				ViewState["EditingMode"] = value;
			}
		}
		private void radiosgeneralesenable()
		{
			rdDiaro.Enable();
			rdSemanal.Enable();
			rdMensual.Enable();
			rdAnual.Enable();
			rdSinperiodicidad.Enable();

		}

		private void RadiosChechek()
		{
			rdDiaro.Checked = false;
			rdSemanal.Checked = false;
			rdMensual.Checked = false;
			rdAnual.Checked = false;
			rdSinperiodicidad.Checked = false;
		}

		private void radiosgeneralesdisables()
		{
			rdDiaro.Disable();
			rdSemanal.Disable();
			rdMensual.Disable();
			rdAnual.Disable();
			rdSinperiodicidad.Disable();
		}

		private void Radiosadentroenable()
		{
			rdCadaDiario.Checked = false;
			rdtodoslosdias.Checked = false;
			rdElDiaMensual.Checked = false;
			rdElMeses.Checked = false;
			rdElDelAño.Checked = false;
			rdElDelAño2.Checked = false;

			rdCadaDiario.Enable();
			rdtodoslosdias.Enable();
			rdElDiaMensual.Enable();
			rdElMeses.Enable();
			rdElDelAño.Enable();
			rdElDelAño2.Enable();

			CBLunes.Checked = false;
			CBMartes.Checked = false;
			CBMiercoles.Checked = false;
			CBJueves.Checked = false;
			CBViernes.Checked = false;
			CBSabado.Checked = false;
			CBDomingo.Checked = false;

			CBLunes.Enable();
			CBMartes.Enable();
			CBMiercoles.Enable();
			CBJueves.Enable();
			CBViernes.Enable();
			CBSabado.Enable();
			CBDomingo.Enable();

		}

		private void RadiosdentroDisable()
		{
			rdCadaDiario.Disable();
			rdtodoslosdias.Disable();
			rdElDiaMensual.Disable();
			rdElMeses.Disable();
			rdElDelAño.Disable();
			rdElDelAño2.Disable();

			CBLunes.Disable();
			CBMartes.Disable();
			CBMiercoles.Disable();
			CBJueves.Disable();
			CBViernes.Disable();
			CBSabado.Disable();
			CBDomingo.Disable();

		}
		private void txtdisable()
		{
			txtFrecuenciaAnual.Disable();
			txtcadaDiario.Disable();
			txtDiasMes.Disable();
			txtCadames.Disable();
			txtcadamensual.Disable();
			txtFrecuenciaSemanal.Disable();
			txtDiasmesaño.Disable();

			DdMesAño1.Disable();
			DdMesAño2.Disable();
			DdDiasMensual.Disable();
			DdDiasAño.Disable();
			DdOrdinalAño.Disable();
			DdOrdinalMensual.Disable();
		}

		private void txtEnable()
		{
			txtIntDiasMes.Text = string.Empty;
			txtintDiasSemanas.Text = string.Empty;
			txtintNumero.Text = string.Empty;

			txtcadaDiario.Text = string.Empty;
			txtcadamensual.Text = string.Empty;
			txtCadames.Text = string.Empty;
			txtDiasMes.Text = string.Empty;
			txtDiasmesaño.Text = string.Empty;
			txtFrecuenciaSemanal.Text = string.Empty;
			txtFrecuenciaAnual.Text = string.Empty;
			txtFrecuenciaAnual.Enable();
			txtcadaDiario.Enable();
			txtDiasMes.Enable();
			txtCadames.Enable();
			txtcadamensual.Enable();
			txtFrecuenciaSemanal.Enable();
			txtDiasmesaño.Enable();

			DdMesAño1.Enable();
			DdMesAño2.Enable();
			DdDiasMensual.Enable();
			DdDiasAño.Enable();
			DdOrdinalAño.Enable();
			DdOrdinalMensual.Enable();
		}
		private List<Departamento> DepartamentoRemoved
		{
			get
			{
				if (ViewState["DepartamentoRemoved"] == null)
					ViewState["DepartamentoRemoved"] = new List<Departamento>();

				return (List<Departamento>)ViewState["DepartamentoRemoved"];
			}
		}
		private List<Area> AreasRemoved
		{
			get
			{
				if (ViewState["AreaRemoved"] == null)
					ViewState["AreaRemoved"] = new List<Area>();

				return (List<Area>)ViewState["AreaRemoved"];
			}
		}
		private void MirarHora()
		{
			lblHora.Visible = true;
			lblTolerancia.Visible = true;
			txtTolerancia.Visible = true;
			txtHora.Visible = true;
			clock.Visible = true;
			min.Visible = true;
		}
		private void OcultarHora()
		{
			lblHora.Visible = false;
			lblTolerancia.Visible = false;
			txtTolerancia.Visible = false;
			txtHora.Visible = false;
			clock.Visible = false;
			min.Visible = false;
		}

		#endregion
		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("Tareas", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				btnEliminarOpcion.Visible = true;
				btnEditarOpcion.Visible = true;
				lblFiltros.Text = "Mostrar";
				PanelDepartamentos.Visible = false;
				PanelPeriodos.Visible = false;
				panelbuscarantecesor.Visible = false;
				PanelDepartamentosGuardar.Visible = false;
				PanelAreasGuardar.Visible = false;
				PanelNotificacion.Visible = false;
				PanelAntecesor.Visible = false;
				userGrid.Visible = false;
				panelArea.Visible = false;
				txtNombre.Disable();
				txtDescripcion.Disable();
				DdUnidadMedida.Enable();
				txtNombreAntecesor.Disable();
				txtNombreDepartamento.Disable();
				btnAceptar.Visible = false;
				btnCancelar.Visible = false;
				txtFechainicio.Disable();
				DdMedicion.Disable();
				DdUnidadMedida.Visible = false;
				nombreunidad.Visible = false;
				txtFechaFin.Disable();
				btnCancelarEliminarDepartamento.Visible = false;
				btnAceptarEliminarDepartamento.Visible = false;
				btnEliminarDepartamento.Disable();
				radiosgeneralesdisables();
				activeOpciones.Visible = false;
				btnAceptarOpcion.Visible = false;
				btnCancelarOpcion.Visible = false;
				btnCancelarEliminarOpcion.Visible = false;
				btnAceptarEliminarOpcion.Visible = false;
				btnEliminarDepartamento.Visible = false;
				btnEliminarDepartamento.Visible = false;
				CbHora.Disable();
				OcultarHora();
				btnCancelarBusquedaAntecesor.Visible = false;
				btncancelarburcarDepartamento.Visible = false;
				cancelarArea.Visible = false;
				DdStatus.Disable();
				DdTipoTarea.Disable();
				CbFoto.Disable();
				CbFoto.Checked = false;
				rdArea.Disable();
				rdDepartamento.Disable();
				txtradio.Text = "Sin periodicidad";
				rdSinperiodicidad.Checked = true;
				VMP.ConsultarTipoFrecuencia(txtradio.Text);
				txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();

				VM.ConsultarDepartamento(SesionActual.uidSucursalActual.Value);
				lblDepartamento.DataSource = VM.ltsDepartamento;
				lblDepartamento.DataTextField = "StrNombre";
				lblDepartamento.DataValueField = "UidDepartamento";
				lblDepartamento.DataBind();


				VM.ConsultarUnidadMedida();
				DdUnidadMedida.DataSource = VM.ltsUnidadMedida;
				DdUnidadMedida.DataTextField = "StrTipoUnidad";
				DdUnidadMedida.DataValueField = "UidUnidadMedida";
				DdUnidadMedida.DataBind();

				lblUnidad.DataSource = VM.ltsUnidadMedida;
				lblUnidad.DataTextField = "StrTipoUnidad";
				lblUnidad.DataValueField = "UidUnidadMedida";
				lblUnidad.DataBind();

				VM.ConsultarMeses();
				DdMesAño1.DataSource = VM.ltsMeses;
				DdMesAño1.DataTextField = "StrMes";
				DdMesAño1.DataValueField = "UidMes";
				DdMesAño1.DataBind();

				DdMesAño2.DataSource = VM.ltsMeses;
				DdMesAño2.DataTextField = "StrMes";
				DdMesAño2.DataValueField = "UidMes";
				DdMesAño2.DataBind();

				VM.ConsultarDias();
				DdDiasAño.DataSource = VM.ltsDias;
				DdDiasAño.DataTextField = "StrDias";
				DdDiasAño.DataValueField = "UidDias";
				DdDiasAño.DataBind();

				DdDiasMensual.DataSource = VM.ltsDias;
				DdDiasMensual.DataTextField = "StrDias";
				DdDiasMensual.DataValueField = "UidDias";
				DdDiasMensual.DataBind();

				VM.ConsultarOrdinal();
				DdOrdinalMensual.DataSource = VM.ltsOrdinal;
				DdOrdinalMensual.DataTextField = "StrOrdinal";
				DdOrdinalMensual.DataValueField = "UidOrdinal";
				DdOrdinalMensual.DataBind();

				DdOrdinalAño.DataSource = VM.ltsOrdinal;
				DdOrdinalAño.DataTextField = "StrOrdinal";
				DdOrdinalAño.DataValueField = "UidOrdinal";
				DdOrdinalAño.DataBind();

				VM.ConsultarMedicion();
				DdMedicion.DataSource = VM.ltsMedicion;
				DdMedicion.DataTextField = "StrTipoMedicion";
				DdMedicion.DataValueField = "UidTipoMedicion";
				DdMedicion.DataBind();

				VM.ConsultarStatus();
				DdStatus.DataSource = VM.ltsStatus;
				DdStatus.DataTextField = "strStatus";
				DdStatus.DataValueField = "UidStatus";
				DdStatus.DataBind();

				VM.ConsultarTipoTarea();
				DdTipoTarea.DataSource = VM.ltsTipoTarea;
				DdTipoTarea.DataTextField = "StrTipoTarea";
				DdTipoTarea.DataValueField = "UidTipoTarea";
				DdTipoTarea.DataBind();

				rbMenor.Disable();
				rbMenorIgual.Disable();
				rbMayor.Disable();
				rbMayorIgual.Disable();
				txtMayorQue.Disable();
				txtMenorQue.Disable();
				rbVerdadero.Disable();
				rbFalso.Disable();
				txtDiasDespues.Disable();
				txtMaximo.Disable();
				cbActivarNotificacion.Disable();
				cbActivarAntecesor.Disable();
				cbUsarNotificacion.Disable();
				cbUsarNotificacion.Checked = false;
				cbActivarAntecesor.Checked = false;
				cbActivarNotificacion.Checked = false;
				notificacionBool.Visible = false;
				notificacionValor.Visible = false;
				notificacionOpcion.Visible = false;
				mostrarAntecesor.Visible = false;

				rdNumeroLimitado.Disable();
				rdIlimitado.Disable();

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				var local = horaLocal.DateTime;

				filtrofechainicio1.Text = local.ToString("dd/MM/yyyy");

				filtrofechainicio2.Text = local.AddDays(1).ToString("dd/MM/yyyy");
			}
		}

		#region Departamentos

		protected void dgvDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			VM.ObtenerDepartamento(new Guid(dgvDepartamentos.SelectedDataKey.Value.ToString()));
			//txtUidDepartamentos.Text = VM.CDepartamento.UidDepartamento.ToString();
			txtBusquedaDeDepartamento.Text = VM.CDepartamento.UidDepartamento.ToString();

			if (rdArea.Checked)
			{
				VM.ObtenerListaArea(new Guid(txtBusquedaDeDepartamento.Text));

				if (VM.ltsArea.Count == 0)
				{
					return;
				}
				dvgArea.DataSource = VM.ltsArea;
				dvgArea.DataBind();
				dvgArea.Visible = true;
				userGrid.Visible = false;
				panelArea.Visible = true;
				cancelarArea.Visible = true;
			}
			else if (rdDepartamento.Checked)
			{
				List<Departamento> departamentos = (List<Departamento>)ViewState["Departamentos"];

				List<Departamento> exists = departamentos.Where(x => x.UidDepartamento == VM.CDepartamento.UidDepartamento).Select(x => x).ToList();
				if (exists.Count <= 0)
					departamentos.Add(VM.CDepartamento);
				dvgDepartamentosGuardar.DataSource = departamentos;
				dvgDepartamentosGuardar.DataBind();

				txtUidDepartamentos.Text = string.Empty;

				PanelDepartamentos.Visible = true;
				PanelDepartamentosGuardar.Visible = true;
				userGrid.Visible = false;
			}
		}

		protected void dgvDepartamentos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvDepartamentos, "Select$" + e.Row.RowIndex);
			}
		}

		protected void dvgArea_SelectedIndexChanged(object sender, EventArgs e)
		{
			VM.ObtenerArea(new Guid(dvgArea.SelectedDataKey.Value.ToString()));

			List<Area> areas = (List<Area>)ViewState["Areas"];
			List<Area> existe = areas.Where(x => x.UidArea == VM.CArea.UidArea).Select(x => x).ToList();
			if (existe.Count <= 0)
				areas.Add(VM.CArea);
			dvgAreasGuardar.DataSource = areas;
			dvgAreasGuardar.DataBind();
			txtUidArea.Text = string.Empty;
			PanelDepartamentos.Visible = true;
			PanelAreasGuardar.Visible = true;
			panelArea.Visible = false;

		}

		protected void dvgArea_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgArea, "Select$" + e.Row.RowIndex);
			}
		}

		protected void dvgAreasGuardar_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgAreasGuardar, "Select$" + e.Row.RowIndex);
			}
		}

		protected void dvgAreasGuardar_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (txtUidTarea.Text.Length <= 0)
			{
				btnEliminarDepartamento.Enable();
				btnEliminarDepartamento.Visible = true;
			}
			else
			{
				btnEliminarDepartamento.Visible = false;
			}


			List<Area> areas = (List<Area>)ViewState["Areas"];
			Area area = areas.Select(x => x).Where(x => x.UidArea.ToString() == dvgAreasGuardar.SelectedDataKey.Value.ToString()).First();

			txtUidArea.Text = area.UidArea.ToString();
			txtNombreDepartamento.Text = area.StrNombre;

			int pos = -1;
			if (ViewState["AreaPreviousRow"] != null)
			{
				pos = (int)ViewState["AreaPreviousRow"];
				GridViewRow previousRow = dvgAreasGuardar.Rows[pos];
				previousRow.RemoveCssClass("success");
			}

			ViewState["AreaPreviousRow"] = dvgAreasGuardar.SelectedIndex;
			dvgAreasGuardar.SelectedRow.AddCssClass("success");


			btnEliminarDepartamento.Enable();
		}

		protected void btnBuscarDepartamentos_Click(object sender, EventArgs e)
		{
			btncancelarburcarDepartamento.Visible = true;

			string NombreDepartamento = txtNombreDepartamento.Text;
			if (SesionActual.uidSucursalActual == null)
			{
				lblMensaje.Text = "Selecciona una sucursal";
				lblMensaje.Visible = true;

			}
			else
			{
				VM.BuscarDepartamento(NombreDepartamento, SesionActual.uidSucursalActual.Value);
				if (VM.ltsDepartamento.Count == 0)
				{
					return;
				}
				dgvDepartamentos.DataSource = VM.ltsDepartamento;
				dgvDepartamentos.DataBind();
				PanelDepartamentosGuardar.Visible = false;
				PanelAreasGuardar.Visible = false;
				userGrid.Visible = true;
			}


			int pos = -1;
			if (ViewState["DepartamentoPreviousRow"] != null)
			{
				pos = (int)ViewState["DepartamentoPreviousRow"];
				GridViewRow previousRow = dvgDepartamentosGuardar.Rows[pos];
				previousRow.RemoveCssClass("success");
			}

		}

		protected void dvgDepartamentosGuardar_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgDepartamentosGuardar, "Select$" + e.Row.RowIndex);
			}
		}

		protected void dvgDepartamentosGuardar_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (txtUidTarea.Text.Length <= 0)
			{
				btnEliminarDepartamento.Enable();
				btnEliminarDepartamento.Visible = true;
			}
			else
			{
				btnEliminarDepartamento.Visible = false;
			}

			List<Departamento> departamentos = (List<Departamento>)ViewState["Departamentos"];
			Departamento departamento = departamentos.Select(x => x).Where(x => x.UidDepartamento.ToString() == dvgDepartamentosGuardar.SelectedDataKey.Value.ToString()).First();

			txtUidDepartamentos.Text = departamento.UidDepartamento.ToString();
			txtNombreDepartamento.Text = departamento.StrNombre;

			int pos = -1;
			if (ViewState["DepartamentoPreviousRow"] != null)
			{
				pos = (int)ViewState["DepartamentoPreviousRow"];
				GridViewRow previousRow = dvgDepartamentosGuardar.Rows[pos];
				previousRow.RemoveCssClass("success");
			}

			ViewState["DepartamentoPreviousRow"] = dvgDepartamentosGuardar.SelectedIndex;
			dvgDepartamentosGuardar.SelectedRow.AddCssClass("success");


			btnEliminarDepartamento.Enable();
		}

		protected void cancelarburcarDepartamento_Click(object sender, EventArgs e)
		{
			userGrid.Visible = false;
			if (rdDepartamento.Checked)
			{
				PanelDepartamentosGuardar.Visible = true;
				PanelAreasGuardar.Visible = false;
			}
			else
			{
				PanelAreasGuardar.Visible = true;
				PanelDepartamentosGuardar.Visible = false;
			}
		}
		protected void cancelarArea_Click(object sender, EventArgs e)
		{
			dvgArea.Visible = false;
			panelArea.Visible = false;
			PanelAreasGuardar.Visible = true;


		}
		protected void btnEliminarDepartamento_Click(object sender, EventArgs e)
		{
			btnAceptarEliminarDepartamento.Visible = true;
			btnCancelarEliminarDepartamento.Visible = true;
			lblAceptarEliminarDepartamento.Visible = true;
			if (rdDepartamento.Checked)
			{
				lblAceptarEliminarDepartamento.Text = "¿Desea eliminar el departamento seleccionado?";
			}
			else if (rdArea.Checked)
			{
				lblAceptarEliminarDepartamento.Text = "¿Desea eliminar el área seleccionada?";
			}
		}

		protected void btnAceptarEliminarDepartamento_Click(object sender, EventArgs e)
		{
			if (rdDepartamento.Checked)
			{
				Guid uid = new Guid(txtUidDepartamentos.Text);
				List<Departamento> departamentos = (List<Departamento>)ViewState["Departamentos"];
				Departamento departamento = departamentos.Select(x => x).Where(x => x.UidDepartamento == uid).First();
				departamentos.Remove(departamento);
				DepartamentoRemoved.Add(departamento);
				txtUidDepartamentos.Text = string.Empty;
				txtNombreDepartamento.Text = string.Empty;
				dvgDepartamentosGuardar.DataSource = departamentos;
				dvgDepartamentosGuardar.DataBind();


				ViewState["DepartamentoPreviousRow"] = null;
			}
			else if (rdArea.Checked)
			{
				Guid UidArea = new Guid(txtUidArea.Text);
				List<Area> areas = (List<Area>)ViewState["Areas"];
				Area area = areas.Select(x => x).Where(x => x.UidArea == UidArea).First();
				areas.Remove(area);
				AreasRemoved.Add(area);
				txtUidArea.Text = string.Empty;
				txtNombreDepartamento.Text = string.Empty;
				dvgAreasGuardar.DataSource = areas;
				dvgAreasGuardar.DataBind();
			}
			btnCancelarEliminarDepartamento.Visible = false;
			btnAceptarEliminarDepartamento.Visible = false;
			lblAceptarEliminarDepartamento.Visible = false;
		}

		protected void btnCancelarEliminarDepartamento_Click(object sender, EventArgs e)
		{
			btnCancelarEliminarDepartamento.Visible = false;
			btnAceptarEliminarDepartamento.Visible = false;
			lblAceptarEliminarDepartamento.Visible = false;
		}
		#endregion

		#region paneles

		protected void tabDatosGenerales_Click(object sender, EventArgs e)
		{
			PanelNotificacion.Visible = false;
			PanelAntecesor.Visible = false;
			PanelDatosGenerales.Visible = true;
			PanelPeriodos.Visible = false;
			PanelDepartamentos.Visible = false;
			panelbuscarantecesor.Visible = false;
			activeDepartamentos.Attributes["class"] = "";
			activePeriodos.RemoveCssClass("active");
			activeDatosGenerales.Attributes["class"] = "active";
			activeAntecesor.Attributes["class"] = "";
			activeNotificacion.RemoveCssClass("active");
			//gridDepartamentos.Visible = false;
			gridAntecesor.Visible = false;
			userGrid.Visible = false;
			PanelSeleccion.Visible = false;
			activeOpciones.Attributes["class"] = "";
			panelArea.Visible = false;
			PanelDepartamentosGuardar.Visible = false;
			PanelAreasGuardar.Visible = false;
		}

		protected void tabPeriodos_Click(object sender, EventArgs e)
		{
			PanelNotificacion.Visible = false;
			PanelAntecesor.Visible = false;
			PanelPeriodos.Visible = true;
			PanelDatosGenerales.Visible = false;
			PanelDepartamentos.Visible = false;
			panelbuscarantecesor.Visible = false;
			Frecuencia.Visible = true;
			activePeriodos.Attributes["class"] = "active";
			activeDepartamentos.Attributes["class"] = "";
			activeDatosGenerales.Attributes["class"] = "";
			activeAntecesor.Attributes["class"] = "";
			activeNotificacion.RemoveCssClass("active");
			// gridDepartamentos.Visible = false;
			gridAntecesor.Visible = false;
			userGrid.Visible = false;
			PanelSeleccion.Visible = false;
			activeOpciones.Attributes["class"] = "";
			panelArea.Visible = false;
			PanelDepartamentosGuardar.Visible = false;
			PanelAreasGuardar.Visible = false;

		}

		protected void tabDepartamentos_Click(object sender, EventArgs e)
		{
			PanelNotificacion.Visible = false;
			PanelAntecesor.Visible = false;
			PanelDepartamentos.Visible = true;
			PanelDatosGenerales.Visible = false;
			PanelPeriodos.Visible = false;
			panelbuscarantecesor.Visible = false;
			activeDepartamentos.Attributes["class"] = "active";
			activePeriodos.Attributes["class"] = "";
			activeDatosGenerales.Attributes["class"] = "";
			activeAntecesor.Attributes["class"] = "";
			activeNotificacion.RemoveCssClass("active");
			gridAntecesor.Visible = false;
			btncancelarburcarDepartamento.Visible = false;
			cancelarArea.Visible = false;
			PanelSeleccion.Visible = false;
			activeOpciones.Attributes["class"] = "";
			panelArea.Visible = false;
			if (rdDepartamento.Checked)
			{
				PanelDepartamentosGuardar.Visible = true;
				PanelAreasGuardar.Visible = false;
			}
			else if (rdArea.Checked)
			{
				PanelDepartamentosGuardar.Visible = false;
				PanelAreasGuardar.Visible = true;
			}
			else if (!rdDepartamento.Checked && !rdArea.Checked)
			{
				PanelDepartamentosGuardar.Visible = false;
				PanelAreasGuardar.Visible = false;
			}

		}

		protected void tabAntecesor_Click(object sender, EventArgs e)
		{
			PanelNotificacion.Visible = false;
			panelbuscarantecesor.Visible = true;
			PanelDatosGenerales.Visible = false;
			PanelPeriodos.Visible = false;
			PanelDepartamentos.Visible = false;
			activeAntecesor.Attributes["class"] = "active";
			activeDatosGenerales.Attributes["class"] = "";
			activeDepartamentos.Attributes["class"] = "";
			activePeriodos.Attributes["class"] = "";
			activeNotificacion.RemoveCssClass("active");
			//gridDepartamentos.Visible = false;
			userGrid.Visible = false;
			btnCancelarBusquedaAntecesor.Visible = false;
			PanelSeleccion.Visible = false;
			activeOpciones.Attributes["class"] = "";
			panelArea.Visible = false;
			PanelDepartamentosGuardar.Visible = false;
			PanelAreasGuardar.Visible = false;
			PanelAntecesor.Visible = true;
		}

		protected void tabNotificacion_Click(object sender, EventArgs e)
		{
			PanelNotificacion.Visible = true;
			panelbuscarantecesor.Visible = true;
			PanelDatosGenerales.Visible = false;
			PanelPeriodos.Visible = false;
			PanelDepartamentos.Visible = false;
			activeAntecesor.Attributes["class"] = "";
			activeDatosGenerales.Attributes["class"] = "";
			activeDepartamentos.Attributes["class"] = "";
			activePeriodos.Attributes["class"] = "";
			activeNotificacion.AddCssClass("active");
			//gridDepartamentos.Visible = false;
			userGrid.Visible = false;
			btnCancelarBusquedaAntecesor.Visible = false;
			PanelSeleccion.Visible = false;
			activeOpciones.Attributes["class"] = "";
			panelArea.Visible = false;
			PanelDepartamentosGuardar.Visible = false;
			PanelAreasGuardar.Visible = false;
			PanelAntecesor.Visible = false;

			if (btnAceptar.Visible == true)
			{
				if (cbActivarNotificacion.Checked)
				{
					string tipo = DdMedicion.SelectedItem.Text;
					setOptionsNotification(tipo);
				}
			}
		}

		protected void tabOpciones_Click(object sender, EventArgs e)
		{
			PanelNotificacion.Visible = false;
			PanelAntecesor.Visible = false;
			PanelSeleccion.Visible = true;
			activeOpciones.Attributes["class"] = "active";
			PanelDatosGenerales.Visible = false;
			PanelPeriodos.Visible = false;
			PanelDepartamentos.Visible = false;
			panelbuscarantecesor.Visible = false;
			activeDepartamentos.Attributes["class"] = "";
			activePeriodos.RemoveCssClass("active");
			activeDatosGenerales.Attributes["class"] = "";
			activeAntecesor.Attributes["class"] = "";
			activeNotificacion.RemoveCssClass("active");
			//gridDepartamentos.Visible = false;
			gridAntecesor.Visible = false;
			userGrid.Visible = false;
			panelArea.Visible = false;
			PanelDepartamentosGuardar.Visible = false;
			PanelAreasGuardar.Visible = false;
		}

		#endregion

		#region Antecesor

		protected void btnBuscarAntecesor_Click(object sender, EventArgs e)
		{
			btnCancelarBusquedaAntecesor.Visible = true;
			if (btnBuscarAntecesor.Text.Contains("Buscar Antecesor"))
			{
				string NombreAntecesor = txtNombreAntecesor.Text;
				VM.BuscarAntecesor(NombreAntecesor);
				if (VM.ltsTarea.Count == 0)
				{
					return;
				}
				dvgAntecesor.DataSource = VM.ltsTarea;
				dvgAntecesor.DataBind();
				panelbuscarantecesor.Visible = false;
				gridAntecesor.Visible = true;
			}
			else
			{
				txtNombreAntecesor.Text = string.Empty;
				txtNombreAntecesor.Enable();
				btnBuscarAntecesor.Text = "Buscar Antecesor";
			}
		}

		protected void dvgAntecesor_SelectedIndexChanged(object sender, EventArgs e)
		{
			VM.ObtenerTarea(new Guid(dvgAntecesor.SelectedDataKey.Value.ToString()));
			txtUidAntecesor.Text = VM.CTarea.UidTarea.ToString();
			txtNombreAntecesor.Text = VM.CTarea.StrNombre;
			txtNombreAntecesor.Disable();

			panelbuscarantecesor.Visible = true;
			gridAntecesor.Visible = false;
			btnBuscarAntecesor.Text = "Seleccionar otro";

			// Reestablecer periodicidad
			rdSemanal.Checked = false;
			rdDiaro.Checked = false;
			rdAnual.Checked = false;
			rdSinperiodicidad.Checked = true;
			txtradio.Text = "Sin periodicidad";
			VMP.ConsultarTipoFrecuencia(txtradio.Text);
			txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
			rdSemanal_CheckedChanged(null, null);
		}

		protected void dvgAntecesor_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgAntecesor, "Select$" + e.Row.RowIndex);
			}
		}

		protected void btnCancelarBusquedaAntecesor_Click(object sender, EventArgs e)
		{
			panelbuscarantecesor.Visible = true;
			gridAntecesor.Visible = false;
			antecesorNotificacion.Visible = false;
		}

		#endregion

		#region Gestion de Tareas

		protected void btnNuevo_Click(object sender, EventArgs e)
		{
			lblMensaje.Text = string.Empty;
			txtUidTarea.Text = string.Empty;
			txtNombre.Enable();
			txtDescripcion.Enable();
			txtNombreAntecesor.Enable();
			txtFechaFin.Enable();
			rdArea.Enable();
			rdDepartamento.Enable();
			rdArea.Checked = false;
			rdDepartamento.Checked = false;
			txtNombre.Text = string.Empty;
			txtDescripcion.Text = string.Empty;
			txtNombreDepartamento.Text = string.Empty;
			txtNombreAntecesor.Text = string.Empty;

			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
			var local = horaLocal.DateTime;
			txtFechainicio.Text = local.ToString("dd/MM/yyyy");

			txtFechaFin.Text = string.Empty;
			btnAceptar.Visible = true;
			btnCancelar.Visible = true;
			btnBuscarAntecesor.Enable();
			txtFechainicio.Enable();
			radiosgeneralesenable();
			RadiosChechek();
			txtEnable();
			txtOpcion.Text = string.Empty;
			Radiosadentroenable();
			DdMedicion.Enable();
			PanelDiario.Visible = false;
			PanelSemanal.Visible = false;
			PanelMensual.Visible = false;
			PanelAnual.Visible = false;
			btnNuevo.Disable();
			btnEditar.Disable();
			btnAgregarOpcion.Visible = true;
			btnEditarOpcion.Visible = true;
			btnEditarOpcion.Disable();
			btnEliminarOpcion.Visible = true;
			btnEliminarOpcion.Disable();
			btnAgregarOpcion.Enable();
			rdSinperiodicidad.Checked = true;
			rdDiaro.Checked = false;
			rdSemanal.Checked = false;
			rdMensual.Checked = false;
			rdAnual.Checked = false;
			DdTipoTarea.Enable();
			DdStatus.Enable();
			int pos;
			if (ViewState["TareaPreviousRow"] != null)
			{
				pos = (int)ViewState["TareaPreviousRow"];
				GridViewRow previousRow = DVGTareas.Rows[pos];
				previousRow.RemoveCssClass("success");
			}
			ViewState["DepartamentoPreviousRow"] = null;
			ViewState["Departamentos"] = new List<Departamento>();
			DepartamentoRemoved.Clear();
			dvgDepartamentosGuardar.DataSource = ViewState["Departamentos"];
			dvgDepartamentosGuardar.DataBind();
			ViewState["AreaPreviousRow"] = null;
			ViewState["Areas"] = new List<Area>();
			DepartamentoRemoved.Clear();
			dvgAreasGuardar.DataSource = ViewState["Areas"];
			dvgAreasGuardar.DataBind();


			ViewState["Opciones"] = new List<TareaOpcion>();
			OpcionRemoved.Clear();
			dgvOpciones.DataSource = ViewState["Opciones"];
			dgvOpciones.DataBind();

			CbHora.Enable();
			CbHora.Checked = false;
			txtHora.Text = string.Empty;
			txtTolerancia.Text = string.Empty;
			OcultarHora();
			txtHora.Enable();
			txtTolerancia.Enable();
			DdUnidadMedida.Enable();
			CbFoto.Enable();
			CbFoto.Checked = false;

			cbActivarAntecesor.Enable();
			cbActivarAntecesor.Checked = false;
			cbUsarNotificacion.Enable();
			cbUsarNotificacion.Enable();
			cbActivarNotificacion.Enable();
			cbActivarNotificacion.Checked = false;
			rbFalso.Enable();
			rbFalso.Checked = false;
			rbVerdadero.Enable();
			rbVerdadero.Checked = false;
			txtMayorQue.Text = "";
			txtMayorQue.Enable();
			txtMenorQue.Text = "";
			txtMenorQue.Enable();

			txtDiasDespues.Enable();
			txtDiasDespues.Text = "0";
			txtMaximo.Enable();
			txtMaximo.Text = "1";
			rdNumeroLimitado.Enable();
			rdIlimitado.Enable();
			rdIlimitado.Checked = true;

			notificacionBool.Visible = false;
			notificacionValor.Visible = false;
			notificacionOpcion.Visible = false;

			antecesorNotificacion.Visible = false;
		}

		protected void btnCancelar_Click(object sender, EventArgs e)
		{
			EditingMode = false;
			txtNombre.Disable();
			txtDescripcion.Disable();
			txtNombreAntecesor.Disable();
			txtNombreDepartamento.Disable();
			rdDepartamento.Disable();
			rdArea.Disable();
			btnAceptar.Visible = false;
			btnCancelar.Visible = false;
			btnBuscarDepartamentos.Disable();
			txtFechainicio.Disable();
			radiosgeneralesdisables();
			btnBuscarAntecesor.Disable();
			btnNuevo.Enable();
			txtFechaFin.Disable();
			DdMedicion.Disable();
			btnAgregarOpcion.Disable();
			btnEditarOpcion.Disable();
			btnEliminarDepartamento.Visible = false;
			btnEliminarOpcion.Disable();
			DdUnidadMedida.Disable();
			txtHora.Disable();
			txtTolerancia.Disable();
			CbHora.Disable();
			DdTipoTarea.Disable();
			DdStatus.Disable();
			CbFoto.Disable();
			rbMenor.Disable();
			rbMenorIgual.Disable();
			rbMayor.Disable();
			rbMayorIgual.Disable();
			txtMayorQue.Disable();
			txtMenorQue.Disable();
			rbVerdadero.Disable();
			rbFalso.Disable();
			txtDiasDespues.Disable();
			txtMaximo.Disable();
			cbActivarNotificacion.Disable();
			cbActivarAntecesor.Disable();
			cbUsarNotificacion.Disable();
			rdNumeroLimitado.Disable();
			rdIlimitado.Disable();
			btnCancelarEliminarOpcion_Click(sender, e);

			if (txtUidTarea.Text.Length > 0)
			{
				VM.ObtenerTarea(new Guid(txtUidTarea.Text));
				txtUidTarea.Text = VM.CTarea.UidTarea.ToString();
				txtNombre.Text = VM.CTarea.StrNombre;
				txtDescripcion.Text = VM.CTarea.StrDescripcion;
				txtUidAntecesor.Text = VM.CTarea.UidAntecesor.ToString();
				txtHora.Text = VM.CTarea.TmHora.ToString();
				txtTolerancia.Text = VM.CTarea.IntTolerancia.ToString();
				DdStatus.SelectedIndex = DdStatus.Items.IndexOf(DdStatus.Items.FindByValue(VM.CTarea.UidStatus.ToString()));
				DdTipoTarea.SelectedIndex = DdTipoTarea.Items.IndexOf(DdTipoTarea.Items.FindByValue(VM.CTarea.UidTipoTarea.ToString()));

				if (VM.CTarea.BlFoto == true)
				{
					CbFoto.Checked = true;
				}
				else
				{
					CbFoto.Checked = false;
				}
				if (txtTolerancia.Text.Length > 0 || txtHora.Text.Length > 0)
				{
					CbHora.Checked = true;
					MirarHora();
				}
				else
				{
					OcultarHora();
					CbHora.Checked = false;
				}
				if (VM.CTarea.UidAntecesor != null)
				{
					VM.CargarAntecesor(txtUidAntecesor.Text);
					txtUidAntecesor.Text = VM.CAntecesor.UidTarea.ToString();
					txtNombreAntecesor.Text = VM.CAntecesor.StrNombre;
				}
				else
				{
					txtUidAntecesor.Text = string.Empty;
					txtNombreAntecesor.Text = string.Empty;
				}
				DdMedicion.SelectedIndex = DdMedicion.Items.IndexOf(DdMedicion.Items.FindByValue(VM.CTarea.UidMedicion.ToString()));
				if (DdMedicion.SelectedItem.Text == "Numerico")
				{
					DdUnidadMedida.Visible = true;
					nombreunidad.Visible = true;
					DdUnidadMedida.SelectedIndex = DdUnidadMedida.Items.IndexOf(DdUnidadMedida.Items.FindByValue(VM.CTarea.UidUnidadMedida.ToString()));
				}
				else if (DdMedicion.SelectedItem.Text == "Seleccionable")
				{
					activeOpciones.Visible = true;
				}
				else
				{
					activeOpciones.Visible = false;
				}

				/*VM.ObtenerTareaDepartamento(txtUidTarea.Text);
                Guid UidDepartamento = VM.CDepartamentoTarea.UidDepartamento;
                VM.ObtenerDepartamento(UidDepartamento);
                txtUidDepartamentos.Text = VM.CDepartamento.UidDepartamento.ToString();
                txtNombreDepartamento.Text = VM.CDepartamento.StrNombre;*/


				VM.ObtenerDepartamentos();
				if (VM.Departamentos?.Count > 0)
				{
					ViewState["Departamentos"] = VM.Departamentos;
					dvgDepartamentosGuardar.DataSource = VM.Departamentos;
					dvgDepartamentosGuardar.DataBind();
					PanelDepartamentosGuardar.Visible = true;
					PanelAreasGuardar.Visible = false;
				}
				else if (VM.Areas?.Count > 0)
				{
					VM.ObtenerAreas();
					ViewState["Areas"] = VM.Areas;
					dvgAreasGuardar.DataSource = VM.Areas;
					dvgAreasGuardar.DataBind();
					PanelAreasGuardar.Visible = true;
					PanelDepartamentosGuardar.Visible = false;
				}





				Guid UidPeriodicidad = VM.CTarea.UidPeriodicidad;
				VMP.ObtenerPeriodicidad(UidPeriodicidad);

				Guid UidTipoFrecuencia = VMP.Cperiodicidad.UidTipoFrecuencia;
				VMP.ObtenerTipoFrecuencia(UidTipoFrecuencia);
				string tipo = VMP.CTipoFrecuencia.StrTipoFrecuencia;

				VMP.ObtenerPeriodicidadMensual(UidPeriodicidad);

				VMP.ObtenerPeriodicidadAnual(UidPeriodicidad);

				VMP.ObtenerPeriodicidadSemanal(UidPeriodicidad);


				if (tipo == "Diaria")
				{
					PanelDiario.Visible = true;
					PanelSemanal.Visible = false;
					PanelMensual.Visible = false;
					PanelAnual.Visible = false;
					rdDiaro.Checked = true;
					rdAnual.Checked = false;
					rdSemanal.Checked = false;
					rdMensual.Checked = false;
					if (VMP.Cperiodicidad.IntFrecuencia == 1)
					{
						rdtodoslosdias.Checked = true;
						rdCadaDiario.Checked = false;
					}
					else
					{
						rdtodoslosdias.Checked = false;
						rdCadaDiario.Checked = true;
						txtcadaDiario.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
					}
				}
				else if (tipo == "Semanal")
				{
					PanelSemanal.Visible = true;
					PanelDiario.Visible = false;
					PanelMensual.Visible = false;
					PanelAnual.Visible = false;
					rdSemanal.Checked = true;
					rdDiaro.Checked = false;
					rdAnual.Checked = false;
					rdMensual.Checked = false;

					if (VMP.CPeriodicidadSemanal.BlLunes)
					{
						CBLunes.Checked = true;
					}
					else
					{
						CBLunes.Checked = false;
					}

					if (VMP.CPeriodicidadSemanal.BlMartes)
					{
						CBMartes.Checked = true;
					}
					else
					{
						CBMartes.Checked = false;
					}
					if (VMP.CPeriodicidadSemanal.BlMiercoles)
					{
						CBMiercoles.Checked = true;
					}
					else
					{
						CBMiercoles.Checked = false;
					}
					if (VMP.CPeriodicidadSemanal.BlJueves)
					{
						CBJueves.Checked = true;
					}
					else
					{
						CBJueves.Checked = false;
					}
					if (VMP.CPeriodicidadSemanal.BlViernes)
					{
						CBViernes.Checked = true;
					}
					else
					{
						CBViernes.Checked = false;
					}
					if (VMP.CPeriodicidadSemanal.BlSabado)
					{
						CBSabado.Checked = true;
					}
					else
					{
						CBSabado.Checked = false;
					}
					if (VMP.CPeriodicidadSemanal.BlDomingo)
					{
						CBDomingo.Checked = true;
					}
					else
					{
						CBDomingo.Checked = false;
					}
					txtFrecuenciaSemanal.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
				}
				else if (tipo == "Mensual")
				{
					PanelMensual.Visible = true;
					PanelDiario.Visible = false;
					PanelSemanal.Visible = false;
					PanelAnual.Visible = false;
					rdMensual.Checked = true;
					rdDiaro.Checked = false;
					rdSemanal.Checked = false;
					rdAnual.Checked = false;
					if (VMP.CPeriodicidadMensual.IntDiasSemana == 0)
					{
						rdElDiaMensual.Checked = true;
						rdElMeses.Checked = false;
						txtDiasMes.Text = VMP.CPeriodicidadMensual.IntDiasMes.ToString();
						txtCadames.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
						txtcadamensual.Text = string.Empty;
					}
					else
					{
						rdElDiaMensual.Checked = false;
						rdElMeses.Checked = true;
						txtcadamensual.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
						txtDiasMes.Text = string.Empty;
						txtCadames.Text = string.Empty;
						#region Ordinal

						if (VMP.CPeriodicidadMensual.IntDiasMes == 1)
						{

							string Ordinal = "Primer";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();

						}
						else if (VMP.CPeriodicidadMensual.IntDiasMes == 2)
						{
							string Ordinal = "Segundo";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasMes == 3)
						{
							string Ordinal = "Tercer";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasMes == 4)
						{
							string Ordinal = "Cuarto";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasMes == -1)
						{
							string Ordinal = "Ultimo";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}
						#endregion

						if (VMP.CPeriodicidadMensual.IntDiasSemana == 1)
						{
							string dias = "Domingo";
							VMP.ObtenerDias(dias);
							DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasSemana == 2)
						{
							string Ordinal = "Lunes";
							VMP.ObtenerDias(Ordinal);
							DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasSemana == 3)
						{
							string Ordinal = "Martes";
							VMP.ObtenerDias(Ordinal);
							DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasSemana == 4)
						{
							string Ordinal = "Miercoles";
							VMP.ObtenerDias(Ordinal);
							DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasSemana == 5)
						{
							string Ordinal = "Jueves";
							VMP.ObtenerDias(Ordinal);
							DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasSemana == 6)
						{
							string Ordinal = "Viernes";
							VMP.ObtenerDias(Ordinal);
							DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadMensual.IntDiasSemana == 7)
						{
							string Ordinal = "Sabado";
							VMP.ObtenerDias(Ordinal);
							DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
						}

					}
				}
				else if (tipo == "Anual")
				{
					PanelAnual.Visible = true;
					PanelDiario.Visible = false;
					PanelSemanal.Visible = false;
					PanelMensual.Visible = false;
					rdAnual.Checked = true;
					rdDiaro.Checked = false;
					rdSemanal.Checked = false;
					rdMensual.Checked = false;
					if (VMP.CPeriodicidadAnual.IntDiasSemanas == 0)
					{
						rdElDelAño.Checked = true;
						rdElDelAño2.Checked = false;
						txtDiasmesaño.Text = VMP.CPeriodicidadAnual.IntDiasMes.ToString();

						if (VMP.CPeriodicidadAnual.IntNumero == 1)
						{
							string mes = "Enero";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 2)
						{
							string mes = "Febrero";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 3)
						{
							string mes = "Marzo";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 4)
						{
							string mes = "Abril";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 5)
						{
							string mes = "Mayo";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 6)
						{
							string mes = "Junio";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 7)
						{
							string mes = "Julio";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 8)
						{
							string mes = "Agosto";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 9)
						{
							string mes = "Septiembre";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 10)
						{
							string mes = "Octubre";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 11)
						{
							string mes = "Noviembre";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();

						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 12)
						{
							string mes = "Diciembre";
							VMP.ObtenerMeses(mes);
							DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
						}

					}
					else
					{
						rdElDelAño.Checked = false;
						rdElDelAño2.Checked = true;
						txtDiasmesaño.Text = string.Empty;
						if (VMP.CPeriodicidadAnual.IntDiasMes == 1)
						{

							string Ordinal = "Primer";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();

						}
						else if (VMP.CPeriodicidadAnual.IntDiasMes == 2)
						{
							string Ordinal = "Segundo";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasMes == 3)
						{
							string Ordinal = "Tercer";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasMes == 4)
						{
							string Ordinal = "Cuarto";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasMes == -1)
						{
							string Ordinal = "Ultimo";
							VMP.ConsultarOrdinal(Ordinal);
							DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
						}

						if (VMP.CPeriodicidadAnual.IntDiasSemanas == 1)
						{
							string dias = "Domingo";
							VMP.ObtenerDias(dias);
							DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 2)
						{
							string Ordinal = "Lunes";
							VMP.ObtenerDias(Ordinal);
							DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 3)
						{
							string Ordinal = "Martes";
							VMP.ObtenerDias(Ordinal);
							DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 4)
						{
							string Ordinal = "Miercoles";
							VMP.ObtenerDias(Ordinal);
							DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 5)
						{
							string Ordinal = "Jueves";
							VMP.ObtenerDias(Ordinal);
							DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 6)
						{
							string Ordinal = "Viernes";
							VMP.ObtenerDias(Ordinal);
							DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 7)
						{
							string Ordinal = "Sabado";
							VMP.ObtenerDias(Ordinal);
							DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
						}

						if (VMP.CPeriodicidadAnual.IntNumero == 1)
						{
							string mes = "Enero";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 2)
						{
							string mes = "Febrero";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 3)
						{
							string mes = "Marzo";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 4)
						{
							string mes = "Abril";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 5)
						{
							string mes = "Mayo";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 6)
						{
							string mes = "Junio";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 7)
						{
							string mes = "Julio";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 8)
						{
							string mes = "Agosto";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 9)
						{
							string mes = "Septiembre";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 10)
						{
							string mes = "Octubre";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 11)
						{
							string mes = "Noviembre";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();

						}
						else if (VMP.CPeriodicidadAnual.IntNumero == 12)
						{
							string mes = "Diciembre";
							VMP.ObtenerMeses(mes);
							DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
						}

					}
				}
				//tipofrecuencia.Text = tipo;
				txtFechainicio.Text = VMP.Cperiodicidad.DtFechaInicio.ToString("dd/MM/yyyy");
				txtFechaFin.Text = VMP.Cperiodicidad.DtFechaFin?.ToString("dd/MM/yyyy");

				VM.ObtenerOpciones();
				ViewState["Opciones"] = VM.Opciones;
				dgvOpciones.DataSource = VM.Opciones;
				dgvOpciones.DataBind();
				btnEditar.Enable();

				txtMayorQue.Text = "";
				txtMenorQue.Text = "";
				rbVerdadero.Disable();
				rbFalso.Disable();
				txtDiasDespues.Disable();
				txtDiasDespues.Text = "0";
				txtMaximo.Disable();
				txtMaximo.Text = "1";
				cbActivarNotificacion.Disable();

				rdNumeroLimitado.Disable();
				rdIlimitado.Disable();

				VM.ObtenerNotificacion(VM.CTarea.UidTarea);

				if (VM.Notificacion != null)
				{
					cbActivarNotificacion.Checked = true;

					string medicion = DdMedicion.SelectedItem.Text;

					if (medicion == "Verdadero/Falso")
					{
						notificacionBool.Visible = true;
						notificacionValor.Visible = false;
						notificacionOpcion.Visible = false;

						if (VM.Notificacion.BlValor.HasValue)
						{
							if (VM.Notificacion.BlValor.Value)
								rbVerdadero.Checked = true;
							else
								rbFalso.Checked = true;
						}
					}

					if (medicion == "Numerico")
					{
						notificacionBool.Visible = false;
						notificacionValor.Visible = true;
						notificacionOpcion.Visible = false;

						if (VM.Notificacion.BlMenorIgual.HasValue && VM.Notificacion.BlMenorIgual.Value)
						{
							rbMenorIgual.Checked = true;
						}
						else
						{
							rbMenor.Checked = true;
						}
						if (VM.Notificacion.BlMayorIgual.HasValue && VM.Notificacion.BlMayorIgual.Value)
						{
							rbMayorIgual.Checked = true;
						}
						else
						{
							rbMayor.Checked = true;
						}
						if (VM.Notificacion.DcMayorQue.HasValue)
						{
							txtMayorQue.Text = VM.Notificacion.DcMayorQue.Value.ToString();
						}
						if (VM.Notificacion.DcMenorQue.HasValue)
						{
							txtMenorQue.Text = VM.Notificacion.DcMenorQue.Value.ToString();
						}
					}
				}
				else
				{
					cbActivarNotificacion.Checked = false;
				}

				VM.ObtenerAntecesor(VM.CTarea.UidTarea);

				if (VM.Antecesor != null)
				{
					cbUsarNotificacion.Checked = true;
					VM.ObtenerTarea(VM.Antecesor.UidTareaAnterior);
					txtUidAntecesor.Text = VM.CTarea.UidAntecesor.ToString();
					txtNombreAntecesor.Text = VM.CTarea.StrNombre;
					if (VM.Antecesor.BlUsarNotificacion.HasValue && VM.Antecesor.BlUsarNotificacion.Value)
					{
						cbUsarNotificacion.Checked = true;
						antecesorNotificacion.Visible = true;
					}
					else
					{
						cbUsarNotificacion.Checked = false;
						antecesorNotificacion.Visible = false;
					}
					mostrarAntecesor.Visible = true;
				}
				else
				{
					cbUsarNotificacion.Checked = false;
					mostrarAntecesor.Visible = false;
				}
			}
			else
			{
				txtNombre.Text = string.Empty;
				txtDescripcion.Text = string.Empty;
				txtFechainicio.Text = string.Empty;
				txtFechaFin.Text = string.Empty;
				rdDepartamento.Checked = false;
				rdArea.Checked = false;
				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelAnual.Visible = false;
				PanelMensual.Visible = false;
				RadiosChechek();
				btncancelarburcarDepartamento.Visible = false;
				cancelarArea.Visible = false;
				ViewState["Departamentos"] = new List<Departamento>();
				dvgDepartamentosGuardar.DataSource = ViewState["Departamentos"];
				dvgDepartamentosGuardar.DataBind();
				ViewState["Areas"] = new List<Area>();
				dvgAreasGuardar.DataSource = ViewState["Areas"];
				dvgAreasGuardar.DataBind();
				ViewState["Opciones"] = null;
				dgvOpciones.DataSource = null;
				dgvOpciones.DataBind();
				int pos;
				if (ViewState["OpcionPreviousRow"] != null)
				{
					pos = (int)ViewState["OpcionPreviousRow"];
					GridViewRow previousRow = dgvOpciones.Rows[pos];
					previousRow.RemoveCssClass("success");
				}
				if (ViewState["DepartamentosPreviousRow"] != null)
				{
					pos = (int)ViewState["DepartamentosPreviousRow"];
					GridViewRow previousRow = dvgDepartamentosGuardar.Rows[pos];
					previousRow.RemoveCssClass("success");
				}
				CbHora.Checked = false;
				txtHora.Text = string.Empty;
				txtTolerancia.Text = string.Empty;
				DdUnidadMedida.Visible = false;
				nombreunidad.Visible = false;
				OcultarHora();
				CbFoto.Checked = false;
				PanelAreasGuardar.Visible = false;
				PanelDepartamentosGuardar.Visible = false;

				txtMayorQue.Text = "";
				txtMenorQue.Text = "";
				rbVerdadero.Disable();
				rbFalso.Disable();
				txtDiasDespues.Disable();
				txtDiasDespues.Text = "0";
				txtMaximo.Disable();
				txtMaximo.Text = "1";
				cbUsarNotificacion.Checked = false;
				cbActivarAntecesor.Checked = false;
				cbActivarNotificacion.Checked = false;
				notificacionBool.Visible = false;
				notificacionValor.Visible = false;
				notificacionOpcion.Visible = false;
				mostrarAntecesor.Visible = false;
				cbActivarNotificacion.Disable();

				rdNumeroLimitado.Disable();
				rdIlimitado.Disable();
			}

		}

		protected void rdSemanal_CheckedChanged(object sender, EventArgs e)
		{
			if (rdSinperiodicidad.Checked)
			{
				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = false;
				PanelAnual.Visible = false;
				txtradio.Text = "Sin periodicidad";
				VMP.ConsultarTipoFrecuencia(txtradio.Text);
				txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
			}
			else if (rdDiaro.Checked)
			{
				PanelDiario.Visible = true;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = false;
				PanelAnual.Visible = false;
				txtradio.Text = "Diaria";
				VMP.ConsultarTipoFrecuencia(txtradio.Text);
				txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
			}
			else if (rdSemanal.Checked)
			{
				PanelDiario.Visible = false;
				PanelSemanal.Visible = true;
				PanelMensual.Visible = false;
				PanelAnual.Visible = false;
				txtradio.Text = "Semanal";
				VMP.ConsultarTipoFrecuencia(txtradio.Text);
				txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
			}
			else if (rdMensual.Checked)
			{
				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = true;
				PanelAnual.Visible = false;
				txtradio.Text = "Mensual";
				VMP.ConsultarTipoFrecuencia(txtradio.Text);
				txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
			}
			else if (rdAnual.Checked)
			{
				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = false;
				PanelAnual.Visible = true;
				txtradio.Text = "Anual";
				VMP.ConsultarTipoFrecuencia(txtradio.Text);
				txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
			}
			else
			{
				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = false;
				PanelAnual.Visible = false;
			}
		}

		protected void btnAceptar_Click(object sender, EventArgs e)
		{
			EditingMode = false;
			bool error = false;
			string Fechainicio = txtFechainicio.Text;
			string Hora = string.Empty;
			string Tolerancia = string.Empty;
			lblErrorDatosGenerales.Text = string.Empty;
			lblErrorDepartamentos.Text = string.Empty;
			lblerrorPeriodicidad.Text = string.Empty;
			frmGrpNombre.RemoveCssClass("has-error");
			frmcbhora.RemoveCssClass("has-error");
			frmHora.RemoveCssClass("has-error");
			frmTolerancia.RemoveCssClass("has-error");

			if (txtNombre.Text.Trim() == string.Empty)
			{
				txtNombre.Focus();
				frmGrpNombre.AddCssClass("has-error");
				error = true;
			}
			string Nombre = txtNombre.Text;
			if (CbHora.Checked)
			{
				if (txtHora.Text.Trim() == string.Empty)
				{
					txtHora.Focus();
					frmHora.AddCssClass("has-error");
					error = true;
				}
				Hora = txtHora.Text;
				if (txtTolerancia.Text.Trim() == string.Empty)
				{
					txtTolerancia.Focus();
					frmTolerancia.AddCssClass("has-error");
					error = true;
				}

				Tolerancia = txtTolerancia.Text;
			}
			else
			{
				Hora = string.Empty;
				Tolerancia = string.Empty;
			}

			if (error)
			{
				lblErrorDatosGenerales.Text = "Algunos campos de datos generales son obligatorios";
				return;
			}

			if (!rdSinperiodicidad.Checked && !rdDiaro.Checked && !rdSemanal.Checked && !rdMensual.Checked && !rdAnual.Checked)
			{
				lblErrorDatosGenerales.Text = "Es necesario que seleccione una periodicidad";
				return;
			}

			if (rdDiaro.Checked)
			{
				if (!rdCadaDiario.Checked && !rdtodoslosdias.Checked)
				{
					lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
					return;
				}
				if (rdCadaDiario.Checked)
				{
					if (txtcadaDiario.Text.Length <= 0)
					{
						lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
						txtcadaDiario.Focus();
						return;
					}
				}
				else
				{
					lblErrorDepartamentos.Text = string.Empty;
				}

			}
			else if (rdSemanal.Checked)
			{
				if (!CBLunes.Checked && !CBMartes.Checked && !CBMiercoles.Checked &&
					!CBJueves.Checked && !CBViernes.Checked && !CBSabado.Checked && !CBDomingo.Checked)
				{
					lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
					return;
				}
				if (txtFrecuenciaSemanal.Text.Length <= 0)
				{
					lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
					txtFrecuenciaSemanal.Focus();
					return;
				}
				else
				{
					lblErrorDatosGenerales.Text = string.Empty;
				}


			}
			else if (rdMensual.Checked)
			{
				if (!rdElDiaMensual.Checked && !rdElMeses.Checked)
				{
					lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
					return;
				}
				if (rdElDiaMensual.Checked)
				{
					if (txtDiasMes.Text.Length <= 0)
					{
						lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
						txtDiasMes.Focus();
						return;
					}
					else if (txtCadames.Text.Length <= 0)
					{
						lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
						txtCadames.Focus();
						return;
					}
					else
					{
						lblErrorDatosGenerales.Text = string.Empty;
					}
				}
				else if (rdElMeses.Checked)
				{
					if (txtcadamensual.Text.Length <= 0)
					{
						lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
						txtcadamensual.Focus();
						return;
					}
					else
					{
						lblErrorDatosGenerales.Text = string.Empty;
					}
				}

			}
			else if (rdAnual.Checked)
			{
				if (txtFrecuenciaAnual.Text.Length <= 0)
				{
					lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
					txtFrecuenciaAnual.Focus();
					return;
				}
				if (!rdElDelAño.Checked && !rdElDelAño2.Checked)
				{
					lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
					return;
				}
				if (rdElDelAño.Checked)
				{
					if (txtDiasmesaño.Text.Length <= 0)
					{
						lblErrorDatosGenerales.Text = "algunos campos de periodicidad son obligatorios";
						txtDiasmesaño.Focus();
						return;
					}
				}
				else
				{
					lblErrorDatosGenerales.Text = string.Empty;
				}
			}

			List<Departamento> departamentos = (List<Departamento>)ViewState["Departamentos"];
			List<Area> areas = (List<Area>)ViewState["Areas"];

			if (!rdDepartamento.Checked && !rdArea.Checked)
			{
				lblErrorDatosGenerales.Text = "algunos campos de asignacion son obligatorios";
				rdDepartamento.Focus();
				return;
			}

			if (departamentos?.Count <= 0 && areas?.Count <= 0)
			{
				lblErrorDatosGenerales.Text = "algunos campos de asignacion son obligatorios";
				return;
			}

			// Validación
			if (cbActivarNotificacion.Checked)
			{
				if (DdMedicion.SelectedItem.Text == "Verdadero/Falso")
				{
					if (!rbFalso.Checked && !rbVerdadero.Checked)
					{
						lblErrorDatosGenerales.Text = "Seleccione al menos una opcion";
						return;
					}
				}
				if (DdMedicion.SelectedItem.Text == "Numerico")
				{
					decimal d;
					if (!string.IsNullOrWhiteSpace(txtMayorQue.Text) && !decimal.TryParse(txtMayorQue.Text, out d))
					{
						lblErrorDatosGenerales.Text = "El valor limite es incorrecto";
						return;
					}
					if (!string.IsNullOrWhiteSpace(txtMenorQue.Text) && !decimal.TryParse(txtMenorQue.Text, out d))
					{
						lblErrorDatosGenerales.Text = "El valor limite es incorrecto";
						return;
					}
				}
			}

			if (cbActivarAntecesor.Checked)
			{
				if (string.IsNullOrWhiteSpace(txtUidAntecesor.Text))
				{
					lblErrorDatosGenerales.Text = "No se ha seleccionado un antecesor.";
					return;
				}
			}

			string Descripcion = txtDescripcion.Text;
			string UidUnidadMedida;
			string FechaFin = txtFechaFin.Text;
			string UidMedicion = DdMedicion.SelectedValue;
			string TipoTarea = DdTipoTarea.SelectedValue;
			string Status = DdStatus.SelectedValue;
			bool foto = false;
			if (CbFoto.Checked)
			{
				foto = true;
			}
			else
			{
				foto = false;
			}

			if (txtUidTarea.Text == string.Empty)
			{
				#region Radio diario
				if (rdSinperiodicidad.Checked)
				{
					VMP.GuardarPeriodicidad("0", txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}
				if (rdCadaDiario.Checked)
				{
					VMP.GuardarPeriodicidad(txtcadaDiario.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}
				else if (rdtodoslosdias.Checked)
				{
					VMP.GuardarPeriodicidad("1", txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}
				#endregion
				#region Mensual
				else if (rdElDiaMensual.Checked)
				{
					VMP.GuardarPeriodicidad(txtCadames.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}
				else if (rdElMeses.Checked)
				{
					VMP.GuardarPeriodicidad(txtcadamensual.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}
				#endregion
				#region Año
				else if (rdElDelAño.Checked)
				{
					VMP.GuardarPeriodicidad(txtFrecuenciaAnual.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}
				else if (rdElDelAño2.Checked)
				{
					VMP.GuardarPeriodicidad(txtFrecuenciaAnual.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}
				#endregion
				#region Semanal 
				else if (rdSemanal.Checked)
				{
					VMP.GuardarPeriodicidad(txtFrecuenciaSemanal.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
				}

				#endregion

				Guid UidPeriodicidad = VMP.Cperiodicidad.UidPeriodicidad;
				if (DdMedicion.SelectedItem.Text == "Numerico")
				{
					UidUnidadMedida = DdUnidadMedida.SelectedValue;
				}
				else
				{
					UidUnidadMedida = string.Empty;
				}

				if (VM.GuardarTarea(Nombre, Descripcion, txtUidAntecesor.Text, UidUnidadMedida,
					UidPeriodicidad, UidMedicion, Hora, Tolerancia, TipoTarea, Status, foto, false, SesionActual.uidSucursalActual.Value))
				{

					lblMensaje.Text = "Guardado Correctamente";
					lblMensaje.Visible = true;
					txtNombre.Text = string.Empty;
					txtDescripcion.Text = string.Empty;
					txtFechainicio.Text = string.Empty;
					txtFechaFin.Text = string.Empty;
					PanelDiario.Visible = false;
					PanelSemanal.Visible = false;
					PanelAnual.Visible = false;
					PanelMensual.Visible = false;
					RadiosChechek();
					dvgDepartamentosGuardar.DataSource = ViewState["Departamentos"];
					dvgDepartamentosGuardar.DataBind();
					dvgAreasGuardar.DataSource = ViewState["Areas"];
					dvgAreasGuardar.DataBind();

					radiosgeneralesdisables();
					PanelDiario.Visible = false;
					PanelSemanal.Visible = false;
					PanelMensual.Visible = false;
					PanelAnual.Visible = false;
					OcultarHora();
					CbHora.Checked = false;
					CbFoto.Checked = false;

				}
				if (txtradio.Text == "Mensual")
				{
					if (rdElDiaMensual.Checked)
					{
						VMP.GuardarPeriodicidadMensual(UidPeriodicidad, txtDiasMes.Text, null);
					}
					else if (rdElMeses.Checked)
					{
						if (DdOrdinalMensual.SelectedItem.Text == "Primer")
						{
							txtIntDiasMes.Text = "1";
						}
						else if (DdOrdinalMensual.SelectedItem.Text == "Segundo")
						{
							txtIntDiasMes.Text = "2";
						}
						else if (DdOrdinalMensual.SelectedItem.Text == "Tercer")
						{
							txtIntDiasMes.Text = "3";
						}
						else if (DdOrdinalMensual.SelectedItem.Text == "Cuarto")
						{
							txtIntDiasMes.Text = "4";
						}
						else if (DdOrdinalMensual.SelectedItem.Text == "Ultimo")
						{
							txtIntDiasMes.Text = "-1";
						}

						if (DdDiasMensual.SelectedItem.Text == "Domingo")
						{
							txtintDiasSemanas.Text = "1";
						}
						else if (DdDiasMensual.SelectedItem.Text == "Lunes")
						{
							txtintDiasSemanas.Text = "2";
						}
						else if (DdDiasMensual.SelectedItem.Text == "Martes")
						{
							txtintDiasSemanas.Text = "3";
						}
						else if (DdDiasMensual.SelectedItem.Text == "Miercoles")
						{
							txtintDiasSemanas.Text = "4";
						}
						else if (DdDiasMensual.SelectedItem.Text == "Jueves")
						{
							txtintDiasSemanas.Text = "5";
						}
						else if (DdDiasMensual.SelectedItem.Text == "Viernes")
						{
							txtintDiasSemanas.Text = "6";
						}
						else if (DdDiasMensual.SelectedItem.Text == "Sabado")
						{
							txtintDiasSemanas.Text = "7";
						}
						VMP.GuardarPeriodicidadMensual(UidPeriodicidad, txtIntDiasMes.Text, txtintDiasSemanas.Text);
					}

				}
				else if (txtradio.Text == "Semanal")
				{
					bool lunes = false;
					bool martes = false;
					bool miercoles = false;
					bool jueves = false;
					bool viernes = false;
					bool sabado = false;
					bool domingo = false;
					if (CBLunes.Checked)
					{
						lunes = true;
					}
					if (CBMartes.Checked)
					{
						martes = true;
					}
					if (CBMiercoles.Checked)
					{
						miercoles = true;
					}
					if (CBJueves.Checked)
					{
						jueves = true;
					}
					if (CBViernes.Checked)
					{
						viernes = true;
					}
					if (CBSabado.Checked)
					{
						sabado = true;
					}
					if (CBDomingo.Checked)
					{
						domingo = true;
					}
					VMP.GuardarPeriodicidadSemanal(UidPeriodicidad, lunes, martes, miercoles, jueves, viernes, sabado, domingo);
				}
				else if (txtradio.Text == "Anual")
				{

					if (rdElDelAño.Checked)
					{
						if (DdMesAño1.SelectedItem.Text == "Enero")
						{
							txtintNumero.Text = "1";
						}
						else if (DdMesAño1.SelectedItem.Text == "Febrero")
						{
							txtintNumero.Text = "2";
						}
						else if (DdMesAño1.SelectedItem.Text == "Febrero")
						{
							txtintNumero.Text = "2";
						}
						else if (DdMesAño1.SelectedItem.Text == "Marzo")
						{
							txtintNumero.Text = "3";
						}
						else if (DdMesAño1.SelectedItem.Text == "Abril")
						{
							txtintNumero.Text = "4";
						}
						else if (DdMesAño1.SelectedItem.Text == "Mayo")
						{
							txtintNumero.Text = "5";
						}
						else if (DdMesAño1.SelectedItem.Text == "Junio")
						{
							txtintNumero.Text = "6";
						}
						else if (DdMesAño1.SelectedItem.Text == "Julio")
						{
							txtintNumero.Text = "7";
						}
						else if (DdMesAño1.SelectedItem.Text == "Agosto")
						{
							txtintNumero.Text = "8";
						}
						else if (DdMesAño1.SelectedItem.Text == "Septiembre")
						{
							txtintNumero.Text = "9";
						}
						else if (DdMesAño1.SelectedItem.Text == "Octubre")
						{
							txtintNumero.Text = "10";
						}
						else if (DdMesAño1.SelectedItem.Text == "Noviembre")
						{
							txtintNumero.Text = "11";
						}
						else if (DdMesAño1.SelectedItem.Text == "Diciembre")
						{
							txtintNumero.Text = "12";
						}
						VMP.GuardarPeriodicidadAnual(UidPeriodicidad, txtDiasmesaño.Text, null, txtintNumero.Text);
					}
					else if (rdElDelAño2.Checked)
					{
						if (DdOrdinalAño.SelectedItem.Text == "Primer")
						{
							txtIntDiasMes.Text = "1";
						}
						else if (DdOrdinalAño.SelectedItem.Text == "Segundo")
						{
							txtIntDiasMes.Text = "2";
						}
						else if (DdOrdinalAño.SelectedItem.Text == "Tercer")
						{
							txtIntDiasMes.Text = "3";
						}
						else if (DdOrdinalAño.SelectedItem.Text == "Cuarto")
						{
							txtIntDiasMes.Text = "4";
						}
						else if (DdOrdinalAño.SelectedItem.Text == "Ultimo")
						{
							txtIntDiasMes.Text = "-1";
						}

						if (DdDiasAño.SelectedItem.Text == "Domingo")
						{
							txtintDiasSemanas.Text = "1";
						}
						else if (DdDiasAño.SelectedItem.Text == "Lunes")
						{
							txtintDiasSemanas.Text = "2";
						}
						else if (DdDiasAño.SelectedItem.Text == "Martes")
						{
							txtintDiasSemanas.Text = "3";
						}
						else if (DdDiasAño.SelectedItem.Text == "Miercoles")
						{
							txtintDiasSemanas.Text = "4";
						}
						else if (DdDiasAño.SelectedItem.Text == "Jueves")
						{
							txtintDiasSemanas.Text = "5";
						}
						else if (DdDiasAño.SelectedItem.Text == "Viernes")
						{
							txtintDiasSemanas.Text = "6";
						}
						else if (DdDiasAño.SelectedItem.Text == "Sabado")
						{
							txtintDiasSemanas.Text = "7";
						}


						if (DdMesAño2.SelectedItem.Text == "Enero")
						{
							txtintNumero.Text = "1";
						}
						else if (DdMesAño2.SelectedItem.Text == "Febrero")
						{
							txtintNumero.Text = "2";
						}
						else if (DdMesAño2.SelectedItem.Text == "Febrero")
						{
							txtintNumero.Text = "2";
						}
						else if (DdMesAño2.SelectedItem.Text == "Marzo")
						{
							txtintNumero.Text = "3";
						}
						else if (DdMesAño2.SelectedItem.Text == "Abril")
						{
							txtintNumero.Text = "4";
						}
						else if (DdMesAño2.SelectedItem.Text == "Mayo")
						{
							txtintNumero.Text = "5";
						}
						else if (DdMesAño2.SelectedItem.Text == "Junio")
						{
							txtintNumero.Text = "6";
						}
						else if (DdMesAño2.SelectedItem.Text == "Julio")
						{
							txtintNumero.Text = "7";
						}
						else if (DdMesAño2.SelectedItem.Text == "Agosto")
						{
							txtintNumero.Text = "8";
						}
						else if (DdMesAño2.SelectedItem.Text == "Septiembre")
						{
							txtintNumero.Text = "9";
						}
						else if (DdMesAño2.SelectedItem.Text == "Octubre")
						{
							txtintNumero.Text = "10";
						}
						else if (DdMesAño2.SelectedItem.Text == "Noviembre")
						{
							txtintNumero.Text = "11";
						}
						else if (DdMesAño2.SelectedItem.Text == "Diciembre")
						{
							txtintNumero.Text = "12";
						}

						VMP.GuardarPeriodicidadAnual(UidPeriodicidad, txtIntDiasMes.Text, txtintDiasSemanas.Text, txtintNumero.Text);
					}


				}

				Guid UidTarea = VM.CTarea.UidTarea;
				if (rdDepartamento.Checked)
				{
					VM.GuardarDepartamentosTareas(departamentos, UidTarea);
				}
				else if (rdArea.Checked)
				{
					VM.GuardarAreaTareas(areas, UidTarea);
				}

				List<TareaOpcion> opciones = (List<TareaOpcion>)ViewState["Opciones"];
				VM.GuardarOpciones(opciones, UidTarea);

				// Guardar notificaciones
				if (cbActivarNotificacion.Checked)
				{
					Notificacion notificacion = new Notificacion();
					if (DdMedicion.SelectedItem.Text == "Verdadero/Falso")
					{
						notificacion.BlValor = rbVerdadero.Checked;
					}
					else if (DdMedicion.SelectedItem.Text == "Numerico")
					{
						if (!string.IsNullOrWhiteSpace(txtMenorQue.Text))
						{
							notificacion.DcMenorQue = Convert.ToDecimal(txtMenorQue.Text);
							notificacion.BlMenorIgual = rbMenorIgual.Checked;
						}
						if (!string.IsNullOrWhiteSpace(txtMayorQue.Text))
						{
							notificacion.DcMayorQue = Convert.ToDecimal(txtMayorQue.Text);
							notificacion.BlMayorIgual = rbMayorIgual.Checked;
						}
					}
					else if (DdMedicion.SelectedItem.Text == "Seleccionable")
					{
						VM.ObtenerOpciones(VM.CTarea.UidTarea);
						List<TareaOpcion> opcionesGuardadas = VM.Opciones;

						string ops = "";

						int[] indexes = lbOpcionesSeleccionadas.GetSelectedIndices();

						// Obtener identificadores actualizados
						foreach (TareaOpcion op in opcionesGuardadas)
						{
							for (int i = 0; i < indexes.Length; i++)
							{
								if (op.StrOpciones == lbOpcionesSeleccionadas.Items[indexes[i]].Text)
								{
									if (ops == "")
										ops = op.UidOpciones.ToString();
									else
										ops += "," + op.UidOpciones.ToString();

									continue;
								}
							}
						}

						notificacion.VchOpciones = ops;
					}

					notificacion.UidTarea = VM.CTarea.UidTarea;
					VM.GuardarNotificacion(notificacion);
				}

				// Guardar la información de Antecesor
				if (!string.IsNullOrWhiteSpace(txtUidAntecesor.Text) && cbActivarAntecesor.Checked)
				{
					Antecesor antecesor = new Antecesor();
					antecesor.IntDiasDespues = Convert.ToInt32(txtDiasDespues.Text);
					if (antecesorNotificacion.Visible && rdNumeroLimitado.Checked)
					{
						antecesor.IntRepeticion = Convert.ToInt32(txtMaximo.Text);
					}
					antecesor.BlUsarNotificacion = cbUsarNotificacion.Checked;
					antecesor.UidTarea = VM.CTarea.UidTarea;
					antecesor.UidTareaAnterior = new Guid(txtUidAntecesor.Text);
					VM.GuardarAntecesor(antecesor);
				}

				rdArea.Checked = false;
				rdDepartamento.Checked = false;
			}
			else
			{
				VM.ObtenerTarea(new Guid(txtUidTarea.Text));
				string medicion = VM.CTarea.StrTipoMedicion;
				if (DdMedicion.SelectedItem.Text == medicion)
				{
					if (VM.ModificarTarea(txtUidTarea.Text, Nombre, Descripcion, Hora, Tolerancia, TipoTarea, Status, foto))
					{
						if (VMP.ModificarPeriodicidad(txtUidPeriodicidad.Text, FechaFin))
						{
							lblMensaje.Text = "Modificado Correctamente";
							lblMensaje.Visible = true;
						}
						btnEditar.Enable();
					}
					List<TareaOpcion> opciones = (List<TareaOpcion>)ViewState["Opciones"];
					VM.GuardarOpciones(opciones, new Guid(txtUidTarea.Text));
				}
				else
				{
					Guid tarea = new Guid(txtUidTarea.Text);
					VM.ModificarCaducado(tarea, true);
					#region Radio diario
					if (rdSinperiodicidad.Checked)
					{
						VMP.GuardarPeriodicidad(null, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}
					if (rdCadaDiario.Checked)
					{
						VMP.GuardarPeriodicidad(txtcadaDiario.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}
					else if (rdtodoslosdias.Checked)
					{
						VMP.GuardarPeriodicidad("1", txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}
					#endregion
					#region Mensual
					else if (rdElDiaMensual.Checked)
					{
						VMP.GuardarPeriodicidad(txtCadames.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}
					else if (rdElMeses.Checked)
					{
						VMP.GuardarPeriodicidad(txtcadamensual.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}
					#endregion
					#region Año
					else if (rdElDelAño.Checked)
					{
						VMP.GuardarPeriodicidad(txtFrecuenciaAnual.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}
					else if (rdElDelAño2.Checked)
					{
						VMP.GuardarPeriodicidad(txtFrecuenciaAnual.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}
					#endregion
					#region Semanal 
					else if (rdSemanal.Checked)
					{
						VMP.GuardarPeriodicidad(txtFrecuenciaSemanal.Text, txtUidTipoFrecuencia.Text, txtFechainicio.Text, FechaFin);
					}

					#endregion

					Guid UidPeriodicidad = VMP.Cperiodicidad.UidPeriodicidad;
					if (DdMedicion.SelectedItem.Text == "Numerico")
					{
						UidUnidadMedida = DdUnidadMedida.SelectedValue;
					}
					else
					{
						UidUnidadMedida = string.Empty;
					}

					if (VM.GuardarTarea(Nombre, Descripcion, txtUidAntecesor.Text, UidUnidadMedida,
						UidPeriodicidad, UidMedicion, Hora, Tolerancia, TipoTarea, Status, foto, false, SesionActual.uidSucursalActual.Value))
					{
						lblMensaje.Text = "Modificado Correctamente";
						lblMensaje.Visible = true;
					}

					if (txtradio.Text == "Mensual")
					{
						if (rdElDiaMensual.Checked)
						{
							VMP.GuardarPeriodicidadMensual(UidPeriodicidad, txtDiasMes.Text, null);
						}
						else if (rdElMeses.Checked)
						{
							if (DdOrdinalMensual.SelectedItem.Text == "Primer")
							{
								txtIntDiasMes.Text = "1";
							}
							else if (DdOrdinalMensual.SelectedItem.Text == "Segundo")
							{
								txtIntDiasMes.Text = "2";
							}
							else if (DdOrdinalMensual.SelectedItem.Text == "Tercer")
							{
								txtIntDiasMes.Text = "3";
							}
							else if (DdOrdinalMensual.SelectedItem.Text == "Cuarto")
							{
								txtIntDiasMes.Text = "4";
							}
							else if (DdOrdinalMensual.SelectedItem.Text == "Ultimo")
							{
								txtIntDiasMes.Text = "-1";
							}

							if (DdDiasMensual.SelectedItem.Text == "Domingo")
							{
								txtintDiasSemanas.Text = "1";
							}
							else if (DdDiasMensual.SelectedItem.Text == "Lunes")
							{
								txtintDiasSemanas.Text = "2";
							}
							else if (DdDiasMensual.SelectedItem.Text == "Martes")
							{
								txtintDiasSemanas.Text = "3";
							}
							else if (DdDiasMensual.SelectedItem.Text == "Miercoles")
							{
								txtintDiasSemanas.Text = "4";
							}
							else if (DdDiasMensual.SelectedItem.Text == "Jueves")
							{
								txtintDiasSemanas.Text = "5";
							}
							else if (DdDiasMensual.SelectedItem.Text == "Viernes")
							{
								txtintDiasSemanas.Text = "6";
							}
							else if (DdDiasMensual.SelectedItem.Text == "Sabado")
							{
								txtintDiasSemanas.Text = "7";
							}
							VMP.GuardarPeriodicidadMensual(UidPeriodicidad, txtIntDiasMes.Text, txtintDiasSemanas.Text);
						}

					}
					else if (txtradio.Text == "Semanal")
					{
						bool lunes = false;
						bool martes = false;
						bool miercoles = false;
						bool jueves = false;
						bool viernes = false;
						bool sabado = false;
						bool domingo = false;
						if (CBLunes.Checked)
						{
							lunes = true;
						}
						if (CBMartes.Checked)
						{
							martes = true;
						}
						if (CBMiercoles.Checked)
						{
							miercoles = true;
						}
						if (CBJueves.Checked)
						{
							jueves = true;
						}
						if (CBViernes.Checked)
						{
							viernes = true;
						}
						if (CBSabado.Checked)
						{
							sabado = true;
						}
						if (CBDomingo.Checked)
						{
							domingo = true;
						}
						VMP.GuardarPeriodicidadSemanal(UidPeriodicidad, lunes, martes, miercoles, jueves, viernes, sabado, domingo);
					}
					else if (txtradio.Text == "Anual")
					{

						if (rdElDelAño.Checked)
						{
							if (DdMesAño1.SelectedItem.Text == "Enero")
							{
								txtintNumero.Text = "1";
							}
							else if (DdMesAño1.SelectedItem.Text == "Febrero")
							{
								txtintNumero.Text = "2";
							}
							else if (DdMesAño1.SelectedItem.Text == "Febrero")
							{
								txtintNumero.Text = "2";
							}
							else if (DdMesAño1.SelectedItem.Text == "Marzo")
							{
								txtintNumero.Text = "3";
							}
							else if (DdMesAño1.SelectedItem.Text == "Abril")
							{
								txtintNumero.Text = "4";
							}
							else if (DdMesAño1.SelectedItem.Text == "Mayo")
							{
								txtintNumero.Text = "5";
							}
							else if (DdMesAño1.SelectedItem.Text == "Junio")
							{
								txtintNumero.Text = "6";
							}
							else if (DdMesAño1.SelectedItem.Text == "Julio")
							{
								txtintNumero.Text = "7";
							}
							else if (DdMesAño1.SelectedItem.Text == "Agosto")
							{
								txtintNumero.Text = "8";
							}
							else if (DdMesAño1.SelectedItem.Text == "Septiembre")
							{
								txtintNumero.Text = "9";
							}
							else if (DdMesAño1.SelectedItem.Text == "Octubre")
							{
								txtintNumero.Text = "10";
							}
							else if (DdMesAño1.SelectedItem.Text == "Noviembre")
							{
								txtintNumero.Text = "11";
							}
							else if (DdMesAño1.SelectedItem.Text == "Diciembre")
							{
								txtintNumero.Text = "12";
							}
							VMP.GuardarPeriodicidadAnual(UidPeriodicidad, txtDiasmesaño.Text, null, txtintNumero.Text);
						}
						else if (rdElDelAño2.Checked)
						{
							if (DdOrdinalAño.SelectedItem.Text == "Primer")
							{
								txtIntDiasMes.Text = "1";
							}
							else if (DdOrdinalAño.SelectedItem.Text == "Segundo")
							{
								txtIntDiasMes.Text = "2";
							}
							else if (DdOrdinalAño.SelectedItem.Text == "Tercer")
							{
								txtIntDiasMes.Text = "3";
							}
							else if (DdOrdinalAño.SelectedItem.Text == "Cuarto")
							{
								txtIntDiasMes.Text = "4";
							}
							else if (DdOrdinalAño.SelectedItem.Text == "Ultimo")
							{
								txtIntDiasMes.Text = "-1";
							}

							if (DdDiasAño.SelectedItem.Text == "Domingo")
							{
								txtintDiasSemanas.Text = "1";
							}
							else if (DdDiasAño.SelectedItem.Text == "Lunes")
							{
								txtintDiasSemanas.Text = "2";
							}
							else if (DdDiasAño.SelectedItem.Text == "Martes")
							{
								txtintDiasSemanas.Text = "3";
							}
							else if (DdDiasAño.SelectedItem.Text == "Miercoles")
							{
								txtintDiasSemanas.Text = "4";
							}
							else if (DdDiasAño.SelectedItem.Text == "Jueves")
							{
								txtintDiasSemanas.Text = "5";
							}
							else if (DdDiasAño.SelectedItem.Text == "Viernes")
							{
								txtintDiasSemanas.Text = "6";
							}
							else if (DdDiasAño.SelectedItem.Text == "Sabado")
							{
								txtintDiasSemanas.Text = "7";
							}


							if (DdMesAño2.SelectedItem.Text == "Enero")
							{
								txtintNumero.Text = "1";
							}
							else if (DdMesAño2.SelectedItem.Text == "Febrero")
							{
								txtintNumero.Text = "2";
							}
							else if (DdMesAño2.SelectedItem.Text == "Febrero")
							{
								txtintNumero.Text = "2";
							}
							else if (DdMesAño2.SelectedItem.Text == "Marzo")
							{
								txtintNumero.Text = "3";
							}
							else if (DdMesAño2.SelectedItem.Text == "Abril")
							{
								txtintNumero.Text = "4";
							}
							else if (DdMesAño2.SelectedItem.Text == "Mayo")
							{
								txtintNumero.Text = "5";
							}
							else if (DdMesAño2.SelectedItem.Text == "Junio")
							{
								txtintNumero.Text = "6";
							}
							else if (DdMesAño2.SelectedItem.Text == "Julio")
							{
								txtintNumero.Text = "7";
							}
							else if (DdMesAño2.SelectedItem.Text == "Agosto")
							{
								txtintNumero.Text = "8";
							}
							else if (DdMesAño2.SelectedItem.Text == "Septiembre")
							{
								txtintNumero.Text = "9";
							}
							else if (DdMesAño2.SelectedItem.Text == "Octubre")
							{
								txtintNumero.Text = "10";
							}
							else if (DdMesAño2.SelectedItem.Text == "Noviembre")
							{
								txtintNumero.Text = "11";
							}
							else if (DdMesAño2.SelectedItem.Text == "Diciembre")
							{
								txtintNumero.Text = "12";
							}

							VMP.GuardarPeriodicidadAnual(UidPeriodicidad, txtIntDiasMes.Text, txtintDiasSemanas.Text, txtintNumero.Text);
						}
					}

					Guid UidTarea = VM.CTarea.UidTarea;
					if (rdDepartamento.Checked)
					{
						VM.GuardarDepartamentosTareas(departamentos, UidTarea);
					}
					else if (rdArea.Checked)
					{
						VM.GuardarAreaTareas(areas, UidTarea);
					}

					List<TareaOpcion> opciones = (List<TareaOpcion>)ViewState["Opciones"];
					VM.GuardarOpciones(opciones, UidTarea);

					rdArea.Checked = false;
					rdDepartamento.Checked = false;

					VM.ActualizarCumplimiento(UidTarea, new Guid(txtUidTarea.Text));
				}

				// Guardar notificaciones
				if (cbActivarNotificacion.Checked)
				{
					Notificacion notificacion = new Notificacion();
					if (DdMedicion.SelectedItem.Text == "Verdadero/Falso")
					{
						notificacion.BlValor = rbVerdadero.Checked;
					}
					else if (DdMedicion.SelectedItem.Text == "Numerico")
					{
						if (!string.IsNullOrWhiteSpace(txtMenorQue.Text))
						{
							notificacion.DcMenorQue = Convert.ToDecimal(txtMenorQue.Text);
							notificacion.BlMenorIgual = rbMenorIgual.Checked;
						}
						if (!string.IsNullOrWhiteSpace(txtMayorQue.Text))
						{
							notificacion.DcMayorQue = Convert.ToDecimal(txtMayorQue.Text);
							notificacion.BlMayorIgual = rbMayorIgual.Checked;
						}
					}
					else if (DdMedicion.SelectedItem.Text == "Seleccionable")
					{
						VM.ObtenerOpciones(VM.CTarea.UidTarea);
						List<TareaOpcion> opcionesGuardadas = VM.Opciones;

						string ops = "";

						int[] indexes = lbOpcionesSeleccionadas.GetSelectedIndices();

						// Obtener identificadores actualizados
						foreach (TareaOpcion op in opcionesGuardadas)
						{
							for (int i = 0; i < indexes.Length; i++)
							{
								if (op.StrOpciones == lbOpcionesSeleccionadas.Items[indexes[i]].Text)
								{
									if (ops == "")
										ops = op.UidOpciones.ToString();
									else
										ops += "," + op.UidOpciones.ToString();

									continue;
								}
							}
						}

						notificacion.VchOpciones = ops;
					}

					notificacion.UidTarea = VM.CTarea.UidTarea;
					VM.GuardarNotificacion(notificacion);
				}

				// Guardar la información de Antecesor
				if (!string.IsNullOrWhiteSpace(txtUidAntecesor.Text) && cbActivarNotificacion.Checked)
				{
					Antecesor antecesor = new Antecesor();
					antecesor.IntDiasDespues = Convert.ToInt32(txtDiasDespues.Text);
					if (antecesorNotificacion.Visible && rdNumeroLimitado.Checked)
					{
						antecesor.IntRepeticion = Convert.ToInt32(txtMaximo.Text);
					}
					antecesor.BlUsarNotificacion = cbUsarNotificacion.Checked;
					antecesor.UidTarea = VM.CTarea.UidTarea;
					antecesor.UidTareaAnterior = new Guid(txtUidAntecesor.Text);
					VM.GuardarAntecesor(antecesor);
				}

				rdArea.Checked = false;
				rdDepartamento.Checked = false;
			}
			CbFoto.Disable();
			txtNombre.Disable();
			txtDescripcion.Disable();
			DdUnidadMedida.Disable();
			txtNombreAntecesor.Disable();
			txtNombreDepartamento.Disable();
			txtFechaFin.Disable();
			DdTipoTarea.Disable();
			DdStatus.Disable();
			rdDepartamento.Disable();
			rdArea.Disable();
			btnAceptar.Visible = false;
			btnCancelar.Visible = false;
			btnBuscarDepartamentos.Disable();
			btnBuscarDepartamentos.Text = "Buscar Departamentos";
			btnBuscarAntecesor.Disable();
			btnBuscarAntecesor.Text = "Buscar Antecesor";
			PanelFiltros.Visible = false;

			btnNuevo.Enable();
			dgvOpciones.DataSource = null;
			dgvOpciones.DataBind();
			ViewState["DepartamentosPreviousRow"] = null;
			dgvDepartamentos.DataSource = null;
			dgvDepartamentos.DataBind();
			ViewState["AreaPreviousRow"] = null;
			dvgArea.DataSource = null;
			dvgArea.DataBind();
			DdMedicion.Disable();
			RadiosChechek();
			btnAgregarOpcion.Disable();
			txtFechainicio.Disable();
			CbHora.Disable();
			txtTolerancia.Disable();
			txtHora.Disable();
			DdUnidadMedida.Disable();
			ViewState["Departamentos"] = new List<Departamento>();
			dvgDepartamentosGuardar.DataSource = ViewState["Departamentos"];
			dvgDepartamentosGuardar.DataBind();
			ViewState["Areas"] = new List<Area>();
			dvgAreasGuardar.DataSource = ViewState["Areas"];
			dvgAreasGuardar.DataBind();
			btnEliminarOpcion.Disable();
			btnEditarOpcion.Disable();
			btnEliminarDepartamento.Visible = false;

			rbMenor.Disable();
			rbMenorIgual.Disable();
			rbMayor.Disable();
			rbMayorIgual.Disable();
			txtMayorQue.Disable();
			txtMenorQue.Disable();
			rbVerdadero.Disable();
			rbFalso.Disable();
			txtDiasDespues.Disable();
			txtMaximo.Disable();
			cbActivarNotificacion.Disable();
			cbActivarAntecesor.Disable();
			cbUsarNotificacion.Disable();
			rdNumeroLimitado.Disable();
			rdIlimitado.Disable();

			btnBuscar_Click(sender, e);
		}

		protected void btnEditar_Click(object sender, EventArgs e)
		{
			EditingMode = true;
			txtNombre.Enable();
			txtDescripcion.Enable();
			txtFechaFin.Enable();
			CbHora.Enable();
			btnEditar.Disable();
			btnNuevo.Disable();
			btnAceptar.Visible = true;
			btnCancelar.Visible = true;
			txtHora.Enable();
			txtTolerancia.Enable();
			DdTipoTarea.Enable();
			DdStatus.Enable();
			CbFoto.Enable();
			DdMedicion.Enable();
			DdUnidadMedida.Enable();
			btnAgregarOpcion.Enable();

			if (txtUidOpcion.Text.Length > 0)
			{
				btnEditarOpcion.Enable();
				btnEliminarOpcion.Enable();
			}

			// No se puede cambiar nada de antecesor
			// De notificacion solo cambiar los valores pero no deshabilitar
			cbActivarNotificacion.Disable();
			rbVerdadero.Enable();
			rbFalso.Enable();
			rbMenor.Enable();
			rbMenorIgual.Enable();
			rbMayor.Enable();
			rbMayorIgual.Enable();
			txtMenorQue.Enable();
			txtMayorQue.Enable();
			lbOpcionesSeleccionadas.Enable();
		}

		protected void DdMedicion_SelectedIndexChanged(object sender, EventArgs e)
		{

			if (DdMedicion.SelectedItem.Text == "Numerico")
			{
				DdUnidadMedida.Visible = true;
				nombreunidad.Visible = true;
				activeOpciones.Visible = false;
			}
			else if (DdMedicion.SelectedItem.Text == "Seleccionable")
			{
				activeOpciones.Visible = true;
				DdUnidadMedida.Visible = false;
				nombreunidad.Visible = false;
			}
			else if (DdMedicion.SelectedItem.Text == "Verdadero/Falso")
			{
				activeOpciones.Visible = false;
				DdUnidadMedida.Visible = false;
				nombreunidad.Visible = false;
			}
		}

		#endregion

		#region Busqueda de Tareas

		protected void DVGTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ViewState["TareaPreviousRow"] = null;
			if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
			{
				string SortExpression = (string)ViewState["SortColumn"];
				SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
				SortTareas(SortExpression, SortDirection, true);
			}
			else
			{
				DVGTareas.DataSource = ViewState["Tarea"];
			}
			DVGTareas.PageIndex = e.NewPageIndex;
			DVGTareas.DataBind();
		}

		protected void DVGTareas_SelectedIndexChanged(object sender, EventArgs e)
		{
			tabDatosGenerales_Click(sender, e);
			btnEditar.Enable();
			btnNuevo.Enable();
			RadiosdentroDisable();
			txtdisable();
			btnEliminarDepartamento.Visible = false;
			btnEliminarOpcion.Disable();
			btnEditarOpcion.Disable();
			btnAceptar.Visible = false;
			btnCancelar.Visible = false;
			txtNombre.Disable();
			txtFechainicio.Disable();
			txtFechaFin.Disable();
			DdTipoTarea.Disable();
			DdStatus.Disable();
			txtDescripcion.Disable();
			DdUnidadMedida.Enable();
			radiosgeneralesdisables();
			btnBuscarAntecesor.Disable();
			txtNombreAntecesor.Disable();
			txtNombreDepartamento.Disable();
			btnBuscarDepartamentos.Disable();
			txtHora.Disable();
			txtTolerancia.Disable();
			CbHora.Disable();
			DdUnidadMedida.Disable();
			lblMensaje.Visible = false;
			string UidTarea = DVGTareas.SelectedDataKey.Value.ToString();
			VM.ObtenerTarea(new Guid(UidTarea));
			txtUidTarea.Text = VM.CTarea.UidTarea.ToString();
			txtNombre.Text = VM.CTarea.StrNombre;
			txtDescripcion.Text = VM.CTarea.StrDescripcion;
			txtUidAntecesor.Text = VM.CTarea.UidAntecesor.ToString();
			DdMedicion.Disable();
			txtHora.Text = VM.CTarea.TmHora.HasValue ? VM.CTarea.TmHora.Value.ToString("hh\\:mm") : "";
			//txtHora.Text = VM.CTarea.TmHora.ToString();
			txtTolerancia.Text = VM.CTarea.IntTolerancia.ToString();
			DdTipoTarea.SelectedIndex = DdTipoTarea.Items.IndexOf(DdTipoTarea.Items.FindByValue(VM.CTarea.UidTipoTarea.ToString()));
			DdStatus.SelectedIndex = DdStatus.Items.IndexOf(DdStatus.Items.FindByValue(VM.CTarea.UidStatus.ToString()));
			#region MyRegion

			if (VM.CTarea.BlFoto == true)
			{
				CbFoto.Checked = true;
			}
			else
			{
				CbFoto.Checked = false;
			}
			if (txtHora.Text.Length > 0)
			{
				CbHora.Checked = true;
				MirarHora();
			}
			else
			{
				OcultarHora();
				CbHora.Checked = false;
			}

			if (VM.CTarea.UidAntecesor != null)
			{
				VM.CargarAntecesor(txtUidAntecesor.Text);
				txtUidAntecesor.Text = VM.CAntecesor.UidTarea.ToString();
				txtNombreAntecesor.Text = VM.CAntecesor.StrNombre;
			}
			else
			{
				txtUidAntecesor.Text = string.Empty;
				txtNombreAntecesor.Text = string.Empty;
			}
			DdUnidadMedida.SelectedIndex = DdUnidadMedida.Items.IndexOf(DdUnidadMedida.Items.FindByValue(VM.CTarea.UidUnidadMedida.ToString()));

			DdMedicion.SelectedIndex = DdMedicion.Items.IndexOf(DdMedicion.Items.FindByValue(VM.CTarea.UidMedicion.ToString()));
			if (DdMedicion.SelectedItem.Text == "Numerico")
			{
				DdUnidadMedida.Visible = true;
				nombreunidad.Visible = true;
				DdUnidadMedida.SelectedIndex = DdUnidadMedida.Items.IndexOf(DdUnidadMedida.Items.FindByValue(VM.CTarea.UidUnidadMedida.ToString()));
				activeOpciones.Visible = false;
			}
			else if (DdMedicion.SelectedItem.Text == "Seleccionable")
			{
				activeOpciones.Visible = true;
				DdUnidadMedida.Visible = false;
				nombreunidad.Visible = false;
			}
			else if (DdMedicion.SelectedItem.Text == "Verdadero/Falso")
			{
				activeOpciones.Visible = false;
				DdUnidadMedida.Visible = false;
				nombreunidad.Visible = false;
			}


			VM.ObtenerDepartamentos();

			VM.ObtenerAreas();
			if (VM.Departamentos.Count == 0 && VM.Areas.Count > 0)
			{
				ViewState["Areas"] = VM.Areas;
				dvgAreasGuardar.DataSource = VM.Areas;
				dvgAreasGuardar.DataBind();
				//PanelAreasGuardar.Visible = true;
				//PanelDepartamentosGuardar.Visible = false;
				rdArea.Checked = true;
				rdDepartamento.Checked = false;
			}
			else
			{
				ViewState["Departamentos"] = VM.Departamentos;
				dvgDepartamentosGuardar.DataSource = VM.Departamentos;
				dvgDepartamentosGuardar.DataBind();
				//PanelDepartamentosGuardar.Visible = true;
				//PanelAreasGuardar.Visible = false;
				rdDepartamento.Checked = true;
				rdArea.Checked = false;
			}

			Guid UidPeriodicidad = VM.CTarea.UidPeriodicidad;
			txtUidPeriodicidad.Text = VM.CTarea.UidPeriodicidad.ToString();
			VMP.ObtenerPeriodicidad(UidPeriodicidad);

			Guid UidTipoFrecuencia = VMP.Cperiodicidad.UidTipoFrecuencia;
			VMP.ObtenerTipoFrecuencia(UidTipoFrecuencia);
			string tipo = VMP.CTipoFrecuencia.StrTipoFrecuencia;

			VMP.ObtenerPeriodicidadMensual(UidPeriodicidad);

			VMP.ObtenerPeriodicidadAnual(UidPeriodicidad);

			VMP.ObtenerPeriodicidadSemanal(UidPeriodicidad);

			if (tipo == "Sin Periodicidad")
			{

				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = false;
				PanelAnual.Visible = false;
				rdDiaro.Checked = false;
				rdAnual.Checked = false;
				rdSemanal.Checked = false;
				rdMensual.Checked = false;
				rdSinperiodicidad.Checked = true;

			}
			else if (tipo == "Diaria")
			{
				PanelDiario.Visible = true;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = false;
				PanelAnual.Visible = false;
				rdDiaro.Checked = true;
				rdAnual.Checked = false;
				rdSemanal.Checked = false;
				rdMensual.Checked = false;
				if (VMP.Cperiodicidad.IntFrecuencia == 1)
				{
					rdtodoslosdias.Checked = true;
					rdCadaDiario.Checked = false;
				}
				else
				{
					rdtodoslosdias.Checked = false;
					rdCadaDiario.Checked = true;
					txtcadaDiario.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
				}
			}
			else if (tipo == "Semanal")
			{
				PanelSemanal.Visible = true;
				PanelDiario.Visible = false;
				PanelMensual.Visible = false;
				PanelAnual.Visible = false;
				rdSemanal.Checked = true;
				rdDiaro.Checked = false;
				rdAnual.Checked = false;
				rdMensual.Checked = false;

				if (VMP.CPeriodicidadSemanal.BlLunes)
				{
					CBLunes.Checked = true;
				}
				else
				{
					CBLunes.Checked = false;
				}

				if (VMP.CPeriodicidadSemanal.BlMartes)
				{
					CBMartes.Checked = true;
				}
				else
				{
					CBMartes.Checked = false;
				}
				if (VMP.CPeriodicidadSemanal.BlMiercoles)
				{
					CBMiercoles.Checked = true;
				}
				else
				{
					CBMiercoles.Checked = false;
				}
				if (VMP.CPeriodicidadSemanal.BlJueves)
				{
					CBJueves.Checked = true;
				}
				else
				{
					CBJueves.Checked = false;
				}
				if (VMP.CPeriodicidadSemanal.BlViernes)
				{
					CBViernes.Checked = true;
				}
				else
				{
					CBViernes.Checked = false;
				}
				if (VMP.CPeriodicidadSemanal.BlSabado)
				{
					CBSabado.Checked = true;
				}
				else
				{
					CBSabado.Checked = false;
				}
				if (VMP.CPeriodicidadSemanal.BlDomingo)
				{
					CBDomingo.Checked = true;
				}
				else
				{
					CBDomingo.Checked = false;
				}
				txtFrecuenciaSemanal.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
			}
			else if (tipo == "Mensual")
			{
				PanelMensual.Visible = true;
				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelAnual.Visible = false;
				rdMensual.Checked = true;
				rdDiaro.Checked = false;
				rdSemanal.Checked = false;
				rdAnual.Checked = false;
				if (VMP.CPeriodicidadMensual.IntDiasSemana == 0)
				{
					rdElDiaMensual.Checked = true;
					rdElMeses.Checked = false;
					txtDiasMes.Text = VMP.CPeriodicidadMensual.IntDiasMes.ToString();
					txtCadames.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
					txtcadamensual.Text = string.Empty;
				}
				else
				{
					rdElDiaMensual.Checked = false;
					rdElMeses.Checked = true;
					txtcadamensual.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
					txtDiasMes.Text = string.Empty;
					txtCadames.Text = string.Empty;
					#region Ordinal

					if (VMP.CPeriodicidadMensual.IntDiasMes == 1)
					{

						string Ordinal = "Primer";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();

					}
					else if (VMP.CPeriodicidadMensual.IntDiasMes == 2)
					{
						string Ordinal = "Segundo";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasMes == 3)
					{
						string Ordinal = "Tercer";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasMes == 4)
					{
						string Ordinal = "Cuarto";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasMes == -1)
					{
						string Ordinal = "Ultimo";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalMensual.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}
					#endregion

					if (VMP.CPeriodicidadMensual.IntDiasSemana == 1)
					{
						string dias = "Domingo";
						VMP.ObtenerDias(dias);
						DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasSemana == 2)
					{
						string Ordinal = "Lunes";
						VMP.ObtenerDias(Ordinal);
						DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasSemana == 3)
					{
						string Ordinal = "Martes";
						VMP.ObtenerDias(Ordinal);
						DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasSemana == 4)
					{
						string Ordinal = "Miercoles";
						VMP.ObtenerDias(Ordinal);
						DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasSemana == 5)
					{
						string Ordinal = "Jueves";
						VMP.ObtenerDias(Ordinal);
						DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasSemana == 6)
					{
						string Ordinal = "Viernes";
						VMP.ObtenerDias(Ordinal);
						DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadMensual.IntDiasSemana == 7)
					{
						string Ordinal = "Sabado";
						VMP.ObtenerDias(Ordinal);
						DdDiasMensual.SelectedValue = VMP.CDias.UidDias.ToString();
					}

				}
			}
			else if (tipo == "Anual")
			{
				PanelAnual.Visible = true;
				PanelDiario.Visible = false;
				PanelSemanal.Visible = false;
				PanelMensual.Visible = false;
				rdAnual.Checked = true;
				rdDiaro.Checked = false;
				rdSemanal.Checked = false;
				rdMensual.Checked = false;
				txtFrecuenciaAnual.Text = VMP.Cperiodicidad.IntFrecuencia.ToString();
				if (VMP.CPeriodicidadAnual.IntDiasSemanas == 0)
				{
					rdElDelAño.Checked = true;
					rdElDelAño2.Checked = false;
					txtDiasmesaño.Text = VMP.CPeriodicidadAnual.IntDiasMes.ToString();

					if (VMP.CPeriodicidadAnual.IntNumero == 1)
					{
						string mes = "Enero";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 2)
					{
						string mes = "Febrero";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 3)
					{
						string mes = "Marzo";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 4)
					{
						string mes = "Abril";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 5)
					{
						string mes = "Mayo";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 6)
					{
						string mes = "Junio";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 7)
					{
						string mes = "Julio";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 8)
					{
						string mes = "Agosto";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 9)
					{
						string mes = "Septiembre";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 10)
					{
						string mes = "Octubre";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 11)
					{
						string mes = "Noviembre";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();

					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 12)
					{
						string mes = "Diciembre";
						VMP.ObtenerMeses(mes);
						DdMesAño1.SelectedValue = VMP.CMeses.UidMes.ToString();
					}

				}
				else
				{
					rdElDelAño.Checked = false;
					rdElDelAño2.Checked = true;
					txtDiasmesaño.Text = string.Empty;
					if (VMP.CPeriodicidadAnual.IntDiasMes == 1)
					{

						string Ordinal = "Primer";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();

					}
					else if (VMP.CPeriodicidadAnual.IntDiasMes == 2)
					{
						string Ordinal = "Segundo";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasMes == 3)
					{
						string Ordinal = "Tercer";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasMes == 4)
					{
						string Ordinal = "Cuarto";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasMes == -1)
					{
						string Ordinal = "Ultimo";
						VMP.ConsultarOrdinal(Ordinal);
						DdOrdinalAño.SelectedValue = VMP.COrdinal.UidOrdinal.ToString();
					}

					if (VMP.CPeriodicidadAnual.IntDiasSemanas == 1)
					{
						string dias = "Domingo";
						VMP.ObtenerDias(dias);
						DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 2)
					{
						string Ordinal = "Lunes";
						VMP.ObtenerDias(Ordinal);
						DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 3)
					{
						string Ordinal = "Martes";
						VMP.ObtenerDias(Ordinal);
						DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 4)
					{
						string Ordinal = "Miercoles";
						VMP.ObtenerDias(Ordinal);
						DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 5)
					{
						string Ordinal = "Jueves";
						VMP.ObtenerDias(Ordinal);
						DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 6)
					{
						string Ordinal = "Viernes";
						VMP.ObtenerDias(Ordinal);
						DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntDiasSemanas == 7)
					{
						string Ordinal = "Sabado";
						VMP.ObtenerDias(Ordinal);
						DdDiasAño.SelectedValue = VMP.CDias.UidDias.ToString();
					}

					if (VMP.CPeriodicidadAnual.IntNumero == 1)
					{
						string mes = "Enero";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 2)
					{
						string mes = "Febrero";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 3)
					{
						string mes = "Marzo";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 4)
					{
						string mes = "Abril";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 5)
					{
						string mes = "Mayo";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 6)
					{
						string mes = "Junio";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 7)
					{
						string mes = "Julio";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 8)
					{
						string mes = "Agosto";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 9)
					{
						string mes = "Septiembre";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 10)
					{
						string mes = "Octubre";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 11)
					{
						string mes = "Noviembre";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();

					}
					else if (VMP.CPeriodicidadAnual.IntNumero == 12)
					{
						string mes = "Diciembre";
						VMP.ObtenerMeses(mes);
						DdMesAño2.SelectedValue = VMP.CMeses.UidMes.ToString();
					}

				}
			}


			#endregion
			//tipofrecuencia.Text = tipo;
			txtFechainicio.Text = VMP.Cperiodicidad.DtFechaInicio.ToString("dd/MM/yyyy");
			txtFechaFin.Text = VMP.Cperiodicidad.DtFechaFin?.ToString("dd/MM/yyyy");
			txtUidPeriodicidad.Text = VMP.Cperiodicidad.UidPeriodicidad.ToString();
			txtUidTipoFrecuencia.Text = VMP.Cperiodicidad.UidTipoFrecuencia.ToString();
			btnAgregarOpcion.Disable();
			btnEliminarOpcion.Disable();
			btnEditarOpcion.Disable();
			if (EditingMode)
			{
				btnCancelar_Click(null, null);
			}
			int pos = -1;
			if (ViewState["TareaPreviousRow"] != null)
			{
				pos = (int)ViewState["TareaPreviousRow"];
				GridViewRow previousRow = DVGTareas.Rows[pos];
				previousRow.RemoveCssClass("success");
			}

			ViewState["TareaPreviousRow"] = DVGTareas.SelectedIndex;
			DVGTareas.SelectedRow.AddCssClass("success");

			VM.ObtenerOpciones();
			ViewState["Opciones"] = VM.Opciones;
			dgvOpciones.DataSource = VM.Opciones;
			dgvOpciones.DataBind();

			lbOpcionesSeleccionadas.DataSource = VM.Opciones;
			lbOpcionesSeleccionadas.DataValueField = "UidOpciones";
			lbOpcionesSeleccionadas.DataTextField = "StrOpciones";
			lbOpcionesSeleccionadas.DataBind();

			VM.ObtenerNotificacion(VM.CTarea.UidTarea);

			if (VM.Notificacion != null)
			{
				cbActivarNotificacion.Checked = true;

				string medicion = DdMedicion.SelectedItem.Text;

				if (medicion == "Verdadero/Falso")
				{
					habilitarCamposNotificacionVerdaderoFalso(false);
					notificacionBool.Visible = true;
					notificacionValor.Visible = false;
					notificacionOpcion.Visible = false;

					if (VM.Notificacion.BlValor.HasValue)
					{
						if (VM.Notificacion.BlValor.Value)
							rbVerdadero.Checked = true;
						else
							rbFalso.Checked = true;
					}
				}

				if (medicion == "Numerico")
				{
					habilitarCamposNotificacionNumerico(false);
					notificacionBool.Visible = false;
					notificacionValor.Visible = true;
					notificacionOpcion.Visible = false;

					if (VM.Notificacion.BlMenorIgual.HasValue && VM.Notificacion.BlMenorIgual.Value)
					{
						rbMenorIgual.Checked = true;
						rbMenor.Checked = false;
					}
					else
					{
						rbMenor.Checked = true;
						rbMenorIgual.Checked = false;
					}
					if (VM.Notificacion.BlMayorIgual.HasValue && VM.Notificacion.BlMayorIgual.Value)
					{
						rbMayorIgual.Checked = true;
						rbMayor.Checked = false;
					}
					else
					{
						rbMayor.Checked = true;
						rbMayorIgual.Checked = false;
					}
					if (VM.Notificacion.DcMayorQue.HasValue)
					{
						txtMayorQue.Text = VM.Notificacion.DcMayorQue.Value.ToString();
					}
					if (VM.Notificacion.DcMenorQue.HasValue)
					{
						txtMenorQue.Text = VM.Notificacion.DcMenorQue.Value.ToString();
					}
				}

				if (medicion == "Seleccionable")
				{
					habilitarCamposNotificacionSeleccionable(false);
					notificacionBool.Visible = false;
					notificacionValor.Visible = false;
					notificacionOpcion.Visible = true;

					if (VM.Notificacion.VchOpciones != null)
					{
						List<Guid> opciones = VM.Notificacion.UidOpciones;

						for (int i = 0; i < lbOpcionesSeleccionadas.Items.Count; i++)
						{
							var item = lbOpcionesSeleccionadas.Items[i];

							foreach (var opcion in opciones)
							{
								if (item.Value.ToString() == opcion.ToString())
								{
									item.Selected = true;
									break;
								}
							}
						}
					}
				}
			}
			else
			{
				cbActivarNotificacion.Checked = false;
				notificacionBool.Visible = false;
				notificacionValor.Visible = false;
				notificacionOpcion.Visible = false;

				rbVerdadero.Checked = false;
				rbFalso.Checked = false;

				rbMenor.Checked = false;
				rbMenorIgual.Checked = false;
				rbMayor.Checked = false;
				rbMayorIgual.Checked = false;
				txtMenorQue.Text = "";
				txtMayorQue.Text = "";

				lbOpcionesSeleccionadas.DataSource = null;
				lbOpcionesSeleccionadas.DataBind();
			}

			VM.ObtenerAntecesor(VM.CTarea.UidTarea);

			if (VM.Antecesor != null)
			{
				cbActivarAntecesor.Checked = true;
				mostrarAntecesor.Visible = true;
				if (VM.Antecesor.BlUsarNotificacion.HasValue && VM.Antecesor.BlUsarNotificacion.Value)
				{
					antecesorNotificacion.Visible = true;
					cbUsarNotificacion.Checked = true;
					txtMaximo.Text = VM.Antecesor.IntRepeticion?.ToString() ?? "";
					if (VM.Antecesor.IntRepeticion.HasValue)
					{
						rdIlimitado.Checked = false;
						rdNumeroLimitado.Checked = true;
					}
					else
					{
						rdIlimitado.Checked = true;
						rdNumeroLimitado.Checked = false;
					}
				}
				else
				{
					cbUsarNotificacion.Checked = false;
					antecesorNotificacion.Visible = false;
				}

				txtDiasDespues.Text = VM.Antecesor.IntDiasDespues.ToString();

				VM.ObtenerTarea(VM.Antecesor.UidTareaAnterior);
				txtUidAntecesor.Text = VM.CTarea.UidAntecesor.ToString();
				txtNombreAntecesor.Text = VM.CTarea.StrNombre;
			}
			else
			{
				txtNombreAntecesor.Text = "";
				txtUidAntecesor.Text = "";
				cbActivarAntecesor.Checked = false;
				cbUsarNotificacion.Checked = false;
				rdIlimitado.Checked = false;
				rdNumeroLimitado.Checked = false;
				txtMaximo.Text = "";
				mostrarAntecesor.Visible = false;
				antecesorNotificacion.Visible = false;
			}
		}

		private void SortTareas(string SortExpression, SortDirection SortDirection, bool same = false)
		{
			List<Tarea> tarea = (List<Tarea>)ViewState["Tarea"];

			if (SortExpression == (string)ViewState["SortColumn"] && !same)
			{
				// We are resorting the same column, so flip the sort direction
				SortDirection =
					((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
					SortDirection.Descending : SortDirection.Ascending;
			}

			if (SortExpression == "Nombre")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					tarea = tarea.OrderBy(x => x.StrNombre).ToList();
				}
				else
				{
					tarea = tarea.OrderByDescending(x => x.StrNombre).ToList();
				}
			}
			else if (SortExpression == "Encargado")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					tarea = tarea.OrderBy(x => x.StrUsuario).ToList();
				}
				else
				{
					tarea = tarea.OrderByDescending(x => x.StrUsuario).ToList();
				}
			}
			else if (SortExpression == "FechaInicio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					tarea = tarea.OrderBy(x => x.DtFechaInicio).ToList();
				}
				else
				{
					tarea = tarea.OrderByDescending(x => x.DtFechaInicio).ToList();
				}
			}
			else if (SortExpression == "Periodicidad")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					tarea = tarea.OrderBy(x => x.StrTipoFrecuencia).ToList();
				}
				else
				{
					tarea = tarea.OrderByDescending(x => x.StrTipoFrecuencia).ToList();
				}
			}
			else if (SortExpression == "Departamento")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					tarea = tarea.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					tarea = tarea.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}

			DVGTareas.DataSource = tarea;
			ViewState["SortColumn"] = SortExpression;
			ViewState["SortColumnDirection"] = SortDirection;
		}

		protected void DVGTareas_Sorting(object sender, GridViewSortEventArgs e)
		{
			ViewState["TareaPreviousRow"] = null;
			SortTareas(e.SortExpression, e.SortDirection);

			DVGTareas.DataBind();
		}

		protected void DVGTareas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(DVGTareas, "Select$" + e.Row.RowIndex);
				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					//PERFIL.CssClass = "glyphicon glyphicon-user";
					e.Row.Cells[5].Text = "(Sin Asignar)";
				}
				if (e.Row.Cells[3].Text.Contains(","))
				{
					string split = e.Row.Cells[3].Text.Split(',')[0];
					e.Row.Cells[3].ToolTip = Server.HtmlDecode(e.Row.Cells[3].Text);
					e.Row.Cells[3].Text = split + ", ...";
				}
				if (e.Row.Cells[5].Text.Contains(","))
				{
					string split = e.Row.Cells[5].Text.Split(',')[0];
					e.Row.Cells[5].ToolTip = Server.HtmlDecode(e.Row.Cells[5].Text);
					e.Row.Cells[5].Text = split + ", ...";
				}
			}
		}

		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			btnBuscar.Disable();
			DVGTareas.Visible = true;
			string Nombre = FiltroNombre.Text.Trim();

			DateTime? DtFechaInicio1 = null;
			if (!string.IsNullOrEmpty(filtrofechainicio1.Text.Trim()))
			{
				DtFechaInicio1 = Convert.ToDateTime(DateTime.ParseExact(filtrofechainicio1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)); ;
			}
			DateTime? DtFechaInicio2 = null;
			if (!string.IsNullOrEmpty(filtrofechainicio1.Text.Trim()))
			{
				DtFechaInicio2 = Convert.ToDateTime(DateTime.ParseExact(filtrofechainicio2.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}

			string departamentos = "";
			int[] i = lblDepartamento.GetSelectedIndices();
			foreach (int j in i)
			{
				string value = lblDepartamento.Items[j].Value;
				if (departamentos.Count() == 0)
					departamentos += value;
				else
					departamentos += "," + value;
			}
			string Departamento = departamentos;

			int folio = 0;

			if (!int.TryParse(txtFolioAntecesor.Text, out folio))
				folio = 0;

			VM.BuscarTarea(Nombre, DtFechaInicio1, DtFechaInicio2, SesionActual.uidSucursalActual.Value, Departamento, null, folio);
			IEnumerable<Tarea> ag =
				from t in VM.ltsTarea
				group new
				{
					t.StrDepartamento,
					t.StrUsuario
				}
				by new
				{
					t.UidTarea,
					t.StrNombre,
					t.DtFechaInicio,
					t.StrTipoFrecuencia,
					t.IntFolio
				}
				into g
				select new Tarea
				{
					UidTarea = g.Key.UidTarea,
					StrNombre = g.Key.StrNombre,
					StrDepartamento = string.Join(", ", g.Select(x => x.StrDepartamento)),
					StrUsuario = string.Join(", ", g.Select(x => x.StrUsuario).Where(x => !string.IsNullOrWhiteSpace(x))),
					DtFechaInicio = g.Key.DtFechaInicio,
					StrTipoFrecuencia = g.Key.StrTipoFrecuencia,
					IntFolio = g.Key.IntFolio,
				};

			ViewState["Tarea"] = ag.ToList();
			DVGTareas.DataSource = ViewState["Tarea"];
			DVGTareas.DataBind();


			PanelFiltros.Visible = false;
			lblFiltros.Text = "Mostrar";
			lblFiltros.CssClass = "glyphicon glyphicon-collapse-down";
			btnLimpiar.Disable();
			ViewState["TareaPreviousRow"] = null;
		}

		protected void btnMostrar_Click(object sender, EventArgs e)
		{
			string texto = lblFiltros.Text;
			DVGTareas.Visible = false;
			if (texto == "Mostrar")
			{
				btnBuscar.Enable();
				btnLimpiar.Enable();
				lblFiltros.Text = "Ocultar";
				lblFiltros.CssClass = "glyphicon glyphicon-collapse-up";
				PanelFiltros.Visible = true;

			}
			else if (texto == "Ocultar")
			{
				PanelFiltros.Visible = false;
				btnBuscar.Disable();
				btnLimpiar.Disable();
				lblFiltros.Text = "Mostrar";
				lblFiltros.CssClass = "glyphicon glyphicon-collapse-down";
				DVGTareas.Visible = true;
			}
		}

		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			FiltroNombre.Text = string.Empty;
			filtrofechainicio1.Text = string.Empty;
			filtrofechainicio2.Text = string.Empty;
			txtFolioAntecesor.Text = string.Empty;
			lblDepartamento.ClearSelection();
			lblUnidad.ClearSelection();
			DVGTareas.DataSource = VM.ltsTarea;
			DVGTareas.DataBind();
		}
		#endregion

		#region Opcion

		protected void btnAgregarOpcion_Click(object sender, EventArgs e)
		{
			EditingMode = true;
			txtUidOpcion.Text = string.Empty;
			txtOpcion.Text = string.Empty;
			txtOrden.Text = "0";
			txtOrden.Enable();
			cbVisible.Enable();
			txtOpcion.Enable();

			btnAceptarOpcion.Visible = true;
			btnCancelarOpcion.Visible = true;

			btnAgregarOpcion.Disable();
			btnEditarOpcion.Disable();
			btnEliminarOpcion.Disable();
			btnEditarOpcion.Disable();

			int pos = -1;
			if (ViewState["OpcionPreviousRow"] != null)
			{
				pos = (int)ViewState["OpcionPreviousRow"];
				GridViewRow previousRow = dgvOpciones.Rows[pos];
				previousRow.RemoveCssClass("success");
			}
		}

		protected void btnEditarOpcion_Click(object sender, EventArgs e)
		{
			EditingMode = true;
			txtOpcion.Enable();
			txtOrden.Enable();
			cbVisible.Enable();

			btnAgregarOpcion.Disable();

			btnEditarOpcion.Disable();

			btnEliminarOpcion.Disable();

			btnAceptarOpcion.Visible = true;

			btnCancelarOpcion.Visible = true;
		}

		protected void btnEliminarOpcion_Click(object sender, EventArgs e)
		{
			lblAceptarEliminarOpcion.Visible = true;
			lblAceptarEliminarOpcion.Text = "¿Desea Eliminar El Telefono Seleccionado?";
			btnAceptarEliminarOpcion.Visible = true;
			btnCancelarEliminarOpcion.Visible = true;
		}

		protected void btnAceptarOpcion_Click(object sender, EventArgs e)
		{
			frmGrpOpcion.RemoveCssClass("has-error");
			lblErrorOpcion.Text = string.Empty;

			if (string.IsNullOrWhiteSpace(txtOpcion.Text))
			{
				lblErrorOpcion.Text = "El campo Opcion no debe estar vacío";
				txtOpcion.Focus();
				frmGrpOpcion.AddCssClass("has-error");

				return;
			}
			List<TareaOpcion> opciones = (List<TareaOpcion>)ViewState["Opciones"];
			TareaOpcion opcion = null;
			int pos = -1;
			if (!string.IsNullOrWhiteSpace(txtUidOpcion.Text))
			{
				IEnumerable<TareaOpcion> dir = from t in opciones where t.UidOpciones.ToString() == txtUidOpcion.Text select t;
				opcion = dir.First();
				pos = opciones.IndexOf(opcion);
				opciones.Remove(opcion);
			}
			else
			{
				opcion = new TareaOpcion();
				opcion.UidOpciones = Guid.NewGuid();
			}
			opcion.StrOpciones = txtOpcion.Text;
			opcion.IntOrden = Convert.ToInt32(txtOrden.Text);
			opcion.BlVisible = cbVisible.Checked;

			if (pos < 0)
				opciones.Add(opcion);
			else
				opciones.Insert(pos, opcion);

			ViewState["Opciones"] = opciones = opciones.OrderBy(x => x.IntOrden).ToList();
			dgvOpciones.DataSource = opciones;
			dgvOpciones.DataBind();

			lbOpcionesSeleccionadas.DataSource = opciones;
			lbOpcionesSeleccionadas.DataTextField = "StrOpciones";
			lbOpcionesSeleccionadas.DataValueField = "UidOpciones";
			lbOpcionesSeleccionadas.DataBind();

			txtUidOpcion.Text = string.Empty;
			txtOpcion.Text = string.Empty;
			txtOpcion.Disable();
			txtOrden.Text = "0";
			txtOrden.Disable();
			cbVisible.Disable();

			btnAceptarOpcion.Visible = false;
			btnCancelarOpcion.Visible = false;

			if (txtUidOpcion.Text.Length == 0)
			{
				btnEditarOpcion.Disable();
				btnEliminarOpcion.Disable();
			}
			else
			{
				btnEditarOpcion.Disable();
				btnEliminarOpcion.Disable();
			}

			btnAgregarOpcion.Enable();
			txtOpcion.Disable();
		}

		protected void btnCancelarOpcion_Click(object sender, EventArgs e)
		{

			frmGrpOpcion.RemoveCssClass("has-error");
			lblErrorOpcion.Text = string.Empty;

			txtOpcion.Disable();

			btnAceptarOpcion.Visible = false;
			btnCancelarOpcion.Visible = false;

			if (txtUidOpcion.Text.Length == 0)
			{
				btnEditarOpcion.Disable();
				btnEliminarOpcion.Disable();
				txtUidOpcion.Text = string.Empty;
				txtOpcion.Text = string.Empty;
			}
			else
			{
				btnEditarOpcion.Enable();
				btnEliminarOpcion.Enable();

				List<TareaOpcion> opciones = (List<TareaOpcion>)ViewState["Opciones"];
				TareaOpcion opcion = opciones.Select(x => x).Where(x => x.UidOpciones.ToString() == dgvOpciones.SelectedDataKey.Value.ToString()).First();

				txtUidOpcion.Text = opcion.UidOpciones.ToString();
				txtOpcion.Text = opcion.StrOpciones;
			}

			btnAgregarOpcion.Enable();

		}

		protected void btnAceptarEliminarOpcion_Click(object sender, EventArgs e)
		{
			btnAgregarOpcion.Enable();

			btnAceptarOpcion.Visible = false;

			btnCancelarOpcion.Visible = false;

			Guid uid = new Guid(txtUidOpcion.Text);

			List<TareaOpcion> opciones = (List<TareaOpcion>)ViewState["Opciones"];
			TareaOpcion opcion = opciones.Select(x => x).Where(x => x.UidOpciones == uid).First();
			opciones.Remove(opcion);
			OpcionRemoved.Add(opcion);

			txtUidOpcion.Text = string.Empty;
			txtOpcion.Text = string.Empty;

			dgvOpciones.DataSource = opciones;
			dgvOpciones.DataBind();
			btnCancelarEliminarOpcion.Visible = false;
			btnAceptarEliminarOpcion.Visible = false;
			lblAceptarEliminarOpcion.Visible = false;
			ViewState["OpcionPreviousRow"] = null;
			btnEliminarOpcion.Disable();
			btnEditarOpcion.Disable();
		}

		protected void btnCancelarEliminarOpcion_Click(object sender, EventArgs e)
		{
			btnAceptarEliminarOpcion.Visible = false;
			btnCancelarEliminarOpcion.Visible = false;
			lblAceptarEliminarOpcion.Visible = false;
			if (txtUidOpcion.Text.Length > 0)
			{
				btnEliminarOpcion.Enable();
				btnEditarOpcion.Enable();
			}
			else
			{
				btnEliminarOpcion.Disable();
				btnEditarOpcion.Disable();
			}
		}

		protected void dgvOpciones_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvOpciones, "Select$" + e.Row.RowIndex);
			}

		}

		protected void dgvOpciones_SelectedIndexChanged(object sender, EventArgs e)
		{

			List<TareaOpcion> opciones = (List<TareaOpcion>)ViewState["Opciones"];
			TareaOpcion opcion = opciones.Select(x => x).Where(x => x.UidOpciones.ToString() == dgvOpciones.SelectedDataKey.Value.ToString()).First();

			txtUidOpcion.Text = opcion.UidOpciones.ToString();
			txtOpcion.Text = opcion.StrOpciones;
			txtOrden.Text = opcion.IntOrden.ToString();
			cbVisible.Checked = opcion.BlVisible;

			if (EditingMode)
			{
				btnEditarOpcion.Enable();
				if (!opcion.ExistsInDatabase)
					btnEliminarOpcion.Enable();
			}

			int pos = -1;
			if (ViewState["OpcionPreviousRow"] != null)
			{
				pos = (int)ViewState["OpcionPreviousRow"];
				GridViewRow previousRow = dgvOpciones.Rows[pos];
				previousRow.RemoveCssClass("success");
			}

			ViewState["OpcionPreviousRow"] = dgvOpciones.SelectedIndex;
			dgvOpciones.SelectedRow.AddCssClass("success");
			btnAceptarOpcion.Visible = false;
			btnCancelarOpcion.Visible = false;
		}

		protected void DVGTareas_PreRender(object sender, EventArgs e)
		{
			GridViewRow pagerRow = DVGTareas.TopPagerRow;

			if (pagerRow != null && pagerRow.Visible == false)
				pagerRow.Visible = true;
		}

		#endregion

		protected void CbHora_CheckedChanged(object sender, EventArgs e)
		{
			if (CbHora.Checked)
			{
				MirarHora();
				txtHora.Enable();
				txtTolerancia.Enable();
			}
			else
			{
				OcultarHora();
			}
		}

		protected void rdCadaDiario_CheckedChanged(object sender, EventArgs e)
		{
			if (rdCadaDiario.Checked)
			{
				txtcadaDiario.Enable();

			}
			else if (rdtodoslosdias.Checked)
			{
				txtcadaDiario.Disable();
				txtcadaDiario.Text = string.Empty;
			}
		}

		protected void rdElDiaMensual_CheckedChanged(object sender, EventArgs e)
		{
			if (rdElDiaMensual.Checked)
			{
				txtDiasMes.Enable();
				txtCadames.Enable();
				txtcadamensual.Disable();
				txtcadamensual.Text = string.Empty;
			}
			else if (rdElMeses.Checked)
			{
				txtDiasMes.Disable();
				txtCadames.Disable();
				txtDiasMes.Text = string.Empty;
				txtCadames.Text = string.Empty;
				txtcadamensual.Enable();
			}
		}

		protected void rdElDelAño_CheckedChanged(object sender, EventArgs e)
		{
			if (rdElDelAño.Checked)
			{
				txtDiasmesaño.Enable();

			}
			else if (rdElDelAño2.Checked)
			{
				txtDiasmesaño.Disable();
				txtDiasmesaño.Text = string.Empty;
			}
		}

		protected void rdDepartamento_CheckedChanged(object sender, EventArgs e)
		{
			if (rdDepartamento.Checked)
			{
				btnBuscarDepartamentos.Enable();
				txtNombreDepartamento.Enable();
				PanelDepartamentosGuardar.Visible = true;
				PanelAreasGuardar.Visible = false;
				ViewState["DepartamentoPreviousRow"] = null;
				ViewState["Departamentos"] = new List<Departamento>();
				DepartamentoRemoved.Clear();
				dvgDepartamentosGuardar.DataSource = ViewState["Departamentos"];
				dvgDepartamentosGuardar.DataBind();
				ViewState["AreaPreviousRow"] = null;
				ViewState["Areas"] = new List<Area>();
				DepartamentoRemoved.Clear();
				dvgAreasGuardar.DataSource = ViewState["Areas"];
				dvgAreasGuardar.DataBind();
				txtNombreDepartamento.Text = string.Empty;
			}
			else if (rdArea.Checked)
			{
				btnBuscarDepartamentos.Enable();
				txtNombreDepartamento.Enable();
				PanelDepartamentosGuardar.Visible = false;
				PanelAreasGuardar.Visible = true;
				ViewState["DepartamentoPreviousRow"] = null;
				ViewState["Departamentos"] = new List<Departamento>();
				DepartamentoRemoved.Clear();
				dvgDepartamentosGuardar.DataSource = ViewState["Departamentos"];
				dvgDepartamentosGuardar.DataBind();
				ViewState["AreaPreviousRow"] = null;
				ViewState["Areas"] = new List<Area>();
				DepartamentoRemoved.Clear();
				dvgAreasGuardar.DataSource = ViewState["Areas"];
				dvgAreasGuardar.DataBind();
				txtNombreDepartamento.Text = string.Empty;
			}
		}

		protected void cbActivarNotificacion_CheckedChanged(object sender, EventArgs e)
		{
			if (cbActivarNotificacion.Checked)
			{
				string tipo = DdMedicion.SelectedItem.Text;
				setOptionsNotification(tipo);
			}
			else
			{
				notificacionBool.Visible = false;
				notificacionValor.Visible = false;
				notificacionOpcion.Visible = false;
			}
		}

		protected void cbUsarNotificacion_CheckedChanged(object sender, EventArgs e)
		{
			antecesorNotificacion.Visible = cbUsarNotificacion.Checked;
		}

		protected void cbActivarAntecesor_CheckedChanged(object sender, EventArgs e)
		{
			mostrarAntecesor.Visible = cbActivarAntecesor.Checked;
		}

		public void setOptionsNotification(string tipo)
		{
			if (tipo == "Verdadero/Falso")
			{
				notificacionBool.Visible = true;
				notificacionValor.Visible = false;
				notificacionOpcion.Visible = false;

				habilitarCamposNotificacionNumerico(false);
				habilitarCamposNotificacionSeleccionable(false);
				habilitarCamposNotificacionVerdaderoFalso(true);
			}
			else if (tipo == "Numerico")
			{
				notificacionBool.Visible = false;
				notificacionValor.Visible = true;
				notificacionOpcion.Visible = false;

				habilitarCamposNotificacionNumerico(true);
				habilitarCamposNotificacionSeleccionable(false);
				habilitarCamposNotificacionVerdaderoFalso(false);
			}
			else if (tipo == "Seleccionable")
			{
				notificacionBool.Visible = false;
				notificacionValor.Visible = false;
				notificacionOpcion.Visible = true;

				habilitarCamposNotificacionNumerico(false);
				habilitarCamposNotificacionSeleccionable(true);
				habilitarCamposNotificacionVerdaderoFalso(false);
			}
		}
		public void habilitarCamposNotificacionNumerico(bool status)
		{
			rbMenor.Enabled = status;
			rbMenorIgual.Enabled = status;
			txtMenorQue.Enabled = status;

			rbMayor.Enabled = status;
			rbMayorIgual.Enabled = status;
			txtMayorQue.Enabled = status;
		}
		public void habilitarCamposNotificacionSeleccionable(bool status)
		{
			lbOpcionesSeleccionadas.Enabled = status;
		}
		public void habilitarCamposNotificacionVerdaderoFalso(bool status)
		{
			rbVerdadero.Enabled = status;
			rbFalso.Enabled = status;
		}
	}
}