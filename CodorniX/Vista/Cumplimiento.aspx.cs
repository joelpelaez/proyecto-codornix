﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class Cumplimiento : System.Web.UI.Page
	{
		private List<Modelo.Cumplimiento> VSTareas
		{
			get => ViewState["Tareas"] as List<Modelo.Cumplimiento>;
			set => ViewState["Tareas"] = value;
		}

		private VMCumplimiento VM = new VMCumplimiento();
		private Sesion SesionActual => (Sesion)Session["Sesion"];

		private bool ModoSupervisor
		{
			get
			{
				if (ViewState["ModoSupervisor"] == null)
					ViewState["ModoSupervisor"] = false;

				return (bool)ViewState["ModoSupervisor"];
			}

			set => ViewState["ModoSupervisor"] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			// No hacer nada si no tiene algun turno iniciado. Master se hace cargo de ello.
			if (SesionActual == null)
				return;

			// No permite acceder en caso de no iniciar una sucursal (solo para administradores).
			if (!SesionActual.uidSucursalActual.HasValue)
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (SesionActual.perfil == "Supervisor" || SesionActual.perfil.Contains("dministrador"))
			{

				// El supervisor no tiene acceso al cumplimiento si no ha tomado  algun turno para cumplir.
				if (SesionActual.UidPeriodos.Count < 0)
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}
				else
				{
					ModoSupervisor = true;
					SesionActual.UidPeriodo = Guid.Empty;

					// El administrador registra sus tareas como propias
					if (!IsPostBack && SesionActual.perfil == "Supervisor")
					{
						panelUsuario.Visible = true;
						rbEncargado.Checked = true;
					}
				}
			}
			else
			{
				if (SesionActual.UidPeriodo == null)
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}
			}

			// Verificar permisos
			if (!Acceso.TieneAccesoAModulo("Cumplimiento", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				btnRealizar.Disable();
				btnPosponer.Disable();
				btnCancelarTarea.Disable();
				btnDeshacer.Disable();
				btnDeshacer.Visible = false;
				rbEncargado.Disable();
				rbSupervisor.Disable();
				frmSiNo.Visible = false;
				frmValue.Visible = false;
				frmTwoValues.Visible = false;
				frmOptions.Visible = false;

				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;

				VSTareas = VM.CumplimientosPendientes;

				VM.ObtenerEstados();
				lbEstados.DataSource = VM.Estados;
				lbEstados.DataTextField = "StrEstadoCumplimiento";
				lbEstados.DataValueField = "UidEstadoCumplimiento";
				lbEstados.DataBind();

				VM.ObtenerDepartamentos(SesionActual.UidPeriodos);
				Departamento departamento = new Departamento();
				departamento.UidDepartamento = Guid.Empty;
				departamento.StrNombre = "Todos";
				VM.Departamentos.Insert(0, departamento);
				ddDepartamentos.DataSource = VM.Departamentos;
				ddDepartamentos.DataTextField = "StrNombre";
				ddDepartamentos.DataValueField = "UidDepartamento";
				ddDepartamentos.SelectedIndex = 0;
				ddDepartamentos.DataBind();

				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();

				VM.ObtenerTiposTarea();
				TipoTarea tipo = new TipoTarea();
				tipo.UidTipoTarea = Guid.Empty;
				tipo.StrTipoTarea = "Todos";
				VM.TiposTarea.Insert(0, tipo);
				ddTipo.DataSource = VM.TiposTarea;
				ddTipo.DataValueField = "UidTipoTarea";
				ddTipo.DataTextField = "StrTipoTarea";
				ddTipo.SelectedIndex = 0;
				ddTipo.DataBind();

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				var local = horaLocal.DateTime;
				VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, null, null, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), local);
				VSTareas = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataBind();
			}
		}

		protected void dgvTareasPendientes_SelectedIndexChanged(object sender, EventArgs e)
		{
			DateTime? proximo;
			Guid uidTarea = new Guid(dgvTareasPendientes.SelectedDataKey.Value.ToString());
			Guid uidDepto = new Guid(dgvTareasPendientes.SelectedDataKey.Values[1].ToString());
			Guid uidArea = new Guid(dgvTareasPendientes.SelectedDataKey.Values[2].ToString());
			Guid uidCumplimiento = new Guid(dgvTareasPendientes.SelectedDataKey.Values[3].ToString());
			Guid uidPeriodo = new Guid(dgvTareasPendientes.SelectedDataKey.Values[4].ToString());

			fldUidTarea.Value = uidTarea.ToString();
			fldUidDepartamento.Value = uidDepto.ToString();
			fldUidArea.Value = uidArea.ToString();
			fldUidCumplimiento.Value = uidCumplimiento.ToString();
			fldUidPerido.Value = uidPeriodo.ToString();

			btnActualizar.Enable();
			btnOK.Visible = false;
			btnCancelar.Visible = false;

			VM.ObtenerCumplimiento(uidCumplimiento);
			VM.ObtenerTarea(uidTarea);
			VM.ObtenerDepartamento(uidDepto);
			VM.ObtenerArea(uidArea);
			VM.ObtenerPeriocidad(VM.Tarea.UidPeriodicidad);
			VM.ObtenerTipoFrecuencia(VM.Periodicidad.UidTipoFrecuencia);

			lblPeriodicidad.Text = VM.TipoFrecuencia.StrTipoFrecuencia;

			proximo = VM.ObtenerFechaSiguienteTarea(uidTarea, DateTime.Now);

			// Activar modo edición si esta completa
			bool editmode = (VM.Cumplimiento != null && VM.Cumplimiento.StrEstadoCumplimiento == "Completo");

			if (editmode)
			{
				fldEditing.Visible = true;
				fldEditing.Value = "1";
				btnRealizar.Text = "Editar";
			}
			else
			{
				fldEditing.Visible = false;
				fldEditing.Value = string.Empty;
				btnRealizar.Text = "Realizar";
			}

			lblTarea.Text = VM.Tarea.StrNombre;
			lblDepto.Text = VM.Departamento.StrNombre;
			lblArea.Text = VM.Area == null ? "(global)" : VM.Area.StrNombre;

			frmSiNo.Visible = false;
			frmValue.Visible = false;
			frmTwoValues.Visible = false;
			frmOptions.Visible = false;

			confirmarCancelar.Visible = false;
			confirmarPosponer.Visible = false;

			lblErrorTarea.Text = string.Empty;

			rbYes.Checked = false;
			rbNo.Checked = false;
			txtValor.Text = string.Empty;
			txtValor1.Text = string.Empty;
			txtValor2.Text = string.Empty;
			rbSupervisor.Disable();
			rbEncargado.Disable();
			rbYes.Disable();
			rbNo.Disable();
			txtValor.Disable();
			ddOpciones.Disable();
			txtObservaciones.Disable();

			switch (VM.Tarea.StrTipoMedicion)
			{
				case "Verdadero/Falso":
					frmSiNo.Visible = true;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.BlValor.HasValue)
						{
							if (VM.Cumplimiento.BlValor.Value)
							{
								rbYes.Checked = true;
								rbNo.Checked = false;
							}
							else
							{
								rbNo.Checked = true;
								rbYes.Checked = false;
							}
						}
					}
					break;
				case "Numerico":
					frmValue.Visible = true;
					lblUnidad.Text = VM.Tarea.StrUnidadMedida;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.DcValor1.HasValue)
						{
							txtValor.Text = VM.Cumplimiento.DcValor1.Value.ToString("0.####");
						}
					}
					else
					{
						txtValor.Text = "";
					}
					break;
				case "Seleccionable":
					VM.ObtenerOpcionesDeTarea(uidTarea);
					frmOptions.Visible = true;
					ddOpciones.DataSource = VM.Opciones;
					ddOpciones.DataValueField = "UidOpciones";
					ddOpciones.DataTextField = "StrOpciones";
					ddOpciones.DataBind();
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.UidOpcion.HasValue)
						{
							ddOpciones.SelectedValue = VM.Cumplimiento.UidOpcion.Value.ToString();
						}
					}
					break;
			}
			if (VM.Cumplimiento == null || VM.Cumplimiento.StrEstadoCumplimiento == "No Realizado")
			{
				btnDeshacer.Disable();
				btnDeshacer.Visible = false;
				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();
				if (VM.Tarea.StrTipoTarea == "Requerida" && !ModoSupervisor)
				{
					btnCancelarTarea.Disable();
					btnPosponer.Disable();
				}
				else if (proximo != null && proximo.Value.Subtract(DateTime.Today).TotalDays <= 1)
				{
					btnPosponer.Disable();
				}
				txtObservaciones.Text = string.Empty;
			}
			else if (VM.Cumplimiento.StrEstadoCumplimiento == "Completo")
			{
				btnDeshacer.Enable();
				btnDeshacer.Visible = true;
				btnRealizar.Enable();
				btnCancelarTarea.Disable();
				btnPosponer.Disable();
				if (!string.IsNullOrWhiteSpace(VM.Cumplimiento.StrObservacion))
					txtObservaciones.Text = VM.Cumplimiento.StrObservacion;
				else
					txtObservaciones.Text = string.Empty;
				if (ModoSupervisor)
				{
					if (VM.Cumplimiento.UidUsuario == SesionActual.uidUsuario)
					{
						rbSupervisor.Checked = true;
						rbEncargado.Checked = false;
					}
					else
					{
						rbSupervisor.Checked = false;
						rbEncargado.Checked = true;
					}
				}
			}
			else
			{
				btnDeshacer.Enable();
				btnDeshacer.Visible = true;
				btnRealizar.Disable();
				btnCancelarTarea.Disable();
				btnPosponer.Disable();
				if (!string.IsNullOrWhiteSpace(VM.Cumplimiento.StrObservacion))
					txtObservaciones.Text = VM.Cumplimiento.StrObservacion;
				else
					txtObservaciones.Text = string.Empty;
			}

			int pos = -1;
			if (ViewState["CumplPreviousRow"] != null)
			{
				pos = (int)ViewState["CumplPreviousRow"];
				GridViewRow previousRow = dgvTareasPendientes.Rows[pos];
				previousRow.RemoveCssClass("success-forced");
			}

			ViewState["CumplPreviousRow"] = dgvTareasPendientes.SelectedIndex;
			dgvTareasPendientes.SelectedRow.AddCssClass("success-forced");

			lblError.Text = string.Empty;
			valorUnico.RemoveCssClass("has-error");
		}

		protected void dgvTareasPendientes_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTareasPendientes, "Select$" + e.Row.RowIndex);

				Label icon = e.Row.FindControl("lblCompleto") as Label;

				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					e.Row.Cells[5].Text = "(global)";
				}

				if (e.Row.Cells[6].Text == "&nbsp;")
				{
					e.Row.Cells[6].Text = "N/A";
				}

				if (e.Row.Cells[8].Text == "Requerida")
				{
					//if (!e.Row.CssClass.Contains("success"))
					//    e.Row.AddCssClass("danger");
					e.Row.AddCssClass("font-bold");
					e.Row.Cells[3].Text += "*";
				}

				if (e.Row.Cells[7].Text == "&nbsp;" || e.Row.Cells[7].Text == "No Realizado")
				{
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-pencil");
					icon.ToolTip = "No Realizado";
				}
				else if (e.Row.Cells[7].Text == "Completo")
				{
					e.Row.AddCssClass("info");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-ok");
					icon.ToolTip = "Completo";
				}
				else if (e.Row.Cells[7].Text == "Pospuesto")
				{
					e.Row.AddCssClass("warning");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-arrow-right");
					icon.ToolTip = "Pospuesto";
				}
				else if (e.Row.Cells[7].Text == "Cancelado")
				{
					e.Row.AddCssClass("danger");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-remove");
					icon.ToolTip = "Cancelado";
				}
			}
		}

		protected void btnRealizar_Click(object sender, EventArgs e)
		{
			rbYes.Enable();
			rbNo.Enable();
			rbNo.Checked = true;
			rbEncargado.Enable();
			rbSupervisor.Enable();
			rbEncargado.Checked = true;
			txtValor.Enable();
			txtValor.Text = "0";
			ddOpciones.Enable();
			txtObservaciones.Enable();
			btnRealizar.Disable();
			btnOK.Visible = true;
			btnCancelar.Visible = true;
		}

		protected void btnOK_Click(object sender, EventArgs e)
		{
			try
			{
				Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
				Guid uidTarea = new Guid(fldUidTarea.Value);
				Guid uidDepartamento = new Guid(fldUidDepartamento.Value);
				Guid uidArea = new Guid(fldUidArea.Value);

				Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
				Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
				Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;
				bool? estado = null;
				decimal? valor1 = null;
				decimal? valor2 = null;
				Guid? opcion = null;

				lblError.Text = string.Empty;
				valorUnico.RemoveCssClass("has-error");

				VM.ObtenerTarea(uidTarea);

				switch (VM.Tarea.StrTipoMedicion)
				{
					case "Verdadero/Falso":
						if (rbYes.Checked)
							estado = true;
						else if (rbNo.Checked)
							estado = false;
						else
							estado = false;
						break;
					case "Numerico":
						try
						{
							valor1 = Convert.ToDecimal(txtValor.Text);
						}
						catch (FormatException ex)
						{
							lblError.Text = "Solo se aceptan valores numéricos";
							valorUnico.AddCssClass("has-error");
							return;
						}
						break;
					case "Seleccionable":
						opcion = new Guid(ddOpciones.SelectedValue.ToString());
						break;
				}

				Guid uidPeriodo = new Guid(fldUidPerido.Value);
				VM.ObtenerPeriodo(uidPeriodo);

				bool editmode = (fldEditing.Value == "1");
				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				if (editmode)
				{
					VM.ActualizarCumplimiento(realCumplimiento.Value, horaLocal, estado, valor1, valor2, opcion, txtObservaciones.Text, null);
				}
				else
				{
					Guid uid = SesionActual.uidUsuario;
					if (ModoSupervisor && rbEncargado.Checked)
					{
						uid = VM.Periodo.UidUsuario;
					}


					VM.RegistrarCumplimiento(ref realCumplimiento, uidTarea, realDepartamento, realArea,
						uid, horaLocal, estado, valor1, valor2, opcion, txtObservaciones.Text, null, VM.Periodo.UidTurno);

					VM.EnviarNotificacion(realCumplimiento.Value);
				}
				rbYes.Disable();
				rbNo.Disable();
				txtValor.Disable();
				ddOpciones.Disable();
				txtObservaciones.Disable();
				btnRealizar.Enable();
				btnOK.Visible = false;
				btnCancelar.Visible = false;

				string lpr = null;
				int[] i = lbEstados.GetSelectedIndices();
				if (i.Length > 0)
				{
					lpr = lbEstados.Items[i[0]].Value.ToString();
					for (int j = 1; j < i.Length; j++)
					{
						lpr += "," + lbEstados.Items[i[j]].Value.ToString();
					}
				}
				DateTime real = horaLocal.DateTime;
				VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, txtNombre.Text, lpr,
					 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), real);
				VSTareas = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataBind();

				ViewState["CumplPreviousRow"] = null;
			}
			catch (Exception ex)
			{
				lblErrorTarea.Text = ex.Message;
				panelAlert.Visible = true;
			}
		}

		protected void btnCancelar_Click(object sender, EventArgs e)
		{
			rbYes.Disable();
			rbNo.Disable();
			txtValor.Disable();
			ddOpciones.Disable();
			btnRealizar.Enable();
			btnOK.Visible = false;
			btnCancelar.Visible = false;
			lblError.Text = string.Empty;
			valorUnico.RemoveCssClass("has-error");
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			frmSiNo.Visible = false;
			frmValue.Visible = false;
			frmTwoValues.Visible = false;
			frmOptions.Visible = false;

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}
			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
			DateTime real = horaLocal.DateTime;
			VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, txtNombre.Text, lpr,
				 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), real);
			VSTareas = VM.CumplimientosPendientes;
			dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
			dgvTareasPendientes.DataBind();

			lblError.Text = string.Empty;
			valorUnico.RemoveCssClass("has-error");

			ViewState["CumplPreviousRow"] = null;

			btnMostrar.Text = "Ocultar";
			panelBusqueda.Visible = false;
			panelResultados.Visible = true;
			btnLimpiar.Visible = false;
			btnActualizar.Visible = false;
		}

		protected void btnCancelarTarea_Click(object sender, EventArgs e)
		{
			confirmarCancelar.Visible = true;
			confirmarPosponer.Visible = false;

			btnRealizar.Disable();
			btnCancelarTarea.Disable();
			btnPosponer.Disable();
		}

		protected void btnPosponer_Click(object sender, EventArgs e)
		{
			confirmarCancelar.Visible = false;
			confirmarPosponer.Visible = true;

			cumplimientoCampos.Visible = false;
			posponerCampos.Visible = true;

			DateTime? proximo = VM.ObtenerFechaSiguienteTarea(new Guid(fldUidTarea.Value), DateTime.Now);
			VM.ObtenerPeriodo(SesionActual.UidPeriodo.Value);

			if (proximo == null || proximo.Value > VM.Periodo?.DtFechaFin)
				proximo = VM.Periodo.DtFechaFin;
			if (proximo == null)
				proximo = DateTime.MaxValue;

			string script = "setStartDateLimit('" + DateTime.Today.AddDays(1.0).ToString("dd/MM/yyyy") + "', '" + proximo?.ToString("dd/MM/yyyy") + "')";
			ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateStartDate", script, true);

			btnRealizar.Disable();
			btnCancelarTarea.Disable();
			btnPosponer.Disable();
		}

		protected void btnCancelarCancelar_Click(object sender, EventArgs e)
		{
			confirmarCancelar.Visible = false;

			btnRealizar.Enable();
			btnCancelarTarea.Enable();
			btnPosponer.Enable();
		}

		protected void btnOKCancelar_Click(object sender, EventArgs e)
		{
			Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
			Guid uidTarea = new Guid(fldUidTarea.Value);
			Guid uidDepartamento = new Guid(fldUidDepartamento.Value);
			Guid uidArea = new Guid(fldUidArea.Value);

			Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
			Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
			Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;

			string msg = string.Empty;

			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));

			int result = VM.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea,
				SesionActual.uidUsuario, null, VMCumplimiento.ModoCambio.Cancelar, horaLocal);

			if (result == 1)
			{
				msg = "La fecha nueva no corresponde a un periodo del usuario actual";
				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarCancelar.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}
			else if (result == 2)
			{
				msg = "La tarea es requerida y no puede cancelarse";

				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarCancelar.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}

			var local = horaLocal.DateTime;
			VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, txtNombre.Text, lpr,
				 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), local);
			VSTareas = VM.CumplimientosPendientes;
			dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
			dgvTareasPendientes.DataBind();

			ViewState["CumplPreviousRow"] = null;

			lblErrorTarea.Text = string.Empty;

			confirmarCancelar.Visible = false;
			btnRealizar.Enable();
			btnCancelarTarea.Enable();
			btnPosponer.Enable();
		}

		protected void btnCancelarPosponer_Click(object sender, EventArgs e)
		{
			confirmarPosponer.Visible = false;

			cumplimientoCampos.Visible = true;
			posponerCampos.Visible = false;

			btnRealizar.Enable();
			btnCancelarTarea.Enable();
			btnPosponer.Enable();
		}

		protected void btnOKPosponer_Click(object sender, EventArgs e)
		{
			Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
			Guid uidTarea = new Guid(fldUidTarea.Value);
			Guid uidDepartamento = new Guid(fldUidDepartamento.Value);
			Guid uidArea = new Guid(fldUidArea.Value);

			Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
			Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
			Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;

			DateTime fecha = Convert.ToDateTime(txtFecha.Text);
			string msg = string.Empty;

			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
			DateTime real = horaLocal.DateTime;
			int result = VM.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea,
				SesionActual.uidUsuario, fecha, VMCumplimiento.ModoCambio.Posponer, real);

			if (result == 1)
			{
				msg = "La fecha nueva no corresponde a un periodo del usuario actual";
				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarPosponer.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}
			else if (result == 2)
			{
				msg = "La tarea es requerida y no puede posponerse";
				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarPosponer.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}

			VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, txtNombre.Text, lpr, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), real);
			VSTareas = VM.CumplimientosPendientes;
			dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
			dgvTareasPendientes.DataBind();

			ViewState["CumplPreviousRow"] = null;

			lblErrorTarea.Text = string.Empty;

			cumplimientoCampos.Visible = true;
			posponerCampos.Visible = false;
			confirmarPosponer.Visible = false;
			btnRealizar.Enable();
			btnCancelarTarea.Enable();
			btnPosponer.Enable();
		}

		protected void btnDeshacer_Click(object sender, EventArgs e)
		{
			Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);

			VM.EnviarDeshacerNotificacion(uidCumplimiento);
			VM.DeshacerCumplimiento(uidCumplimiento);

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}

			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
			var local = horaLocal.DateTime;
			VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos,
				txtNombre.Text, lpr, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), local);
			dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
			dgvTareasPendientes.DataBind();

			ViewState["CumplPreviousRow"] = null;

			lblErrorTarea.Text = string.Empty;

			btnRealizar.Disable();
			btnCancelarTarea.Disable();
			btnPosponer.Disable();
			lblTarea.Text = "(ninguno)";
			lblArea.Text = "(ninguno)";
			lblDepto.Text = "(ninguno)";
		}

		protected void btnCloseAlert_Click(object sender, EventArgs e)
		{
			panelAlert.Visible = false;
			lblErrorTarea.Text = string.Empty;
		}

		protected void dgvTareasPendientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
			{
				string SortExpression = (string)ViewState["SortColumn"];
				SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
				SortTareas(SortExpression, SortDirection, true);
			}
			else
			{
				dgvTareasPendientes.DataSource = VSTareas;
			}
			dgvTareasPendientes.PageIndex = e.NewPageIndex;
			dgvTareasPendientes.DataBind();
		}

		protected void dgvTareasPendientes_Sorting(object sender, GridViewSortEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			SortTareas(e.SortExpression, e.SortDirection, false);
			dgvTareasPendientes.DataBind();
		}

		private void SortTareas(string SortExpression, SortDirection SortDirection, bool same = false)
		{
			if (SortExpression == (string)ViewState["SortColumn"] && !same)
			{
				// We are resorting the same column, so flip the sort direction
				SortDirection =
					((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
					SortDirection.Descending : SortDirection.Ascending;
			}

			if (SortExpression == "Folio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolioTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolioTarea).ToList();
				}
			}
			if (SortExpression == "FolioCumpl")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolio).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolio).ToList();
				}
			}
			if (SortExpression == "Tarea")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrTarea).ToList();
				}
			}
			else if (SortExpression == "Departamento")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}
			else if (SortExpression == "Area")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrArea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrArea).ToList();
				}
			}
			else if (SortExpression == "Estado")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
			}

			dgvTareasPendientes.DataSource = VSTareas;
			ViewState["SortColumn"] = SortExpression;
			ViewState["SortColumnDirection"] = SortDirection;
		}

		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			txtNombre.Text = "";
			lbEstados.ClearSelection();
		}

		private class EstadoComparator : IComparer<string>
		{
			private int GetValue(string s)
			{
				if (s == "No Realizado")
					return -1;
				else if (s == "Completado")
					return 3;
				else if (s == "Cancelado")
					return 2;
				else if (s == "Pospuesto")
					return 1;
				else
					return 10;
			}
			public int Compare(string x, string y)
			{
				return GetValue(x) - GetValue(y);
			}
		}

		protected void btnMostrar_Click(object sender, EventArgs e)
		{
			if (btnMostrar.Text == "Mostrar")
			{
				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;
			}
			else
			{
				btnMostrar.Text = "Mostrar";
				panelBusqueda.Visible = true;
				panelResultados.Visible = false;
				btnLimpiar.Visible = true;
				btnActualizar.Visible = true;
			}
		}

		protected void ddDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid selected = new Guid(ddDepartamentos.SelectedValue);
			if (selected != Guid.Empty)
			{
				VM.ObtenerAreas(selected);
				Area area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000001"); // General
				area.StrNombre = "General";
				VM.Areas.Insert(0, area);
				area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000000"); // Todos
				area.StrNombre = "Todos";
				VM.Areas.Insert(0, area);
				ddAreas.DataSource = VM.Areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.DataBind();

				ddAreas.SelectedIndex = 0;
			}
			else
			{
				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();
			}
		}
	}
}