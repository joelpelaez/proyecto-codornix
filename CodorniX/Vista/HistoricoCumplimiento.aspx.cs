﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class HistoricoCumplimiento : System.Web.UI.Page
	{
		private List<Modelo.Cumplimiento> VSTareas
		{
			get => ViewState["Tareas"] as List<Modelo.Cumplimiento>;
			set => ViewState["Tareas"] = value;
		}

		private VMHistoricoCumplimiento VM = new VMHistoricoCumplimiento();
		private Sesion SesionActual => (Sesion)Session["Sesion"];

		private bool ModoSupervisor
		{
			get
			{
				if (ViewState["ModoSupervisor"] == null)
					ViewState["ModoSupervisor"] = false;

				return (bool)ViewState["ModoSupervisor"];
			}

			set => ViewState["ModoSupervisor"] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			// No hacer nada si no tiene algun turno iniciado. Master se hace cargo de ello.
			if (SesionActual == null)
				return;

			// No permite acceder en caso de no iniciar una sucursal (solo para administradores).
			if (!SesionActual.uidSucursalActual.HasValue)
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (SesionActual.perfil == "Supervisor" || SesionActual.perfil == "Administrador")
			{

				// El supervisor o administrador puede acceder a casi todos los cumplimientos
				ModoSupervisor = true;
			}
			else
			{
				if (SesionActual.UidPeriodo == null)
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}
			}

			// Verificar permisos
			if (!Acceso.TieneAccesoAModulo("HistoricoCumplimiento", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				frmSiNo.Visible = false;
				frmValue.Visible = false;
				frmTwoValues.Visible = false;
				frmOptions.Visible = false;

				btnMostrar.Text = "Mostrar";
				panelBusqueda.Visible = true;
				panelResultados.Visible = false;
				btnLimpiar.Visible = true;
				btnActualizar.Visible = true;

				VM.ObtenerEstados();
				lbEstados.DataSource = VM.Estados;
				lbEstados.DataTextField = "StrEstadoCumplimiento";
				lbEstados.DataValueField = "UidEstadoCumplimiento";
				lbEstados.DataBind();

				VM.ObtenerDepartamentos(SesionActual.UidPeriodos);
				Departamento departamento = new Departamento();
				departamento.UidDepartamento = Guid.Empty;
				departamento.StrNombre = "Todos";
				VM.Departamentos.Insert(0, departamento);
				ddDepartamentos.DataSource = VM.Departamentos;
				ddDepartamentos.DataTextField = "StrNombre";
				ddDepartamentos.DataValueField = "UidDepartamento";
				ddDepartamentos.SelectedIndex = 0;
				ddDepartamentos.DataBind();

				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();

				VM.ObtenerTiposTarea();
				TipoTarea tipo = new TipoTarea();
				tipo.UidTipoTarea = Guid.Empty;
				tipo.StrTipoTarea = "Todos";
				VM.TiposTarea.Insert(0, tipo);
				ddTipo.DataSource = VM.TiposTarea;
				ddTipo.DataValueField = "UidTipoTarea";
				ddTipo.DataTextField = "StrTipoTarea";
				ddTipo.SelectedIndex = 0;
				ddTipo.DataBind();

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				var local = horaLocal.DateTime;

				txtFechaInicio.Text = txtFechaFin.Text = local.ToString("dd/MM/yyyy");

				lblError.Visible = false;
			}
		}

		protected void dgvTareasPendientes_SelectedIndexChanged(object sender, EventArgs e)
		{
			DateTime? proximo;
			Guid uidTarea = new Guid(dgvTareasPendientes.SelectedDataKey.Value.ToString());
			Guid uidDepto = new Guid(dgvTareasPendientes.SelectedDataKey.Values[1].ToString());
			Guid uidArea = new Guid(dgvTareasPendientes.SelectedDataKey.Values[2].ToString());
			Guid uidCumplimiento = new Guid(dgvTareasPendientes.SelectedDataKey.Values[3].ToString());
			Guid uidPeriodo = new Guid(dgvTareasPendientes.SelectedDataKey.Values[4].ToString());

			fldUidTarea.Value = uidTarea.ToString();
			fldUidDepartamento.Value = uidDepto.ToString();
			fldUidArea.Value = uidArea.ToString();
			fldUidCumplimiento.Value = uidCumplimiento.ToString();
			fldUidPerido.Value = uidPeriodo.ToString();

			VM.ObtenerCumplimiento(uidCumplimiento);
			VM.ObtenerTarea(uidTarea);
			VM.ObtenerDepartamento(uidDepto);
			VM.ObtenerArea(uidArea);
			VM.ObtenerPeriocidad(VM.Tarea.UidPeriodicidad);
			VM.ObtenerTipoFrecuencia(VM.Periodicidad.UidTipoFrecuencia);

			lblPeriodicidad.Text = VM.TipoFrecuencia.StrTipoFrecuencia;

			proximo = VM.ObtenerFechaSiguienteTarea(uidTarea, DateTime.Now);


			lblTarea.Text = VM.Tarea.StrNombre;
			lblDepto.Text = VM.Departamento.StrNombre;
			lblArea.Text = VM.Area == null ? "(global)" : VM.Area.StrNombre;

			frmSiNo.Visible = false;
			frmValue.Visible = false;
			frmTwoValues.Visible = false;
			frmOptions.Visible = false;

			rbYes.Checked = false;
			rbNo.Checked = false;
			txtValor.Text = string.Empty;
			txtValor1.Text = string.Empty;
			txtValor2.Text = string.Empty;
			rbYes.Disable();
			rbNo.Disable();
			txtValor.Disable();
			ddOpciones.Disable();
			txtObservaciones.Disable();

			switch (VM.Tarea.StrTipoMedicion)
			{
				case "Verdadero/Falso":
					frmSiNo.Visible = true;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.BlValor.HasValue)
						{
							if (VM.Cumplimiento.BlValor.Value)
							{
								rbYes.Checked = true;
								rbNo.Checked = false;
							}
							else
							{
								rbNo.Checked = true;
								rbYes.Checked = false;
							}
						}
					}
					break;
				case "Numerico":
					frmValue.Visible = true;
					lblUnidad.Text = VM.Tarea.StrUnidadMedida;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.DcValor1.HasValue)
						{
							txtValor.Text = VM.Cumplimiento.DcValor1.Value.ToString("0.####");
						}
					}
					else
					{
						txtValor.Text = "";
					}
					break;
				case "Seleccionable":
					VM.ObtenerOpcionesDeTarea(uidTarea);
					frmOptions.Visible = true;
					ddOpciones.DataSource = VM.Opciones;
					ddOpciones.DataValueField = "UidOpciones";
					ddOpciones.DataTextField = "StrOpciones";
					ddOpciones.DataBind();
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.UidOpcion.HasValue)
						{
							ddOpciones.SelectedValue = VM.Cumplimiento.UidOpcion.Value.ToString();
						}
					}
					break;
			}

			lblFechaCumplimiento.Text = VM.Cumplimiento.DtFechaHora?.ToString("dd/MM/yyyy HH:mm:ss");

			if (!string.IsNullOrWhiteSpace(VM.Cumplimiento.StrObservacion))
				txtObservaciones.Text = VM.Cumplimiento.StrObservacion;
			else
				txtObservaciones.Text = string.Empty;

			int pos = -1;
			if (ViewState["CumplPreviousRow"] != null)
			{
				pos = (int)ViewState["CumplPreviousRow"];
				GridViewRow previousRow = dgvTareasPendientes.Rows[pos];
				previousRow.RemoveCssClass("success-forced");
			}

			ViewState["CumplPreviousRow"] = dgvTareasPendientes.SelectedIndex;
			dgvTareasPendientes.SelectedRow.AddCssClass("success-forced");

		}

		protected void dgvTareasPendientes_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTareasPendientes, "Select$" + e.Row.RowIndex);

				Label icon = e.Row.FindControl("lblCompleto") as Label;

				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					e.Row.Cells[5].Text = "(global)";
				}

				if (e.Row.Cells[6].Text == "&nbsp;")
				{
					e.Row.Cells[6].Text = "N/A";
				}

				if (e.Row.Cells[8].Text == "Requerida")
				{
					e.Row.AddCssClass("font-bold");
					e.Row.Cells[3].Text += "*";
				}

				if (e.Row.Cells[7].Text == "&nbsp;" || e.Row.Cells[7].Text == "No Realizado")
				{
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-pencil");
					icon.ToolTip = "No Realizado";
				}
				else if (e.Row.Cells[7].Text == "Completo")
				{
					e.Row.AddCssClass("info");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-ok");
					icon.ToolTip = "Completo";
				}
				else if (e.Row.Cells[7].Text == "Pospuesto")
				{
					e.Row.AddCssClass("warning");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-arrow-right");
					icon.ToolTip = "Pospuesto";
				}
				else if (e.Row.Cells[7].Text == "Cancelado")
				{
					e.Row.AddCssClass("danger");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-remove");
					icon.ToolTip = "Cancelado";
				}
			}
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			try
			{
				if (lblError.Visible == true) { lblError.Visible = false; }
				frmSiNo.Visible = false;
				frmValue.Visible = false;
				frmTwoValues.Visible = false;
				frmOptions.Visible = false;

				if (string.IsNullOrEmpty(txtFechaInicio.Text))
				{
					txtFechaInicio.Attributes.Add("style", "border:1px solid red;");
					return;
				}
				else if (txtFechaInicio.Attributes.Count == 1)
				{
					txtFechaInicio.Attributes.Remove("style");
				}

				if (string.IsNullOrEmpty(txtFechaFin.Text))
				{
					txtFechaFin.Attributes.Add("style", "border:1px solid red;");
					return;
				}
				else if (txtFechaFin.Attributes.Count == 1)
				{
					txtFechaFin.Attributes.Remove("style");
				}

				string lpr = null;
				int[] i = lbEstados.GetSelectedIndices();
				if (i.Length > 0)
				{
					lpr = lbEstados.Items[i[0]].Value.ToString();
					for (int j = 1; j < i.Length; j++)
					{
						lpr += "," + lbEstados.Items[i[j]].Value.ToString();
					}
				}

				Guid? usuario;
				if (ModoSupervisor)
					usuario = null;
				else
					usuario = SesionActual.uidUsuario;
				VM.ObtenerTareas(
					usuario, 
					SesionActual.UidPeriodo.Value, 
					SesionActual.UidPeriodos, 
					txtNombre.Text, 
					lpr,
					new Guid(ddDepartamentos.SelectedValue),
					new Guid(ddAreas.SelectedValue), 
					new Guid(ddTipo.SelectedValue),
					Convert.ToDateTime(DateTime.ParseExact(txtFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)), 
					Convert.ToDateTime(DateTime.ParseExact(txtFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture))
					);

				VSTareas = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataBind();

				ViewState["CumplPreviousRow"] = null;

				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;
			}
			catch (Exception ex)
			{
				lblError.Visible = true;
				lblError.Text = "Error: " + ex.Message;
			}
		}


		protected void dgvTareasPendientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
			{
				string SortExpression = (string)ViewState["SortColumn"];
				SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
				SortTareas(SortExpression, SortDirection, true);
			}
			else
			{
				dgvTareasPendientes.DataSource = VSTareas;
			}
			dgvTareasPendientes.PageIndex = e.NewPageIndex;
			dgvTareasPendientes.DataBind();
		}

		protected void dgvTareasPendientes_Sorting(object sender, GridViewSortEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			SortTareas(e.SortExpression, e.SortDirection, false);
			dgvTareasPendientes.DataBind();
		}

		private void SortTareas(string SortExpression, SortDirection SortDirection, bool same = false)
		{
			if (SortExpression == (string)ViewState["SortColumn"] && !same)
			{
				// We are resorting the same column, so flip the sort direction
				SortDirection =
					((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
					SortDirection.Descending : SortDirection.Ascending;
			}

			if (SortExpression == "Folio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolioTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolioTarea).ToList();
				}
			}
			if (SortExpression == "FolioCumpl")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolio).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolio).ToList();
				}
			}
			if (SortExpression == "Tarea")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrTarea).ToList();
				}
			}
			else if (SortExpression == "Departamento")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}
			else if (SortExpression == "Area")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrArea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrArea).ToList();
				}
			}
			else if (SortExpression == "Estado")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
			}

			dgvTareasPendientes.DataSource = VSTareas;
			ViewState["SortColumn"] = SortExpression;
			ViewState["SortColumnDirection"] = SortDirection;
		}

		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			txtNombre.Text = "";
			lbEstados.ClearSelection();
			if (lblError.Visible == true) { lblError.Visible = false; }
		}

		private class EstadoComparator : IComparer<string>
		{
			private int GetValue(string s)
			{
				if (s == "No Realizado")
					return -1;
				else if (s == "Completado")
					return 3;
				else if (s == "Cancelado")
					return 2;
				else if (s == "Pospuesto")
					return 1;
				else
					return 10;
			}
			public int Compare(string x, string y)
			{
				return GetValue(x) - GetValue(y);
			}
		}

		protected void btnMostrar_Click(object sender, EventArgs e)
		{
			if (btnMostrar.Text == "Mostrar")
			{
				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;
			}
			else
			{
				btnMostrar.Text = "Mostrar";
				panelBusqueda.Visible = true;
				panelResultados.Visible = false;
				btnLimpiar.Visible = true;
				btnActualizar.Visible = true;
			}
		}

		protected void ddDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid selected = new Guid(ddDepartamentos.SelectedValue);
			if (selected != Guid.Empty)
			{
				VM.ObtenerAreas(selected);
				Area area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000001"); // General
				area.StrNombre = "General";
				VM.Areas.Insert(0, area);
				area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000000"); // Todos
				area.StrNombre = "Todos";
				VM.Areas.Insert(0, area);
				ddAreas.DataSource = VM.Areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.DataBind();

				ddAreas.SelectedIndex = 0;
			}
			else
			{
				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();
			}
		}
	}
}