﻿using CodorniX.Modelo.Entity;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class EjemploReporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<TareaListado> lista = new List<TareaListado>();
                TareaListado tarea = new TareaListado();
                tarea.UidTarea = Guid.Empty;
                tarea.StrNombre = "HO";
                tarea.StrDepartamento = "LA";
                tarea.StrTipo = "OPCION";
                lista.Add(tarea);

                tarea = new TareaListado();
                tarea.UidTarea = Guid.Empty;
                tarea.StrNombre = "HSO";
                tarea.StrDepartamento = "LSA";
                tarea.StrTipo = "BOOL";
                lista.Add(tarea);

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ListaTareas.rdlc");
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Tareas", lista));
                ReportViewer1.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
                ReportViewer1.LocalReport.Refresh();
            }
            else
            {

                ReportViewer1.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
            }
        }

        private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            Guid uid = new Guid(e.Parameters["ID"].Values[0]);


            e.DataSources.Clear();


            List<Opcion> opciones = new List<Opcion>();
            Opcion opcion = new Opcion();
            opcion.StrOpcion = "Aaa";
            opciones.Add(opcion);
            opcion = new Opcion();
            opcion.StrOpcion = "Vas";
            opciones.Add(opcion);

            opcion.StrOpcion = "AHSH";
            opciones.Add(opcion);

            e.DataSources.Add(new ReportDataSource("Opciones", opciones));
        }
    }
}