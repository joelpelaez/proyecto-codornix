﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="HistoricoCumplimiento.aspx.cs" Inherits="CodorniX.Vista.HistoricoCumplimiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="text-center">
								Lista de Tareas del Día
							</div>
						</div>
					</div>
				</div>
				<div class="text-right">
					<div class="btn-group">
						<asp:LinkButton runat="server" ID="btnMostrar" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click" Text="Mostrar" />
						<asp:LinkButton runat="server" ID="btnLimpiar" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Limpiar
						</asp:LinkButton>
						<asp:LinkButton runat="server" ID="btnActualizar" CssClass="btn btn-sm btn-default" OnClick="btnActualizar_Click">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Buscar
						</asp:LinkButton>
					</div>
				</div>
				<div class="col-md-12">
					<asp:Label ID="lblError" Text="Error" CssClass="text-danger" runat="server" />
				</div>
				<div class="panel-body">
					<asp:PlaceHolder ID="panelBusqueda" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<h6>Tarea</h6>
								<asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
							</div>
							<div class="col-xs-6">
								<h6>Fecha Inicio</h6>
								<div class="input-group date start-date">
									<asp:TextBox ID="txtFechaInicio" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-xs-6">
								<h6>Fecha Fin</h6>
								<div class="input-group date start-date">
									<asp:TextBox ID="txtFechaFin" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-xs-6">
								<h6>Departamento</h6>
								<asp:DropDownList ID="ddDepartamentos" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddDepartamentos_SelectedIndexChanged" />
							</div>
							<div class="col-xs-6">
								<h6>Área</h6>
								<asp:DropDownList ID="ddAreas" runat="server" CssClass="form-control" />
							</div>
							<div class="col-xs-6">
								<h6>Estado</h6>
								<asp:ListBox ID="lbEstados" runat="server" CssClass="form-control" SelectionMode="Multiple" />
							</div>
							<div class="col-xs-6">
								<h6>Tipo Tarea</h6>
								<asp:DropDownList ID="ddTipo" runat="server" CssClass="form-control" />
							</div>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="panelResultados" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<asp:GridView runat="server" AllowPaging="true" AllowSorting="true" PageSize="5" ID="dgvTareasPendientes" CssClass="table table-bordered table-condensed table-striped" AutoGenerateColumns="false" DataKeyNames="UidTarea,UidDepartamento,UidArea,UidCumplimiento,UidPeriodo" OnSelectedIndexChanged="dgvTareasPendientes_SelectedIndexChanged" OnRowDataBound="dgvTareasPendientes_RowDataBound" OnPageIndexChanging="dgvTareasPendientes_PageIndexChanging" OnSorting="dgvTareasPendientes_Sorting">
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
									<EmptyDataTemplate>
										No existe cumplimiento con los parametros actuales
									</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
										<asp:BoundField DataField="IntFolioTarea" HeaderText="Folio T." SortExpression="Folio" />
										<asp:BoundField DataField="IntFolio" HeaderText="Folio C." SortExpression="FolioCumpl" />
										<asp:BoundField DataField="StrTarea" HeaderText="Tarea" SortExpression="Tarea" />
										<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
										<asp:BoundField DataField="StrArea" HeaderText="Área" SortExpression="Area" />
										<asp:BoundField DataField="DtFechaHora" HeaderText="Hora" SortExpression="Hora" HtmlEncode="false" DataFormatString="{0:dd/MM/yy HH\:mm}" />
										<asp:BoundField DataField="StrEstadoCumplimiento" HeaderText="Completado" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
										<asp:BoundField DataField="StrTipoTarea" HeaderText="Requerido" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
										<asp:TemplateField HeaderText="Estado" SortExpression="Estado">
											<ItemTemplate>
												<asp:Label runat="server" ID="lblCompleto" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="text-center">
						Tarea
					</div>
				</div>
				<div class="panel-body">
					<asp:HiddenField runat="server" ID="fldUidTarea" />
					<asp:HiddenField runat="server" ID="fldUidDepartamento" />
					<asp:HiddenField runat="server" ID="fldUidArea" />
					<asp:HiddenField runat="server" ID="fldUidCumplimiento" />
					<asp:HiddenField runat="server" ID="fldUidPerido" />
					<asp:HiddenField runat="server" ID="fldEditing" />
					<div class="row">
						<div class="col-xs-12">
							<h6>Departamento:</h6>
							<asp:TextBox runat="server" ID="lblDepto" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-xs-12">
							<h6>Área:</h6>
							<asp:TextBox runat="server" ID="lblArea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-xs-12">
							<h6>Tarea:</h6>
							<asp:TextBox runat="server" ID="lblTarea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-xs-6">
							<h6>Tipo:</h6>
							<asp:TextBox runat="server" ID="lblTipoTarea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-xs-6">
							<h6>Periodicidad:</h6>
							<asp:TextBox runat="server" ID="lblPeriodicidad" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-xs-6">
							<h6>Fecha de cumplimiento</h6>
							<asp:TextBox runat="server" ID="lblFechaCumplimiento" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-inline">
								<h6>Valor:</h6>
								<asp:PlaceHolder runat="server" ID="frmSiNo">
									<label class="radio-inline">
										<asp:RadioButton runat="server" ID="rbYes" GroupName="rbgSiNo" />
										Sí
									</label>
									<label class="radio-inline">
										<asp:RadioButton runat="server" ID="rbNo" GroupName="rbgSiNo" />
										No
									</label>
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="frmValue">
									<div class="form-group" id="valorUnico" runat="server">
										<div class="input-group">
											<asp:TextBox runat="server" ID="txtValor" CssClass="form-control" />
											<div class="input-group-addon">
												<asp:Label runat="server" ID="lblUnidad" />
											</div>
										</div>
									</div>
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="frmTwoValues">
									<div class="input-group">
										<div class="input-group-addon">
											Desde:
										</div>
										<asp:TextBox runat="server" ID="txtValor1" CssClass="form-control" />
										<div class="input-group-addon">
											<asp:Label runat="server" ID="lblUnidad1" />
										</div>
									</div>
									<div class="input-group">
										<div class="input-group-addon">
											Hasta:
										</div>
										<asp:TextBox runat="server" ID="txtValor2" CssClass="form-control" />
										<div class="input-group-addon">
											<asp:Label runat="server" ID="lblUnidad2" />
										</div>
									</div>
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="frmOptions">
									<asp:DropDownList runat="server" ID="ddOpciones" CssClass="form-control" />
								</asp:PlaceHolder>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<label>Observaciones:</label>
							<asp:TextBox TextMode="MultiLine" ID="txtObservaciones" runat="server" CssClass="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		//<![CDATA[
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date.start-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
		}

		function setStartDateLimit(start, end) {
			$(".input-group.date.start-date").datepicker('remove');
			$(".input-group.date.start-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: start,
				endDate: end
			})
			startDateReady = true;
		}


		function pageLoad() {
			enableDatapicker();
		}
        //]]>
	</script>
</asp:Content>
