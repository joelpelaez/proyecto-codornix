﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
    public partial class Asignacion : Page
    {
        private readonly VMAsignacion VM = new VMAsignacion();
        private readonly VMPeriodicidad VMP = new VMPeriodicidad();
        private bool ModoSupervisor = false;

        private Sesion SesionActual => (Sesion)Session["Sesion"];

        private List<Periodo> Periodos
        {
            get
            {
                if (ViewState["Periodos"] == null)
                    ViewState["Periodos"] = new List<Periodo>();

                return (List<Periodo>)ViewState["Periodos"];
            }
            set => ViewState["Periodos"] = value;
        }

        private List<Encargado> Encargados
        {
            get
            {
                if (ViewState["Usuarios"] == null)
                    ViewState["Usuarios"] = new List<Usuario>();

                return (List<Encargado>)ViewState["Usuario"];
            }
            set => ViewState["Usuario"] = value;
        }

        private ListaInactividades Inactividades
        {
            get
            {
                if (ViewState["Inactividades"] == null)
                    ViewState["Inactividades"] = new ListaInactividades();

                return (ListaInactividades)ViewState["Inactividades"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null) return;

            if (!SesionActual.uidSucursalActual.HasValue)
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!Acceso.TieneAccesoAModulo("Asignacion", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (SesionActual.perfil == "Supervisor")
            {

                // El supervisor no tiene acceso al cumplimiento si no ha tomado  algun turno para cumplir.
                if (SesionActual.UidDepartamentos.Count == 0)
                {
                    Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                    return;
                }

                ModoSupervisor = true;
            }

            if (!IsPostBack)
            {
                if (ModoSupervisor)
                    VM.ObtenerDepartamentosPorLista(SesionActual.UidDepartamentos);
                else
                    VM.ObtenerDepartamentos(SesionActual.uidSucursalActual.Value);

                lbDepartamentos.DataSource = VM.Departamentos;
                lbDepartamentos.DataValueField = "UidDepartamento";
                lbDepartamentos.DataTextField = "StrNombre";
                lbDepartamentos.DataBind();
                ddDepartamentos.DataSource = VM.Departamentos;
                ddDepartamentos.DataValueField = "UidDepartamento";
                ddDepartamentos.DataTextField = "StrNombre";
                ddDepartamentos.DataBind();

                VM.ObtenerTurnos();
                lbTurno.DataSource = VM.Turnos;
                lbTurno.DataValueField = "UidTurno";
                lbTurno.DataTextField = "StrTurno";
                lbTurno.DataBind();
                ddTurno.DataSource = VM.Turnos;
                ddTurno.DataValueField = "UidTurno";
                ddTurno.DataTextField = "StrTurno";
                ddTurno.DataBind();

                btnNuevo.Enable();
                btnEditar.Disable();
                btnOKDatos.Visible = false;
                btnCancelarDatos.Visible = false;

                VM.ObtenerTiposInactividad();
                ddTiposInactividad.DataSource = VM.TiposInactividad;
                ddTiposInactividad.DataValueField = "UidTipoInactividad";
                ddTiposInactividad.DataTextField = "StrTipoInactividad";
                ddTiposInactividad.DataBind();

                VM.ObtenerDias();
                DdDiasMensual.DataSource = VM.Dias;
                DdDiasMensual.DataTextField = "StrDias";
                DdDiasMensual.DataBind();

                VM.ObtenerOrdinales();
                DdOrdinalMensual.DataSource = VM.Ordinales;
                DdOrdinalMensual.DataTextField = "StrOrdinal";
                DdOrdinalMensual.DataBind();

                txtNombre.Disable();
                btnBuscarUsuario.Disable();
                ddDepartamentos.Disable();
                ddTurno.Disable();
                txtFechaInicio.Disable();
                txtFechaFin.Disable();

                btnAgregarInactividad.Disable();
                btnEditarInactividad.Disable();
                btnEliminarInactividad.Disable();

                dgvPeriodosInactividad.DataSource = null;
                dgvPeriodosInactividad.DataBind();
            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            if (btnMostrar.Text.Contains("Mostrar"))
            {
                panelBusqueda.Visible = false;
                panelResultados.Visible = true;
                btnBorrar.Visible = false;
                btnBuscar.Visible = false;
                btnMostrar.Text = "Ocultar";
            }
            else
            {
                panelBusqueda.Visible = true;
                panelResultados.Visible = false;
                btnBorrar.Visible = true;
                btnBuscar.Visible = true;
                btnMostrar.Text = "Mostrar";
            }
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            busquedaNombre.Text = string.Empty;
            busquedaNombre.Enable();

            busquedaFechaInicio.Text = string.Empty;
            busquedaFechaFin.Text = string.Empty;
            lbDepartamentos.ClearSelection();
            lbTurno.ClearSelection();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            btnMostrar_Click(sender, e);

            var usuario = busquedaNombre.Text;

            var departamentos = "";
            var i = lbDepartamentos.GetSelectedIndices();
            foreach (var j in i)
            {
                var value = lbDepartamentos.Items[j].Value;
                if (!departamentos.Any())
                    departamentos += value;
                else
                    departamentos += "," + value;
            }

            var turno = "";
            i = lbTurno.GetSelectedIndices();
            foreach (var j in i)
            {
                var value = lbDepartamentos.Items[j].Value;
                if (!turno.Any())
                    turno += value;
                else
                    turno += "," + value;
            }

            DateTime? fechaInicio = null;
			if (!string.IsNullOrWhiteSpace(busquedaFechaInicio.Text))
			{ fechaInicio = Convert.ToDateTime(DateTime.ParseExact(busquedaFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)); }

            DateTime? fechaFin = null;
			if (!string.IsNullOrWhiteSpace(busquedaFechaFin.Text))
			{ fechaFin = Convert.ToDateTime(DateTime.ParseExact(busquedaFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)); }

            if (ModoSupervisor && departamentos == String.Empty)
            {
                foreach (var depto in SesionActual.UidDepartamentos)
                {
                    var value = depto.ToString();
                    if (!departamentos.Any())
                        departamentos += value;
                    else
                        departamentos += "," + value;
                }
            }

            VM.BuscarAsignaciones(fechaInicio, null, null, fechaFin, usuario, turno, departamentos,
                SesionActual.uidSucursalActual.Value);
            dgvPeriodos.DataSource = VM.Periodos;
            dgvPeriodos.DataBind();
            Periodos = VM.Periodos;
        }


        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Inactividades.Resetear();
            dgvPeriodosInactividad.DataSource = Inactividades.periodosInactividad;
            dgvPeriodosInactividad.DataBind();
            uidPeriodo.Value = string.Empty;
            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.Text = "Buscar Encargado";
            btnBuscarUsuario.Enable();
            uidUsuario.Value = string.Empty;
            txtNombre.Enable();
            txtNombre.Text = string.Empty;

            txtFechaInicio.Text = string.Empty;
            txtFechaInicio.Enable();
            txtFechaFin.Text = string.Empty;
            txtFechaFin.Enable();
            ddTurno.SelectedIndex = 0;
            ddTurno.Enable();
            ddDepartamentos.SelectedIndex = 0;
            ddDepartamentos.Enable();

            btnNuevo.Disable();
            btnEditar.Disable();
            btnOKDatos.Visible = true;
            btnOKDatos.Enable();
            btnCancelarDatos.Visible = true;
            btnCancelarDatos.Enable();

            btnAgregarInactividad.Enable();

            UpdateStartDate();
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            userFields.Visible = true;
            userGrid.Visible = false;

            txtFechaFin.Enable();

            btnNuevo.Disable();
            btnEditar.Disable();
            btnOKDatos.Visible = true;
            btnOKDatos.Enable();
            btnCancelarDatos.Visible = true;
            btnCancelarDatos.Enable();
            btnBuscarUsuario.Text = "Seleccionar otro";

            btnAgregarInactividad.Enable();

            UpdateEndDate();
        }

        protected void btnOKDatos_Click(object sender, EventArgs e)
        {
            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.Text = "Buscar Encargado";

            if (string.IsNullOrWhiteSpace(uidUsuario.Value))
            {
                lbMensaje.Text = "Debe seleccionar un encargado para el area";
                btnBuscarUsuario.AddCssClass("btn-primary").RemoveCssClass("btn-default");
                btnBuscarUsuario.Focus();
                frmGrpEncargado.AddCssClass("has-error");
                return;
            }

            btnBuscarUsuario.RemoveCssClass("btn-primary").AddCssClass("btn-default");
            frmGrpEncargado.RemoveCssClass("has-error");
            var uidUsu = new Guid(uidUsuario.Value);

            if (string.IsNullOrWhiteSpace(txtFechaInicio.Text))
            {
                lbMensaje.Text = "Debe seleccionar una fecha de inicio válida";
                txtFechaInicio.Focus();
                frmGrpFechaInicio.AddCssClass("has-error");
                return;
            }

            frmGrpFechaInicio.RemoveCssClass("has-error");

            var fechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));

            DateTime? fechaFin = null;
			if (!string.IsNullOrWhiteSpace(txtFechaFin.Text)) { fechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)); }

            var uidTurno = new Guid(ddTurno.SelectedValue);
            var uidDepartamento = new Guid(ddDepartamentos.SelectedValue);

            var periodo = new Periodo
            {
                UidUsuario = uidUsu,
                DtFechaInicio = fechaInicio,
                DtFechaFin = fechaFin,
                UidTurno = uidTurno,
                UidDepartamento = uidDepartamento
            };

            var code = VM.GuardarAsignacion(periodo);
            switch (code)
            {
                case Periodo.ReturnCode.Success:
                    lbMensaje.Text = "Se ha asignado el departamento exitosamente.";
                    break;
                case Periodo.ReturnCode.SuccessWithChanges:
                    lbMensaje.Text = "Se ha asignado el departamento, la asignación anterior ha sido modificada.";
                    break;
                case Periodo.ReturnCode.SuccessUpdate:
                    lbMensaje.Text = "Se ha actualizado la asignación.";
                    break;
                case Periodo.ReturnCode.FailureSame:
                    lbMensaje.Text = "No se ha podido insertar, existe una asignación similar.";
                    break;
                case Periodo.ReturnCode.FailureMax:
                    lbMensaje.Text =
                        "No se ha podido asignar el departamento al encargado, no tiene espacio para manejar otra.";
                    break;
                case Periodo.ReturnCode.Undefined:
                    lbMensaje.Text = "Error al procesar la solicitud, intentelo nuevamente.";
                    break;
            }

            // Guardar inactividades

            foreach (var inactividad in Inactividades.inactividades)
            {
                var periodoInactividad = inactividad.periodoInactividad;
                var periodicidad = inactividad.periodicidad;
                var semanal = inactividad.periodicidadSemanal;
                var mensual = inactividad.periodicidadMensual;

                if (inactividad.create)
                {
                    if (periodicidad != null)
                    {
                        periodicidad.GuardarDatos();
                        VM.ObtenerTipoFrecuencia(periodicidad.UidTipoFrecuencia);
                        switch (VM.TipoFrecuencia.StrTipoFrecuencia)
                        {
                            case "Diaria":
                                // none
                                break;
                            case "Semanal":
                                semanal.UidPeriodicidad = periodicidad.UidPeriodicidad;
                                semanal.GuardarDatos();
                                break;
                            case "Mensual":
                                mensual.UidPeriodicidad = periodicidad.UidPeriodicidad;
                                mensual.GuardarDatos();
                                break;
                        }
                    }

                    periodoInactividad.UidPeriodicidad = periodicidad.UidPeriodicidad;
                    periodoInactividad.UidPeriodo = periodo.UidPeriodo;
                    VM.GuardarInactividad(periodoInactividad);
                }

                if (inactividad.update) VM.GuardarInactividad(periodoInactividad);
            }

            // END

            userFields.Visible = true;
            userGrid.Visible = false;
            uidUsuario.Value = string.Empty;
            txtNombre.Disable();
            txtNombre.Text = string.Empty;
            txtFechaInicio.Text = string.Empty;
            txtFechaInicio.Disable();
            txtFechaFin.Text = string.Empty;
            txtFechaFin.Disable();
            ddTurno.SelectedIndex = 0;
            ddTurno.Disable();
            ddDepartamentos.SelectedIndex = 0;
            ddDepartamentos.Disable();

            btnNuevo.Enable();
            btnOKDatos.Visible = false;
            btnOKDatos.Disable();
            btnCancelarDatos.Visible = false;
            btnCancelarDatos.Disable();

            btnMostrar.Text = "Mostrar";
            btnBuscar_Click(sender, e);
        }

        protected void btnCancelarDatos_Click(object sender, EventArgs e)
        {
            userFields.Visible = true;
            userGrid.Visible = false;

            if (!string.IsNullOrWhiteSpace(uidPeriodo.Value))
            {
                VM.ObtenerAsignacion(new Guid(uidPeriodo.Value));
                VM.ObtenerUsuario(VM.Periodo.UidUsuario);

                uidUsuario.Value = VM.Periodo.UidUsuario.ToString();
                txtNombre.Text = VM.Usuario.STRNOMBRE;
                ddDepartamentos.SelectedValue = VM.Periodo.UidDepartamento.ToString();
                ddTurno.SelectedValue = VM.Periodo.UidTurno.ToString();
                txtFechaInicio.Text = VM.Periodo.DtFechaInicio.ToString("dd/MM/yyyy");
                txtFechaFin.Text = VM.Periodo.DtFechaFin?.ToString("dd/MM/yyyy");

                VM.ObtenerInactividades(new Guid(uidPeriodo.Value), null, null, null, null, null);
                Inactividades.Resetear();
                Inactividades.Rellenar(VM.PeriodosInactividad);
                dgvPeriodosInactividad.DataSource = Inactividades.periodosInactividad;
                dgvPeriodosInactividad.DataBind();

                btnEditar.Enable();
                btnBuscarUsuario.Text = "Seleccionar otro";
            }
            else
            {
                uidUsuario.Value = string.Empty;
                txtNombre.Text = string.Empty;

                txtFechaInicio.Text = string.Empty;
                txtFechaFin.Text = string.Empty;
                ddTurno.SelectedIndex = 0;
                ddDepartamentos.SelectedIndex = 0;

                Inactividades.Resetear();
                dgvPeriodosInactividad.DataSource = Inactividades.periodosInactividad;
                dgvPeriodosInactividad.DataBind();

                btnBuscarUsuario.Text = "Buscar Encargado";
            }

            lbMensaje.Text = string.Empty;
            txtNombre.Disable();
            txtFechaInicio.Disable();
            txtFechaFin.Disable();
            ddTurno.Disable();
            ddDepartamentos.Disable();
            btnNuevo.Enable();
            btnOKDatos.Visible = false;
            btnOKDatos.Disable();
            btnCancelarDatos.Visible = false;
            btnCancelarDatos.Disable();
            btnAgregarInactividad.Disable();
            btnEditarInactividad.Disable();
            btnEliminarInactividad.Disable();
            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.RemoveCssClass("btn-primary").AddCssClass("btn-default");
            frmGrpEncargado.RemoveCssClass("has-error");
            frmGrpFechaInicio.RemoveCssClass("has-error");
        }

        protected void btnBuscarUsuario_Click(object sender, EventArgs e)
        {
            if (btnBuscarUsuario.Text.Contains("Buscar Encargado"))
            {
                var nombre = txtNombre.Text;

                VM.BuscarUsuario(nombre, SesionActual.uidSucursalActual.Value);

                if (VM.Encargados.Count == 0) return;

                Encargados = VM.Encargados;
                dgvUsuario.DataSource = VM.Encargados;
                dgvUsuario.DataBind();

                userFields.Visible = false;
                userGrid.Visible = true;
            }
            else
            {
                uidUsuario.Value = string.Empty;
                txtNombre.Text = string.Empty;
                txtNombre.Enable();
                btnBuscarUsuario.Text = "Buscar Encargado";
            }
        }

        protected void dgvUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            VM.ObtenerUsuario(new Guid(dgvUsuario.SelectedDataKey.Value.ToString()));
            uidUsuario.Value = VM.Usuario.UIDUSUARIO.ToString();
            txtNombre.Text = VM.Usuario.STRNOMBRE + ' ' + VM.Usuario.STRAPELLIDOPATERNO;
            txtNombre.Disable();

            userFields.Visible = true;
            userGrid.Visible = false;
            btnBuscarUsuario.Text = "Seleccionar otro";
            UpdateStartDate();
        }

        protected void dgvUsuario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                e.Row.Attributes["onclick"] =
                    ClientScript.GetPostBackClientHyperlink(dgvUsuario, "Select$" + e.Row.RowIndex);
        }

        protected void dgvPeriodos_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnCancelarDatos_Click(sender, e);

            var uidPeriodo = new Guid(dgvPeriodos.SelectedDataKey.Value.ToString());
            VM.ObtenerAsignacion(uidPeriodo);
            VM.ObtenerUsuario(VM.Periodo.UidUsuario);

            this.uidPeriodo.Value = VM.Periodo.UidPeriodo.ToString();
            uidUsuario.Value = VM.Periodo.UidUsuario.ToString();
            txtNombre.Text = VM.Usuario.STRNOMBRE;
            ddDepartamentos.SelectedValue = VM.Periodo.UidDepartamento.ToString();
            ddTurno.SelectedValue = VM.Periodo.UidTurno.ToString();
            txtFechaInicio.Text = VM.Periodo.DtFechaInicio.ToString("dd/MM/yyyy");
            txtFechaFin.Text = VM.Periodo.DtFechaFin?.ToString("dd/MM/yyyy");

            VM.ObtenerInactividades(uidPeriodo, null, null, null, null, null);
            Inactividades.Resetear();
            Inactividades.Rellenar(VM.PeriodosInactividad);
            dgvPeriodosInactividad.DataSource = Inactividades.periodosInactividad;
            dgvPeriodosInactividad.DataBind();
            btnEditar.Enable();


            btnAgregarInactividad.Disable();
            btnEditarInactividad.Disable();
            btnEliminarInactividad.Disable();

            var pos = -1;
            if (ViewState["PeriodosPreviousRow"] != null)
            {
                pos = (int)ViewState["PeriodosPreviousRow"];
                var previousRow = dgvPeriodos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["PeriodosPreviousRow"] = dgvPeriodos.SelectedIndex;
            dgvPeriodos.SelectedRow.AddCssClass("success");
        }

        protected void dgvPeriodos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                e.Row.Attributes["onclick"] =
                    ClientScript.GetPostBackClientHyperlink(dgvPeriodos, "Select$" + e.Row.RowIndex);
        }

        protected void ddDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateStartDate();
        }

        protected void ddTurno_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateStartDate();
        }

        private void UpdateStartDate()
        {
            DateTime newStart;
            var departamento = new Guid(ddDepartamentos.SelectedValue);
            var turno = new Guid(ddTurno.SelectedValue);
            VM.ObtenerUltimaAsignacion(departamento, turno);
            if (VM.Periodo != null && VM.Periodo.DtFechaFin.HasValue)
                newStart = VM.Periodo.DtFechaFin.Value;
            else
                newStart = DateTime.Today;
            var script = "setStartDateLimit('" + newStart.ToString("dd/MM/yyyy") + "')";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateStartDate", script, true);
        }

        private void UpdateEndDate()
        {
            var newStart = DateTime.Today;
            if (!string.IsNullOrWhiteSpace(uidPeriodo.Value))
            {
                var periodo = new Guid(uidPeriodo.Value);
                VM.ObtenerAsignacion(periodo);
                if (VM.Periodo != null && VM.Periodo.DtFechaFin.HasValue)
                    newStart = VM.Periodo.DtFechaFin.Value.AddDays(1);
            }

            var script = "setEndDateLimit('" + newStart.ToString("dd/MM/yyyy") + "')";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateEndDate", script, true);
        }

        private void SortPeriodo(string SortExpression, SortDirection SortDirection, bool same = false)
        {
            if (SortExpression == (string)ViewState["SortColumn"] && !same)
                SortDirection =
                    (SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending
                        ? SortDirection.Descending
                        : SortDirection.Ascending;

            if (SortExpression == "NombreUsuario")
            {
                if (SortDirection == SortDirection.Ascending)
                    Periodos = Periodos.OrderBy(x => x.StrNombreUsuario).ToList();
                else
                    Periodos = Periodos.OrderByDescending(x => x.StrNombreUsuario).ToList();
            }
            else if (SortExpression == "NombreDepto")
            {
                if (SortDirection == SortDirection.Ascending)
                    Periodos = Periodos.OrderBy(x => x.StrNombreDepto).ToList();
                else
                    Periodos = Periodos.OrderByDescending(x => x.StrNombreDepto).ToList();
            }
            else if (SortExpression == "Turno")
            {
                if (SortDirection == SortDirection.Ascending)
                    Periodos = Periodos.OrderBy(x => x.StrTurno).ToList();
                else
                    Periodos = Periodos.OrderByDescending(x => x.StrTurno).ToList();
            }
            else if (SortExpression == "FechaInicio")
            {
                if (SortDirection == SortDirection.Ascending)
                    Periodos = Periodos.OrderBy(x => x.DtFechaInicio).ToList();
                else
                    Periodos = Periodos.OrderByDescending(x => x.DtFechaInicio).ToList();
            }
            else
            {
                if (SortDirection == SortDirection.Ascending)
                    Periodos = Periodos.OrderBy(x => x.DtFechaFin).ToList();
                else
                    Periodos = Periodos.OrderByDescending(x => x.DtFechaFin).ToList();
            }

            dgvPeriodos.DataSource = Periodos;
            ViewState["SortColumn"] = SortExpression;
            ViewState["SortColumnDirection"] = SortDirection;
        }

        protected void dgvPeriodos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PeriodosPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                var SortExpression = (string)ViewState["SortColumn"];
                var SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortPeriodo(SortExpression, SortDirection, true);
            }
            else
            {
                dgvPeriodos.DataSource = Periodos;
            }

            dgvPeriodos.PageIndex = e.NewPageIndex;
            dgvPeriodos.DataBind();
        }

        protected void dgvPeriodos_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["PeriodosPreviousRow"] = null;
            SortPeriodo(e.SortExpression, e.SortDirection);
            dgvPeriodos.DataBind();
        }

        private void SortEncargado(string SortExpression, SortDirection SortDirection, bool same = false)
        {
            if (SortExpression == (string)ViewState["EncargadoSortColumn"] && !same)
                SortDirection =
                    (SortDirection)ViewState["EncargadoSortColumnDirection"] == SortDirection.Ascending
                        ? SortDirection.Descending
                        : SortDirection.Ascending;

            if (SortExpression == "Nombre")
                if (SortDirection == SortDirection.Ascending)
                    Encargados = Encargados.OrderBy(x => x.STRNOMBRE).ToList();
                else
                    Encargados = Encargados.OrderByDescending(x => x.STRNOMBRE).ToList();
            else if (SortExpression == "ApellidoPaterno")
                if (SortDirection == SortDirection.Ascending)
                    Encargados = Encargados.OrderBy(x => x.STRAPELLIDOPATERNO).ToList();
                else
                    Encargados = Encargados.OrderByDescending(x => x.STRAPELLIDOPATERNO).ToList();
            else if (SortExpression == "ApellidoMaterno")
                if (SortDirection == SortDirection.Ascending)
                    Encargados = Encargados.OrderBy(x => x.STRAPELLIDOMATERNO).ToList();
                else
                    Encargados = Encargados.OrderByDescending(x => x.STRAPELLIDOMATERNO).ToList();
            dgvUsuario.DataSource = Encargados;
            ViewState["EncargadoSortColumn"] = SortExpression;
            ViewState["EncargadoSortColumnDirection"] = SortDirection;
        }

        protected void dgvUsuario_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["EncargadoPreviousRow"] = null;
            SortEncargado(e.SortExpression, e.SortDirection);
            dgvUsuario.DataBind();
        }

        protected void dgvUsuario_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["EncargadoPreviousRow"] = null;
            if (ViewState["EncargadoSortColumn"] != null && ViewState["EncargadoSortColumnDirection"] != null)
            {
                var SortExpression = (string)ViewState["EncargadoSortColumn"];
                var SortDirection = (SortDirection)ViewState["EncargadoSortColumnDirection"];
                SortEncargado(SortExpression, SortDirection, true);
            }
            else
            {
                dgvUsuario.DataSource = Periodos;
            }

            dgvUsuario.PageIndex = e.NewPageIndex;
            dgvUsuario.DataBind();
        }

        protected void rdCadaDiario_CheckedChanged(object sender, EventArgs e)
        {
            if (rdCadaDiario.Checked)
            {
                txtcadaDiario.Enable();
            }
            else if (rdtodoslosdias.Checked)
            {
                txtcadaDiario.Disable();
                txtcadaDiario.Text = string.Empty;
            }
        }

        protected void rdElDiaMensual_CheckedChanged(object sender, EventArgs e)
        {
            if (rdElDiaMensual.Checked)
            {
                txtDiasMes.Enable();
                txtCadames.Enable();
                txtcadamensual.Disable();
                txtcadamensual.Text = string.Empty;
            }
            else if (rdElMeses.Checked)
            {
                txtDiasMes.Disable();
                txtCadames.Disable();
                txtDiasMes.Text = string.Empty;
                txtCadames.Text = string.Empty;
                txtcadamensual.Enable();
            }
        }


        protected void rdSemanal_CheckedChanged(object sender, EventArgs e)
        {
            if (rdSinperiodicidad.Checked)
            {
				chkSinPeriodicidad();
            }
            else if (rdDiaro.Checked)
            {
				chkDiario();
            }
            else if (rdSemanal.Checked)
            {
				chkSemanal();
            }
            else if (rdMensual.Checked)
            {
				chkMensual();
            }
            else
            {
                PanelDiario.Visible = false;
                PanelSemanal.Visible = false;
                PanelMensual.Visible = false;
            }
        }
		public void chkSinPeriodicidad() {
			PanelDiario.Visible = false;
			PanelSemanal.Visible = false;
			PanelMensual.Visible = false;
			txtradio.Text = "Sin periodicidad";
			VMP.ConsultarTipoFrecuencia(txtradio.Text);
			txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
		}
		public void chkDiario() {
			PanelDiario.Visible = true;
			PanelSemanal.Visible = false;
			PanelMensual.Visible = false;
			txtradio.Text = "Diaria";
			VMP.ConsultarTipoFrecuencia(txtradio.Text);
			txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
		}
		public void chkSemanal() {
			PanelDiario.Visible = false;
			PanelSemanal.Visible = true;
			PanelMensual.Visible = false;
			txtradio.Text = "Semanal";
			VMP.ConsultarTipoFrecuencia(txtradio.Text);
			txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
		}
		public void chkMensual()
		{
			PanelDiario.Visible = false;
			PanelSemanal.Visible = false;
			PanelMensual.Visible = true;
			txtradio.Text = "Mensual";
			VMP.ConsultarTipoFrecuencia(txtradio.Text);
			txtUidTipoFrecuencia.Text = VMP.CTipoFrecuencia.UidTipoFrecuencia.ToString();
		}
		protected void disablePeriodsFields(bool status) {
			rdSinperiodicidad.Enabled = status;

			rdDiaro.Enabled = status;
			rdCadaDiario.Enabled = status;
			txtcadaDiario.Enabled = status;
			rdtodoslosdias.Enabled = status;

			rdSemanal.Enabled = status;
			txtFrecuenciaSemanal.Enabled = status;
			CBLunes.Enabled = status;
			CBMartes.Enabled = status;
			CBMiercoles.Enabled = status;
			CBJueves.Enabled = status;
			CBViernes.Enabled = status;
			CBSabado.Enabled = status;
			CBDomingo.Enabled = status;

			rdMensual.Enabled = status;
			rdElDiaMensual.Enabled = status;
			txtDiasMes.Enabled = status;
			txtCadames.Enabled = status;
			rdElMeses.Enabled = status;
			DdOrdinalMensual.Enabled = status;
			DdDiasMensual.Enabled = status;
			txtcadamensual.Enabled = status;
		}

        protected void dgvPeriodosInactividad_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                e.Row.Attributes["onclick"] =
                    ClientScript.GetPostBackClientHyperlink(dgvPeriodosInactividad, "Select$" + e.Row.RowIndex);
        }

        protected void dgvPeriodosInactividad_SelectedIndexChanged(object sender, EventArgs e)
        {
			if (btnOKDatos.Visible == true) {
				var idPeriodoInactividad = new Guid(dgvPeriodosInactividad.SelectedDataKey.Value.ToString());
				uidPeriodoInactividad.Value = idPeriodoInactividad.ToString();
				var periodoInactividad = Inactividades.Encontrar(idPeriodoInactividad);
				Periodicidad periodicidad = null;
				PeriodicidadSemanal semanal = null;
				PeriodicidadMensual mensual = null;

				txtFechaInactividadInicio.Text = periodoInactividad.DtFechaInicio.ToString("dd/MM/yyyy");
				txtFechaInactividadFin.Text = periodoInactividad.DtFechaFin?.ToString("dd/MM/yyyy");
				txtNotas.Text = periodoInactividad.StrNotas;
				ddTiposInactividad.SelectedValue = periodoInactividad.UidTipoInactividad.ToString();

				CBLunes.Checked = false;
				CBMartes.Checked = false;
				CBMiercoles.Checked = false;
				CBJueves.Checked = false;
				CBViernes.Checked = false;
				CBSabado.Checked = false;
				CBDomingo.Checked = false;

				txtFrecuenciaSemanal.Text = string.Empty;
				txtcadaDiario.Text = string.Empty;
				txtcadamensual.Text = string.Empty;
				txtCadames.Text = string.Empty;

				panelCamposInactividad.Visible = true;
				panelGridInactividad.Visible = false;

				if (periodoInactividad.UidPeriodicidad.HasValue)
				{
					if (periodoInactividad.BlUserCreated)
					{
						periodicidad = Inactividades.EncontrarInactividad(idPeriodoInactividad).periodicidad;
					}
					else
					{
						VM.ObtenerPeriodicidad(periodoInactividad.UidPeriodicidad.Value);
						periodicidad = VM.Periodicidad;
					}

					VM.ObtenerTipoFrecuencia(periodicidad.UidTipoFrecuencia);
					rdSemanal_CheckedChanged(sender, e);

					if (VM.TipoFrecuencia == null)
						rdSinperiodicidad.Checked = true;
					else
						switch (VM.TipoFrecuencia.StrTipoFrecuencia)
						{
							case "Diaria":
								chkDiario();
								rdDiaro.Checked = true;
								if (periodicidad.IntFrecuencia == 1)
									rdtodoslosdias.Checked = true;
								else
									rdCadaDiario.Checked = true;
								txtcadaDiario.Text = periodicidad.IntFrecuencia.ToString();
								break;
							case "Semanal":
								chkSemanal();
								rdSemanal.Checked = true;
								VM.ObtenerPeriodicidadSemanal(periodicidad.UidPeriodicidad);
								semanal = VM.PeriodicidadSemanal;
								if (semanal.BlLunes)
									CBLunes.Checked = true;
								if (semanal.BlMartes)
									CBMartes.Checked = true;
								if (semanal.BlMiercoles)
									CBMiercoles.Checked = true;
								if (semanal.BlJueves)
									CBJueves.Checked = true;
								if (semanal.BlViernes)
									CBViernes.Checked = true;
								if (semanal.BlSabado)
									CBSabado.Checked = true;
								if (semanal.BlDomingo)
									CBDomingo.Checked = true;
								txtFrecuenciaSemanal.Text = periodicidad.IntFrecuencia.ToString();
								break;
							case "Mensual":
								chkMensual();
								rdMensual.Checked = true;
								VM.ObtenerPeriodicidadMensual(periodicidad.UidPeriodicidad);
								mensual = VM.PeriodicidadMensual;
								break;
						}

					disablePeriodsFields(false);
					btnEditarInactividad.Enable();
					btnEliminarInactividad.Enable();					
				}
			}
        }

        protected void tabDatos_Click(object sender, EventArgs e)
        {
            activoDatos.AddCssClass("active");
            activoInactividad.RemoveCssClass("active");

            panelDatos.Visible = true;
            panelInactividad.Visible = false;
        }

        protected void tabInactividad_Click(object sender, EventArgs e)
        {
            activoDatos.RemoveCssClass("active");
            activoInactividad.AddCssClass("active");

            panelDatos.Visible = false;
            panelInactividad.Visible = true;
        }

        protected void btnAgregarInactividad_Click(object sender, EventArgs e)
        {
            btnAgregarInactividad.Disable();
            btnEditarInactividad.Disable();
            btnEliminarInactividad.Disable();
            btnOKInactividad.Visible = true;
            btnOKInactividad.Enable();
            btnCancelarInactividad.Visible = true;
            btnCancelarInactividad.Enable();

            panelCamposInactividad.Visible = true;
            panelGridInactividad.Visible = false;

            txtFechaInactividadInicio.Enable();
            txtFechaInactividadInicio.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFechaInactividadFin.Enable();
            txtFechaInactividadFin.Text = DateTime.Now.ToString("dd/MM/yyyy");
            ddTiposInactividad.SelectedIndex = 0;
            ddTiposInactividad.Enable();
            txtNotas.Enable();
            txtNotas.Text = string.Empty;

            rdSinperiodicidad.Checked = true;
            rdSinperiodicidad.Enable();
            rdDiaro.Enable();
            rdSemanal.Enable();
            rdMensual.Enable();

            rdCadaDiario.Enable();
            rdtodoslosdias.Enable();

            CBLunes.Checked = false;
            CBMartes.Checked = false;
            CBMiercoles.Checked = false;
            CBJueves.Checked = false;
            CBViernes.Checked = false;
            CBSabado.Checked = false;
            CBDomingo.Checked = false;

            CBLunes.Enable();
            CBMartes.Enable();
            CBMiercoles.Enable();
            CBJueves.Enable();
            CBViernes.Enable();
            CBSabado.Enable();
            CBDomingo.Enable();

            rdElDiaMensual.Enable();
            rdElMeses.Enable();

            txtFrecuenciaSemanal.Text = "1";
            txtcadaDiario.Text = "1";
            txtcadamensual.Text = "1";
            txtCadames.Text = "1";
            txtDiasMes.Text = "1";
        }

        protected void btnEditarInactividad_Click(object sender, EventArgs e)
        {
            btnAgregarInactividad.Disable();
            btnEditarInactividad.Disable();
            btnEliminarInactividad.Disable();
            btnOKInactividad.Visible = true;
            btnOKInactividad.Enable();
            btnCancelarInactividad.Visible = true;
            btnCancelarInactividad.Enable();


            panelCamposInactividad.Visible = true;
            panelGridInactividad.Visible = false;

            txtFechaInactividadInicio.Enable();
            txtFechaInactividadFin.Enable();
            ddTiposInactividad.Enable();
            txtNotas.Enable();

            rdSinperiodicidad.Checked = true;
            rdSinperiodicidad.Enable();
            rdDiaro.Enable();
            rdSemanal.Enable();
            rdMensual.Enable();

            rdCadaDiario.Enable();
            rdtodoslosdias.Enable();

            CBLunes.Enable();
            CBMartes.Enable();
            CBMiercoles.Enable();
            CBJueves.Enable();
            CBViernes.Enable();
            CBSabado.Enable();
            CBDomingo.Enable();

            rdElDiaMensual.Enable();
            rdElMeses.Enable();
        }

        protected void btnEliminarInactividad_Click(object sender, EventArgs e)
        {
            btnAgregarInactividad.Disable();
            btnEditarInactividad.Disable();
            btnEliminarInactividad.Disable();
            btnOKInactividad.Visible = true;
            btnOKInactividad.Enable();
            btnCancelarInactividad.Visible = true;
            btnCancelarInactividad.Enable();
        }

        protected void btnOKInactividad_Click(object sender, EventArgs e)
        {
            if (uidPeriodoInactividad.Value != string.Empty)
            {
                var periodo = new PeriodoInactividad();
                periodo.UidPeriodoInactividad = new Guid(uidPeriodoInactividad.Value);
                try
                {
                    periodo.DtFechaInicio = Convert.ToDateTime(txtFechaInactividadInicio.Text);
                }
                catch
                {
                    return;
                }

                try
                {
                    periodo.DtFechaFin = Convert.ToDateTime(txtFechaInactividadFin.Text);
                }
                catch
                {
                    return;
                }

                Inactividades.Actualizar(periodo);

                return;
            }

            var periodoInactividad = new PeriodoInactividad();
            try
            {
                periodoInactividad.DtFechaInicio = Convert.ToDateTime(txtFechaInactividadInicio.Text);
            }
            catch
            {
                return;
            }

            try
            {
                periodoInactividad.DtFechaFin = Convert.ToDateTime(txtFechaInactividadFin.Text);
            }
            catch
            {
                return;
            }

            // Se agregaga al guardar
            //periodoInactividad.UidPeriodo = new Guid(uidPeriodo.Value);
            periodoInactividad.UidTipoInactividad = new Guid(ddTiposInactividad.SelectedValue);
            periodoInactividad.StrTipoInactividad = ddTiposInactividad.SelectedItem.Text;
            periodoInactividad.StrNotas = txtNotas.Text;
            var inactividad = new Inactividad();
            inactividad.periodoInactividad = periodoInactividad;

            if (!rdSinperiodicidad.Checked)
            {
                var uidPeriodicidad = Guid.Empty;
                var periodicidad = new Periodicidad();
                periodicidad.DtFechaInicio = Convert.ToDateTime(txtFechaInactividadInicio.Text);
                periodicidad.DtFechaFin = Convert.ToDateTime(txtFechaInactividadFin.Text);
                VM.BuscarTipoFrecuencia(txtradio.Text);
                periodicidad.UidTipoFrecuencia = VM.TipoFrecuencia.UidTipoFrecuencia;
                if (txtradio.Text == "Diaria")
                {
                    periodicidad.IntFrecuencia = Convert.ToInt32(txtcadaDiario.Text);
                    inactividad.periodicidad = periodicidad;
                }

                if (txtradio.Text == "Mensual")
                {
                    var mensual = new PeriodicidadMensual();
                    mensual.UidPeriodicidad = uidPeriodicidad;
                    if (rdElDiaMensual.Checked)
                    {
                        periodicidad.IntFrecuencia = Convert.ToInt32(txtCadames.Text);
                        mensual.IntDiasMes = Convert.ToInt32(txtDiasMes.Text);
                        mensual.IntDiasSemana = 0;
                    }
                    else if (rdElMeses.Checked)
                    {
                        if (DdOrdinalMensual.SelectedItem.Text == "Primer")
                            mensual.IntDiasMes = 1;
                        else if (DdOrdinalMensual.SelectedItem.Text == "Segundo")
                            mensual.IntDiasMes = 2;
                        else if (DdOrdinalMensual.SelectedItem.Text == "Tercer")
                            mensual.IntDiasMes = 3;
                        else if (DdOrdinalMensual.SelectedItem.Text == "Cuarto")
                            mensual.IntDiasMes = 4;
                        else if (DdOrdinalMensual.SelectedItem.Text == "Ultimo") mensual.IntDiasMes = -1;

                        if (DdDiasMensual.SelectedItem.Text == "Domingo")
                            mensual.IntDiasSemana = 1;
                        else if (DdDiasMensual.SelectedItem.Text == "Lunes")
                            mensual.IntDiasSemana = 2;
                        else if (DdDiasMensual.SelectedItem.Text == "Martes")
                            mensual.IntDiasSemana = 3;
                        else if (DdDiasMensual.SelectedItem.Text == "Miercoles")
                            mensual.IntDiasSemana = 4;
                        else if (DdDiasMensual.SelectedItem.Text == "Jueves")
                            mensual.IntDiasSemana = 5;
                        else if (DdDiasMensual.SelectedItem.Text == "Viernes")
                            mensual.IntDiasSemana = 6;
                        else if (DdDiasMensual.SelectedItem.Text == "Sabado") mensual.IntDiasSemana = 7;
                        periodicidad.IntFrecuencia = Convert.ToInt32(txtcadamensual.Text);
                    }

                    inactividad.periodicidad = periodicidad;
                    inactividad.periodicidadMensual = mensual;
                }
                else if (txtradio.Text == "Semanal")
                {
                    var semana = new PeriodicidadSemanal();
                    semana.UidPeriodicidad = uidPeriodicidad;

                    if (CBLunes.Checked) semana.BlLunes = true;
                    if (CBMartes.Checked) semana.BlMartes = true;
                    if (CBMiercoles.Checked) semana.BlMartes = true;
                    if (CBJueves.Checked) semana.BlJueves = true;
                    if (CBViernes.Checked) semana.BlViernes = true;
                    if (CBSabado.Checked) semana.BlSabado = true;
                    if (CBDomingo.Checked) semana.BlDomingo = true;
                    periodicidad.IntFrecuencia = Convert.ToInt32(txtFrecuenciaSemanal.Text);
                    inactividad.periodicidad = periodicidad;
                    inactividad.periodicidadSemanal = semana;
                }
            }

            Inactividades.Agregar(inactividad);

            dgvPeriodosInactividad.DataSource = Inactividades.periodosInactividad;
            dgvPeriodosInactividad.DataBind();

            btnOKInactividad.Disable();
            btnOKInactividad.Visible = false;
            btnCancelarInactividad.Disable();
            btnCancelarInactividad.Visible = false;

            panelCamposInactividad.Visible = false;
            panelGridInactividad.Visible = true;

            btnAgregarInactividad.Enable();
        }

        protected void btnCancelarInactividad_Click(object sender, EventArgs e)
        {
            btnAgregarInactividad.Enable();
            if (!string.IsNullOrEmpty(uidPeriodoInactividad.Value))
            {
                btnEditarInactividad.Enable();
                btnEliminarInactividad.Enable();
            }

            btnOKInactividad.Disable();
            btnOKInactividad.Visible = false;
            btnCancelarInactividad.Disable();
            btnCancelarInactividad.Visible = false;

            panelCamposInactividad.Visible = false;
            panelGridInactividad.Visible = true;
        }

        [Serializable]
        private class Inactividad
        {
            public bool create;
            public Periodicidad periodicidad;
            public PeriodicidadAnual periodicidadAnual;
            public PeriodicidadMensual periodicidadMensual;
            public PeriodicidadSemanal periodicidadSemanal;
            public PeriodoInactividad periodoInactividad;
            public bool update;
        }

        [Serializable]
        private class ListaInactividades
        {
            public readonly List<Inactividad> inactividades = new List<Inactividad>();
            public readonly List<PeriodoInactividad> periodosInactividad = new List<PeriodoInactividad>();

            public PeriodoInactividad Encontrar(Guid uid)
            {
                return inactividades.Find(x => x.periodoInactividad.UidPeriodoInactividad == uid)?.periodoInactividad;
            }

            public Inactividad EncontrarInactividad(Guid uid)
            {
                return inactividades.Find(x => x.periodoInactividad.UidPeriodoInactividad == uid);
            }

            public void Agregar(Inactividad inactividad)
            {
                if (inactividades.Find(x =>
                        x.periodoInactividad.UidPeriodoInactividad ==
                        inactividad.periodoInactividad.UidPeriodoInactividad) != null)
                    return;

                periodosInactividad.Add(inactividad.periodoInactividad);
                inactividad.create = true;
                inactividades.Add(inactividad);
            }

            public void Agregar(PeriodoInactividad periodo, Periodicidad periodicidad, PeriodicidadSemanal semanal,
                PeriodicidadMensual mensual, PeriodicidadAnual anual)
            {
                var uidNuevo = periodo.UidPeriodoInactividad;
                if (inactividades.Find(x =>
                        x.periodoInactividad.UidPeriodoInactividad == periodo.UidPeriodoInactividad) != null)
                    return;

                var inactividad = new Inactividad();
                periodosInactividad.Add(periodo);
                inactividad.periodoInactividad = periodo;
                inactividad.periodicidad = periodicidad;
                inactividad.periodicidadSemanal = semanal;
                inactividad.periodicidadMensual = mensual;
                inactividad.periodicidadAnual = anual;
                inactividad.create = true;
                inactividades.Add(inactividad);
            }

            public void Actualizar(PeriodoInactividad periodo)
            {
                var index = inactividades.FindIndex(x =>
                    x.periodoInactividad.UidPeriodoInactividad == periodo.UidPeriodoInactividad);
                inactividades[index].periodoInactividad.DtFechaInicio = periodo.DtFechaInicio;
                inactividades[index].periodoInactividad.DtFechaFin = periodo.DtFechaFin;
                inactividades[index].periodoInactividad.StrNotas = periodo.StrNotas;
                index = periodosInactividad.FindIndex(x => x.UidPeriodoInactividad == periodo.UidPeriodoInactividad);
                periodosInactividad[index].DtFechaInicio = periodo.DtFechaInicio;
                periodosInactividad[index].DtFechaFin = periodo.DtFechaFin;
                periodosInactividad[index].StrNotas = periodo.StrNotas;
                if (!inactividades[index].create)
                    inactividades[index].update = true;
            }

            public void Eliminar(PeriodoInactividad periodo)
            {
                inactividades.RemoveAt(inactividades.FindIndex(x =>
                    x.periodoInactividad.UidPeriodoInactividad == periodo.UidPeriodoInactividad));
                periodosInactividad.RemoveAt(periodosInactividad.FindIndex(x =>
                    x.UidPeriodoInactividad == periodo.UidPeriodoInactividad));
            }

            public void Rellenar(List<PeriodoInactividad> periodos)
            {
                foreach (var periodo in periodos)
                {
                    periodosInactividad.Add(periodo);
                    var inactividad = new Inactividad();
                    inactividad.periodoInactividad = periodo;
                    inactividades.Add(inactividad);
                }
            }

            public void Resetear()
            {
                inactividades.RemoveAll(x => true);
                periodosInactividad.RemoveAll(x => true);
            }
        }
    }
}