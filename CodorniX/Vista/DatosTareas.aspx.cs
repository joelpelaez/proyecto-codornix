﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using CodorniX.Modelo;
using System.Globalization;

namespace CodorniX.Vista
{
    public partial class DatosTareas : System.Web.UI.Page
    {
        VMDatosTareas VM = new VMDatosTareas();
        VMPeriodicidad VMP = new VMPeriodicidad();
        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;
            if (!IsPostBack)
            {
                fechaFin.Visible = false;
                antecesor.Visible = false;
                lhora.Visible = false;
                ltolerancia.Visible = false;
                gridopciones.Visible = false;
                unidad.Visible = false;

                Datos.Visible = false;
                txtNombre.Disable();
                txtFechaInicio.Disable();
                txtFechaFin.Disable();
                txtTipoTarea.Disable();
                txtStatus.Disable();
                txtFoto.Disable();
                txtTipoMedicion.Disable();
                txtunidadmedida.Disable();
                lblOpciones.Disable();
                lblDepartamento.Disable();
                txtHora.Disable();
                txtTolerancia.Disable();
                txtAntecesor.Disable();
                txtTipoFrecuencia.Disable();
                txtFrecuencia.Disable();

                divgrid.Visible = false;
                divUnidadMedida.Visible = false;
                Hora.Visible = false;
                Tolerancia.Visible = false;


                VM.ConsultarDepartamento(SesionActual.uidSucursalActual.Value);
                lblDepartamento.DataSource = VM.ltsDepartamento;
                lblDepartamento.DataTextField = "StrNombre";
                lblDepartamento.DataValueField = "UidDepartamento";
                lblDepartamento.DataBind();

                VM.ConsultarUnidadMedida();
                lblUnidad.DataSource = VM.ltsUnidadMedida;
                lblUnidad.DataTextField = "StrTipoUnidad";
                lblUnidad.DataValueField = "UidUnidadMedida";
                lblUnidad.DataBind();
            }
        }

        #region grid

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            btnBuscar.Disable();
            DVGTareas.Visible = true;
            string Nombre = FiltroNombre.Text.Trim();

			DateTime? DtFechaInicio1 = null;
			if (!string.IsNullOrEmpty(filtrofechainicio1.Text.Trim()))
			{
				DtFechaInicio1 = Convert.ToDateTime(DateTime.ParseExact(filtrofechainicio1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)); ;
			}
			DateTime? DtFechaInicio2 = null;
			if (!string.IsNullOrEmpty(filtrofechainicio1.Text.Trim()))
			{
				DtFechaInicio2 = Convert.ToDateTime(DateTime.ParseExact(filtrofechainicio2.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}

			string Encargado = FiltroEncargado.Text.Trim();

            string departamentos = "";
            int[] i = lblDepartamento.GetSelectedIndices();
            foreach (int j in i)
            {
                string value = lblDepartamento.Items[j].Value;
                if (departamentos.Count() == 0)
                    departamentos += value;
                else
                    departamentos += "," + value;
            }
            string Departamento = departamentos;

            VM.BuscarTarea(Nombre, DtFechaInicio1, DtFechaInicio2, SesionActual.uidSucursalActual.Value, Departamento, Encargado);
            DVGTareas.DataSource = VM.ltsTarea;

            DVGTareas.DataBind();
            ViewState["Tarea"] = VM.ltsTarea;

            PanelFiltros.Visible = false;
            lblFiltros.Text = "Mostrar";
            lblFiltros.CssClass = "glyphicon glyphicon-collapse-down";
            btnLimpiar.Disable();
            ViewState["TareaPreviousRow"] = null;
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            string texto = lblFiltros.Text;
            DVGTareas.Visible = false;
            if (texto == "Mostrar")
            {
                btnBuscar.Enable();
                btnLimpiar.Enable();
                lblFiltros.Text = "Ocultar";
                lblFiltros.CssClass = "glyphicon glyphicon-collapse-up";
                PanelFiltros.Visible = true;

            }
            else if (texto == "Ocultar")
            {
                PanelFiltros.Visible = false;
                btnBuscar.Disable();
                btnLimpiar.Disable();
                lblFiltros.Text = "Mostrar";
                lblFiltros.CssClass = "glyphicon glyphicon-collapse-down";
                DVGTareas.Visible = true;
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            FiltroNombre.Text = string.Empty;
            DVGTareas.DataSource = VM.ltsTarea;
            DVGTareas.DataBind();
        }

        protected void DVGTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["TareaPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["SortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortTareas(SortExpression, SortDirection, true);
            }
            else
            {
                DVGTareas.DataSource = ViewState["Tarea"];
            }
            DVGTareas.PageIndex = e.NewPageIndex;
            DVGTareas.DataBind();
        }

        private void SortTareas(string SortExpression, SortDirection SortDirection, bool same = false)
        {
            List<Tarea> tarea = (List<Tarea>)ViewState["Tarea"];

            if (SortExpression == (string)ViewState["SortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                SortDirection =
                    ((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }

            if (SortExpression == "Nombre")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    tarea = tarea.OrderBy(x => x.StrNombre).ToList();
                }
                else
                {
                    tarea = tarea.OrderByDescending(x => x.StrNombre).ToList();
                }
            }
            else if (SortExpression == "Encargado")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    tarea = tarea.OrderBy(x => x.StrUsuario).ToList();
                }
                else
                {
                    tarea = tarea.OrderByDescending(x => x.StrUsuario).ToList();
                }
            }
            else if (SortExpression == "FechaInicio")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    tarea = tarea.OrderBy(x => x.DtFechaInicio).ToList();
                }
                else
                {
                    tarea = tarea.OrderByDescending(x => x.DtFechaInicio).ToList();
                }
            }
            else if (SortExpression == "Periodicidad")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    tarea = tarea.OrderBy(x => x.StrTipoFrecuencia).ToList();
                }
                else
                {
                    tarea = tarea.OrderByDescending(x => x.StrTipoFrecuencia).ToList();
                }
            }
            else if (SortExpression == "Departamento")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    tarea = tarea.OrderBy(x => x.StrDepartamento).ToList();
                }
                else
                {
                    tarea = tarea.OrderByDescending(x => x.StrDepartamento).ToList();
                }
            }

            DVGTareas.DataSource = tarea;
            ViewState["SortColumn"] = SortExpression;
            ViewState["SortColumnDirection"] = SortDirection;
        }

        protected void DVGTareas_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["TareaPreviousRow"] = null;
            SortTareas(e.SortExpression, e.SortDirection);

            DVGTareas.DataBind();
        }

        protected void DVGTareas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(DVGTareas, "Select$" + e.Row.RowIndex);
                if (e.Row.Cells[5].Text == "&nbsp;")
                {
                    //PERFIL.CssClass = "glyphicon glyphicon-user";
                    e.Row.Cells[5].Text = "(Sin Asignar)";
                }
            }
        }

        #endregion
        protected void DVGTareas_SelectedIndexChanged(object sender, EventArgs e)
        {
            string UidTarea = DVGTareas.SelectedDataKey.Value.ToString();
            VM.ObtenerTarea(new Guid(UidTarea));
            txtUidTarea.Text = VM.CTarea.UidTarea.ToString();
            lblNombre.Text = VM.CTarea.StrNombre;
            lblTipoMedicion.Text = VM.CTarea.StrTipoMedicion;
            if (VM.CTarea.StrTipoTarea == "Requerida")
            {
                lblTipoTarea.Text = "Si";

            }
            else
            {
                lblTipoTarea.Text = "No";
            }

            VM.ObtenerOpciones();
            DdSeleccion.DataSource = VM.Opciones;
            DdSeleccion.DataTextField = "StrOpciones";
            DdSeleccion.DataValueField = "UidOpciones";
            DdSeleccion.DataBind();
            
            lblUnidadMedida.Text = VM.CTarea.StrUnidadMedida;
            if (VM.CTarea.StrTipoMedicion == "Seleccionable")
            {
                unidad.Visible = false;
                gridopciones.Visible = true;
            }
            else if (VM.CTarea.StrTipoMedicion == "Numerico")
            {
                unidad.Visible = true;
                gridopciones.Visible = false;
            }
            else if (VM.CTarea.StrTipoMedicion == "Verdadero/Falso")
            {
                unidad.Visible = false;
                gridopciones.Visible = false;
            }

            if (VM.CTarea.StrUsuario != null)
            {
                lblEncargado.Text = VM.CTarea.StrUsuario;
            }
            else
            {
                lblEncargado.Text = "No hay Responsanble";
            }


            lblDescripcion.Text = VM.CTarea.StrDescripcion;

            if (VM.CTarea.UidAntecesor != null)
            {
                antecesor.Visible = true;
                VM.CargarAntecesor(VM.CTarea.UidAntecesor.ToString());
                lblAntecesor.Text = VM.CAntecesor.StrNombre;
            }
            else
            {
                antecesor.Visible = false;
            }
            if (VM.CTarea.TmHora != null)
            {
                lhora.Visible = true;
                lblhora.Text = VM.CTarea.TmHora.HasValue ? VM.CTarea.TmHora.Value.ToString("hh\\:mm") : "";
                ltolerancia.Visible = true;
                lblTolerancia.Text = VM.CTarea.IntTolerancia.ToString();
            }
            else
            {
                lhora.Visible = false;
                ltolerancia.Visible = false;
            }

            Guid UidPeriodicidad = VM.CTarea.UidPeriodicidad;
            txtUidPeriodicidad.Text = VM.CTarea.UidPeriodicidad.ToString();
            VMP.ObtenerPeriodicidad(UidPeriodicidad);
            lblFechainicio.Text = VMP.Cperiodicidad.DtFechaInicio.ToString("dd/MM/yyyy");

            if (VMP.Cperiodicidad.DtFechaFin != null)
            {
                fechaFin.Visible = true;
                lblFechaFin.Text = VMP.Cperiodicidad.DtFechaFin?.ToString("dd/MM/yyyy");
            }
            else
            {
                fechaFin.Visible = false;
            }

            int pos = -1;
            if (ViewState["TareaPreviousRow"] != null)
            {
                pos = (int)ViewState["TareaPreviousRow"];
                GridViewRow previousRow = DVGTareas.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["TareaPreviousRow"] = DVGTareas.SelectedIndex;
            DVGTareas.SelectedRow.AddCssClass("success");
        }
    }
}