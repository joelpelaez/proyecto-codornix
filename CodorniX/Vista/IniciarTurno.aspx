﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="Site1.Master" CodeBehind="IniciarTurno.aspx.cs" Inherits="CodorniX.Vista.IniciarTurno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6">
            <asp:PlaceHolder runat="server" ID="PanelBusqueda">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center">Buscar Tareas</div>

                    <div class="panel-body">
                        <div id="divdepartamentos" runat="server" style="padding-top: 25px">
                            <asp:Label runat="server">Departamentos</asp:Label>
                            <asp:GridView ID="dvgDepartamentos" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false" DataKeyNames="UidDepartamento">
                                <EmptyDataTemplate>No hay Departamentos Asignados</EmptyDataTemplate>
                                <Columns>
                                    <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                    <asp:BoundField DataField="StrDepartamento" HeaderText="Nombre" />
                                    <asp:BoundField DataField="IntTareasCumplidas" HeaderText="Tareas Cumplidas" />
                                    <asp:BoundField DataField="IntTareasNoCumplidas" HeaderText="Tareas No Cumplidas" />
                                    <asp:BoundField DataField="IntNumTareasRequeridasdNoCumplidas" HeaderText="Tareas Requeridas No Cumplidas" />
                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>

                </div>
            </asp:PlaceHolder>
        </div>

        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading text-center">Inicio de turno</div>

                <div class="text-left">
                    <asp:LinkButton ID="btnIniciarTurno" OnClick="btnIniciarTurno_Click" CssClass="btn btn-sm btn-default" runat="server">
                            <span class="glyphicon glyphicon-file"></span>
                            Iniciar Turno
                    </asp:LinkButton><asp:LinkButton ID="btnCerrarTurno" OnClick="btnCerrarTurno_Click" CssClass="btn btn-sm disabled btn-default" runat="server">
                            <span class="glyphicon glyphicon-cog"></span>
                            Cerra Turno
                    </asp:LinkButton>
                    <asp:Label Style="color: red;" ID="lblError" Text="" runat="server" />
                    <asp:TextBox ID="txtintNumero" CssClass="hide" runat="server" />
                    <asp:TextBox ID="txtUidInicioTurno" CssClass="hide" runat="server" />
                    <asp:TextBox ID="txtFecha" CssClass="hide" runat="server" />

                    <div class="pull-right">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="btnReporte" runat="server" OnClick="btnReporte_Click" CssClass="btn btn-sm btn-default" Text="Generar Reporte" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnReporte" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:LinkButton ID="Cumplimiento" OnClick="Cumplimiento_Click" CssClass="btn btn-sm disabled btn-default" runat="server">
                            <span class="glyphicon glyphicon-cog"></span>
                            Cumplimiento
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder runat="server" ID="PanelIniciarTurno">
                        <div class="col-md-4">
                            <h6>Hora Inicio</h6>
                            <asp:TextBox ID="txtHoraInicio" runat="server" CssClass="form-control" />
                        </div>
                        <div class="col-md-4">
                            <h6>Hora Fin</h6>
                            <asp:TextBox ID="txtHoraFin" runat="server" CssClass="form-control" />
                        </div>
                    </asp:PlaceHolder>
                </div>

            </div>
        </div>

    </div>
</asp:Content>
