﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class AsignacionInactividad : System.Web.UI.Page
    {
        private VMAsignacionInactividad VM = new VMAsignacionInactividad();

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }
        
        private bool ModoSupervisor = false;

        private Guid UidEncargado
        {
            get { return (Guid)Session["UidEncargado"]; }
            set { Session["UidEncargado"] = value; }
        }

        private Guid UidPeriodoInactividad
        {
            get { return (Guid)Session["UidPeriodoInactividad"]; }
            set { Session["UidPeriodoInactividad"] = value; }
        }

        private DateTime FechaInicio
        {
            get { return (DateTime)Session["FechaInicio"]; }
            set { Session["FechaInicio"] = value; }
        }

        private DateTime FechaFin
        {
            get { return (DateTime)Session["FechaFin"]; }
            set { Session["FechaFin"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
            {
                return;
            }

            if (!SesionActual.uidSucursalActual.HasValue)
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!Acceso.TieneAccesoAModulo("Asignacion", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (SesionActual.perfil == "Supervisor")
            {

                // El supervisor no tiene acceso al cumplimiento si no ha tomado  algun turno para cumplir.
                if (SesionActual.UidDepartamentos.Count == 0)
                {
                    Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                    return;
                }

                ModoSupervisor = true;
            }

            if (!IsPostBack)
            {
                FechaInicio = default(DateTime);
                FechaFin = default(DateTime);
                
                if (ModoSupervisor)
                    VM.ObtenerDepartamentosPorLista(SesionActual.UidDepartamentos);
                else
                    VM.ObtenerDepartamentos(SesionActual.uidSucursalActual.Value);

                lbDepartamentos.DataSource = VM.Departamentos;
                lbDepartamentos.DataTextField = "StrNombre";
                lbDepartamentos.DataValueField = "UidDepartamento";
                lbDepartamentos.DataBind();

                txtFechaInicio.Text = DateTime.Today.ToString("dd/MM/yyyy");
                txtFechaFin.Text = txtFechaInicio.Text;
            }
        }

        private void RealizarBusqueda()
        {
            string lpr = null;
            int[] i = lbDepartamentos.GetSelectedIndices();
            if (i.Length > 0)
            {
                lpr = lbDepartamentos.Items[i[0]].Value.ToString();
                for (int j = 1; j < i.Length; j++)
                {
                    lpr += "," + lbDepartamentos.Items[i[j]].Value.ToString();
                }
            }

            if (ModoSupervisor && lpr == null)
            {
                foreach (var depto in SesionActual.UidDepartamentos)
                {
                    lpr = "";
                    var value = depto.ToString();
                    if (!lpr.Any())
                        lpr += value;
                    else
                        lpr += "," + value;
                }
            }

            DateTime fechaInicio, fechaFin;
            if (!DateTime.TryParse(txtFechaInicio.Text, out fechaInicio))
                return;
            if (!DateTime.TryParse(txtFechaFin.Text, out fechaFin))
                return;

            FechaInicio = fechaInicio;
            FechaFin = fechaFin;

            VM.ObtenerPeriodos(SesionActual.uidSucursalActual.Value, fechaInicio, fechaFin, lpr);

            dgvListaInactividades.DataSource = VM.Periodos;
            dgvListaInactividades.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            RealizarBusqueda();
            btnMostrar.Text = "Mostrar";
            btnMostrar_Click(sender, e);
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            lbDepartamentos.ClearSelection();
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            if (btnMostrar.Text == "Mostrar")
            {
                panelBusqueda.Visible = false;
                panelResultados.Visible = true;
                btnMostrar.Text = "Ocultar";
            }
            else
            {
                panelBusqueda.Visible = true;
                panelResultados.Visible = false;
                btnMostrar.Text = "Mostrar";
            }
        }

        protected void btnAsignar_Click(object sender, EventArgs e)
        {
            GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

            foreach (GridViewRow row in gridViewRowCollection)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
                    if (checkBox.Checked)
                    {
                        DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);

                        VM.AsignarDia(UidPeriodoInactividad, fecha, UidEncargado);
                    }
                }
            }
            ListarDias();
        }

        protected void btnQuitar_Click(object sender, EventArgs e)
        {
            GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

            foreach (GridViewRow row in gridViewRowCollection)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
                    if (checkBox.Checked)
                    {
                        DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);

                        VM.QuitarDia(UidPeriodoInactividad, fecha);
                    }
                }
            }

            ListarDias();
        }

        protected void dgvEncargados_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(dgvEncargados.SelectedDataKey.Value.ToString());
            UidEncargado = uid;
            int pos = -1;
            if (ViewState["EncargadosPreviousRow"] != null)
            {
                pos = (int)ViewState["EncargadosPreviousRow"];
                GridViewRow previousRow = dgvEncargados.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["EncargadosPreviousRow"] = dgvEncargados.SelectedIndex;
            dgvEncargados.SelectedRow.AddCssClass("success");
        }

        protected void dgvEncargados_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void dgvDiasInactividad_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dgvDiasInactividad_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string status = e.Row.Cells[3].Text;
                LinkButton button = e.Row.FindControl("QuitarButton") as LinkButton;
                if (status == "False")
                {
                    button.Disable();
                }
                else
                {
                    button.Enable();
                }
            }
        }

        protected void dgvListaInactividades_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(dgvListaInactividades.SelectedDataKey.Value.ToString());
            DateTime fechaInicio = default(DateTime);
            DateTime fechaFin = default(DateTime);
            DateTime fechaInicioCalculado = FechaInicio;
            DateTime fechaFinCalculado = FechaFin;

            UidPeriodoInactividad = uid;

            if (DateTime.TryParse(dgvListaInactividades.SelectedDataKey.Values[1].ToString(), out fechaInicio))
                FechaInicio = fechaInicio > FechaInicio ? fechaInicio : fechaInicio;
            if (DateTime.TryParse(dgvListaInactividades.SelectedDataKey.Values[2].ToString(), out fechaFin))
                FechaFin = fechaFin < FechaFin ? fechaFin : FechaFin;
            
            ListarDias();

            int pos = -1;
            if (ViewState["ListaPreviousRow"] != null)
            {
                pos = (int)ViewState["ListaPreviousRow"];
                GridViewRow previousRow = dgvEncargados.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
			
        }

        private void ListarDias()
        {
            VM.ObtenerDias(UidPeriodoInactividad, FechaInicio, FechaFin);
            dgvDiasInactividad.DataSource = VM.Dias;
            dgvDiasInactividad.DataBind();
        }

        protected void dgvListaInactividades_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvListaInactividades, "Select$" + e.Row.RowIndex);
            }
        }

        protected void btnBuscarEncargado_Click(object sender, EventArgs e)
        {
            VM.ObtenerEncargado(SesionActual.uidSucursalActual.Value, txtNombreEncargado.Text);

            ViewState["EncargadosPreviousRow"] = null;

            dgvEncargados.DataSource = VM.Encargados;
            dgvEncargados.DataBind();
        }

        protected void cbTodos_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

            foreach (GridViewRow row in gridViewRowCollection)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
                    checkBox.Checked = true;
                }
            }
        }

        protected void cbSeleccionado_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

            CheckBox cb = dgvDiasInactividad.HeaderRow.FindControl("cbTodos") as CheckBox;
            bool enableQuit = false, checkAll = true;
            foreach (GridViewRow row in gridViewRowCollection)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
                    if (checkAll && !checkBox.Checked)
                    {
                        checkAll = false;
                    }
                    if (!enableQuit && checkBox.Checked)
                    {
                        enableQuit = true;
                    }
                }
            }

            if (checkAll)
                cb.Checked = true;
            else
                cb.Checked = false;

            if (enableQuit)
                btnQuitarTodos.Enable();
            else
                btnQuitarTodos.Disable();
        }

        protected void dgvDiasInactividad_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Quitar")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                DateTime fecha = Convert.ToDateTime(dgvDiasInactividad.DataKeys[i].Value);
                VM.QuitarDia(UidPeriodoInactividad, fecha);
            }
        }

        protected void btnQuitarTodos_Click(object sender, EventArgs e)
        {
            GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

            foreach (GridViewRow row in gridViewRowCollection)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
                    if (checkBox.Checked)
                    {
                        DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);

                        VM.QuitarDia(UidPeriodoInactividad, fecha);
                    }
                }
            }

            ListarDias();
        }

        protected void dgvEncargados_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Asignar")
            {
                int i = Convert.ToInt32(e.CommandArgument);
                Guid uid = new Guid(dgvEncargados.DataKeys[i].Value.ToString());

                GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

                foreach (GridViewRow row in gridViewRowCollection)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
                        if (checkBox.Checked)
                        {
                            DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);

                            VM.AsignarDia(UidPeriodoInactividad, fecha, uid);
                        }
                    }
                }
                ListarDias();
            }
        }
    }
}