﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class ReporteSupervisor : System.Web.UI.Page
    {
        VMReporteSupervisor VM = new VMReporteSupervisor();

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null || !SesionActual.uidTurnoSupervisor.HasValue)
                return;

            if (!Acceso.TieneAccesoAModulo("ReporteSupervision", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Width = Unit.Percentage(100);
            ReportViewer1.Height = Unit.Percentage(100);

            if (!IsPostBack)
            {
                var date = SesionActual.GetDateTime();

                VM.ObtenerUsuario(SesionActual.uidUsuario);
                VM.ObtenerSucursal(SesionActual.uidSucursalActual.Value);
                VM.ObtenerEmpresa(VM.Sucursal.UidEmpresa);
                VM.ObtenerTurnoSupervisor(SesionActual.uidTurnoSupervisor.Value);
                VM.ObtenerReportesCumplimiento(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, date);
                VM.ObtenerReportesRevision(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, date);

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReporteSupervision.rdlc");
                ReportParameter[] param = new ReportParameter[6];
                param[0] = new ReportParameter("NombreSupervisor", VM.Usuario.StrNombreCompleto);
                param[1] = new ReportParameter("Fecha", date.ToString("dd/MM/yyyy"));
                param[2] = new ReportParameter("NombreEmpresa", VM.Empresa.StrNombreComercial);
                param[3] = new ReportParameter("NombreSucursal", VM.Sucursal.StrNombre);
                param[4] = new ReportParameter("HoraInicio", VM.TurnoSupervisor.DtFechaInicio?.ToString("HH:mm:ss") ?? "(sin iniciar)");
                param[5] = new ReportParameter("HoraFin", VM.TurnoSupervisor.DtFechaFin?.ToString("HH:mm:ss") ?? "(sin cerrar)");
                ReportViewer1.LocalReport.SetParameters(param);
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Cumplimiento", VM.ReportesCumplimiento));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Revision", VM.ReportesRevision));

                ReportViewer1.LocalReport.Refresh();
            }
        }
    }
}