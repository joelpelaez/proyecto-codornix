﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using CodorniX.Modelo;
using CodorniX.Util;

namespace CodorniX.Vista
{
	public partial class Site1 : System.Web.UI.MasterPage
	{
		VMLogin _CVMLogin = new VMLogin();
		VMModulo _CVMModulo = new VMModulo();
		VMIniciarTurno VM = new VMIniciarTurno();
		Login lo = new Login();
		string uidinicioturno;
		string horainicio;
		string horafin;
		string numero;
		string fechainicio;
		Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{

			ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition()
			{
				Path = "~/Scripts/jquery-3.2.1.min.js",
				DebugPath = "~/Scripts/jquery-3.2.1.js",
			});

			if (Session["Sesion"] == null)
			{
				Response.Redirect("Login.aspx", false);
				return;
			}

			string strperfil = "";
			strperfil = Acceso.ObtenerAppWeb(SesionActual.uidPerfilActual.Value);

			Guid idPerfil = Guid.Empty;
			idPerfil = SesionActual.uidPerfilActual.Value;

			Usuario usuario = new Usuario.Repository().Find(new Guid(SesionActual.uidUsuario.ToString()));
			lblUsuario.Text = usuario.STRNOMBRE + " " + usuario.STRAPELLIDOPATERNO;

			UpdateNavbar();
		}

		protected void btnCerrarSession_Click(object sender, EventArgs e)
		{
			Session["Sesion"] = null;
			Response.Redirect("Login.aspx", false);
		}

		public void ActivarAdministrador()
		{
			activoAdmin.Attributes["class"] = "active";
		}

		public void ActivarEmpresa()
		{
			activoEmpresa.Attributes["class"] = "active";
		}
		public void ActivarSucursal()
		{
			activoSucursales.Attributes["class"] = "active";
		}
		public void ActivarDepartamento()
		{
			activoDepartamentos.Attributes["class"] = "active";
		}
		public void ActivarTarea()
		{
			ActivoTareas.Attributes["class"] = "active";
		}
		public void ActivarDatosTarea()
		{
			//ActivoDatosTareas.Attributes["class"] = "active";
		}
		public void ActivarPerfiles()
		{
			activoPerfiles.Attributes["class"] = "active";
		}
		public void ActivarPerfilEmpresa()
		{
			ActivoPerfilEmpresa.Attributes["class"] = "active";
		}

		public void Backsite()
		{
			// Show all
			menuTurno.Visible = false;
			activocumplimiento.Visible = false;
		}

		public void Backend()
		{
			menuTurno.Visible = false;
			activoEmpresa.Visible = false;
			activoAdmin.Visible = false;
			menuEmpresas.Visible = false;
		}

		public void Frontend()
		{
			activoEmpresa.Visible = false;
			activoAdmin.Visible = false;
			menuEmpresas.Visible = false;
			activoSucursales.Visible = false;
			activoEncargado.Visible = false;
			activoDepartamentos.Visible = false;
			ActivoTareas.Visible = false;
			menuTurno.Visible = true;
			activocumplimiento.Visible = false;
			activoAsignacion.Visible = false;
			activoAsignacionSupr.Visible = false;
			ActivoPerfilEmpresa.Visible = false;
			menuSucursales.Visible = false;
			activoDatosSupervision.Visible = false;
		}

		public void UpdateNavbar()
		{
			string strperfil = Acceso.ObtenerAppWeb(SesionActual.uidPerfilActual.Value);
			Empresa empresa = new Empresa.Repository().Find(SesionActual.uidEmpresaActual.GetValueOrDefault(Guid.Empty));
			if (empresa != null)
			{
				activoSucursales.Visible = true;
				activoEncargado.Visible = true;
				empresaActual.Visible = true;
				lblEmpresa.Text = empresa.StrNombreComercial;
			}
			else
			{
				menuSucursales.Visible = false;
				activoSucursales.Visible = false;
				activoEncargado.Visible = false;
				empresaActual.Visible = false;
			}

			Sucursal sucursal = new Sucursal.Repository().Find(SesionActual.uidSucursalActual.GetValueOrDefault(Guid.Empty));
			if (sucursal != null)
			{
				activoDepartamentos.Visible = true;
				sucursalActual.Visible = true;
				ActivoTareas.Visible = true;
				activoAsignacion.Visible = true;
				activoAsignacionSupr.Visible = true;
				activoAsignacionInactividad.Visible = true;
				lblSucursal.Text = sucursal.StrNombre;
				activoControl.Visible = true;
				if (SesionActual.UidPeriodos.Count > 0)
				{
					activoCumplimientoAdministrador.Visible = true;
					activoHistoricoCumplimientoAdministrador.Visible = true;
				}
			}
			else
			{
				activoDepartamentos.Visible = false;
				sucursalActual.Visible = false;
				ActivoTareas.Visible = false;
				activoAsignacion.Visible = false;
				activoAsignacionSupr.Visible = false;
				activoAsignacionInactividad.Visible = false;
				activoControl.Visible = false;
				activoCumplimientoAdministrador.Visible = false;
				activoHistoricoCumplimientoAdministrador.Visible = false;
			}

			if (strperfil == "Backsite")
			{
				Backsite();
			}
			else if (strperfil == "Backend")
			{
				Backend();
			}
			else if (strperfil == "Frontend")
			{
				VM.ObtenerPerfil(SesionActual.uidPerfilActual.Value);
				if (VM.Perfil.strPerfil == "Supervisor")
				{
					Frontend();
					menuTurno.Visible = false;
					menuSuper.Visible = true;
					if (SesionActual.uidTurnoSupervisor != null)
					{
						activoDatosSupervision.Visible = true;
					}
					if (SesionActual.UidPeriodos.Count > 0)
					{
						activoCumplimientoSupervision.Visible = true;
						activoHistoricoCumplimientoSupervision.Visible = true;
					}
					else
					{
						activoCumplimientoSupervision.Visible = false;
						activoHistoricoCumplimientoSupervision.Visible = false;
					}
					return;
				}
				Frontend();

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				fechainicio = horaLocal.DateTime.ToString();
				VM.ObtenerInicioTurno(SesionActual.uidUsuario);
				VM.ObtenerCumplimiento(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario, DateTime.Now);

				if (VM.CIniciarTurno != null)
				{
					if (VM.CIniciarTurno.DtFechaHoraInicio != null && VM.CIniciarTurno.DtFechaHoraFin != null)
					{
						NoTurno();
					}
					else
					{
						string estadoTurno = "";
						VM.ObtenerEstadoTurno(VM.CIniciarTurno.UidEstadoTurno);
						estadoTurno = VM.EstadoTurno.StrEstadoTurno;
						bool abiertoEncargado = VM.CIniciarTurno._blAbiertoEncargado;
						bool abiertoSupervisor = VM.CIniciarTurno._blAbiertoSupervisor;
						if (abiertoEncargado || (!abiertoEncargado && !abiertoSupervisor)) { SesionActual.UidPeriodo = VM.CIniciarTurno.UidPeriodo; }

						if (estadoTurno.Equals("Abierto por Supervisor") || estadoTurno.Equals("Bloqueado"))
						{
							NoTurno();
						}
						else if (estadoTurno.Equals("Abierto (Controlado)"))
						{
							if (SesionActual.UidPeriodo != null)
							{
								Sicumplimiento();
								validarCumplimiento(fechainicio);
								Noiniciarturno();
								noCerrarTurno();
							}
							else
							{
								siturno();
								siiniciarturno();
								noCerrarTurno();
								Nocumplimiento();
							}
						}
						else if (estadoTurno.Equals("Abierto") && !abiertoEncargado)
						{
							siturno();
							siiniciarturno();
							noCerrarTurno();
							Nocumplimiento();
						}
						else
						{
							uidinicioturno = VM.CIniciarTurno.UidInicioTurno.ToString();
							horainicio = VM.CIniciarTurno.DtFechaHoraInicio.ToString("hh\\:mm");

							if (VM.CIniciarTurno.DtFechaHoraInicio == null && VM.CIniciarTurno.DtFechaHoraFin == null)
							{
								NoTurno();
							}
							else if (VM.CIniciarTurno.DtFechaHoraFin == null)
							{
								SiCerrarTurno();
								Noiniciarturno();
							}
							validarCumplimiento(fechainicio);
						}
					}
				}
				else
				{
					if (SesionActual.UidPeriodo == null) { NoTurno(); }
					else { siturno(); siiniciarturno(); noCerrarTurno(); Nocumplimiento(); }
					/*if (VM.ltsDepartamento.Count > 0)
					{
						siiniciarturno();
						activocerrarturno.Visible = false;
						activoiniciarturno.Visible = true;
						noCerrarTurno();
						uidinicioturno = string.Empty;
						activocumplimiento.Visible = false;
						horainicio = string.Empty;
						horafin = string.Empty;
					}
					else
					{
						NoTurno();
					}*/
				}
			}
		}

		protected void btnIniciarTurno_Click(object sender, EventArgs e)
		{
			if (!(Page is Bienvenido))
			{
				Response.Redirect("Bienvenido.aspx", false);
			}
			IniciarTurno();
		}
		public void IniciarTurno()
		{
			if (SesionActual.UidPeriodo != null)
			{
				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				var local = horaLocal.DateTime;

				DateTime fechahorainicio = DateTime.Now;
				VM.ObtenerPeriodoTurno(SesionActual.UidPeriodo.Value);
				VM.ObtenerTurnoUsuario(SesionActual.UidPeriodo.Value, local.ToString("dd/MM/yyyy"));

				if (VM.CIniciarTurno == null)
				{
					VM.ObtenerInicioTurno(SesionActual.uidUsuario);
					if (VM.CIniciarTurno == null)
					{
						bool blEstatus = false;
						if (VM.Guardar(SesionActual.uidUsuario, fechahorainicio, SesionActual.UidPeriodo.Value, true, false))
						{
							activocumplimiento.Visible = true;
							btncerrarturno.Enable();
							activocerrarturno.Visible = true;
							btnIniciarTurno.Disable();
							activoiniciarturno.Visible = false;
							blEstatus = true;
						}
						if (blEstatus) { VM.ModificarEstadoTurno(VM.CIniciarTurno.UidInicioTurno, "Abierto"); UpdateNavbar(); }

						if (Page is Bienvenido)
						{
							Bienvenido aspx = (Bienvenido)Page;
							aspx.ActualizarGrid();
							if (blEstatus) { aspx.llenarhora(); }
						}
					}
					else
					{
						VM.ObtenerEstadoTurno(VM.CIniciarTurno.UidEstadoTurno);
					}
				}
				else
				{
					Bienvenido aspx = (Bienvenido)Page;
					aspx.NoIniciar();
				}
			}
			else
			{

				VM.ObtenerInicioTurno(SesionActual.uidUsuario);
				Guid uidInicioTurno = VM.CIniciarTurno.UidInicioTurno;
				VMBienvenido vMBienvenido = new VMBienvenido();
				vMBienvenido.ActualizarEstatusInicioTurnoEncargado(uidInicioTurno);
				SesionActual.UidPeriodo = VM.CIniciarTurno.UidPeriodo;
				UpdateNavbar();

				if (Page is Bienvenido)
				{
					Bienvenido aspx = (Bienvenido)Page;
					aspx.ActualizarGrid();
				}
			}
			UpdateNavbar();
		}

		protected void btncerrarturno_Click(object sender, EventArgs e)
		{
			if (Page is Bienvenido)
			{
				CerrarTurno();
			}
			else
			{
				Response.Redirect("Bienvenido.aspx", false);
			}
		}
		public void CerrarTurno()
		{
			Bienvenido aspx = (Bienvenido)Page;
			if (SesionActual.UidPeriodo != null)
			{
				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				var local = horaLocal.DateTime;

				DateTime fechahorainicio = DateTime.Now;
				VM.ObtenerPeriodoTurno(SesionActual.UidPeriodo.Value);
				VM.ObtenerTurnoUsuario(SesionActual.UidPeriodo.Value, local.ToString("dd/MM/yyyy"));

				if (VM.CIniciarTurno != null)
				{
					VM.ObtenerEstadoTurno(VM.CIniciarTurno.UidEstadoTurno);

					if (VM.EstadoTurno.StrEstadoTurno == "Bloqueado" || VM.EstadoTurno.StrEstadoTurno == "Abierto por Supervisor" || VM.EstadoTurno.StrEstadoTurno == "Abierto (Controlado)")
					{
						return;
					}
				}
				aspx.cerrarturno();
			}
		}

		protected void btnHome_Click(object sender, EventArgs e)
		{
			Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
		}

		public void Noiniciarturno()
		{
			btnIniciarTurno.Visible = false;
			btnIniciarTurno.Disable();
		}
		public void Sicumplimiento()
		{
			activocumplimiento.Visible = true;
		}
		public void SiCerrarTurno()
		{
			btncerrarturno.Enable();
			btncerrarturno.Visible = true;
		}
		public void noCerrarTurno()
		{
			btncerrarturno.Disable();
			btncerrarturno.Visible = false;
		}
		public void Nocumplimiento()
		{
			activocumplimiento.Visible = false;
		}
		public void NoTurno()
		{
			menuTurno.Visible = false;
		}
		public void siturno()
		{
			menuTurno.Visible = true;
		}
		public void siiniciarturno()
		{
			btnIniciarTurno.Enable();
			btnIniciarTurno.Visible = true;
		}

		public void NoCumplimiento()
		{
			activocumplimiento.Visible = false;
		}

		public void SiCumplimiento()
		{
			activocumplimiento.Visible = true;
		}
		public void validarCumplimiento(string strFechaInicio)
		{
			if (SesionActual.UidPeriodo.HasValue)
			{
				VM.ObtenerInicioPorPeriodo(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, Convert.ToDateTime(strFechaInicio));

				if (VM.CIniciarTurno2 != null)
				{
					activocumplimiento.Visible = true;
					activoHistoricoCumplimiento.Visible = true;
				}
				else
				{
					activocumplimiento.Visible = false;
					activoHistoricoCumplimiento.Visible = false;
				}
			}
			else
			{
				activocumplimiento.Visible = false;
				activoHistoricoCumplimiento.Visible = false;
			}
		}

		public void AceptarCerrarTurno()
		{
			Bienvenido aspx = (Bienvenido)Page;
			VM.ObtenerNoCumplidos(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario, fechainicio);

			if (VM.CTareasNoCumplidas != null)
			{
				int num = VM.ObtenerNumeroTareasSinEstado(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, DateTime.Today);
				if (VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas == 0)
				{
					VM.Modificar(VM.CIniciarTurno.UidInicioTurno, SesionActual.GetDateTimeOffset(), numero);
					VM.ModificarEstadoTurno(VM.CIniciarTurno.UidInicioTurno, "Cerrado");
					btncerrarturno.Disable();
					activocumplimiento.Visible = false;
					aspx.llenarhora();
					aspx.requeridoSicumplido();
				}
				else
				{
					aspx.requeridonocumplido();
					SiCumplimiento();
				}
			}
			else
			{
				VM.Modificar(VM.CIniciarTurno.UidInicioTurno, SesionActual.GetDateTimeOffset(), numero);
				btncerrarturno.Visible = false;
				activocumplimiento.Visible = false;
			}

			aspx.btnCancelarCerrarTurno_Click(null, null);
			aspx.btnActualizar_Click(null, null);
		}
	}
}