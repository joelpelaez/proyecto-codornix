﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Notificaciones.aspx.cs" Inherits="CodorniX.Vista.Notificaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Notificaciones</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                Lista de Tareas del Día
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <div class="btn-group">
                        <asp:LinkButton runat="server" ID="btnMostrar" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click" Text="Mostrar" />
                        <asp:LinkButton runat="server" ID="btnLimpiar" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Limpiar
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-sm btn-default" OnClick="btnBuscar_Click">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Buscar
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="panelBusqueda" runat="server">
                        <div class="row">
                            <div class="col-xs-6">
                                <h6>Fecha</h6>
                                <div class="input-group date">
                                    <span class="input-group-addon">Desde:</span>
                                    <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="form-control" />
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                                <div class="input-group date" style="padding-top: 10px">
                                    <span class="input-group-addon">Hasta:</span>
                                    <asp:TextBox ID="txtFechaFin" runat="server" CssClass="form-control" />
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <h6>Estado</h6>
                                <asp:ListBox ID="lbEstados" runat="server" CssClass="form-control" SelectionMode="Multiple" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelResultados" runat="server">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:GridView runat="server" AllowPaging="true" AllowSorting="true" PageSize="5" ID="dgvNotificaciones" CssClass="table table-bordered table-condensed table-striped" AutoGenerateColumns="false" DataKeyNames="UidMensajeNotificacion" OnSelectedIndexChanged="dgvNotificaciones_SelectedIndexChanged" OnRowDataBound="dgvNotificaciones_RowDataBound" OnPageIndexChanging="dgvNotificaciones_PageIndexChanging" OnSorting="dgvNotificaciones_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No tienes ninguna tarea asignada para el día de hoy.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField DataField="StrTarea" HeaderText="Tarea" SortExpression="Tarea" />
                                        <asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
                                        <asp:BoundField DataField="StrEstadoNotificacion" HeaderText="Completado" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                        <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCompleto" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="text-center">
                        Notificacion
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="btn-group">
                            <asp:LinkButton runat="server" ID="btnLeido" CssClass="btn btn-sm btn-default" OnClick="btnLeido_Click" Text="Leido" />
                        </div>
                    </div>
                </div>
                <asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
                    <div class="alert alert-danger">
                        <asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">x</asp:LinkButton>
                        <strong>Error: </strong><asp:Label runat="server" ID="lblErrorTarea" />
                    </div>
                </asp:PlaceHolder>
                <div class="panel-body">
                    <asp:HiddenField runat="server" ID="fldUidMensajeNotificacion" />
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>
                                <asp:Label runat="server" ID="lblDepto" Text="(ninguna)" /></h3>
                        </div>
                        <div class="col-xs-12">
                            <h3>
                                <asp:Label runat="server" ID="lblArea" Text="(ninguna)" /></h3>
                        </div>
                        <div class="col-xs-12">
                            <h4>
                                <asp:Label runat="server" ID="lblTarea" Text="(ninguna)" /></h4>
                        </div>
                        <div class="col-xs-12">
                            <h5>
                                <asp:Label runat="server" ID="lblTipoTarea" Text="(ninguna)" /></h5>
                        </div>
                        <div class="col-xs-12">
                            <h5>
                                <asp:Label runat="server" ID="lblResultado" Text="(ninguno)" />
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        //<![CDATA[
        var startDateReady = false;
        var endDateReady = false;

        function enableDatapicker() {
            if (!startDateReady) {
                $(".input-group.date").datepicker({
                    todayBtn: true,
                    clearBtn: true,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'es',
                }).on('changeDate', function (e) {
                    setEndDateLimit(e.format());
                });
            }
        }

        function setStartDateLimit(start, end) {
            $(".input-group.date").datepicker('remove');
            $(".input-group.date").datepicker({
                todayBtn: true,
                clearBtn: true,
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                startDate: start,
                endDate: end
            })
            startDateReady = true;
        }


        function pageLoad() {
            enableDatapicker();
        }
        //]]>
    </script>
</asp:Content>
