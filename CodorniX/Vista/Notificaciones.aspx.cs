﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class Notificaciones : System.Web.UI.Page
    {
        private VMNotificacion VM = new VMNotificacion();

        private List<MensajeNotificacion> VSNotificaciones
        {
            get
            {
                return ViewState["Notificaciones"] as List<MensajeNotificacion>;
            }
            set
            {
                ViewState["Notificaciones"] = value;
            }
        }

        private Sesion SesionActual
        {
            get
            {
                return (Sesion)Session["Sesion"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // No hacer nada si no tiene algun turno iniciado. Master se hace cargo de ello.
            if (SesionActual == null)
                return;

            // No permite acceder en caso de no iniciar una sucursal (solo para administradores).
            if (!SesionActual.uidSucursalActual.HasValue)
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }
            
            // Verificar permisos
            if (!Acceso.TieneAccesoAModulo("Cumplimiento", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!IsPostBack)
            {
                btnLeido.Disable();
                RealizarBusqueda();
                btnMostrar.Text = "Ocultar";
                panelBusqueda.Visible = false;
                panelResultados.Visible = true;
                btnLimpiar.Visible = false;
                btnBuscar.Visible = false;
            }
        }

        protected void btnLeido_Click(object sender, EventArgs e)
        {
            Guid uid = new Guid(fldUidMensajeNotificacion.Value);

            if (btnLeido.Text == "Leido")
            {
                btnLeido.Text = "No leido";
                VM.CambiarEstado(uid, "Leido");
                RealizarBusqueda();
            }
            else
            {
                btnLeido.Text = "Leido";
                VM.CambiarEstado(uid, "No leido");
                RealizarBusqueda();
            }
        }

        protected void dgvNotificaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(dgvNotificaciones.SelectedDataKey.Value.ToString());
            fldUidMensajeNotificacion.Value = uid.ToString();
            VM.ObtenerNotificacion(uid);

            lblTarea.Text = VM.Notificacion.StrTarea;
            lblDepto.Text = VM.Notificacion.StrDepartamento;
            lblArea.Text = VM.Notificacion.StrArea;
            lblResultado.Text = VM.Notificacion.StrResultado;

            string estado = VM.Notificacion.StrEstadoNotificacion;

            if (estado == "Leido")
            {
                btnLeido.Text = "No leido";
            }
            else
            {
                btnLeido.Text = "Leido";
            }

            btnLeido.Enable();

            int pos = -1;
            if (ViewState["NotiPreviousRow"] != null)
            {
                pos = (int)ViewState["NotiPreviousRow"];
                GridViewRow previousRow = dgvNotificaciones.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["NotiPreviousRow"] = dgvNotificaciones.SelectedIndex;
            dgvNotificaciones.SelectedRow.AddCssClass("success");
        }

        protected void dgvNotificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvNotificaciones, "Select$" + e.Row.RowIndex);

                Label icon = e.Row.FindControl("lblCompleto") as Label;

               
                if (e.Row.Cells[3].Text == "No leido")
                {
                    icon.AddCssClass("glyphicon").AddCssClass("glyphicon-pencil");
                    icon.ToolTip = "No Realizado";
                }
                else if (e.Row.Cells[3].Text == "Leido")
                {
                    icon.AddCssClass("glyphicon").AddCssClass("glyphicon-ok");
                    icon.ToolTip = "Completo";
                }
            }
        }

        protected void dgvNotificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["NotiPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["SortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortNotificaciones(SortExpression, SortDirection, true);
            }
            else
            {
                dgvNotificaciones.DataSource = VSNotificaciones;
            }
            dgvNotificaciones.PageIndex = e.NewPageIndex;
            dgvNotificaciones.DataBind();
        }

        protected void dgvNotificaciones_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["NotiPreviousRow"] = null;
            SortNotificaciones(e.SortExpression, e.SortDirection, false);
            dgvNotificaciones.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            RealizarBusqueda();
            btnMostrar.Text = "Ocultar";
            panelBusqueda.Visible = false;
            panelResultados.Visible = true;
            btnLimpiar.Visible = false;
            btnBuscar.Visible = false;
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            lbEstados.ClearSelection();
            txtFechaInicio.Text = "";
            txtFechaFin.Text = "";
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            if (btnMostrar.Text == "Mostrar")
            {
                btnMostrar.Text = "Ocultar";
                panelBusqueda.Visible = false;
                panelResultados.Visible = true;
                btnLimpiar.Visible = false;
                btnBuscar.Visible = false;
            }
            else
            {
                btnMostrar.Text = "Mostrar";
                panelBusqueda.Visible = true;
                panelResultados.Visible = false;
                btnLimpiar.Visible = true;
                btnBuscar.Visible = true;
            }
        }

        protected void btnCloseAlert_Click(object sender, EventArgs e)
        {

        }

        private void RealizarBusqueda()
        {
            string deptos = null;
            if (SesionActual.UidDepartamentos.Count > 0)
            {
                deptos = SesionActual.UidDepartamentos[0].ToString();
                for (int i = 1; i < SesionActual.UidDepartamentos.Count; i++)
                {
                    deptos += "," + SesionActual.UidDepartamentos[i].ToString();
                }
            }

            string deptosBusqueda = null;

            string estados = null;
            int[] j = lbEstados.GetSelectedIndices();
            if (j.Length > 0)
            {
                estados = lbEstados.Items[j[0]].Value;
                for (int i = 1; i < j.Length; i++)
                {
                    estados += "," + lbEstados.Items[j[i]].Value;
                }
            }

            DateTime? fechaInicio = null, fechaFin = null;

            if (!string.IsNullOrWhiteSpace(txtFechaInicio.Text))
            {
                fechaInicio = Convert.ToDateTime(txtFechaInicio.Text);
            }
            if (!string.IsNullOrWhiteSpace(txtFechaFin.Text))
            {
                fechaFin = Convert.ToDateTime(txtFechaFin.Text);
            }

            VM.ObtenerNotificaciones(SesionActual.uidSucursalActual.Value, deptos, null, estados, fechaInicio, fechaFin);
            VSNotificaciones = VM.Mensajes;
            dgvNotificaciones.DataSource = VSNotificaciones;
            dgvNotificaciones.DataBind();
        }

        private void SortNotificaciones(string sortExpression, SortDirection sortDirection, bool same = false)
        {
            if (sortExpression == (string)ViewState["SortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                sortDirection =
                    ((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }
            
            if (sortExpression == "Tarea")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                    VSNotificaciones = VSNotificaciones.OrderBy(x => x.StrTarea).ToList();
                }
                else
                {
                    VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.StrTarea).ToList();
                }
            }
            else if (sortExpression == "Departamento")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                    VSNotificaciones = VSNotificaciones.OrderBy(x => x.StrDepartamento).ToList();
                }
                else
                {
                    VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.StrDepartamento).ToList();
                }
            }
            else if (sortExpression == "Area")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                    VSNotificaciones = VSNotificaciones.OrderBy(x => x.StrArea).ToList();
                }
                else
                {
                    VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.StrArea).ToList();
                }
            }
            else if (sortExpression == "Estado")
            {
                if (sortDirection == SortDirection.Ascending)
                {
                    VSNotificaciones = VSNotificaciones.OrderBy((x => x.StrEstadoNotificacion)).ToList();
                }
                else
                {
                    VSNotificaciones = VSNotificaciones.OrderByDescending((x => x.StrEstadoNotificacion)).ToList();
                }
            }

            dgvNotificaciones.DataSource = VSNotificaciones;
            ViewState["SortColumn"] = sortExpression;
            ViewState["SortColumnDirection"] = sortDirection;
        }
    }
}