﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using Microsoft.Reporting.WebForms;
using System.IO;

namespace CodorniX.Vista
{
    public partial class IniciarTurno : System.Web.UI.Page
    {
        VMIniciarTurno VM = new VMIniciarTurno();
        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;
            
            if (!IsPostBack)
            {
                btnIniciarTurno.Enable();
                btnCerrarTurno.Disable();
                txtHoraInicio.Disable();
                txtHoraFin.Disable();
                txtHoraInicio.Text = string.Empty;
                txtFecha.Text = DateTime.Now.ToString();

                VM.ObtenerCumplimiento(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario, DateTime.Now);
                dvgDepartamentos.Visible = true;
                dvgDepartamentos.DataSource = VM.ltsDepartamento;
                dvgDepartamentos.DataBind();

                VM.ObtenerInicioTurno(SesionActual.uidUsuario);
                if (VM.CIniciarTurno!=null)
                {

                    txtUidInicioTurno.Text = VM.CIniciarTurno.UidInicioTurno.ToString();
                    txtHoraInicio.Text = VM.CIniciarTurno.DtFechaHoraInicio.ToString("hh\\:mm");
                    btnCerrarTurno.Enable();
                    btnIniciarTurno.Disable();
                    Cumplimiento.Enable();

                    
                }
                else
                {   
                    //VM.ObtenerAsignacion(SesionActual.uidUsuario);
                    if (VM.ltsDepartamento.Count>0)
                    {
                        btnIniciarTurno.Enable();
                        txtUidInicioTurno.Text = string.Empty;

                        btnCerrarTurno.Disable();
                        txtHoraInicio.Text = string.Empty;
                        txtHoraFin.Text = string.Empty;
                        Cumplimiento.Disable();
                    }
                    else
                    {
                        btnIniciarTurno.Disable();
                        txtUidInicioTurno.Text = string.Empty;

                        btnCerrarTurno.Disable();
                        txtHoraInicio.Text = string.Empty;
                        txtHoraFin.Text = string.Empty;
                        Cumplimiento.Disable();
                    }

                    
                    
                }
                
            }
        }

        protected void DVGResumenTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btnIniciarTurno_Click(object sender, EventArgs e)
        {
            txtHoraInicio.Text=  DateTime.Now.TimeOfDay.ToString("hh\\:mm");
            
            //VM.Guardar(SesionActual.uidUsuario, txtHoraInicio.Text );
            btnIniciarTurno.Disable();
            btnCerrarTurno.Enable();
            Cumplimiento.Enable();
        }

        protected void btnCerrarTurno_Click(object sender, EventArgs e)
        {
            VM.ObtenerInicioTurno(SesionActual.uidUsuario);
            txtHoraInicio.Text = VM.CIniciarTurno.DtFechaHoraInicio.ToString("hh\\:mm");
            
            VM.ObtenerCumplimiento(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario,Convert.ToDateTime( txtFecha.Text));

            if (VM.CTareasNoCumplidas!=null)
            {
                txtintNumero.Text = VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas.ToString();
                if (VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas == 0)
                {
                    VM.Modificar(VM.CIniciarTurno.UidInicioTurno, SesionActual.GetDateTimeOffset(), txtintNumero.Text);
                    btnCerrarTurno.Disable();
                }
                else
                {
                    lblError.Text = "Necesitas realizar todas las tareas requeridas";
                }
            }
        }

        protected void DVGResumenTareas_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void Cumplimiento_Click(object sender, EventArgs e)
        {
            Response.Redirect("Cumplimiento.aspx", false);
            return;
        }

        protected void btnReporte_Click(object sender, EventArgs e)
        {
            LocalReport lr = new LocalReport();
            lr.ReportPath = Server.MapPath("~/Reports/ReporteTareas.rdlc");
            ReportParameter[] param = new ReportParameter[5];
            VM.ObtenerCumplimientos(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, DateTime.Now, "");
            List<Modelo.Cumplimiento> completos = (from c in VM.Cumplimientos where c.StrEstadoCumplimiento == "Completo" select c).ToList();
            List<Modelo.Cumplimiento> incompleto = (from c in VM.Cumplimientos where c.StrEstadoCumplimiento != "Completo" select c).ToList();
            VM.ObtenerUsuario(SesionActual.uidUsuario);
            string nombreCompleto = VM.Encargado.STRNOMBRE + " " + VM.Encargado.STRAPELLIDOPATERNO + " " + VM.Encargado.STRAPELLIDOMATERNO;
            param[0] = new ReportParameter("NombreEncargado", nombreCompleto);
            param[1] = new ReportParameter("NombreDepartamento", "Prueba");
            param[2] = new ReportParameter("Fecha", DateTime.Today.ToString("yyyy/MM/dd"));
            param[3] = new ReportParameter("NombreEmpresa", "Empresa Prueba");
            param[4] = new ReportParameter("NombreSucursal", "Sucursal Prueba");
            lr.SetParameters(param);
            lr.DataSources.Add(new ReportDataSource("DataSet1", VM.Cumplimientos));
            lr.DataSources.Add(new ReportDataSource("TareasNoRealizadas", incompleto));
            string deviceInfo =
                "<DeviceInfo>" +
                "<OutputFormat>EMF</OutputFormat>" +
                "<PageWidth>8.5in</PageWidth>" +
                "<PageHeight>11in</PageHeight>" +
                "<MarginTop>0.4in</MarginTop>" +
                "<MarginLeft>0.8in</MarginLeft>" +
                "<MarginRight>0.8in</MarginRight>" +
                "<MarginBottom>0.4in</MarginBottom>" +
                "</DeviceInfo>";
            byte[] bytes = lr.Render("PDF", deviceInfo);
            using (FileStream fs = new FileStream(@"C:\Users\chess\output.pdf", FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.TransmitFile(@"C:\Users\chess\output.pdf");
            Response.End();
        }
    }
}