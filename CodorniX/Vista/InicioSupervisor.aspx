﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="InicioSupervisor.aspx.cs" Inherits="CodorniX.Vista.InicioSupervisor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Supervisión</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-xs-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="text-center">
						Turno de Supervisor
					</div>
				</div>
				<div class="text-right">
					<div class="btn-group">
						<asp:LinkButton ID="btnActualizar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnActualizar_Click">
                            Actualizar
						</asp:LinkButton>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12">
							<asp:Label runat="server" ID="lblMessage" Visible="false"></asp:Label>
							<asp:GridView ID="dvgTurnos" runat="server" CssClass="table table-bordered table-sm table-condensed" AutoGenerateColumns="false" OnRowDataBound="dvgTurnos_RowDataBound" OnSelectedIndexChanged="dvgTurnos_SelectedIndexChanged" DataKeyNames="UidTurnoSupervisor">
								<EmptyDataTemplate>No hay Historial</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolio" HeaderText="Folio" />
									<asp:BoundField DataField="Usuario" HeaderText="Usuario" />
									<asp:BoundField DataField="FechaInicio" HeaderText="Inicio" />
									<asp:BoundField DataField="FechaFin" HeaderText="Fin" />
									<asp:BoundField DataField="StrEstadoTurno" HeaderText="Estado" />
								</Columns>
							</asp:GridView>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="text-left">
							<div class="btn-group">
								<asp:LinkButton ID="btnReporteSupervisor" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnReporteSupervisor_Click">
                                    Reporte Supervisión
								</asp:LinkButton>
								<asp:LinkButton ID="btnResumenSupervision" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnResumenSupervision_Click">
                                    Resumen Supervisión
								</asp:LinkButton>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="text-right">
							<div class="btn-group">
								<asp:LinkButton ID="btnIniciarTurno" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnIniciarTurno_Click">
                                    Iniciar Turno
								</asp:LinkButton>
								<asp:LinkButton ID="btnCerrarTurno" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnCerrarTurno_Click">
                                    Cerrar Turno
								</asp:LinkButton>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
