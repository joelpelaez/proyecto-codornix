﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Revisiones.aspx.cs" Inherits="CodorniX.Vista.Revisiones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                Lista de Tareas del Día
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <div class="btn-group">
                        <asp:LinkButton runat="server" ID="btnMostrar" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click" Text="Mostrar" />
                        <asp:LinkButton runat="server" ID="btnLimpiar" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Limpiar
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnActualizar" CssClass="btn btn-sm btn-default" OnClick="btnActualizar_Click">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Buscar
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:PlaceHolder ID="panelBusqueda" runat="server">
                        <div class="row">
                            <div class="col-xs-6">
                                <h6>Tarea</h6>
                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Departamento</h6>
                                <asp:DropDownList ID="ddDepartamentos" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddDepartamentos_SelectedIndexChanged" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Área</h6>
                                <asp:DropDownList ID="ddAreas" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-6">
                                <h6>Estado</h6>
                                <asp:DropDownList ID="ddEstado" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Text="Todos" />
                                    <asp:ListItem Value="1" Text="No Revisado" />
                                    <asp:ListItem Value="2" Text="Revisado" />
                                </asp:DropDownList>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelResultados" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:GridView runat="server" PageSize="5" ID="dgvTareasPendientes" AllowPaging="true" AllowSorting="true" CssClass="table table-bordered table-condensed table-striped" AutoGenerateColumns="false" DataKeyNames="UidCumplimiento,UidRevision" OnSelectedIndexChanged="dgvTareasPendientes_SelectedIndexChanged" OnRowDataBound="dgvTareasPendientes_RowDataBound" OnPageIndexChanging="dgvTareasPendientes_PageIndexChanging" OnSorting="dgvTareasPendientes_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
                                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No tienes ninguna revision asignada para el día de hoy.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
                                        <asp:BoundField DataField="IntFolio" HeaderText="Folio" SortExpression="Folio" />
                                        <asp:BoundField DataField="StrTarea" HeaderText="Tarea" SortExpression="Tarea" />
                                        <asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
                                        <asp:BoundField DataField="StrArea" HeaderText="Área" SortExpression="Area" />
                                        <asp:BoundField DataField="StrEstado" HeaderText="Completado" SortExpression="Completo" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="text-center">
                        Tarea
                    </div>
                </div>
                <asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
                    <div class="alert alert-info">
                        <asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close">x</asp:LinkButton>
                        <strong>Mensaje: </strong>Confirmar tarea realizada correctamente.
                    </div>
                </asp:PlaceHolder>
                <div class="text-left">
                    <div class="btn-group">
                        <asp:LinkButton runat="server" ID="btnHecho" CssClass="btn btn-sm btn-success" OnClick="btnHecho_Click">
                            Aceptar
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnRealizar" CssClass="btn btn-sm btn-default" OnClick="btnRealizar_Click">
                            Editar
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnOK" CssClass="btn btn-sm btn-success" Visible="false" OnClick="btnOK_Click">
                            <span class="glyphicon glyphicon-ok"></span>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btnCancelar" CssClass="btn btn-sm btn-danger" Visible="false" OnClick="btnCancelar_Click">
                            <span class="glyphicon glyphicon-remove"></span>
                        </asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:HiddenField runat="server" ID="fldUidTarea" />
                    <asp:HiddenField runat="server" ID="fldUidDepartamento" />
                    <asp:HiddenField runat="server" ID="fldUidArea" />
                    <asp:HiddenField runat="server" ID="fldUidCumplimiento" />
                    <ul class="nav nav-tabs">
                        <li class="active" id="activeCumplimiento" runat="server">
                            <asp:LinkButton ID="btnCumplimiento" runat="server" Text="Cumplimiento" OnClick="btnCumplimiento_Click" />
                        </li>
                        <li id="activeRevision" runat="server">
                            <asp:LinkButton ID="tabRevision" runat="server" Text="Revisión" OnClick="tabRevision_Click" />
                        </li>
                        <li id="activeSucesores" runat="server">
                            <asp:LinkButton ID="tabSucesores" runat="server" Text="Sucesores" OnClick="tabSucesores_Click" />
                        </li>
                    </ul>
                    <asp:PlaceHolder ID="panelCumplimiento" runat="server">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3>Departamento:
                                        <asp:Label runat="server" ID="lblDepto" Text="(ninguna)" /></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Área:
                                        <asp:Label runat="server" ID="lblArea" Text="(ninguna)" /></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:Label runat="server" ID="lblTarea" Text="Tarea 1" />:&nbsp;
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                Valor original:
                                <asp:Label runat="server" ID="lblValorOriginal" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Observaciones:</label>
                                <asp:TextBox TextMode="MultiLine" ID="txtObservaciones" runat="server" CssClass="form-control disabled" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelRevision" runat="server" Visible="false">
                        
                        <div class="row">
                            <div class="col-xs-12">
                                Valor revisado:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <asp:Label runat="server" ID="lblError" ForeColor="DarkRed" CssClass="has-error" />
                                    </div>
                                </div>
                                <asp:PlaceHolder runat="server" ID="frmSiNo">
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rbYes" GroupName="rbgSiNo" />
                                        Sí
                                    </label>
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rbNo" GroupName="rbgSiNo" />
                                        No
                                    </label>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="frmValue">
                                    <div class="form-group" id="valorUnico" runat="server">
                                        <div class="input-group">
                                            <asp:TextBox runat="server" ID="txtValor" CssClass="form-control" />
                                            <div class="input-group-addon">
                                                <asp:Label runat="server" ID="lblUnidad" />
                                            </div>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="frmTwoValues">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            Desde:
                                        </div>
                                        <asp:TextBox runat="server" ID="txtValor1" CssClass="form-control" />
                                        <div class="input-group-addon">
                                            <asp:Label runat="server" ID="lblUnidad1" />
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            Hasta:
                                        </div>
                                        <asp:TextBox runat="server" ID="txtValor2" CssClass="form-control" />
                                        <div class="input-group-addon">
                                            <asp:Label runat="server" ID="lblUnidad2" />
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="frmOptions">
                                    <asp:DropDownList runat="server" ID="ddOpciones" CssClass="form-control" />
                                </asp:PlaceHolder>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:DropDownList ID="ddCalificacion" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Notas:</label>
                                <asp:TextBox TextMode="MultiLine" ID="txtNotas" runat="server" CssClass="form-control disabled" Enabled="false" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="panelSucesores" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:GridView ID="dgvSucesores" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered" DataKeyNames="UidTarea,UidCumplimiento" OnRowCommand="dgvSucesores_RowCommand" OnRowDataBound="dgvSucesores_RowDataBound">
                                    <EmptyDataTemplate>
                                        La tarea no tiene ninguna sucesora.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:BoundField DataField="StrTarea" HeaderText="Tarea" />
                                        <asp:BoundField DataField="StrEstadoCumplimiento" HeaderText="Estado" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="CrearButton" runat="server" CommandName="Crear" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" CssClass="btn btn-success btn-sm" Text="Crear" />
                                                <asp:LinkButton ID="HabilitarButton" runat="server" CommandName="Habilitar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" CssClass="btn btn-sm btn-default" Text="Habilitar" />
                                                <asp:LinkButton ID="DeshabilitarButton" runat="server" CommandName="Deshabilitar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" CssClass="btn btn-default btn-sm" Text="Deshabilitar" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
