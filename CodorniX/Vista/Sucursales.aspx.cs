﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;

namespace CodorniX.Vista
{
    public partial class Sucursales : System.Web.UI.Page
    {
        VMSucursales VM = new VMSucursales();

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }

        private List<SucursalDireccion> DireccionRemoved
        {
            get
            {
                if (ViewState["DireccionRemoved"] == null)
                    ViewState["DireccionRemoved"] = new List<SucursalDireccion>();

                return (List<SucursalDireccion>)ViewState["DireccionRemoved"];
            }
        }

        private List<SucursalTelefono> TelefonoRemoved
        {
            get
            {
                if (ViewState["TelefonoRemoved"] == null)
                    ViewState["TelefonoRemoved"] = new List<SucursalTelefono>();

                return (List<SucursalTelefono>)ViewState["TelefonoRemoved"];
            }
        }

        private bool EditingMode
        {
            get
            {
                if (ViewState["EditingMode"] == null)
                    return false;

                return (bool)ViewState["EditingMode"];
            }
            set
            {
                ViewState["EditingMode"] = value;
            }
        }

        private bool EditingModeDireccion
        {
            get
            {
                if (ViewState["EditingModeDireccion"] == null)
                    return false;

                return (bool)ViewState["EditingModeDireccion"];
            }
            set
            {
                ViewState["EditingModeDireccion"] = value;
            }
        }

        #region Private methods

        private void ActivarCamposDatos(bool enable)
        {
            if (enable)
            {
                btnNuevaSucursal.AddCssClass("disabled");
                btnNuevaSucursal.Enabled = false;

                btnEditarSucursal.AddCssClass("disabled");
                btnEditarSucursal.Enabled = false;

                txtNombre.RemoveCssClass("disabled");
                txtNombre.Enabled = true;

                ddTipoSucursal.Enable();

                btnOkSucursal.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnOkSucursal.Enabled = true;

                btnCancelarSucursal.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnCancelarSucursal.Enabled = true;

                dgvDirecciones.Enabled = true;

                dgvTelefonos.Enabled = true;
                ddZonaHoraria.Enable();
                EditingMode = true;
            }
            else
            {
                btnNuevaSucursal.RemoveCssClass("disabled");
                btnNuevaSucursal.Enabled = true;

                btnEditarSucursal.RemoveCssClass("disabled");
                btnEditarSucursal.Enabled = true;

                txtNombre.AddCssClass("disabled");
                txtNombre.Enabled = false;

                ddTipoSucursal.Disable();

                txtFechaRegistro.AddCssClass("disabled");
                txtFechaRegistro.Enabled = false;

                btnOkSucursal.AddCssClass("disabled").AddCssClass("hidden");
                btnOkSucursal.Enabled = false;

                btnCancelarSucursal.AddCssClass("disabled").AddCssClass("hidden");
                btnCancelarSucursal.Enabled = false;

                dgvDirecciones.Enabled = false;

                dgvTelefonos.Enabled = false;
                ddZonaHoraria.Disable();
                EditingMode = false;
            }
        }

        private void ActivarCamposDireccion(bool enable)
        {
            if (enable)
            {
                btnOkDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnOkDireccion.Enabled = true;

                btnCancelarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnCancelarDireccion.Enabled = true;

                ddPais.RemoveCssClass("disabled");
                ddPais.Enabled = true;

                ddEstado.RemoveCssClass("disabled");
                ddEstado.Enabled = true;

                txtMunicipio.RemoveCssClass("disabled");
                txtMunicipio.Enabled = true;

                txtCiudad.RemoveCssClass("disabled");
                txtCiudad.Enabled = true;

                txtColonia.RemoveCssClass("disabled");
                txtColonia.Enabled = true;

                txtCalle.RemoveCssClass("disabled");
                txtCalle.Enabled = true;

                txtConCalle.RemoveCssClass("disabled");
                txtConCalle.Enabled = true;

                txtYCalle.RemoveCssClass("disabled");
                txtYCalle.Enabled = true;

                txtNoExt.RemoveCssClass("disabled");
                txtNoExt.Enabled = true;

                txtNoInt.RemoveCssClass("disabled");
                txtNoInt.Enabled = true;

                txtReferencia.RemoveCssClass("disabled");
                txtReferencia.Enabled = true;

            }
            else
            {
                btnOkDireccion.AddCssClass("disabled");
                btnOkDireccion.Enabled = false;

                btnCancelarDireccion.AddCssClass("disabled");
                btnCancelarDireccion.Enabled = false;

                ddPais.AddCssClass("disabled");
                ddPais.Enabled = false;

                ddEstado.AddCssClass("disabled");
                ddEstado.Enabled = false;

                txtMunicipio.AddCssClass("disabled");
                txtMunicipio.Enabled = false;

                txtCiudad.AddCssClass("disabled");
                txtCiudad.Enabled = false;

                txtColonia.AddCssClass("disabled");
                txtColonia.Enabled = false;

                txtCalle.AddCssClass("disabled");
                txtCalle.Enabled = false;

                txtConCalle.AddCssClass("disabled");
                txtConCalle.Enabled = false;

                txtYCalle.AddCssClass("disabled");
                txtYCalle.Enabled = false;

                txtNoExt.AddCssClass("disabled");
                txtNoExt.Enabled = false;

                txtNoInt.AddCssClass("disabled");
                txtNoInt.Enabled = false;

                txtReferencia.AddCssClass("disabled");
                txtReferencia.Enabled = false;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            FUImagen.Attributes["onchange"] = "upload(this)";
            if (SesionActual == null)
                return;

            if (!Acceso.TieneAccesoAModulo("Sucursales", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!IsPostBack)
            {
                Site1 master = (Site1)Master;
                master.ActivarSucursal();
                lblErrorTelefono.Visible = false;
                lblErrorDireccion.Visible = false;
                lblErrorSucursal.Visible = false;

                //Botones de aceptar eliminar direccion
                lblAceptarEliminarDireccion.Visible = false;
                btnCancelarEliminarDireccion.Visible = false;
                btnAceptarEliminarDireccion.Visible = false;

                btnAceptarEliminarTelefono.Visible = false;
                btnCancelarEliminarTelefono.Visible = false;
                lblAceptarEliminarTelefono.Visible = false;

                dgvSucursales.Visible = false;
                dgvSucursales.AddCssClass("hidden");
                dgvSucursales.DataSource = null;
                dgvSucursales.DataBind();

                btnEncargados.Enabled = false;
                btnEncargados.CssClass = "btn btn-default disabled btn-sm";

                btnMostrarBusqueda.Text = "Ocultar";
                btnBorrarBusqueda.Visible = true;
                btnBorrarBusqueda.RemoveCssClass("hidden").RemoveCssClass("disabled");
                btnBuscar.Visible = true;
                btnBuscar.RemoveCssClass("hidden").RemoveCssClass("disabled");

                ddZonaHoraria.Disable();
                ddZonaHoraria.DataSource = TimeZoneInfo.GetSystemTimeZones();
                ddZonaHoraria.DataValueField = "Id";
                ddZonaHoraria.DataTextField = "DisplayName";
                ddZonaHoraria.DataBind();
                ddZonaHoraria.SelectedValue = "UTC";

                dgvDirecciones.DataSource = null;
                dgvDirecciones.DataBind();

                dgvTelefonos.DataSource = null;
                dgvTelefonos.DataBind();

                VM.ObtenerPaises();
                ddPais.DataSource = VM.Paises;
                ddPais.DataValueField = "UidPais";
                ddPais.DataTextField = "StrNombre";
                ddPais.DataBind();

                ddPais_SelectedIndexChanged(null, null);

                VM.ObtenerTipoTelefonos();
                ddTipoTelefono.DataSource = VM.TipoTelefonos;
                ddTipoTelefono.DataValueField = "UidTipoTelefono";
                ddTipoTelefono.DataTextField = "StrTipoTelefono";
                ddTipoTelefono.DataBind();

                ActivarCamposDatos(false);
                ActivarCamposDireccion(false);

                btnAgregarDireccion.AddCssClass("disabled");
                btnAgregarTelefono.AddCssClass("disabled");
                btnEditarSucursal.Enabled = false;
                btnEditarSucursal.AddCssClass("disabled");
                lbTipoTelefono.DataSource = VM.TipoTelefonos;
                lbTipoTelefono.DataValueField = "UidTipoTelefono";
                lbTipoTelefono.DataTextField = "StrTipoTelefono";
                lbTipoTelefono.DataBind();

                VM.ObtenerTipoSucursales();
                ddTipoSucursal.DataSource = VM.TipoSucursales;
                ddTipoSucursal.DataValueField = "UidTipoSucursal";
                ddTipoSucursal.DataTextField = "StrTipoSucursal";
                ddTipoSucursal.DataBind();
                FUImagen.Enabled = false;
                lbTipoSucursal.DataSource = VM.TipoSucursales;
                lbTipoSucursal.DataValueField = "UidTipoSucursal";
                lbTipoSucursal.DataTextField = "StrTipoSucursal";
                lbTipoSucursal.DataBind();

                string todos = "";
                int[] i = lbTipoTelefono.GetSelectedIndices();
                foreach (int j in i)
                {
                    string value = lbTipoTelefono.Items[j].Value;
                    if (todos.Count() == 0)
                        todos += value;
                    else
                        todos += "," + value;
                }
                ImgSucursales.ImageUrl = "Imgenes/Default.jpg";
                ImgSucursales.DataBind();
            }


            ScriptManager.RegisterStartupScript(this, typeof(Page), "initializeDatapicker", "enableDatapicker()", true);
        }

        #region Panel izquierdo

        protected void dgvSucursales_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvSucursales, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvSucursales_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnEncargados.Enabled = true;
            btnEncargados.CssClass = "btn btn-default btn-sm";
            Guid uid = new Guid(dgvSucursales.SelectedDataKey.Value.ToString());
            VM.ObtenerSucursal(uid);
            Guid? anterior = SesionActual.uidSucursalActual; 
            if (anterior == null || !anterior.Equals(VM.Sucursal.UidSucursal))
            {
                SesionActual.uidSucursalActual = VM.Sucursal.UidSucursal;
                Site1 master = (Site1)Page.Master;
                master.UpdateNavbar();
            }
            uidSucursal.Text = VM.Sucursal.UidSucursal.ToString();
            ddTipoSucursal.SelectedValue = VM.Sucursal.UidTipoSucursal.ToString();
            try
            {

                ddZonaHoraria.SelectedValue = VM.Sucursal.ZonaHoraria;
            }
            catch
            {

            }
            txtNombre.Text = VM.Sucursal.StrNombre;
            txtFechaRegistro.Text = VM.Sucursal.DtFechaRegistro.ToString("dd/MM/yyyy");
            ImgSucursales.ImageUrl = Page.ResolveUrl(VM.Sucursal.RutaImagen);
            VM.ObtenerDirecciones();
            ViewState["Direcciones"] = VM.Direcciones;
            DireccionRemoved.Clear();
            dgvDirecciones.DataSource = ViewState["Direcciones"];
            dgvDirecciones.DataBind();

            VM.ObtenerTelefonos();
            ViewState["Telefonos"] = VM.Telefonos;
            TelefonoRemoved.Clear();
            dgvTelefonos.DataSource = ViewState["Telefonos"];
            dgvTelefonos.DataBind();

            ActivarCamposDireccion(false);
            ActivarCamposDatos(false);
            btnEditarSucursal.RemoveCssClass("disabled");
            btnEditarSucursal.Enabled = true;

            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            btnObtenerDireccionEmpresa.Disable();

            if (VM.Direcciones.Count == 0)
            {
                btnAgregarDireccion.Visible = true;
                btnEditarDireccion.Visible = false;
                btnEliminarDireccion.Visible = false;
            }
            else
            {
                btnAgregarDireccion.Visible = false;
                btnEditarDireccion.Visible = true;
                btnEliminarDireccion.Visible = true;
            }

            ViewState["DireccionPreviousRow"] = null;

            btnAgregarTelefono.AddCssClass("disabled");
            btnAgregarTelefono.Enabled = false;
            txtTelefono.Enabled = false;


            int pos = -1;
            if (ViewState["SucursalPreviousRow"] != null)
            {
                pos = (int)ViewState["SucursalPreviousRow"];
                GridViewRow previousRow = dgvSucursales.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["SucursalPreviousRow"] = dgvSucursales.SelectedIndex;
            dgvSucursales.SelectedRow.AddCssClass("success");

            ViewState["TelefonoPreviousRow"] = null;
            ViewState["DireccionPreviousRow"] = null;
        }

        protected void ddPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(ddPais.SelectedValue.ToString());
            VM.ObtenerEstados(uid);
            ddEstado.DataSource = VM.Estados;
            ddEstado.DataValueField = "UidEstado";
            ddEstado.DataTextField = "StrNombre";
            ddEstado.DataBind();
        }

        private void SortSucursal(string SortExpression, SortDirection SortDirection, bool same = false)
        {
            List<Sucursal> empresas = (List<Sucursal>)ViewState["Sucursales"];

            if (SortExpression == (string)ViewState["SortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                SortDirection =
                    ((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }

            if (SortExpression == "Nombre")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    empresas = empresas.OrderBy(x => x.StrNombre).ToList();
                }
                else
                {
                    empresas = empresas.OrderByDescending(x => x.StrNombre).ToList();
                }
            }
            else if (SortExpression == "TipoSucursal")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    empresas = empresas.OrderBy(x => x.StrTipoSucursal).ToList();
                }
                else
                {
                    empresas = empresas.OrderByDescending(x => x.StrTipoSucursal).ToList();
                }
            }
            else
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    empresas = empresas.OrderBy(x => x.DtFechaRegistro).ToList();
                }
                else
                {
                    empresas = empresas.OrderByDescending(x => x.DtFechaRegistro).ToList();
                }
            }
            dgvSucursales.DataSource = empresas;
            ViewState["SortColumn"] = SortExpression;
            ViewState["SortColumnDirection"] = SortDirection;
        }

        protected void dgvSucursales_Sorting(object sender, GridViewSortEventArgs e)
        {
            // Invalidate Last position
            ViewState["SucursalPreviousRow"] = null;
            SortSucursal(e.SortExpression, e.SortDirection);

            dgvSucursales.DataBind();
        }


        protected void dgvSucursales_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Invalidate Last Position
            ViewState["SucursalPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["SortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortSucursal(SortExpression, SortDirection, true);
            }
            else
            {
                dgvSucursales.DataSource = ViewState["Sucursales"];
            }
            dgvSucursales.PageIndex = e.NewPageIndex;
            dgvSucursales.DataBind();
        }

        protected void btnBorrarBusqueda_Click(object sender, EventArgs e)
        {
            txtBusquedaNombre.Text = string.Empty;
            lbTipoSucursal.ClearSelection();
            txtBusquedaRegistradoAntes.Text = string.Empty;
            txtBusquedaRegistradoDespues.Text = string.Empty;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            String uids = "";
            seccionBusqueda.Visible = false;

            string nombreComercial = txtBusquedaNombre.Text;

            foreach (int pos in lbTipoSucursal.GetSelectedIndices())
            {
                if (uids.Length > 0)
                    uids += "," + lbTipoSucursal.Items[pos].Value;
                else
                    uids += lbTipoSucursal.Items[pos].Value;
            }

            DateTime? registradoDespues = null;
            try
            {
                registradoDespues = Convert.ToDateTime(DateTime.ParseExact(txtBusquedaRegistradoDespues.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
            catch (FormatException) {}

            DateTime? registradoAntes = null;
            try
            {
                registradoAntes = Convert.ToDateTime(DateTime.ParseExact(txtBusquedaRegistradoAntes.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
            catch (FormatException){}

            VM.BuscarSucursales(nombreComercial, registradoDespues, registradoAntes, uids, SesionActual.uidEmpresaActual.Value);
            ViewState["Sucursales"] = VM.Sucursales;
            dgvSucursales.DataSource = VM.Sucursales;
            dgvSucursales.DataBind();
            dgvSucursales.Visible = true;
            dgvSucursales.RemoveCssClass("hidden");

            dgvDirecciones.DataSource = null;
            dgvDirecciones.DataBind();

            dgvTelefonos.DataSource = null;
            dgvTelefonos.DataBind();

            btnMostrarBusqueda.Text = "Mostrar";
            btnBorrarBusqueda.Visible = false;
            btnBuscar.Visible = false;
        }

        protected void btnMostrarBusqueda_Click(object sender, EventArgs e)
        {
            if (btnMostrarBusqueda.Text == "Mostrar")
            {
                dgvSucursales.Visible = false;
                dgvSucursales.AddCssClass("hidden");

                seccionBusqueda.Visible = true;

                btnBorrarBusqueda.Visible = true;
                btnBuscar.Visible = true;

                btnMostrarBusqueda.Text = "Ocultar";
            }
            else
            {
                dgvSucursales.Visible = true;
                dgvSucursales.RemoveCssClass("hidden");

                seccionBusqueda.Visible = false;

                btnBorrarBusqueda.Visible = false;
                btnBuscar.Visible = false;

                btnMostrarBusqueda.Text = "Mostrar";
            }
        }

        #endregion

        #region Panel derecho (edicion)


        protected void btnNuevaSucursal_Click(object sender, EventArgs e)
        {
            ActivarCamposDatos(true);
            uidSucursal.Text = string.Empty;
            txtNombre.Text = string.Empty;
            FUImagen.Enabled = true;
            ddTipoSucursal.SelectedIndex = 0;
            txtFechaRegistro.Text = DateTime.Now.ToString("yyyy/MM/dd");

            ViewState["Direcciones"] = new List<SucursalDireccion>();
            DireccionRemoved.Clear();
            dgvDirecciones.DataSource = ViewState["Direcciones"];
            dgvDirecciones.DataBind();

            ViewState["Telefonos"] = new List<SucursalTelefono>();
            TelefonoRemoved.Clear();
            dgvTelefonos.DataSource = ViewState["Telefonos"];
            dgvTelefonos.DataBind();

            btnAgregarDireccion.Visible = true;
            btnEditarDireccion.Visible = false;
            btnEliminarDireccion.Visible = false;
            btnObtenerDireccionEmpresa.Enable();

            btnAgregarDireccion.RemoveCssClass("disabled");
            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;

            int pos = -1;
            if (ViewState["SucursalPreviousRow"] != null)
            {
                pos = (int)ViewState["SucursalPreviousRow"];
                GridViewRow previousRow = dgvSucursales.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
            ViewState["SucursalPreviousRow"] = null;
        }

        protected void btnEditarSucursal_Click(object sender, EventArgs e)
        {
            ActivarCamposDatos(true);
            btnAgregarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarDireccion.Enabled = true;
            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;
            FUImagen.Enabled = true;
            btnObtenerDireccionEmpresa.Enable();
            if (uidDireccion.Text.Length > 0)
            {
                btnEditarDireccion.Enable();
                btnEliminarDireccion.Enable();
            }
            if (uidTelefono.Text.Length > 0)
            {
                btnEditarTelefono.Enable();
                btnEliminarTelefono.Enable();
            }
        }

        protected void btnOkSucursal_Click(object sender, EventArgs e)
        {
            lblErrorSucursal.Visible = true;
            Sucursal sucursal = null;
            if (!string.IsNullOrWhiteSpace(uidSucursal.Text))
            {
                VM.ObtenerSucursal(new Guid(uidSucursal.Text));
                sucursal = VM.Sucursal;
            }
            else
            {
                sucursal = new Sucursal();
            }

            // Eliminar marcas de error
            frmGrpNombre.RemoveCssClass("has-error");

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            frmGrpTelefono.RemoveCssClass("has-error");

            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                lblErrorSucursal.Text = "El campo Nombre no debe estar vacío";
                txtNombre.Focus();
                frmGrpNombre.AddCssClass("has-error");
                return;
            }

            sucursal.StrNombre = txtNombre.Text;

            sucursal.UidEmpresa = SesionActual.uidEmpresaActual.Value;

            sucursal.UidTipoSucursal = new Guid(ddTipoSucursal.SelectedValue);

            sucursal.ZonaHoraria = ddZonaHoraria.SelectedValue;

            if (uidSucursal.Text !=string.Empty)
            {
                string UidSucursal = dgvSucursales .SelectedDataKey.Value.ToString();
                VM.ObtenerSucursal (new Guid(UidSucursal));
                string Ruta = VM.Sucursal.RutaImagen;

                if (File.Exists(Server.MapPath(Ruta)))
                {
                    File.Delete(Server.MapPath(Ruta));

                }
            }

            if(ViewState["rutaimg"]!=null)
                 sucursal.RutaImagen = ViewState["rutaimg"].ToString();

            VM.GuardarSucursal(sucursal);
            txtNombre.Text = string.Empty;
            List<SucursalDireccion> direcciones = (List<SucursalDireccion>)ViewState["Direcciones"];
            VM.GuardarDirecciones(direcciones, sucursal.UidSucursal);

            VM.EliminarDirecciones(DireccionRemoved);

            List<SucursalTelefono> telefonos = (List<SucursalTelefono>)ViewState["Telefonos"];
            VM.GuardarTelefonos(telefonos, sucursal.UidSucursal);

            VM.EliminarTelefonos(TelefonoRemoved);

            ActivarCamposDatos(false);
            ActivarCamposDireccion(false);

            VM.ObtenerSucursales(SesionActual.uidEmpresaActual.Value);
            dgvSucursales.DataSource = VM.Sucursales;
            dgvSucursales.DataBind();

            dgvDirecciones.DataSource = null;
            dgvDirecciones.DataBind();

            dgvTelefonos.DataSource = null;
            dgvTelefonos.DataBind();

            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");

            btnOkDireccion.AddCssClass("disabled");
            btnCancelarDireccion.AddCssClass("disabled");
            btnObtenerDireccionEmpresa.Disable();

            ActivarCamposDireccion(false);

            btnAgregarTelefono.AddCssClass("disabled");
            btnEditarTelefono.AddCssClass("disabled");
            btnEliminarTelefono.AddCssClass("disabled");

            btnOKTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnCancelarTelefono.AddCssClass("disabled").AddCssClass("hidden");

            txtTelefono.Text = string.Empty;
            uidTelefono.Text = string.Empty;
            ddTipoTelefono.SelectedIndex = 0;

            if (uidTelefono.Text.Length == 0)
            {
                btnEditarSucursal.Disable();
            }
            FUImagen.Enabled = false;
        }

        protected void btnCancelarSucursal_Click(object sender, EventArgs e)
        {
            ActivarCamposDatos(false);
            lblErrorSucursal.Visible = false;
            lblErrorSucursal.Text = "";
            lblErrorDireccion.Visible = false;
            lblErrorDireccion.Text = "";
            lblErrorTelefono.Visible = false;
            lblErrorTelefono.Text = "";
            FUImagen.Enabled = false;
            if (EditingModeDireccion)
            {
                btnCancelarDireccion_Click(null, null);
            }

            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");

            btnOkDireccion.AddCssClass("disabled");
            btnCancelarDireccion.AddCssClass("disabled");
            btnObtenerDireccionEmpresa.Disable();

            ActivarCamposDireccion(false);

            btnAgregarTelefono.AddCssClass("disabled");
            btnEditarTelefono.AddCssClass("disabled");
            btnEliminarTelefono.AddCssClass("disabled");

            btnOKTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnCancelarTelefono.AddCssClass("disabled").AddCssClass("hidden");

            txtTelefono.Text = string.Empty;
            uidTelefono.Text = string.Empty;
            ddTipoTelefono.SelectedIndex = 0;

            // Eliminar marcas de error
            frmGrpNombre.RemoveCssClass("has-error");

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            frmGrpTelefono.RemoveCssClass("has-error");

            btnCancelarEliminarDireccion_Click(sender, e);
            btnCancelarEliminarTelefono_Click(sender, e);


            if (uidSucursal.Text.Length == 0)
            {
                uidSucursal.Text = string.Empty;
                txtNombre.Text = string.Empty;
                ddTipoSucursal.SelectedIndex = 0;
                txtFechaRegistro.Text = string.Empty;

                dgvDirecciones.DataSource = null;
                dgvDirecciones.DataBind();

                dgvTelefonos.DataSource = null;
                dgvTelefonos.DataBind();

                btnEditarSucursal.Disable();

                btnAgregarDireccion.Visible = true;
                btnEditarDireccion.Visible = false;
                btnEliminarDireccion.Visible = false;

                if (Session["RutaImagen"] != null)
                {
                    string Ruta = Session["RutaImagen"].ToString();

                    //Borra la imagen de la empresa
                    if (File.Exists(Server.MapPath(Ruta)))
                    {
                        File.Delete(Server.MapPath(Ruta));
                    }
                    //Recarga el controlador de la imagen con una imagen default
                    ImgSucursales.ImageUrl = "Img/Default.jpg";
                    ImgSucursales.DataBind();
                }
            }
            else
            {
                VM.ObtenerSucursal(new Guid(uidSucursal.Text));
                Session["SucursalActual"] = VM.Sucursal;
                Label lblSucursal = (Label)Page.Master.FindControl("lblSucursal");
                lblSucursal.Text = VM.Sucursal.StrNombre;
                uidSucursal.Text = VM.Sucursal.UidSucursal.ToString();
                txtNombre.Text = VM.Sucursal.StrNombre;
                ddTipoSucursal.SelectedValue = VM.Sucursal.UidTipoSucursal.ToString();
                txtFechaRegistro.Text = VM.Sucursal.DtFechaRegistro.ToString("dd/MM/yyyy");
                ImgSucursales.ImageUrl = Page.ResolveUrl(VM.Sucursal.RutaImagen);
                VM.ObtenerDirecciones();
                ViewState["Direcciones"] = VM.Direcciones;
                DireccionRemoved.Clear();
                dgvDirecciones.DataSource = ViewState["Direcciones"];
                dgvDirecciones.DataBind();

                VM.ObtenerTelefonos();
                ViewState["Telefonos"] = VM.Telefonos;
                TelefonoRemoved.Clear();
                dgvTelefonos.DataSource = ViewState["Telefonos"];
                dgvTelefonos.DataBind();


                if (VM.Direcciones.Count == 0)
                {
                    btnAgregarDireccion.Visible = true;
                    btnEditarDireccion.Visible = false;
                    btnEliminarDireccion.Visible = false;
                }
                else
                {
                    btnAgregarDireccion.Visible = false;
                    btnEditarDireccion.Visible = true;
                    btnEliminarDireccion.Visible = true;
                }

            }
        }

        #endregion

        #region Panel derecho (tabs)

        protected void tabDatos_Click(object sender, EventArgs e)
        {
            lblErrorTelefono.Visible = false;
            lblErrorSucursal.Visible = false;
            lblErrorDireccion.Visible = false;
            panelDatosSucursal.Visible = true;
            activeDatos.Attributes["class"] = "active";
            panelDirecciones.Visible = false;
            activeDirecciones.Attributes["class"] = "";
            panelTelefonos.Visible = false;
            activeTelefonos.Attributes["class"] = "";

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            else
            {
                panelDireccion.Visible = false;
                panelSucursal.Visible = true;
            }
        }

        protected void tabDirecciones_Click(object sender, EventArgs e)
        {

            lblErrorTelefono.Visible = false;
            lblErrorSucursal.Visible = false;
            lblErrorDireccion.Visible = false;
            panelDatosSucursal.Visible = false;
            activeDatos.Attributes["class"] = "";
            panelDirecciones.Visible = true;
            activeDirecciones.Attributes["class"] = "active";
            panelTelefonos.Visible = false;
            activeTelefonos.Attributes["class"] = "";
        }

        protected void tabTelefonos_Click(object sender, EventArgs e)
        {
            lblErrorTelefono.Visible = false;
            lblErrorSucursal.Visible = false;
            lblErrorDireccion.Visible = false;
            panelDatosSucursal.Visible = false;
            activeDatos.Attributes["class"] = "";
            panelDirecciones.Visible = false;
            activeDirecciones.Attributes["class"] = "";
            panelTelefonos.Visible = true;
            activeTelefonos.Attributes["class"] = "active";

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            else
            {
                panelDireccion.Visible = false;
                panelSucursal.Visible = true;
            }
        }

        #endregion

        #region Panel derecho (Direcciones)

        protected void dgvDirecciones_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            VM.ObtenerDireccion(new Guid(dgvDirecciones.SelectedDataKey.Value.ToString()));
            uidDireccion.Text = VM.Direccion.UidDireccion.ToString();
            ddPais.SelectedValue = VM.Direccion.UidPais.ToString();
            ddPais_SelectedIndexChanged(sender, e);
            ddEstado.SelectedValue = VM.Direccion.UidPais.ToString();
            txtMunicipio.Text = VM.Direccion.StrMunicipio;
            txtCiudad.Text = VM.Direccion.StrCiudad;
            txtColonia.Text = VM.Direccion.StrColonia;
            txtCalle.Text = VM.Direccion.StrCalle;
            txtConCalle.Text = VM.Direccion.StrConCalle;
            txtYCalle.Text = VM.Direccion.StrYCalle;
            txtReferencia.Text = VM.Direccion.StrReferencia;
            txtNoExt.Text = VM.Direccion.StrNoExt;
            txtNoInt.Text = VM.Direccion.StrNoInt;

            ActivarCamposDireccion(true);
        }

        protected void btnOkDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = false;
            lblErrorDireccion.Visible = true;
            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            if (string.IsNullOrWhiteSpace(txtMunicipio.Text))
            {
                lblErrorDireccion.Text = "El campo Municipio no debe estar vacío";
                txtMunicipio.Focus();
                frmGrpMunicipio.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtCiudad.Text))
            {
                lblErrorDireccion.Text = "El campo Ciudad no debe estar vacío";
                txtCiudad.Focus();
                frmGrpCiudad.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtColonia.Text))
            {
                lblErrorDireccion.Text = "El campo Colonia no debe estar vacío";
                txtColonia.Focus();
                frmGrpColonia.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Calle no debe estar vacío";
                txtCalle.Focus();
                frmGrpCalle.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtConCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Con Calle no debe estar vacío";
                txtConCalle.Focus();
                frmGrpConCalle.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtYCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Y Calle no debe estar vacío";
                txtYCalle.Focus();
                frmGrpYCalle.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtNoExt.Text))
            {
                lblErrorDireccion.Text = "El campo No. Exterior no debe estar vacío";
                txtNoExt.Focus();
                frmGrpNoExt.AddCssClass("has-error");
                return;
            }

            List<SucursalDireccion> direcciones = (List<SucursalDireccion>)ViewState["Direcciones"];
            SucursalDireccion direccion = null;
            int pos = -1;
            if (!string.IsNullOrWhiteSpace(uidDireccion.Text))
            {
                IEnumerable<SucursalDireccion> dir = from d in direcciones where d.UidDireccion.ToString() == uidDireccion.Text select d;
                direccion = dir.First();
                pos = direcciones.IndexOf(direccion);
                direcciones.Remove(direccion);
            }
            else
            {
                direccion = new SucursalDireccion();
                direccion.UidDireccion = Guid.NewGuid();
            }
            direccion.UidPais = new Guid(ddPais.SelectedValue);
            direccion.UidEstado = new Guid(ddEstado.SelectedValue);
            direccion.StrMunicipio = txtMunicipio.Text;
            direccion.StrCiudad = txtCiudad.Text;
            direccion.StrColonia = txtColonia.Text;
            direccion.StrCalle = txtCalle.Text;
            direccion.StrConCalle = txtConCalle.Text;
            direccion.StrYCalle = txtYCalle.Text;
            direccion.StrNoExt = txtNoExt.Text;
            direccion.StrNoInt = txtNoInt.Text;
            direccion.StrReferencia = txtReferencia.Text;

            ActivarCamposDireccion(false);
            if (pos < 0)
                direcciones.Add(direccion);
            else
                direcciones.Insert(pos, direccion);

            dgvDirecciones.DataSource = direcciones;
            dgvDirecciones.DataBind();

            if (direcciones.Count == 0)
            {
                btnAgregarDireccion.Visible = true;
                btnEditarDireccion.Visible = false;
                btnEliminarDireccion.Visible = false;
            }
            else
            {
                btnAgregarDireccion.Visible = false;
                btnEditarDireccion.Visible = true;
                btnEliminarDireccion.Visible = true;
            }


            btnAgregarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");

            panelSeleccionDireccion.Visible = false;
            panelSucursal.Visible = true;
            panelDireccion.Visible = false;
        }

        protected void btnCancelarDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = false;
            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            ActivarCamposDireccion(false);

            uidDireccion.Text = string.Empty;
            ddPais.SelectedIndex = 0;
            ddEstado.SelectedIndex = 0;
            txtMunicipio.Text = string.Empty;
            txtCiudad.Text = string.Empty;
            txtColonia.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtConCalle.Text = string.Empty;
            txtYCalle.Text = string.Empty;
            txtNoExt.Text = string.Empty;
            txtNoInt.Text = string.Empty;
            txtReferencia.Text = string.Empty;

            btnAgregarDireccion.Enabled = true;
            btnAgregarDireccion.RemoveCssClass("disabled");

            panelSeleccionDireccion.Visible = false;
            

            if (uidDireccion.Text.Length == 0)
            {
                btnEditarDireccion.Enabled = true;
                btnEditarDireccion.RemoveCssClass("disabled");

                btnEliminarDireccion.Enabled = true;
                btnEliminarDireccion.RemoveCssClass("disabled");
            }
            else
            {
                btnEditarDireccion.Enabled = false;
                btnEditarDireccion.AddCssClass("disabled");
                btnEliminarDireccion.Enabled = false;
                btnEliminarDireccion.AddCssClass("disabled");


            }

            panelSucursal.Visible = true;
            panelDireccion.Visible = false;
        }

        protected void dgvDirecciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvDirecciones, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvDirecciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<SucursalDireccion> direcciones = (List<SucursalDireccion>)ViewState["Direcciones"];
            SucursalDireccion empresaDireccion = direcciones.Select(x => x).Where(x => x.UidDireccion.ToString() == dgvDirecciones.SelectedDataKey.Value.ToString()).First();
            uidDireccion.Text = empresaDireccion.UidDireccion.ToString();
            ddPais.SelectedValue = empresaDireccion.UidPais.ToString();
            ddPais_SelectedIndexChanged(sender, e);
            ddEstado.SelectedValue = empresaDireccion.UidEstado.ToString();
            txtMunicipio.Text = empresaDireccion.StrMunicipio;
            txtCiudad.Text = empresaDireccion.StrCiudad;
            txtColonia.Text = empresaDireccion.StrColonia;
            txtCalle.Text = empresaDireccion.StrCalle;
            txtConCalle.Text = empresaDireccion.StrConCalle;
            txtYCalle.Text = empresaDireccion.StrYCalle;
            txtReferencia.Text = empresaDireccion.StrReferencia;
            txtNoExt.Text = empresaDireccion.StrNoExt;
            txtNoInt.Text = empresaDireccion.StrNoInt;

            if (EditingMode)
            {
                btnEditarDireccion.Enabled = true;
                btnEditarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEliminarDireccion.Enabled = true;
                btnEliminarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            }

            int pos = -1;
            if (ViewState["DireccionPreviousRow"] != null)
            {
                pos = (int)ViewState["DireccionPreviousRow"];
                GridViewRow previousRow = dgvDirecciones.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["DireccionPreviousRow"] = dgvDirecciones.SelectedIndex;
            dgvDirecciones.SelectedRow.AddCssClass("success");

            panelDireccion.Visible = true;
            panelSucursal.Visible = false;

            btnCancelarDireccion.Visible = false;
            btnOkDireccion.Visible = false;
            btnCerrarDireccion.Visible = true;
        }

        protected void btnAgregarDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = true;
            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            uidDireccion.Text = string.Empty;
            ddPais.SelectedIndex = 0;
            ddEstado.SelectedIndex = 0;
            txtMunicipio.Text = string.Empty;
            txtCiudad.Text = string.Empty;
            txtColonia.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtConCalle.Text = string.Empty;
            txtYCalle.Text = string.Empty;
            txtNoExt.Text = string.Empty;
            txtNoInt.Text = string.Empty;
            txtReferencia.Text = string.Empty;

            ActivarCamposDireccion(true);

            panelSucursal.Visible = false;
            panelDireccion.Visible = true;

            btnOkDireccion.Visible = true;
            btnCancelarDireccion.Visible = true;
            btnCerrarDireccion.Visible = false;

            int pos = -1;
            if (ViewState["DireccionPreviousRow"] != null)
            {
                pos = (int)ViewState["DireccionPreviousRow"];
                GridViewRow previousRow = dgvDirecciones.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
        }

        protected void btnEditarDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = true;
            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            ActivarCamposDireccion(true);

            btnCerrarDireccion.Visible = false;
            btnOkDireccion.Visible = true;
            btnCancelarDireccion.Visible = true;

            panelSucursal.Visible = false;
            panelDireccion.Visible = true;
        }

        protected void btnEliminarDireccion_Click(object sender, EventArgs e)
        {
            lblAceptarEliminarDireccion.Visible = true;
            lblAceptarEliminarDireccion.Text = "¿Desea eliminar la direccion seleccionada?";
            btnAceptarEliminarDireccion.Visible = true;
            btnCancelarEliminarDireccion.Visible = true;

        }

        protected void btnAceptarEliminarDireccion_Click(object sender, EventArgs e)
        {
            ActivarCamposDireccion(false);
            Guid uid = new Guid(uidDireccion.Text);

            List<SucursalDireccion> direcciones = (List<SucursalDireccion>)ViewState["Direcciones"];
            SucursalDireccion direccion = direcciones.Select(x => x).Where(x => x.UidDireccion == uid).First();
            direcciones.Remove(direccion);
            DireccionRemoved.Add(direccion);
            dgvDirecciones.DataSource = direcciones;
            dgvDirecciones.DataBind();

            uidDireccion.Text = string.Empty;
            ddPais.SelectedIndex = 0;
            ddEstado.SelectedIndex = 0;
            txtMunicipio.Text = string.Empty;
            txtCiudad.Text = string.Empty;
            txtColonia.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtConCalle.Text = string.Empty;
            txtYCalle.Text = string.Empty;
            txtNoExt.Text = string.Empty;
            txtNoInt.Text = string.Empty;
            txtReferencia.Text = string.Empty;

            btnAgregarDireccion.RemoveCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            btnAceptarEliminarDireccion.Visible = false;
            btnCancelarEliminarDireccion.Visible = false;
            lblAceptarEliminarDireccion.Visible = false;

            if (direcciones.Count == 0)
            {
                btnAgregarDireccion.Visible = true;
                btnEditarDireccion.Visible = false;
                btnEliminarDireccion.Visible = false;
            }
            else
            {
                btnAgregarDireccion.Visible = false;
                btnEditarDireccion.Visible = true;
                btnEliminarDireccion.Visible = true;
            }

            ViewState["DireccionPreviousRow"] = null;
        }

        #endregion

        #region Panel derecho (Telefono)
        protected void dgvTelefonos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTelefonos, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvTelefonos_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<SucursalTelefono> telefonos = (List<SucursalTelefono>)ViewState["Telefonos"];
            SucursalTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono.ToString() == dgvTelefonos.SelectedDataKey.Value.ToString()).First();

            uidTelefono.Text = telefono.UidTelefono.ToString();
            txtTelefono.Text = telefono.StrTelefono;
            ddTipoTelefono.SelectedValue = telefono.UidTipoTelefono.ToString();

            if (EditingMode)
            {
                btnEditarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEditarTelefono.Enabled = true;
                btnEliminarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEliminarTelefono.Enabled = true;
                btnOKTelefono.AddCssClass("disabled").AddCssClass("hidden");
                btnOKTelefono.Enabled = false;
                btnCancelarTelefono.AddCssClass("disabled").AddCssClass("hidden");
                btnCancelarTelefono.Enabled = false;
            }

            int pos = -1;
            if (ViewState["TelefonoPreviousRow"] != null)
            {
                pos = (int)ViewState["TelefonoPreviousRow"];
                GridViewRow previousRow = dgvTelefonos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["TelefonoPreviousRow"] = dgvTelefonos.SelectedIndex;
            dgvTelefonos.SelectedRow.AddCssClass("success");

        }

        protected void btnAgregarTelefono_Click(object sender, EventArgs e)
        {
            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtTelefono.Enabled = true;
            txtTelefono.RemoveCssClass("disabled");
            ddTipoTelefono.SelectedIndex = 0;
            ddTipoTelefono.RemoveCssClass("disabled");
            ddTipoTelefono.Enabled = true;

            btnOKTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnOKTelefono.Enabled = true;
            btnCancelarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnCancelarTelefono.Enabled = true;

            btnAgregarTelefono.Disable();
            btnEditarTelefono.Disable();
            btnEliminarTelefono.Disable();

            int pos = -1;
            if (ViewState["TelefonoPreviousRow"] != null)
            {
                pos = (int)ViewState["TelefonoPreviousRow"];
                GridViewRow previousRow = dgvTelefonos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
        }

        protected void btnOKTelefono_Click(object sender, EventArgs e)
        {
            lblErrorTelefono.Visible = true;
            frmGrpTelefono.RemoveCssClass("has-error");

            if (string.IsNullOrWhiteSpace(txtTelefono.Text))
            {
                lblErrorTelefono.Text = "El campo Telefono no debe estar vacío";
                txtTelefono.Focus();
                frmGrpTelefono.AddCssClass("has-error");
                return;
            }
            List<SucursalTelefono> telefonos = (List<SucursalTelefono>)ViewState["Telefonos"];
            SucursalTelefono telefono = null;
            int pos = -1;
            if (!string.IsNullOrWhiteSpace(uidTelefono.Text))
            {
                IEnumerable<SucursalTelefono> dir = from t in telefonos where t.UidTelefono.ToString() == uidTelefono.Text select t;
                telefono = dir.First();
                pos = telefonos.IndexOf(telefono);
                telefonos.Remove(telefono);
            }
            else
            {
                telefono = new SucursalTelefono();
                telefono.UidTelefono = Guid.NewGuid();
            }
            telefono.StrTelefono = txtTelefono.Text;
            telefono.UidTipoTelefono = new Guid(ddTipoTelefono.SelectedValue);
            telefono.StrTipoTelefono = ddTipoTelefono.SelectedItem.Text;

            if (pos < 0)
                telefonos.Add(telefono);
            else
                telefonos.Insert(pos, telefono);

            dgvTelefonos.DataSource = telefonos;
            dgvTelefonos.DataBind();

            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtTelefono.Enabled = false;
            txtTelefono.AddCssClass("disabled");
            ddTipoTelefono.SelectedIndex = 0;
            ddTipoTelefono.AddCssClass("disabled");
            ddTipoTelefono.Enabled = false;

            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnOKTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnCancelarTelefono.Enabled = false;

            btnEditarTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnEditarTelefono.Enabled = false;
            btnEliminarTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnEliminarTelefono.Enabled = false;

            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;
        }

        protected void btnCancelarTelefono_Click(object sender, EventArgs e)
        {
            frmGrpTelefono.RemoveCssClass("has-error");

            txtTelefono.Enabled = false;
            txtTelefono.AddCssClass("disabled");
            ddTipoTelefono.AddCssClass("disabled");
            ddTipoTelefono.Enabled = false;

            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnOKTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnCancelarTelefono.Enabled = false;

            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;


            if (uidTelefono.Text.Length == 0)
            {
                btnEditarTelefono.Disable();
                btnEliminarTelefono.Disable();

                ddTipoTelefono.SelectedIndex = 0;
                txtTelefono.Text = string.Empty;
            }
            else
            {
                btnEliminarTelefono.Enable();
                btnEditarTelefono.Enable();

                List<SucursalTelefono> telefonos = (List<SucursalTelefono>)ViewState["Telefonos"];
                SucursalTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono.ToString() == dgvTelefonos.SelectedDataKey.Value.ToString()).First();

                uidTelefono.Text = telefono.UidTelefono.ToString();
                txtTelefono.Text = telefono.StrTelefono;
                ddTipoTelefono.SelectedValue = telefono.UidTipoTelefono.ToString();
            }
        }

        protected void btnEditarTelefono_Click(object sender, EventArgs e)
        {
            txtTelefono.Enabled = true;
            txtTelefono.RemoveCssClass("disabled");

            ddTipoTelefono.Enabled = true;
            ddTipoTelefono.RemoveCssClass("disabled");

            btnAgregarTelefono.Enabled = false;
            btnAgregarTelefono.AddCssClass("disabled");

            btnEditarTelefono.Enabled = false;
            btnEditarTelefono.AddCssClass("disabled");

            btnEliminarTelefono.Enabled = false;
            btnEliminarTelefono.AddCssClass("disabled");

            btnOKTelefono.Enabled = true;
            btnOKTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");

            btnCancelarTelefono.Enabled = true;
            btnCancelarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
        }

        protected void btnEliminarTelefono_Click(object sender, EventArgs e)
        {
            lblAceptarEliminarTelefono.Visible = true;
            lblAceptarEliminarTelefono.Text = "¿Desea Eliminar el telefono seleccionado?";
            btnAceptarEliminarTelefono.Visible = true;
            btnCancelarEliminarTelefono.Visible = true;
        }

        protected void btnAceptarEliminarTelefono_Click(object sender, EventArgs e)
        {
            btnAgregarTelefono.Enabled = true;
            btnAgregarTelefono.RemoveCssClass("disabled");

            btnOKTelefono.Enabled = false;
            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");

            btnCancelarTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");

            Guid uid = new Guid(uidTelefono.Text);

            List<SucursalTelefono> telefonos = (List<SucursalTelefono>)ViewState["Telefonos"];
            SucursalTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono == uid).First();
            telefonos.Remove(telefono);
            TelefonoRemoved.Add(telefono);

            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            ddTipoTelefono.SelectedIndex = 0;

            dgvTelefonos.DataSource = telefonos;
            dgvTelefonos.DataBind();

            btnCancelarEliminarTelefono.Visible = false;
            btnAceptarEliminarTelefono.Visible = false;
            lblAceptarEliminarTelefono.Visible = false;
            ViewState["TelefonoPreviousRow"] = null;
        }

        protected void btnCancelarEliminarTelefono_Click(object sender, EventArgs e)
        {
            btnCancelarEliminarTelefono.Visible = false;
            btnAceptarEliminarTelefono.Visible = false;
            lblAceptarEliminarTelefono.Visible = false;
        }

        #endregion

        protected void btnCancelarEliminarDireccion_Click(object sender, EventArgs e)
        {
            btnCancelarEliminarDireccion.Visible = false;
            btnAceptarEliminarDireccion.Visible = false;
            lblAceptarEliminarDireccion.Visible = false;
        }

        protected void btnCerrarDireccion_Click(object sender, EventArgs e)
        {
            panelDireccion.Visible = false;
            panelSucursal.Visible = true;
            btnCerrarDireccion.Visible = false;
            btnOkDireccion.Visible = true;
            btnCancelarDireccion.Visible = true;
        }

        protected void ddDireccionesEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(ddDireccionesEmpresa.SelectedValue);
            VM.ObtenerEmpresaDireccion(uid);

            uidDireccion.Text = string.Empty;
            ddPais.SelectedValue = VM.Direccion.UidPais.ToString();
            ddEstado.SelectedValue = VM.Direccion.UidEstado.ToString();
            txtMunicipio.Text = VM.Direccion.StrMunicipio;
            txtCiudad.Text = VM.Direccion.StrCiudad;
            txtColonia.Text = VM.Direccion.StrColonia;
            txtCalle.Text = VM.Direccion.StrCalle;
            txtConCalle.Text = VM.Direccion.StrConCalle;
            txtYCalle.Text = VM.Direccion.StrYCalle;
            txtNoExt.Text = VM.Direccion.StrNoExt;
            txtNoInt.Text = VM.Direccion.StrNoInt;
            txtReferencia.Text = VM.Direccion.StrReferencia;

            panelDireccion.Visible = true;
            panelSucursal.Visible = false;
            btnOkDireccion.Visible = true;
            btnCancelarDireccion.Visible = true;
            btnCerrarDireccion.Visible = false;
            btnOkDireccion.Enable();
            btnCancelarDireccion.Enable();

            List<SucursalDireccion> direcciones = (List<SucursalDireccion>)ViewState["Direcciones"];
            if (direcciones.Count > 0)
            {
                if (direcciones[0].ExistsInDatabase)
                    DireccionRemoved.Add(direcciones[0]);
                direcciones.Clear();
            }
            btnAgregarDireccion.Visible = true;
            btnEditarDireccion.Visible = false;
            btnEliminarDireccion.Visible = false;
            dgvDirecciones.DataSource = direcciones;
            dgvDirecciones.DataBind();
        }

        protected void btnObtenerDireccionEmpresa_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = true;
            VM.ObtenerEmpresaDirecciones(SesionActual.uidEmpresaActual.Value);

            ddDireccionesEmpresa.DataSource = VM.EmpresaDirecciones;
            ddDireccionesEmpresa.DataValueField = "UidDireccion";
            ddDireccionesEmpresa.DataTextField = "LongDirection";
            ddDireccionesEmpresa.DataBind();
            panelSeleccionDireccion.Visible = true;

            if (ddDireccionesEmpresa.Items.Count > 0)
                ddDireccionesEmpresa_SelectedIndexChanged(null, null);
        }

        protected void imagen(object sender, EventArgs e)
        {
            if (FUImagen.HasFile)
            {
                string extencion = Path.GetExtension(FUImagen.FileName).ToLower();
                string[] arreglo = { ".jpg", ".png", ".jpeg" };
                for (int i = 0; i < arreglo.Length; i++)
                {
                    if (extencion == arreglo[i])
                    {
                        string Nombrearchivo = Path.GetFileName(FUImagen.FileName);
                        int numero = new Random().Next(999999999);
                        string ruta = "~/Vista/Imagenes/Sucursales/"+ uidSucursal.Text + '_' + numero + Nombrearchivo;


                        //guardar img
                        FUImagen.SaveAs(Server.MapPath(ruta));

                        string rutaimg = ruta + "?" + (numero - 1);

                        ViewState["rutaimg"] = ruta;

                        ImgSucursales.ImageUrl = rutaimg;

                    }
                }
            }
        }

        protected void btnEncargados_Click(object sender, EventArgs e)
        {
            Response.Redirect("Encargados.aspx");
        }
    }

}