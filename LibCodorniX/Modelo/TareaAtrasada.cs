﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodorniX.Util;

namespace CodorniX.Modelo
{
    [Serializable]
    public class TareaAtrasada
    {
        private Guid _UidTarea;
        public Guid UidTarea
        {
            get { return _UidTarea; }
            set { _UidTarea = value; }
        }
		public string strIdTarea {
			get { return _UidTarea.ToString(); }
		}

        private Guid? _UidDepartamento;
        public Guid? UidDepartamento
        {
            get { return _UidDepartamento; }
            set { _UidDepartamento = value; }
        }
		public string strIdDepartamento {
			get { return _UidDepartamento.ToString(); }
		}

        private Guid? _UidArea;
        public Guid? UidArea
        {
            get { return _UidArea; }
            set { _UidArea = value; }
        }
		public string strIdArea {
			get { return _UidArea.ToString(); }
		}

        private Guid? _UidCumplimiento;
        public Guid? UidCumplimiento
        {
            get { return _UidCumplimiento; }
            set { _UidCumplimiento = value; }
        }
		public string strIdCumplimiento {
			get { return _UidCumplimiento.ToString(); }
		}

        private Guid? _UidTurno;
        public Guid? UidTurno
        {
            get { return _UidTurno; }
            set { _UidTurno = value; }
        }
		public string strIdTurno {
			get { return _UidTurno.ToString(); }
		}

        private string _StrTarea;
        public string StrTarea
        {
            get { return _StrTarea; }
            set { _StrTarea = value; }
        }

        private string _StrDepartamento;
        public string StrDepartamento
        {
            get { return _StrDepartamento; }
            set { _StrDepartamento = value; }
        }

        private string _StrArea;
        public string StrArea
        {
            get { return _StrArea; }
            set { _StrArea = value; }
        }

        private string _StrTurno;
        public string StrTurno
        {
            get { return _StrTurno; }
            set { _StrTurno = value; }
        }

		/* Android app */
		private string _strTipo;
		public string strTipo
		{
			get { return _strTipo; }
			set { _strTipo = value; }
		}

		private string _strFechaProgramada;
		public string strFechaProgramada
		{
			get { return _strFechaProgramada; }
			set { _strFechaProgramada = value; }
		}

		private string _strFechaProxima;
		public string strFechaProxima
		{
			get { return _strFechaProxima; }
			set { _strFechaProxima = value; }
		}

		private string _strServerMessage;
		public string strServerMessage
		{
			get { return _strServerMessage; }
			set { _strServerMessage = value; }
		}



		public class Repository
        {
            Connection conn = new Connection();

            public List<TareaAtrasada> Buscar(Guid uidUsuario, Guid uidSucursal, DateTime fecha, DateTime? fechaInicio, 
                DateTime? fechaFin, string departamentos)
            {
                List<TareaAtrasada> tareas = new List<TareaAtrasada>();

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TareasAtrasadas_Search";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

                if (fechaInicio.HasValue)
                    command.AddParameter("@DtFechaInicio", fechaInicio, SqlDbType.DateTime);
                if (fechaFin.HasValue)
                    command.AddParameter("@DtFechaFin", fechaFin, SqlDbType.DateTime);
                command.AddParameter("@UidDepartamentos", departamentos, SqlDbType.NVarChar, 2000);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    TareaAtrasada tarea = new TareaAtrasada();
                    tarea._UidTarea = new Guid(row["UidTarea"].ToString());
                    tarea._UidDepartamento = new Guid(row["UidDepartamento"].ToString());
                    tarea._UidArea = row.IsNull("UidArea") ? (Guid?)null : new Guid(row["UidArea"].ToString());
                    tarea._UidCumplimiento = row.IsNull("UidCumplimiento") ? (Guid?)null : new Guid(row["UidCumplimiento"].ToString());
                    tarea._StrTarea = row["VchTarea"].ToString();
                    tarea._StrDepartamento = row["VchDepartamento"].ToString();
                    tarea._StrArea = row["VchArea"].ToString();
                    tarea._StrTurno = row["VchTurno"].ToString();
					tarea._strServerMessage = "";
                    tareas.Add(tarea);
                }

                return tareas;
            }

            public int CancelarTarea(Guid uidTarea, Guid? uidDepto, Guid? uidArea, Guid? uidCumplimiento, DateTime fecha)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TareasAtrazadas_Do";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTarea", uidTarea, SqlDbType.UniqueIdentifier);
                if (uidCumplimiento.HasValue)
                    command.AddParameter("@UidCumplimiento", uidCumplimiento.Value, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaHora", fecha, SqlDbType.DateTime);
                if (uidDepto.HasValue)
                    command.AddParameter("@UidDepartamento", uidDepto, SqlDbType.UniqueIdentifier);
                if (uidArea.HasValue)
                    command.AddParameter("@UidArea", uidArea.Value, SqlDbType.UniqueIdentifier);
                command.AddParameter("@VchEstado", "Cancelado", SqlDbType.NVarChar, 50);

                command.Parameters.Add("@Estado", SqlDbType.TinyInt);
                command.Parameters["@Estado"].Direction = ParameterDirection.Output;

                conn.ExecuteCommand(command, false);

                int value = (byte)command.Parameters["@Estado"].Value;

                command.Dispose();

                return value;
            }

            public int PosponerTarea(Guid uidTarea, Guid? uidDepto, Guid? uidArea, Guid? uidCumplimiento,
                DateTime fechaActual, DateTime fechaNueva)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TareasAtrazadas_Do";
                command.CommandType = CommandType.StoredProcedure;

                if (uidCumplimiento.HasValue)
                    command.AddParameter("@UidCumplimiento", uidCumplimiento.Value, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidTarea", uidTarea, SqlDbType.UniqueIdentifier);
                if (uidDepto.HasValue)
                    command.AddParameter("@UidDepartamento", uidDepto, SqlDbType.UniqueIdentifier);
                if (uidArea.HasValue)
                    command.AddParameter("@UidArea", uidArea.Value, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaHora", fechaActual, SqlDbType.DateTime);
                command.AddParameter("@DtFechaNueva", fechaNueva, SqlDbType.DateTime);
                command.AddParameter("@VchEstado", "Pospuesto", SqlDbType.NVarChar, 50);

                command.Parameters.Add("@Estado", SqlDbType.TinyInt);
                command.Parameters["@Estado"].Direction = ParameterDirection.Output;

                conn.ExecuteCommand(command, false);

                int value = (byte)command.Parameters["@Estado"].Value;

                command.Dispose();

                return value;
            }

            public List<Departamento> ObtenerDepartamentosDeSupervision(Guid uidSupervisor, Guid uidSucursal, DateTime fecha)
            {
                List<Departamento> departamentos = new List<Departamento>();
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TareasAtrasadas_ObtenerDepartamentos";
                command.CommandType = CommandType.StoredProcedure;

                
                command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);
                command.AddParameter("@UidUsuario", uidSupervisor, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
                
                DataTable table = conn.ExecuteQuery(command);
                
                foreach (DataRow row in table.Rows)
                {
                    Departamento depto = new Departamento();
                    depto.UidDepartamento = new Guid(row["UidDepartamento"].ToString());
                    depto.StrNombre = row["VchNombre"].ToString();
                    departamentos.Add(depto);
                }

                return departamentos;
            }
        }
    }
}
