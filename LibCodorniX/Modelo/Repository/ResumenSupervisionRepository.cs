﻿using CodorniX.Modelo.Model;
using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo.Repository
{
    public class ResumenSupervisionRepository
    {
        private Connection conn = new Connection();

        public List<ResumenSupervision> FindBySupervisor(Guid uidSupervisor, Guid uidSucursal, DateTimeOffset fecha)
        {
            List<ResumenSupervision> listado = new List<ResumenSupervision>();
            ResumenSupervision tarea = null;

            SqlCommand command = new SqlCommand();
            command.CommandText = "usp_Supervision_ReporteSupervision";
            command.CommandType = CommandType.StoredProcedure;

            command.AddParameter("@UidUsuario", uidSupervisor, SqlDbType.UniqueIdentifier);
            command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
            command.AddParameter("@DtFecha", fecha, SqlDbType.DateTimeOffset);

            DataTable table = conn.ExecuteQuery(command);

            foreach (DataRow row in table.Rows)
            {
                tarea = new ResumenSupervision();
                tarea.IntFolio = Convert.ToInt32(row["IntFolio"]);
                tarea.NumTareasCumplidas = Convert.ToInt32(row["NumTareasCumplidas"]);
                tarea.NumTareasNoCumplidas = Convert.ToInt32(row["NumTareasNoCumplidas"]);
                tarea.NumTareasRevisadas = Convert.ToInt32(row["NumTareasRevisadas"]);
                tarea.NumTareasNoRevisadas = Convert.ToInt32(row["NumTareasNoRevisadas"]);
                tarea.Departamento = row["VchDepartamento"].ToString();
                tarea.Turno = row["VchTurno"].ToString();
                tarea.DtInicioTurno = row.IsNull("DtFechaHoraInicio") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHoraInicio"].ToString());
                tarea.DtFinTurno = row.IsNull("DtFechaHoraFin") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHoraFin"].ToString());
                tarea.EstadoTurno = row["VchEstadoTurno"].ToString();
                listado.Add(tarea);
            }

            return listado;
        }
    }
}
