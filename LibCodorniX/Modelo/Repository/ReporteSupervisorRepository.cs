﻿using CodorniX.Modelo.Model;
using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo.Repository
{
    public class ReporteSupervisorRepository
    {
        Connection conn = new Connection();

        public List<ReporteSupervisor> ObtenerReporteCumplimiento(Guid uidUsuario, Guid uidSucursal, DateTime fecha)
        {
            List<ReporteSupervisor> reportes = new List<ReporteSupervisor>();
            ReporteSupervisor reporte = null;

            SqlCommand command = new SqlCommand();
            command.CommandText = "usp_Supervision_ReporteCumplimiento";
            command.CommandType = CommandType.StoredProcedure;

            command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
            command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
            command.AddParameter("@DtFecha", fecha, SqlDbType.Date);

            DataTable table = conn.ExecuteQuery(command);

            foreach (DataRow row in table.Rows)
            {
                reporte = new ReporteSupervisor();
                reporte.FolioCumplimiento = Convert.ToInt32(row["IntFolioCumpl"]);
                reporte.FolioTarea = Convert.ToInt32(row["IntFolio"]);
                reporte.Tarea = row["VchTarea"].ToString();
                reporte.Departamento = row["VchDepartamento"].ToString();
                reporte.Area = row["VchArea"].ToString();
                reporte.FechaHora = DateTimeOffset.Parse(row["DtFechaHora"].ToString());
                reporte.Valor = row["VchResultado"].ToString();
                reporte.Correccion = row["VchCorreccion"].ToString();
                reporte.Observaciones = row["VchObservacion"].ToString();
                reporte.Usuario = row["VchUsuario"].ToString();
                reportes.Add(reporte);
            }

            return reportes;
        }

        public List<ReporteSupervisor> ObtenerReporteRevision(Guid uidUsuario, Guid uidSucursal, DateTime fecha)
        {
            List<ReporteSupervisor> reportes = new List<ReporteSupervisor>();
            ReporteSupervisor reporte = null;

            SqlCommand command = new SqlCommand();
            command.CommandText = "usp_Supervision_ReporteRevision";
            command.CommandType = CommandType.StoredProcedure;

            command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
            command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
            command.AddParameter("@DtFecha", fecha, SqlDbType.Date);

            DataTable table = conn.ExecuteQuery(command);

            foreach (DataRow row in table.Rows)
            {
                reporte = new ReporteSupervisor();
                reporte.FolioCumplimiento = Convert.ToInt32(row["IntFolioCumpl"]);
                reporte.FolioTarea = Convert.ToInt32(row["IntFolio"]);
                reporte.Tarea = row["VchTarea"].ToString();
                reporte.Departamento = row["VchDepartamento"].ToString();
                reporte.Area = row["VchArea"].ToString();
                reporte.FechaHora = DateTimeOffset.Parse(row["DtFechaHora"].ToString());
                reporte.Valor = row["VchResultado"].ToString();
                reporte.Correccion = row["VchCorreccion"].ToString();
                reporte.Observaciones = row["VchObservacion"].ToString();
                reporte.Usuario = row["VchUsuario"].ToString();
                reportes.Add(reporte);
            }

            return reportes;
        }
    }
}
