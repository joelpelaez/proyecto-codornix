﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Modelo
{
    [Serializable]
    public class Sesion
    {
        public Guid uidUsuario;
        public List<Guid> uidEmpresasPerfiles;
        public List<Guid> uidSucursalesPerfiles;
        public Guid? uidEmpresaActual;
        public Guid? uidSucursalActual;
        public Guid? uidPerfilActual;
        public Guid? uidNivelAccesoActual;
        public Guid? UidPeriodo;
        public Guid? UidTurno;
        public Guid? uidTurnoSupervisor;
        public List<Guid> UidPeriodos = new List<Guid>();
        public List<Guid> UidAsignacionesSupervision = new List<Guid>();
        public List<Guid> UidDepartamentos = new List<Guid>();
        public string appWeb;
        public string perfil;

		public string Messsage;

		public string strIdUsuario {get { return uidUsuario.ToString(); }}
		public string strIdEmpresa { get { return uidEmpresaActual.ToString(); } }
		public string strIdSucursal { get { return uidSucursalActual.ToString(); } }
		public string strIdPerfil { get { return uidPerfilActual.ToString(); } }
		public string strIdNivelAcceso { get { return uidNivelAccesoActual.ToString(); } }
		public string strIdPeriodo { get { return UidPeriodo.ToString(); } }
		public string strIdTurno { get { return UidTurno.ToString(); } }
		public string strIdTurnoSupervisor { get { return uidTurnoSupervisor.ToString(); } }
	}
}