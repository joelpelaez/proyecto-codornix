﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
    [Serializable]
    public class PeriodicidadSemanal
    {
        [NonSerialized]
        Conexion Conexion = new Conexion();
        private Guid _UidPeriodicidad;

        public Guid UidPeriodicidad
        {
            get { return _UidPeriodicidad; }
            set { _UidPeriodicidad = value; }
        }

        private bool _BlLunes;

        public bool BlLunes
        {
            get { return _BlLunes; }
            set { _BlLunes = value; }
        }

        private bool _BlMartes;

        public bool BlMartes
        {
            get { return _BlMartes; }
            set { _BlMartes = value; }
        }

        private bool _BlMiercoles;

        public bool BlMiercoles
        {
            get { return _BlMiercoles; }
            set { _BlMiercoles = value; }
        }

        private bool _BlJueves;

        public bool BlJueves
        {
            get { return _BlJueves; }
            set { _BlJueves = value; }
        }

        private bool _BlViernes;

        public bool BlViernes
        {
            get { return _BlViernes; }
            set { _BlViernes = value; }
        }

        private bool __BlSabado;

        public bool BlSabado
        {
            get { return __BlSabado; }
            set { __BlSabado = value; }
        }
        private bool _BlDomingo;

        public bool BlDomingo
        {
            get { return _BlDomingo; }
            set { _BlDomingo = value; }
        }

        public bool GuardarDatos()
        {

            bool Resultado = false;
            SqlCommand Comando = new SqlCommand();

            if (Conexion == null)
                Conexion = new Conexion();

            try
            {
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.CommandText = "usp_PeriodicidadSemanal";

                Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
                Comando.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

                Comando.Parameters.Add("@BitLunes", SqlDbType.Bit);
                Comando.Parameters["@BitLunes"].Value = BlLunes;

                Comando.Parameters.Add("@BitMartes", SqlDbType.Bit);
                Comando.Parameters["@BitMartes"].Value = BlMartes;

                Comando.Parameters.Add("@BitMiercoles", SqlDbType.Bit);
                Comando.Parameters["@BitMiercoles"].Value = BlMiercoles;

                Comando.Parameters.Add("@BitJueves", SqlDbType.Bit);
                Comando.Parameters["@BitJueves"].Value = BlJueves;

                Comando.Parameters.Add("@BitViernes", SqlDbType.Bit);
                Comando.Parameters["@BitViernes"].Value = BlViernes;

                Comando.Parameters.Add("@BitSabado", SqlDbType.Bit);
                Comando.Parameters["@BitSabado"].Value = BlSabado;

                Comando.Parameters.Add("@BitDomingo", SqlDbType.Bit);
                Comando.Parameters["@BitDomingo"].Value = BlDomingo;

                Resultado = Conexion.ManipilacionDeDatos(Comando);
                Comando.Dispose();




            }
            catch (Exception)
            {
                throw;
            }
            return Resultado;
        }

        public class Repositorio
        {
            Conexion Conexion = new Conexion();

            public PeriodicidadSemanal ConsultarPeriodicidadSemanal(Guid Periodicidad)
            {
                PeriodicidadSemanal periodicidadsemanal = null;

                DataTable table = null;

                SqlCommand comando = new SqlCommand();
                comando.CommandText = "usp_BuscarPeriodicidadSemanal";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
                comando.Parameters["@UidPeriodicidad"].Value = Periodicidad;

                table = Conexion.Busquedas(comando);


                foreach (DataRow row in table.Rows)
                {
                    periodicidadsemanal = new PeriodicidadSemanal()
                    {
                        UidPeriodicidad = (Guid)row["UidPeriodicidad"],
                        BlLunes = (bool)row["BitLunes"],
                        BlMartes = (bool)row["BitMartes"],
                        BlMiercoles = (bool)row["BitMiercoles"],
                        BlJueves = (bool)row["BitJueves"],
                        BlViernes = (bool)row["BitViernes"],
                        BlSabado = (bool)row["BitSabado"],
                        BlDomingo = (bool)row["BitDomingo"],
                    };

                }


                return periodicidadsemanal;


            }
        }
    }
}
