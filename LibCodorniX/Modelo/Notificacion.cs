﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    public class Notificacion
    {
        private Guid _UidNotificacion;

        public Guid UidNotificacion
        {
            get { return _UidNotificacion; }
            set { _UidNotificacion = value; }
        }

        private Guid _UidTarea;

        public Guid UidTarea
        {
            get { return _UidTarea; }
            set { _UidTarea = value; }
        }

        private bool? _BlValor;

        public bool? BlValor
        {
            get { return _BlValor; }
            set { _BlValor = value; }
        }

        private decimal? _DcMenorQue;

        public decimal? DcMenorQue
        {
            get { return _DcMenorQue; }
            set { _DcMenorQue = value; }
        }

        private decimal? _DcMayorQue;

        public decimal? DcMayorQue
        {
            get { return _DcMayorQue; }
            set { _DcMayorQue = value; }
        }

        private bool? _BlMenorIgual;

        public bool? BlMenorIgual
        {
            get { return _BlMenorIgual; }
            set { _BlMenorIgual = value; }
        }

        private bool? _BlMayorIgual;

        public bool? BlMayorIgual
        {
            get { return _BlMayorIgual; }
            set { _BlMayorIgual = value; }
        }

        private string _VchOpciones;

        public string VchOpciones
        {
            get { return _VchOpciones; }
            set { _VchOpciones = value; }
        }

        public List<Guid> UidOpciones
        {
            get
            {
                List<Guid> list = new List<Guid>();
                Guid opcion;
                string [] uids = _VchOpciones.Split(',');

                foreach (string uid in uids)
                {
                    opcion = new Guid(uid);
                    list.Add(opcion);
                }

                return list;
            }
        }

        public class Repository
        {
            Connection conn = new Connection();

            public Notificacion Get(Guid uidTarea)
            {
                Notificacion notificacion = null;
                SqlCommand command = new SqlCommand();

                command.CommandText = "usp_Notificacion_Get";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTarea", uidTarea, SqlDbType.UniqueIdentifier);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    notificacion = new Notificacion();
                    notificacion._UidNotificacion = new Guid(row["UidNotificacion"].ToString());
                    notificacion._UidTarea = new Guid(row["UidTarea"].ToString());
                    notificacion._BlValor = row.IsNull("BlValor") ? (bool?)null : Convert.ToBoolean(row["BlValor"].ToString());
                    notificacion._DcMenorQue = row.IsNull("DcMenorQue") ? (decimal?)null : Convert.ToDecimal(row["DcMenorQue"].ToString());
                    notificacion._DcMayorQue = row.IsNull("DcMayorQue") ? (decimal?)null : Convert.ToDecimal(row["DcMayorQue"].ToString());
                    notificacion._BlMenorIgual = row.IsNull("BitMenorIgual") ? (bool?)null : Convert.ToBoolean(row["BitMenorIgual"].ToString());
                    notificacion._BlMayorIgual = row.IsNull("BitMayorIgual") ? (bool?)null : Convert.ToBoolean(row["BitMayorIgual"].ToString());
                    notificacion._VchOpciones = row["VchOpciones"].ToString();
                }

                return notificacion;
            }

            public void Set(Notificacion notificacion)
            {
                SqlCommand command = new SqlCommand();

                command.CommandText = "usp_Notificacion_Set";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTarea", notificacion.UidTarea, SqlDbType.UniqueIdentifier);
                if (notificacion._BlValor.HasValue)
                    command.AddParameter("@BlValor", notificacion._BlValor.Value, SqlDbType.Bit);
                if (notificacion._DcMenorQue.HasValue)
                {
                    command.AddParameter("@DcMenorQue", notificacion._DcMenorQue.Value, SqlDbType.Decimal);
                    command.Parameters["@DcMenorQue"].Scale = 4;
                    command.Parameters["@DcMenorQue"].Precision = 18;
                }
                if (notificacion._DcMayorQue.HasValue)
                {
                    command.AddParameter("@DcMayorQue", notificacion._DcMayorQue.Value, SqlDbType.Decimal);
                    command.Parameters["@DcMayorQue"].Scale = 4;
                    command.Parameters["@DcMayorQue"].Precision = 18;
                }
                if (notificacion._BlMenorIgual.HasValue)
                    command.AddParameter("@BitMenorIgual", notificacion._BlMenorIgual.Value, SqlDbType.Bit);
                if (notificacion._BlMayorIgual.HasValue)
                    command.AddParameter("@BitMayorIgual", notificacion._BlMayorIgual.Value, SqlDbType.Bit);

                command.AddParameter("@VchOpciones", notificacion._VchOpciones, SqlDbType.NVarChar, 2000);

                conn.ExecuteCommand(command);
            }
        
        }
    }
}
