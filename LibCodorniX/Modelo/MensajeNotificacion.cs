﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    [Serializable]
    public class MensajeNotificacion
    {
        private Guid _UidMensajeNotificacion;
        public Guid UidMensajeNotificacion
        {
            get { return _UidMensajeNotificacion; }
            set { _UidMensajeNotificacion = value; }
        }
		public string strIdMensajeNotificacion { get { return _UidMensajeNotificacion.ToString(); } }

        private Guid _UidEstadoNotificacion;
        public Guid UidEstadoNotificacion
        {
            get { return _UidEstadoNotificacion; }
            set { _UidEstadoNotificacion = value; }
        }
		public string strIdEstadoNotificacion { get { return _UidEstadoNotificacion.ToString(); } }

        private Guid _UidCumplimiento;
        public Guid UidCumplimiento
        {
            get { return _UidCumplimiento; }
            set { _UidCumplimiento = value; }
        }
		public string strIdCumplimiento { get { return _UidCumplimiento.ToString(); } }

        // EXTRA:
        private string _StrTarea;
        public string StrTarea
        {
            get { return _StrTarea; }
            set { _StrTarea = value; }
        }

        private string _StrDepartamento;
        public string StrDepartamento
        {
            get { return _StrDepartamento; }
            set { _StrDepartamento = value; }
        }

        private string _StrEstadoNotificacion;
        public string StrEstadoNotificacion
        {
            get { return _StrEstadoNotificacion; }
            set { _StrEstadoNotificacion = value; }
        }

        private string _StrResultado;
        public string StrResultado
        {
            get { return _StrResultado; }
            set { _StrResultado = value; }
        }

        private string _StrArea;
        public string StrArea
        {
            get { return _StrArea; }
            set { _StrArea = value; }
        }

        public class Repository
        {
            Connection conn = new Connection();

            public List<MensajeNotificacion> Search(Guid uidSucursal, string uidDepartamentos,
                string uidDepartamentosbusqueda, string uidEstados, DateTime? fechaInicio, DateTime? fechaFin)
            {
                List<MensajeNotificacion> notificaciones = new List<MensajeNotificacion>();
                MensajeNotificacion mensaje = null;

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_MensajeNotificacion_Search";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
                command.AddParameter("@VchDepartamentos", uidDepartamentos, SqlDbType.NVarChar, 2000);
                command.AddParameter("@VchDepartamentosBusqueda", uidDepartamentosbusqueda, SqlDbType.NVarChar, 2000);
                command.AddParameter("@VchEstados", uidEstados, SqlDbType.NVarChar, 2000);
                if (fechaInicio.HasValue)
                    command.AddParameter("@DtFechaInicio", fechaInicio.Value, SqlDbType.DateTime);
                if (fechaFin.HasValue)
                    command.AddParameter("@DtFechaFin", fechaFin.Value, SqlDbType.DateTime);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    mensaje = new MensajeNotificacion();
                    mensaje._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
                    mensaje._UidEstadoNotificacion = new Guid(row["UidEstadoNotificacion"].ToString());
                    mensaje._UidMensajeNotificacion = new Guid(row["UidMensajeNotificacion"].ToString());
                    mensaje._StrResultado = row["VchResultado"].ToString();
                    mensaje._StrTarea = row["VchTarea"].ToString();
                    mensaje._StrDepartamento = row["VchDepartamento"].ToString();
                    mensaje._StrEstadoNotificacion = row["VchEstadoNotificacion"].ToString();
                    notificaciones.Add(mensaje);
                }

                return notificaciones;
            }

            public MensajeNotificacion Find(Guid uid)
            {
                MensajeNotificacion mensaje = null;

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_MensajeNotificacion_Find";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidMensajeNotificacion", uid, SqlDbType.UniqueIdentifier);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    mensaje = new MensajeNotificacion();
                    mensaje._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
                    mensaje._UidEstadoNotificacion = new Guid(row["UidEstadoNotificacion"].ToString());
                    mensaje._UidMensajeNotificacion = new Guid(row["UidMensajeNotificacion"].ToString());
                    mensaje._StrResultado = row["VchResultado"].ToString();
                    mensaje._StrTarea = row["VchTarea"].ToString();
                    mensaje._StrDepartamento = row["VchDepartamento"].ToString();
                    mensaje._StrEstadoNotificacion = row["VchEstadoNotificacion"].ToString();
                    mensaje._StrArea = row["VchArea"].ToString();
                }

                return mensaje;
            }

            public MensajeNotificacion FindByCumplimiento(Guid uid)
            {
                MensajeNotificacion mensaje = null;

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_MensajeNotificacion_FindByCumplimiento";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidCumplimiento", uid, SqlDbType.UniqueIdentifier);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    mensaje = new MensajeNotificacion();
                    mensaje._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
                    mensaje._UidEstadoNotificacion = new Guid(row["UidEstadoNotificacion"].ToString());
                    mensaje._UidMensajeNotificacion = new Guid(row["UidMensajeNotificacion"].ToString());
                    mensaje._StrResultado = row["VchResultado"].ToString();
                    mensaje._StrTarea = row["VchTarea"].ToString();
                    mensaje._StrDepartamento = row["VchDepartamento"].ToString();
                    mensaje._StrEstadoNotificacion = row["VchEstadoNotificacion"].ToString();
                    mensaje._StrArea = row["VchArea"].ToString();
                }

                return mensaje;
            }

            public void ChangeState(Guid uid, string state)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_MensajeNotificacion_CambiarEstado";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidMensajeNotificacion", uid, SqlDbType.UniqueIdentifier);
                command.AddParameter("@VchEstado", state, SqlDbType.NVarChar, 50);

                conn.ExecuteCommand(command);
            }

            public List<Departamento> GetDepartamentos(Guid uidUsuario, DateTime fecha)
            {
                List<Departamento> deptos = new List<Departamento>();

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_Supervisor_Departamentos";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    Departamento depto = new Departamento();
                    depto.UidDepartamento = new Guid(row["UidDepartamento"].ToString());
                    depto.StrNombre = row["VchNombre"].ToString();
                    deptos.Add(depto);
                }

                return deptos;
            }
        }
    }
}
