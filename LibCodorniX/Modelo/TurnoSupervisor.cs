﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    public class TurnoSupervisor
    {
        public Guid UidTurnoSupervisor { get; set; }
		public string strIdTurnoSupervisor { get { return UidTurnoSupervisor.ToString(); } }

        public Guid UidUsuario { get; set; }
		public string strIdUsuario { get { return UidUsuario.ToString(); } }

        public Guid UidSucursal { get; set; }
		public string strIdSucursal { get { return UidSucursal.ToString(); } }

        public Guid UidEstadoTurno { get; set; }
		public string strIdEstadoTurno { get { return UidEstadoTurno.ToString(); } }

        public int IntFolio { get; set; }
        public DateTimeOffset? DtFechaInicio { get; set; }
        public DateTimeOffset? DtFechaFin { get; set; }

        // EXTRA
        public string StrNombre { get; set; }
        public string StrApellidoPaterno { get; set; }
        public string StrApellidoMaterno { get; set; }
        public string StrEstadoTurno { get; set; }

        // Dynamic
        public string FechaInicio
        {
            get => DtFechaInicio?.ToString("dd/MM/yyyy HH:mm") ?? "(no iniciado)";
        }
        public string FechaFin
        {
            get => DtFechaFin?.ToString("dd/MM/yyyy HH:mm") ?? "(no cerrado)";
        }
        public string Usuario
        {
            get
            {
                string nombreCompleto = StrNombre + " " + StrApellidoPaterno;
                if (StrApellidoMaterno == null)
                    nombreCompleto += " " + StrApellidoMaterno;

                return nombreCompleto;
            }
        }

        public class Repository
        {
            Connection conn = new Connection();

            public void CrearTurno(Guid uidUsuario, Guid uidSucursal, DateTimeOffset fechaInicio)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TurnoSupervisor_CrearTurno";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaInicio", fechaInicio, SqlDbType.DateTimeOffset);

                conn.ExecuteCommand(command);
            }

            public void ModificarEstado(Guid uidTurnoSupervisor, string estado)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TurnoSupervisor_CambiarEstado";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTurnoSupervisor", uidTurnoSupervisor, SqlDbType.UniqueIdentifier);
                command.AddParameter("@VchEstado", estado, SqlDbType.NVarChar, 50);

                conn.ExecuteCommand(command);
            }

            public void ModificarFechaFin(Guid uidTurnoSupervisor, DateTimeOffset fechaFin)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TurnoSupervisor_CambiarFechaFin";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTurnoSupervisor", uidTurnoSupervisor, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaFin", fechaFin, SqlDbType.DateTimeOffset);

                conn.ExecuteCommand(command);
            }

            public TurnoSupervisor ObtenerTurnoHoy(Guid uidUsuario, Guid uidSucursal, DateTime fecha)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TurnoSupervisor_GetByDay";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFecha", fecha, SqlDbType.Date);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    TurnoSupervisor turno = new TurnoSupervisor();
                    turno.UidTurnoSupervisor = row.IsNull("UidTurnoSupervisor") ? Guid.Empty : new Guid(row["UidTurnoSupervisor"].ToString());
                    turno.UidUsuario = new Guid(row["UidUsuario"].ToString());
                    turno.UidSucursal = row.IsNull("UidSucursal") ? uidSucursal : new Guid(row["UidSucursal"].ToString());
                    turno.IntFolio = Convert.ToInt32(row["IntFolio"].ToString());
                    turno.DtFechaInicio = row.IsNull("DtFechaInicio") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaInicio"].ToString());
                    turno.DtFechaFin = row.IsNull("DtFechaFin") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaFin"].ToString());
                    turno.StrEstadoTurno = row["VchEstadoTurno"].ToString();
                    turno.StrNombre = row["VchNombre"].ToString();
                    turno.StrApellidoPaterno = row["VchApellidoPaterno"].ToString();
                    turno.StrApellidoMaterno = row["VchApellidoMaterno"].ToString();
                    return turno;
                }
                return null;
            }

            public TurnoSupervisor Find(Guid uid)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TurnoSupervisor_Find";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTurnoSupervisor", uid, SqlDbType.UniqueIdentifier);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    TurnoSupervisor turno = new TurnoSupervisor();
                    turno.UidTurnoSupervisor = new Guid(row["UidTurnoSupervisor"].ToString());
                    turno.UidUsuario = new Guid(row["UidUsuario"].ToString());
                    turno.UidSucursal = new Guid(row["UidSucursal"].ToString());
                    turno.IntFolio = Convert.ToInt32(row["IntFolio"].ToString());
                    turno.DtFechaInicio = DateTimeOffset.Parse(row["DtFechaInicio"].ToString());
                    turno.DtFechaFin = row.IsNull("DtFechaFin") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaFin"].ToString());
                    turno.UidEstadoTurno = new Guid(row["UidEstadoTurno"].ToString());
                    return turno;
                }
                return null;
            }
        }
    }
}
