﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    public class Hora
    {
        public static DateTimeOffset ObtenerHoraServidor()
        {
            Connection connection = new Connection();
            SqlCommand command = new SqlCommand();
            command.CommandText = "SELECT SYSDATETIMEOFFSET() as hora";

            DataTable table = connection.ExecuteQuery(command);

            DateTimeOffset time = DateTimeOffset.Parse(table.Rows[0]["hora"].ToString());

            return time;
        }

        public static TimeZoneInfo ObtenerZonaHoraria(Guid sucursal)
        {
            Connection connection = new Connection();
            SqlCommand command = new SqlCommand();
            command.CommandText = "SELECT VchZonaHoraria FROM Sucursal WHERE UidSucursal = @uid";
            command.AddParameter("@uid", sucursal, SqlDbType.UniqueIdentifier);

            DataTable table = connection.ExecuteQuery(command);

            string timezone = table.Rows[0]["VchZonaHoraria"].ToString();
            if (timezone == null)
                return TimeZoneInfo.Local;

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timezone);

            if (timeZoneInfo == null)
                return TimeZoneInfo.Local;

            return timeZoneInfo;
        }
    }
}
