﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CodorniX.Modelo
{
    [Serializable]
    public class Periodo
    {
        private Guid _UidPeriodo;

        public Guid UidPeriodo
        {
            get { return _UidPeriodo; }
            set { _UidPeriodo = value; }
        }

        private DateTime _DtFechaInicio;

        public DateTime DtFechaInicio
        {
            get { return _DtFechaInicio; }
            set { _DtFechaInicio = value; }
        }

        private DateTime? _DtFechaFin;

        public DateTime? DtFechaFin
        {
            get { return _DtFechaFin; }
            set { _DtFechaFin = value; }
        }

        private Guid _UidDepartamento;

        public Guid UidDepartamento
        {
            get { return _UidDepartamento; }
            set { _UidDepartamento = value; }
        }

        private Guid _UidUsuario;

        public Guid UidUsuario
        {
            get { return _UidUsuario; }
            set { _UidUsuario = value; }
        }

        private Guid _UidTurno;

        public Guid UidTurno
        {
            get { return _UidTurno; }
            set { _UidTurno = value; }
        }

        // EXTRA
        private String _StrNombreUsuario;

        public String StrNombreUsuario
        {
            get { return _StrNombreUsuario; }
            set { _StrNombreUsuario = value; }
        }

        private String _StrDepto;

        public String StrNombreDepto
        {
            get { return _StrDepto; }
            set { _StrDepto = value; }
        }
        
        private String _StrTurno;

        public String StrTurno
        {
            get { return _StrTurno; }
            set { _StrTurno = value; }
        }


        public enum ReturnCode
        {
            Success = 0,
            SuccessWithChanges = 1,
            SuccessUpdate = 2,
            FailureSame = -1,
            FailureMax = -2,
            Undefined = -3
        }

        public class Repository
        {
            Connection conn = new Connection();

            public ReturnCode Save(Periodo periodo)
            {
                ReturnCode code = ReturnCode.Undefined;
                SqlCommand command = new SqlCommand();
                try
                {
                    command.CommandText = "usp_Periodo_Add";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@DtFechaInicio", periodo._DtFechaInicio, SqlDbType.DateTime);
                    command.AddParameter("@DtFechaFin", periodo._DtFechaFin, SqlDbType.DateTime);
                    command.AddParameter("@UidDepartamento", periodo._UidDepartamento, SqlDbType.UniqueIdentifier);
                    command.AddParameter("@UidUsuario", periodo._UidUsuario, SqlDbType.UniqueIdentifier);
                    command.AddParameter("@UidTurno", periodo._UidTurno, SqlDbType.UniqueIdentifier);

                    command.Parameters.Add("@UidPeriodo", SqlDbType.UniqueIdentifier);
                    command.Parameters["@UidPeriodo"].Direction = ParameterDirection.Output;

                    command.Parameters.Add("@IntResult", SqlDbType.Int);
                    command.Parameters["@IntResult"].Direction = ParameterDirection.Output;

                    conn.ExecuteCommand(command, false);

                    periodo._UidPeriodo = (Guid)command.Parameters["@UidPeriodo"].Value;
                    int tmpcode = (int)command.Parameters["@IntResult"].Value;
                    code = (ReturnCode)tmpcode;

                    command.Dispose();
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error receiving table Periodo", e);
                }

                return code;
            }

            public Periodo Find(Guid uid)
            {
                Periodo periodo = null;

                SqlCommand command = new SqlCommand();

                try
                {
                    command.CommandText = "usp_Periodo_Find";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidPeriodo", uid, SqlDbType.UniqueIdentifier);

                    DataTable table = conn.ExecuteQuery(command);

                    foreach (DataRow row in table.Rows)
                    {
                        periodo = new Periodo()
                        {
                            _UidPeriodo = (Guid)row["UidPeriodo"],
                            _DtFechaInicio = (DateTime)row["DtFechaInicio"],
                            _DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
                            _UidDepartamento = (Guid)row["UidDepartamento"],
                            _UidUsuario = (Guid)row["UidUsuario"],
                            _UidTurno = (Guid)row["UidTurno"],
                        };
                    }
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error fetching data from Periodo", e);
                }

                return periodo;
            }

            public Periodo BuscarPeriodo(Guid uid)
            {
                Periodo periodo = null;

                SqlCommand command = new SqlCommand();

                try
                {
                    command.CommandText = "usp_BuscarPeriodo ";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidUsuario", uid, SqlDbType.UniqueIdentifier);

                    DataTable table = conn.ExecuteQuery(command);

                    foreach (DataRow row in table.Rows)
                    {
                        periodo = new Periodo()
                        {
                            _UidPeriodo = (Guid)row["UidPeriodo"],
                            _DtFechaInicio = (DateTime)row["DtFechaInicio"],
                            _DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
                            _UidDepartamento = (Guid)row["UidDepartamento"],
                            _UidUsuario = (Guid)row["UidUsuario"],
                            _UidTurno = (Guid)row["UidTurno"],
                        };
                    }
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error fetching data from Periodo", e);
                }

                return periodo;
            }

            public Periodo ObtenerPeriodoTurno(Guid uid)
            {
                Periodo periodo = null;

                SqlCommand command = new SqlCommand();

                try
                {
                    command.CommandText = "usp_ObtenerPeriodoTurno ";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidPeriodo", uid, SqlDbType.UniqueIdentifier);

                    DataTable table = conn.ExecuteQuery(command);

                    foreach (DataRow row in table.Rows)
                    {
                        periodo = new Periodo()
                        {
                            _UidPeriodo = (Guid)row["UidPeriodo"],
                            _DtFechaInicio = (DateTime)row["DtFechaInicio"],
                            _DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
                            _UidDepartamento = (Guid)row["UidDepartamento"],
                            _UidUsuario = (Guid)row["UidUsuario"],
                            _UidTurno = (Guid)row["UidTurno"],
                        };
                    }
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error fetching data from Periodo", e);
                }

                return periodo;
            }

            public Periodo Last(Guid departamento, Guid turno)
            {
                Periodo periodo = null;

                SqlCommand command = new SqlCommand();

                try
                {
                    command.CommandText = "usp_Periodo_Last";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidDepartamento", departamento, SqlDbType.UniqueIdentifier);
                    command.AddParameter("@UidTurno", turno, SqlDbType.UniqueIdentifier);

                    DataTable table = conn.ExecuteQuery(command);

                    foreach (DataRow row in table.Rows)
                    {
                        periodo = new Periodo()
                        {
                            _UidPeriodo = (Guid)row["UidPeriodo"],
                            _DtFechaInicio = (DateTime)row["DtFechaInicio"],
                            _DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
                            _UidDepartamento = (Guid)row["UidDepartamento"],
                            _UidUsuario = (Guid)row["UidUsuario"],
                            _UidTurno = (Guid)row["UidTurno"],
                        };
                    }
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error fetching data from Periodo", e);
                }

                return periodo;
            }

            public List<Periodo> FindAll()
            {
                List<Periodo> periodos = new List<Periodo>();
                Periodo periodo = null;

                SqlCommand command = new SqlCommand();

                try
                {
                    command.CommandText = "usp_Periodo_FindAll";
                    command.CommandType = CommandType.StoredProcedure;

                    DataTable table = conn.ExecuteQuery(command);

                    foreach (DataRow row in table.Rows)
                    {
                        periodo = new Periodo()
                        {
                            _UidPeriodo = (Guid)row["UidPeriodo"],
                            _DtFechaInicio = (DateTime)row["DtFechaInicio"],
                            _DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
                            _UidDepartamento = (Guid)row["UidDepartamento"],
                            _UidUsuario = (Guid)row["UidUsuario"],
                            _UidTurno = (Guid)row["UidTurno"],
                        };

                        periodos.Add(periodo);
                    }
                }
                catch (SqlException e)
                {

                    throw new DatabaseException("Error fetching data from Periodo", e);
                }

                return periodos;
            }

            public List<Periodo> FindBy(Criteria criteria, bool fetchExtra = false)
            {
                List<Periodo> periodos = new List<Periodo>();
                Periodo periodo = null;

                SqlCommand command = new SqlCommand();

                Usuario.Repository usuarioRepository = null;
                Departamento.Repository departamentoRepository = null;
                Turno.Repository turnoRepository = null;

                if (fetchExtra)
                {
                    usuarioRepository = new Usuario.Repository();
                    departamentoRepository = new Departamento.Repository();
                    turnoRepository = new Turno.Repository();
                }

                try
                {
                    command.CommandText = "usp_Periodo_Search";
                    command.CommandType = CommandType.StoredProcedure;

                    InjectCriteria(command, criteria);

                    DataTable table = conn.ExecuteQuery(command);

                    foreach (DataRow row in table.Rows)
                    {
                        periodo = new Periodo()
                        {
                            _UidPeriodo = (Guid)row["UidPeriodo"],
                            _DtFechaInicio = DateTime.Parse(row["DtFechaInicio"].ToString()),
                            _DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : DateTime.Parse(row["DtFechaFin"].ToString()),
                            _UidDepartamento = (Guid)row["UidDepartamento"],
                            _UidUsuario = (Guid)row["UidUsuario"],
                            _UidTurno = (Guid)row["UidTurno"],
                        };

                        if (fetchExtra)
                        {
                            Usuario usuario = usuarioRepository.Find(periodo._UidUsuario);
                            periodo._StrNombreUsuario = usuario.STRNOMBRE + " " + usuario.STRAPELLIDOPATERNO;

                            Turno turno = turnoRepository.Find(periodo._UidTurno);
                            periodo._StrTurno = turno.StrTurno;

                            Departamento departamento = departamentoRepository.Encontrar(periodo._UidDepartamento);
                            periodo._StrDepto = departamento.StrNombre;

                        }

                        periodos.Add(periodo);
                    }
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error fetching data from Periodo", e);
                }

                return periodos;
            }

            private void InjectCriteria(SqlCommand command, Criteria criteria)
            {
                if (criteria.FechaInicioDespuesDe.HasValue)
                {
                    command.AddParameter("@DtFechaInicioDespuesDe", criteria.FechaInicioDespuesDe.Value, SqlDbType.DateTime);
                }
                if (criteria.FechaInicioAntesDe.HasValue)
                {
                    command.AddParameter("@DtFechaInicioAntesDe", criteria.FechaInicioAntesDe.Value, SqlDbType.DateTime);
                }
                if (criteria.FechaFinDespuesDe.HasValue)
                {
                    command.AddParameter("@DtFechaFinDespuesDe", criteria.FechaFinDespuesDe.Value, SqlDbType.DateTime);
                }
                if (criteria.FechaFinAntesDe.HasValue)
                {
                    command.AddParameter("@DtFechaFinAntesDe", criteria.FechaFinAntesDe.Value, SqlDbType.DateTime);
                }
                if (criteria.Usuario.HasValue)
                {
                    command.AddParameter("@UidUsuario", criteria.Usuario.Value, SqlDbType.UniqueIdentifier);
                }
                if (!string.IsNullOrWhiteSpace(criteria.Usuarios))
                {
                    command.AddParameter("@VchUsuario", criteria.Usuarios, SqlDbType.NVarChar, 4000);
                }
                if (!string.IsNullOrWhiteSpace(criteria.Turnos))
                {
                    command.AddParameter("@UidTurno", criteria.Turnos, SqlDbType.NVarChar, 4000);
                }
                if (!string.IsNullOrWhiteSpace(criteria.Departamentos))
                {
                    command.AddParameter("@UidDepartamento", criteria.Departamentos, SqlDbType.NVarChar, 4000);
                }
                if (criteria.Sucursal != Guid.Empty)
                {
                    command.AddParameter("@UidSucursal", criteria.Sucursal, SqlDbType.UniqueIdentifier);
                }
            }
        }

        public class Criteria
        {
            public DateTime? FechaInicioDespuesDe { get; set; }
            public DateTime? FechaInicioAntesDe { get; set; }
            public DateTime? FechaFinDespuesDe { get; set; }
            public DateTime? FechaFinAntesDe { get; set; }
            public Guid? Usuario { get; set; }
            public String Usuarios { get; set; }
            public String Turnos { get; set; }
            public String Departamentos { get; set; }
            public Guid Sucursal { get; set; }
        }
    }
}