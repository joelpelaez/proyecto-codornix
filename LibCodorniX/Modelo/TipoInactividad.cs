﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    [Serializable]
    public class TipoInactividad
    {
        private Guid _UidTipoInactividad;

        public Guid UidTipoInactividad
        {
            get { return _UidTipoInactividad; }
            set { _UidTipoInactividad = value; }
        }

        private string _StrTipoInactividad;

        public string StrTipoInactividad
        {
            get { return _StrTipoInactividad; }
            set { _StrTipoInactividad = value; }
        }

        public class Repository
        {
            Connection conn = new Connection();

            public List<TipoInactividad> FindAll()
            {
                List<TipoInactividad> tipos = new List<TipoInactividad>();
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TipoInactividad_FindAll";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    TipoInactividad tipo = new TipoInactividad();
                    tipo._UidTipoInactividad = new Guid(row["UidTipoInactividad"].ToString());
                    tipo._StrTipoInactividad = row["VchTipoInactividad"].ToString();
                    tipos.Add(tipo);
                }

                return tipos;
            }

            public TipoInactividad Find(Guid uid)
            {
                TipoInactividad tipo = null;

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_TipoInactividad_Find";
                command.CommandType = CommandType.StoredProcedure;

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    tipo = new TipoInactividad();
                    tipo._UidTipoInactividad = new Guid(row["UidTipoInactividad"].ToString());
                    tipo._StrTipoInactividad = row["VchTipoInactividad"].ToString();
                }

                return tipo;
            }
        }
    }
}
