﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using CodorniX.Util;

namespace CodorniX.Modelo
{
    [Serializable]
    public class TareaOpcion : Opciones
    {
        private Guid _UidTarea;

        public Guid UidTarea
        {
            get { return _UidTarea; }
            set { _UidTarea = value; }
        }

        public class Repositorio
        {
            Conexion Conexion = new Conexion();

            public bool Guardar(TareaOpcion tareaopcion)
            {
                if (tareaopcion._ExistsInDatabase)
                    return Modificacion(tareaopcion);
                else
                {
                    tareaopcion._ExistsInDatabase = true;
                    return Guardado(tareaopcion);
                }
            }

            private bool Guardado(TareaOpcion TareaOpcion)
            {
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_TareaOpcion_Add";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidTarea", TareaOpcion._UidTarea, SqlDbType.UniqueIdentifier);
                    command.AddParameter("@VchOpciones", TareaOpcion.StrOpciones, SqlDbType.NVarChar, 20);
                    command.AddParameter("@IntOrden", TareaOpcion.IntOrden, SqlDbType.Int);

                    return Conexion.ManipilacionDeDatos(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Cannot save a Opciones from Tarea", e);
                }
            }

            protected bool Modificacion(Opciones Opciones)
            {
                try
                {
                    SqlCommand comando = new SqlCommand();
                    comando.CommandText = "usp_TareaOpcion_Update";
                    comando.CommandType = CommandType.StoredProcedure;

                    comando.Parameters.Add("@UidOpciones", SqlDbType.UniqueIdentifier);
                    comando.Parameters["@UidOpciones"].Value = Opciones.UidOpciones;

                    comando.Parameters.Add("@VchOpciones", SqlDbType.NVarChar, 20);
                    comando.Parameters["@VchOpciones"].Value = Opciones.StrOpciones;

                    comando.AddParameter("@IntOrden", Opciones.IntOrden, SqlDbType.Int);
                    comando.AddParameter("@BitVisible", Opciones.BlVisible, SqlDbType.Bit);

                    return Conexion.ManipilacionDeDatos(comando);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Cannot update a Opciones", e);
                }
            }

            public List<TareaOpcion> Buscar(Guid uid)
            {
                DataTable table = new DataTable();
                List<TareaOpcion> opciones = new List<TareaOpcion>();
                TareaOpcion TareaOpcion = null;
                try
                {
                    SqlCommand comando = new SqlCommand();
                    comando.CommandText = "usp_TareaTelefono_Buscar";
                    comando.CommandType = CommandType.StoredProcedure;

                    comando.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
                    comando.Parameters["@UidTarea"].Value = uid;

                    table = Conexion.Busquedas(comando);

                    foreach (DataRow row in table.Rows)
                    {

                        TareaOpcion = new TareaOpcion()
                        {
                            _UidTarea = uid,
                            UidOpciones = new Guid(row["UidOpciones"].ToString()),
                            StrOpciones = row["VchOpciones"].ToString(),
                            IntOrden = row.IsNull("IntOrden") ? 0 : Convert.ToInt32(row["IntOrden"].ToString()),
                            BlVisible = Convert.ToBoolean(row["BitVisible"].ToString()),
                            _ExistsInDatabase = true
                        };

                        opciones.Add(TareaOpcion);
                    }
                }
                catch (Exception e)
                {
                    throw;
                }

                return opciones;
            }
        }
    }
}
