﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
    [Serializable]
    public class PeriodicidadMensual
    {
        [NonSerialized]
        Conexion Conexion = new Conexion();
        private Guid _UidPeriodicidad;

        public Guid UidPeriodicidad
        {
            get { return _UidPeriodicidad; }
            set { _UidPeriodicidad = value; }
        }

        private int _IntDiasMes;

        public int IntDiasMes
        {
            get { return _IntDiasMes; }
            set { _IntDiasMes = value; }
        }

        private int _IntDiasSemana;

        public int IntDiasSemana
        {
            get { return _IntDiasSemana; }
            set { _IntDiasSemana = value; }
        }

        public bool GuardarDatos()
        {

            bool Resultado = false;
            SqlCommand Comando = new SqlCommand();

            if (Conexion == null)
                Conexion = new Conexion();

            try
            {
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.CommandText = "usp_PeriodicidadMensual";

                Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
                Comando.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

                Comando.Parameters.Add("@IntDiasMes", SqlDbType.Int);
                Comando.Parameters["@IntDiasMes"].Value = IntDiasMes;

                Comando.Parameters.Add("@IntDiasSemanas", SqlDbType.Int);
                Comando.Parameters["@IntDiasSemanas"].Value = IntDiasSemana;

                Resultado = Conexion.ManipilacionDeDatos(Comando);
                Comando.Dispose();




            }
            catch (Exception)
            {
                throw;
            }
            return Resultado;
        }

        public class Repositorio
        {
            Conexion Conexion = new Conexion();
            public PeriodicidadMensual ConsultarPeriodicidadMensual(Guid Periodicidad)
            {
                PeriodicidadMensual periodicidadmensual = null;

                DataTable table = null;

                SqlCommand comando = new SqlCommand();
                comando.CommandText = "usp_BuscarPeriodicidadMensual";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
                comando.Parameters["@UidPeriodicidad"].Value = Periodicidad;

                table = Conexion.Busquedas(comando);


                foreach (DataRow row in table.Rows)
                {
                    periodicidadmensual = new PeriodicidadMensual()
                    {
                        UidPeriodicidad = (Guid)row["UidPeriodicidad"],
                        IntDiasMes = (int)row["IntDiasMes"],
                        IntDiasSemana = (int)row["IntDiasSemana"],
                    };

                }


                return periodicidadmensual;


            }
        }
    }
}
