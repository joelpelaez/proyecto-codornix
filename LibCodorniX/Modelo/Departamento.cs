﻿
using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CodorniX.Modelo
{
    /// <summary>
    /// Clase que representa los departamentos.
    /// </summary>
    [Serializable]
    public class Departamento
    {

        private bool _blCreadoPorUsuario = false;

        private Guid _UidDepartamento;
        /// <summary>
        /// Identificador único del departamento.
        /// </summary>
        public Guid UidDepartamento
        {
            set { _UidDepartamento = value; }
            get { return _UidDepartamento; }
        }
		public string strIdDepartamento {
			get { return _UidDepartamento.ToString(); }
		}

        private string _StrNombre;
        /// <summary>
        /// Nombre del departamento.
        /// </summary>
        public string StrNombre
        {
            get { return _StrNombre; }
            set { _StrNombre = value; }
        }

        private string _StrDescripcion;
        /// <summary>
        /// Descripción del departamento.
        /// </summary>
        public string StrDescripcion
        {
            get { return _StrDescripcion; }
            set { _StrDescripcion = value; }
        }

        private string _StrURL;
        /// <summary>
        /// Dirección URL de la imagen o ícono del departamento.
        /// </summary>
        public string StrURL
        {
            get { return _StrURL; }
            set { _StrURL = value; }
        }

        private Guid _UidStatus;
        /// <summary>
        /// Identificador único del estado del área.
        /// </summary>
        public Guid UidStatus
        {
            get { return _UidStatus; }
            set { _UidStatus = value; }
        }
		public string strIdStatus {
			get { return _UidStatus.ToString(); }
		}

        private Guid _UidSucursal;
        /// <summary>
        /// Identificador único de la sucursal al cual pertenece.
        /// </summary>
        public Guid UidSucursal
        {
            get { return _UidSucursal; }
            set { _UidSucursal = value; }
        }
		public string strIdSucursal {
			get { return _UidStatus.ToString(); }
		}


        private Departamento(Guid uid, string nombre, string descripcion, string url, Guid status, Guid sucursal)
        {
            _UidDepartamento = uid;
            _StrNombre = nombre;
            _StrDescripcion = descripcion;
            _StrURL = url;
            _UidStatus = status;
            _UidSucursal = sucursal;
            _blCreadoPorUsuario = false;
        }

        /// <summary>
        /// Crea un nuevo objeto <see cref="Departamento"/> sin datos.
        /// </summary>
        public Departamento()
        {
            _blCreadoPorUsuario = true;
        }

        /// <summary>
        /// Clase repositorio de <see cref="Departamento"/>
        /// </summary>
        public class Repository
        {
            private Connection _clsConexion = new Connection();

            private bool InternalSave(Departamento departamento)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_Departamento_Add";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
                command.Parameters["@VchNombre"].Value = departamento._StrNombre;

                command.AddParameter("@VchDescripcion", departamento._StrDescripcion, SqlDbType.NVarChar, 200);

                command.AddParameter("@VchURL", departamento._StrURL, SqlDbType.NVarChar, 255);

                command.AddParameter("@UidStatus", departamento._UidStatus, SqlDbType.UniqueIdentifier);

                command.AddParameter("@UidSucursal", departamento._UidSucursal, SqlDbType.UniqueIdentifier);

                command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
                command.Parameters["@UidDepartamento"].Direction = ParameterDirection.Output;

                bool result = _clsConexion.ExecuteCommand(command, false);

                departamento._UidDepartamento = (Guid)command.Parameters["@UidDepartamento"].Value;

                command.Dispose();

                return result;
            }

            private bool InternalUpdate(Departamento departamento)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_Departamento_Update";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
                command.Parameters["@UidDepartamento"].Value = departamento._UidDepartamento;

                command.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
                command.Parameters["@VchNombre"].Value = departamento._StrNombre;

                command.AddParameter("@VchDescripcion", departamento._StrDescripcion, SqlDbType.NVarChar, 200);

                command.AddParameter("@VchURL", departamento._StrURL, SqlDbType.NVarChar, 255);

                command.AddParameter("@UidStatus", departamento._UidStatus, SqlDbType.UniqueIdentifier);

                command.AddParameter("@UidSucursal", departamento._UidSucursal, SqlDbType.UniqueIdentifier);

                return _clsConexion.ExecuteCommand(command);
            }

            /// <summary>
            /// Guarda un nuevo departamento.
            /// </summary>
            /// <param name="departamento">objeto departamento.</param>
            /// <returns>true si fue agregado correctamente, false si no.</returns>
            public bool Guardar(Departamento departamento)
            {
                bool result = false;

                try
                {
                    if (departamento._blCreadoPorUsuario)
                        result = InternalSave(departamento);
                    else if (!departamento._blCreadoPorUsuario && !(departamento._UidDepartamento == null))
                        result = InternalUpdate(departamento);
                    else
                        throw new DatabaseException("Invalid User object");

                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error saving User object", e);
                }

                return result;
            }

            /// <summary>
            /// Obtiene un departamento a partir de su identificador.
            /// </summary>
            /// <param name="uid">Identificador único del departamento.</param>
            /// <returns>objeto departamento</returns>
            public Departamento Encontrar(Guid uid)
            {
                Departamento departamento = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Departamento_Find";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
                    command.Parameters["@UidDepartamento"].Value = uid;

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                foreach (DataRow row in table.Rows)
                {
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid sucursal = (Guid)row["UidSucursal"];
                    departamento = new Departamento(uid, nombre, descripcion, url, status, sucursal);
                }

                return departamento;
            }

            /// <summary>
            /// Obtiene una 
            /// </summary>
            /// <param name="uiddepartamento"></param>
            /// <returns></returns>
            public List<Departamento> Encontrardepartamento(Guid uiddepartamento)
            {
                List<Departamento> departamentos = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Departamento_Find";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
                    command.Parameters["@UidDepartamento"].Value = uiddepartamento;

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                departamentos = new List<Departamento>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidDepartamento"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid sucursal = (Guid)row["UidSucursal"];
                    Departamento departamento = new Departamento(uid, nombre, descripcion, url, status, sucursal);
                    departamentos.Add(departamento);
                }

                return departamentos;
            }

            /// <summary>
            /// no used
            /// </summary>
            /// <param name="uidSucursal"></param>
            /// <returns></returns>
            public List<Departamento> EncontrarTodos(Guid uidSucursal)
            {
                List<Departamento> departamentos = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Departamento_Search";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                departamentos = new List<Departamento>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidDepartamento"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid sucursal = (Guid)row["UidSucursal"];
                    Departamento departamento = new Departamento(uid, nombre, descripcion, url, status, sucursal);
                    departamentos.Add(departamento);
                }

                return departamentos;
            }

            public List<Departamento> EncontrarPorListaDePeriodos(string uidPeriodos)
            {
                List<Departamento> departamentos = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Departamento_FindByList";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidPeriodos", uidPeriodos, SqlDbType.NVarChar, 4000);

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                departamentos = new List<Departamento>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidDepartamento"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid sucursal = (Guid)row["UidSucursal"];
                    Departamento departamento = new Departamento(uid, nombre, descripcion, url, status, sucursal);
                    departamentos.Add(departamento);
                }

                return departamentos;
            }

            public List<Departamento> EncontrarPorLista(string uidDepartamentos)
            {
                List<Departamento> departamentos = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Departamento_FindByListReal";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidDepartamentos", uidDepartamentos, SqlDbType.NVarChar, 4000);

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                departamentos = new List<Departamento>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidDepartamento"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid sucursal = (Guid)row["UidSucursal"];
                    Departamento departamento = new Departamento(uid, nombre, descripcion, url, status, sucursal);
                    departamentos.Add(departamento);
                }

                return departamentos;
            }

            /// <summary>
            /// Obtiene los departamentos por un criterio de búsqueda.
            /// </summary>
            /// <param name="criteria">Criterio de búsqueda</param>
            /// <returns>Lista de departamentos, puede estar vacía.</returns>
            public List<Departamento> EncontrarPor(Criterio criteria)
            {
                List<Departamento> departamentos = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Departamento_Search";
                    command.CommandType = CommandType.StoredProcedure;

                    InjectParameters(command, criteria);

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                departamentos = new List<Departamento>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidDepartamento"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid sucursal = (Guid)row["UidSucursal"];
                    Departamento departamento = new Departamento(uid, nombre, descripcion, url, status, sucursal);
                    departamentos.Add(departamento);
                }

                return departamentos;
            }

            /// <summary>
            /// no used
            /// </summary>
            /// <param name="Nombre"></param>
            /// <param name="Sucursal"></param>
            /// <returns></returns>
            [Obsolete]
            public List<Departamento> BuscarDepartamento(string Nombre, string Sucursal)
            {
                List<Departamento> departamentos = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_BuscarDepartamento";
                    command.CommandType = CommandType.StoredProcedure;

                    if (Nombre != string.Empty)
                    {
                        command.Parameters.Add("@VchNombre", SqlDbType.NVarChar);
                        command.Parameters["@VchNombre"].Value = Nombre;
                    }

                    if (Sucursal != string.Empty)
                    {
                        command.Parameters.Add("@VchSucursal", SqlDbType.NVarChar);
                        command.Parameters["@VchSucursal"].Value = Sucursal;
                    }
                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                 throw new DatabaseException("Error populating table Use", e);
                }

                departamentos = new List<Departamento>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidDepartamento"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid sucursal = (Guid)row["UidSucursal"];
                    Departamento departamento = new Departamento(uid, nombre, descripcion, url, status, sucursal);
                    departamentos.Add(departamento);
                }

                return departamentos;
            }

            private void InjectParameters(SqlCommand command, Criterio criteria)
            {
                if (!string.IsNullOrWhiteSpace(criteria.Nombre))
                {
                    command.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
                    command.Parameters["@VchNombre"].Value = criteria.Nombre;
                }
                if (!string.IsNullOrWhiteSpace(criteria.Descripcion))
                {
                    command.AddParameter("@VchDescripcion", criteria.Descripcion, SqlDbType.NVarChar, 200);
                }
                if (criteria.Sucursal != Guid.Empty)
                {
                    command.AddParameter("@UidSucursal", criteria.Sucursal, SqlDbType.UniqueIdentifier);
                }
            }
        }

        /// <summary>
        /// Clase criterio de <see cref="Departamento"/>.
        /// </summary>
        public class Criterio
        {
            /// <summary>
            /// Nombre del departamento.
            /// </summary>
            public string Nombre { get; set; }

            /// <summary>
            /// Descripción del departamento.
            /// </summary>
            public string Descripcion { get; set; }

            /// <summary>
            /// Identificador único de la sucursal.
            /// </summary>
            public Guid Sucursal { get; set; }
        }
    }
}

