﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using CodorniX.Util;

namespace CodorniX.Modelo
{
    [Serializable]
    public class Periodicidad
    {
        [NonSerialized]
        Conexion conexion = new Conexion();
        private Guid _UidPeriodicidad;
        public Guid UidPeriodicidad
        {
            get { return _UidPeriodicidad; }
            set { _UidPeriodicidad = value; }
        }
		public string strIdPeriodicidad { get { return _UidPeriodicidad.ToString(); } }

        private int _IntFrecuencia;
        public int IntFrecuencia
        {
            get { return _IntFrecuencia; }
            set { _IntFrecuencia = value; }
        }

        private Guid _UidTipoFrecuencia;
        public Guid UidTipoFrecuencia
        {
            get { return _UidTipoFrecuencia; }
            set { _UidTipoFrecuencia = value; }
        }
		public string strIdTipoFrecuencia { get { return _UidTipoFrecuencia.ToString(); } }

        private DateTime _DtFechaInicio;
        public DateTime DtFechaInicio
        {
            get { return _DtFechaInicio; }
            set { _DtFechaInicio = value; }
        }
		public string strDtFechaInicio { get { return _DtFechaInicio.ToString(); } }

        private DateTime? _DtFechaFin;
        public DateTime? DtFechaFin
        {
            get { return _DtFechaFin; }
            set { _DtFechaFin = value; }
        }
		public string strDtFechaFin { get { return _DtFechaFin.ToString(); } }

		public bool GuardarDatos()
        {

            bool Resultado = false;
            SqlCommand Comando = new SqlCommand();

            if (conexion == null)
                conexion = new Conexion();

            try
            {
                Comando.CommandType = CommandType.StoredProcedure;

                Comando.CommandText = "usp_Periodicidad_Add";

                Comando.Parameters.Add("@IntFrecuencia", SqlDbType.Int);
                Comando.Parameters["@IntFrecuencia"].Value = IntFrecuencia;

                Comando.Parameters.Add("@UidTipoFrecuencia", SqlDbType.UniqueIdentifier);
                Comando.Parameters["@UidTipoFrecuencia"].Value = UidTipoFrecuencia;

                Comando.Parameters.Add("@DtFechaInicio", SqlDbType.Date);
                Comando.Parameters["@DtFechaInicio"].Value = DtFechaInicio;

                Comando.Parameters.Add("@DtFechaFin", SqlDbType.Date);
                Comando.Parameters["@DtFechaFin"].Value = DtFechaFin;

                Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
                Comando.Parameters["@UidPeriodicidad"].Direction = ParameterDirection.Output;

                Resultado = conexion.ManipilacionDeDatos(Comando);
                _UidPeriodicidad = (Guid)Comando.Parameters["@UidPeriodicidad"].Value;
                Comando.Dispose();




            }
            catch (Exception)
            {

                throw;
            }
            return Resultado;
        }
        public bool ModificarDatos()
        {

            bool Resultado = false;
            SqlCommand Comando = new SqlCommand();

            Comando.CommandType = CommandType.StoredProcedure;
            Comando.CommandText = "usp_ModificarPeriodicidad";

            Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
            Comando.Parameters["@UidPeriodicidad"].Value = _UidPeriodicidad;
            

            Comando.Parameters.Add("@DtFechaFin", SqlDbType.DateTime);
            Comando.Parameters["@DtFechaFin"].Value = DtFechaFin;


            Resultado = conexion.ManipilacionDeDatos(Comando);

            return Resultado;
        }
        public class Repositorio
        {
            Conexion Conexion = new Conexion();
            public Periodicidad ConsultarPeriodicidad(Guid Periodicidad)
            {
                Periodicidad periodicidad = null;

                DataTable table = null;

                SqlCommand comando = new SqlCommand();
                comando.CommandText = "usp_BuscarPeriodicidad";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
                comando.Parameters["@UidPeriodicidad"].Value = Periodicidad;

                table = Conexion.Busquedas(comando);

               
                    foreach (DataRow row in table.Rows)
                    {
                        periodicidad = new Periodicidad()
                        {
                            UidPeriodicidad = (Guid)row["UidPeriodicidad"],
                            IntFrecuencia = (int)row["IntFrecuencia"],
                            UidTipoFrecuencia = (Guid)row["UidTipoFrecuencia"],
                            DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString()),
                            DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaFin"].ToString()),
                        };
                        
                    }
                

                return periodicidad;


            }
        }
    }
}
