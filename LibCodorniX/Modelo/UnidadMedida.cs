﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CodorniX.Modelo
{
    [Serializable]
    public class UnidadMedida
    {
        private Guid _UidUnidadMedida;

        public Guid UidUnidadMedida
        {
            get { return _UidUnidadMedida; }
            set { _UidUnidadMedida = value; }
        }

        private string _StrTipoUnidad;

        public string StrTipoUnidad
        {
            get { return _StrTipoUnidad; }
            set { _StrTipoUnidad = value; }
        }

        public class Repositorio
        {
            public List<UnidadMedida> ConsultarUnidadMedida()
            {
                List<UnidadMedida> unidades = new List<UnidadMedida>();

                SqlCommand comando = new SqlCommand();

                try
                {
                    comando.CommandText = "usp_ConsultarUnidadMedida";
                    comando.CommandType = CommandType.StoredProcedure;

                    DataTable table = new Connection().ExecuteQuery(comando);

                    foreach (DataRow row in table.Rows)
                    {
                        UnidadMedida unidad = new UnidadMedida()
                        {
                            UidUnidadMedida = (Guid)row["UidUnidadMedida"],
                            StrTipoUnidad = (string)row["VchTipoUnidad"],
                        };
                        unidades.Add(unidad);
                    }
                }
                catch (SqlException e)
                {
                    throw;
                }

                return unidades;


            }
        }
    }
}
