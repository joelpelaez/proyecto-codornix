﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
   public class TipoTarea
    {
        private Guid _UidTipoTarea;

        public Guid UidTipoTarea
        {
            get { return _UidTipoTarea; }
            set { _UidTipoTarea = value; }
        }

        private string _StrTipoTarea;

        public string StrTipoTarea
        {
            get { return _StrTipoTarea; }
            set { _StrTipoTarea = value; }
        }

        public class Repositorio
        {
            public List<TipoTarea> ConsultarTipoTarea()
            {
                List<TipoTarea> TiposTareas = new List<TipoTarea>();

                SqlCommand comando = new SqlCommand();

                try
                {
                    comando.CommandText = "usp_ConsultarTipoTarea";
                    comando.CommandType = CommandType.StoredProcedure;

                    DataTable table = new Connection().ExecuteQuery(comando);

                    foreach (DataRow row in table.Rows)
                    {
                        TipoTarea tipotarea = new TipoTarea()
                        {
                            UidTipoTarea = (Guid)row["UidTipoTarea"],
                            StrTipoTarea = (string)row["VchTipoTarea"],
                        };
                        TiposTareas.Add(tipotarea);
                    }
                }
                catch (SqlException e)
                {
                    throw;
                }

                return TiposTareas;


            }
        }
    }
}
