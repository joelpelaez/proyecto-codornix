﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo.Entity
{
    public class TareaListado
    {
        private Guid _UidTarea;

        public Guid UidTarea
        {
            get { return _UidTarea; }
            set { _UidTarea = value; }
        }

        private string _StrNombre;

        public string StrNombre
        {
            get { return _StrNombre; }
            set { _StrNombre = value; }
        }

        private string _StrDepartamento;

        public string StrDepartamento
        {
            get { return _StrDepartamento; }
            set { _StrDepartamento = value; }
        }

        private string _StrArea;

        public string StrArea
        {
            get { return _StrArea; }
            set { _StrArea = value; }
        }

        private string _StrTipo;

        public string StrTipo
        {
            get { return _StrTipo; }
            set { _StrTipo = value; }
        }

        private string _StrUnidadMedida;

        public string StrUnidadMedida
        {
            get { return _StrUnidadMedida; }
            set { _StrUnidadMedida = value; }
        }

    }
}
