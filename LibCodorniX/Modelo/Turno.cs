﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CodorniX.Modelo
{
	[Serializable]
	public class Turno
	{
		private Guid _UidTurno;
		public Guid UidTurno
		{
			get { return _UidTurno; }
		}
		public string strIdTurno { get { return _UidTurno.ToString(); } }

		private string _StrTurno;
		public string StrTurno
		{
			get { return _StrTurno; }
			set { _StrTurno = value; }
		}

		public class Repository
		{
			Connection conn = new Connection();

			public List<Turno> FindAll()
			{
				List<Turno> turnos = new List<Turno>();

				SqlCommand command = new SqlCommand();
				try
				{
					command.CommandText = "usp_Turno_FindAll";
					command.CommandType = CommandType.StoredProcedure;

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Turno turno = new Turno()
						{
							_UidTurno = (Guid)row["UidTurno"],
							_StrTurno = row["VchTurno"].ToString(),
						};
						turnos.Add(turno);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Cannot get Turnos", e);
				}

				return turnos;
			}

			public Turno Find(Guid uid)
			{
				Turno turno = null;

				SqlCommand command = new SqlCommand();
				try
				{
					command.CommandText = "usp_Turno_Find";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidTurno", uid, SqlDbType.UniqueIdentifier);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						turno = new Turno()
						{
							_UidTurno = (Guid)row["UidTurno"],
							_StrTurno = row["VchTurno"].ToString(),
						};
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Cannot get Turnos", e);
				}

				return turno;
			}
		}
	}
}