﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	public class ServerMessage
	{
		private string strValue;

		public string Value
		{
			get { return strValue; }
			set { strValue = value; }
		}

	}
}
