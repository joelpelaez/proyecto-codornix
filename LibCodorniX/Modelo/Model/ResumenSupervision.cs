﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo.Model
{
    public class ResumenSupervision
    {
        public int IntFolio { get; set; }
        public int NumTareasCumplidas { get; set; }
        public int NumTareasNoCumplidas { get; set; }
        public int NumTareasRevisadas { get; set; }
        public int NumTareasNoRevisadas { get; set; }
        public string Departamento { get; set; }
        public string Turno { get; set; }
        public DateTimeOffset? DtInicioTurno { get; set; }
        public DateTimeOffset? DtFinTurno { get; set; }
        public string EstadoTurno { get; set; }

        public string FechaInicioTurno
        {
            get => DtInicioTurno.HasValue ? DtInicioTurno?.ToString("dd/MM/yyyy HH:mm") : "(no iniciado)";
        }

        public string FechaFinTurno
        {
            get => DtFinTurno.HasValue ? DtFinTurno?.ToString("dd/MM/yyyy HH:mm") : "(no terminado)";
        }
    }
}
