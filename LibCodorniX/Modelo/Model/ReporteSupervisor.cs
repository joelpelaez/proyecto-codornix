﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo.Model
{
    public class ReporteSupervisor
    {
        public int FolioCumplimiento { get; set; }
        public int FolioTarea { get; set; }
        public Guid UidCumplimiento { get; set; }
        public Guid UidTarea { get; set; }
        public Guid? UidArea { get; set; }
        public Guid UidDepartamento { get; set; }
        public string Area { get; set; }
        public string Departamento { get; set; }
        public string Tarea { get; set; }
        public DateTimeOffset FechaHora { get; set; }
        public string TipoCumplimiento { get; set; }
        public string TipoMedicion { get; set; }
        public string Valor { get; set; }
        public string Correccion { get; set; }
        public string Observaciones { get; set; }
        public string TipoTarea { get; set; }
        public string Usuario { get; set; }

        public string Fecha
        {
            get => FechaHora.ToString("dd/MM/yyyy HH:mm");
        }
    }
}
