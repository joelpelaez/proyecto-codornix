﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    public class EstadoCumplimiento
    {
        private Guid _UidEstadoCumplimiento;
        public Guid UidEstadoCumplimiento
        {
            get { return _UidEstadoCumplimiento; }
            set { _UidEstadoCumplimiento = value; }
        }
		public string strIdEstadoCumplimiento { get { return _UidEstadoCumplimiento.ToString(); } }

        private string _StrEstadoCumplimiento;
        public string StrEstadoCumplimiento
        {
            get { return _StrEstadoCumplimiento; }
            set { _StrEstadoCumplimiento = value; }
        }

        public class Repository
        {
            Connection conn = new Connection();

            public List<EstadoCumplimiento> FindAll()
            {
                List<EstadoCumplimiento> estados = new List<EstadoCumplimiento>();
                EstadoCumplimiento estado = null;

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_EstadoCumplimiento_FindAll";
                command.CommandType = CommandType.StoredProcedure;

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    estado = new EstadoCumplimiento();
                    estado._UidEstadoCumplimiento = new Guid(row["UidEstadoCumplimiento"].ToString());
                    estado._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
                    estados.Add(estado);
                }

                return estados;
            }
        }
    }
}
