﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CodorniX.Util;

namespace CodorniX.Modelo
{
	/// <summary>
	/// Asignación de supervision y funciones de soporte
	/// </summary>
	[Serializable]
	public class AsignacionSupervision
	{
		/// <summary>
		/// Codigo de error de los procesos de asignación.
		/// </summary>
		public enum ReturnCode
		{
			Success = 0,
			SuccessWithChanges = 1,
			SuccessUpdate = 2,
			FailureSame = -1,
			FailureMax = -2,
			Undefined = -3
		}

		private DateTime? _DtFechaFin;
		private DateTime _DtFechaInicio;

		private string _StrDepto;
		// EXTRA
		private string _StrNombreUsuario;

		private string _StrTurno;
		private Guid _UidAsignacion;

		private Guid _UidDepartamento;

		private Guid _UidTurno;

		private Guid _UidUsuario;

		public Guid UidAsignacion
		{
			get => _UidAsignacion;
			set => _UidAsignacion = value;
		}
		public string strIdAsignacion { get { return _UidAsignacion.ToString(); } }

		public DateTime DtFechaInicio
		{
			get => _DtFechaInicio;
			set => _DtFechaInicio = value;
		}
		public string strDtFechaInicio { get { return _DtFechaInicio.ToString("dd/MM/yyyy"); } }

		public DateTime? DtFechaFin
		{
			get => _DtFechaFin;
			set => _DtFechaFin = value;
		}
		public string strDtFechaFin { get { return _DtFechaFin == null ? "empty" : _DtFechaFin?.ToString("dd/MM/yyyy"); } }

		public Guid UidDepartamento
		{
			get => _UidDepartamento;
			set => _UidDepartamento = value;
		}
		public string strIdDepartamento { get { return _UidDepartamento.ToString(); } }

		public Guid UidUsuario
		{
			get => _UidUsuario;
			set => _UidUsuario = value;
		}
		public string strIdUsuario { get { return _UidUsuario.ToString(); } }

		public Guid UidTurno
		{
			get => _UidTurno;
			set => _UidTurno = value;
		}
		public string strIdTurno { get { return _UidTurno.ToString(); } }

		public string StrNombreUsuario
		{
			get => _StrNombreUsuario;
			set => _StrNombreUsuario = value;
		}

		public string StrNombreDepto
		{
			get => _StrDepto;
			set => _StrDepto = value;
		}

		public string StrTurno
		{
			get => _StrTurno;
			set => _StrTurno = value;
		}

		public class Repository
		{
			private readonly Connection conn = new Connection();

			public static List<Guid> ObtenerDepartamentosSupervisados(Guid uidUsuario, DateTime fecha)
			{
				var guids = new List<Guid>();
				var conn = new Connection();

				var command = new SqlCommand();
				command.CommandText = "usp_Supervisor_ListaDepartamentos";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
				command.AddParameter("@DtFecha", fecha, SqlDbType.Date);

				var table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows) guids.Add(new Guid(row["UidDepartamento"].ToString()));

				return guids;
			}

			public ReturnCode Save(AsignacionSupervision AsignacionSupervision)
			{
				var code = ReturnCode.Undefined;
				var command = new SqlCommand();
				try
				{
					command.CommandText = "usp_AsignacionSupervision_Add";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@DtFechaInicio", AsignacionSupervision._DtFechaInicio, SqlDbType.DateTime);
					command.AddParameter("@DtFechaFin", AsignacionSupervision._DtFechaFin, SqlDbType.DateTime);
					command.AddParameter("@UidDepartamento", AsignacionSupervision._UidDepartamento,
						SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidUsuario", AsignacionSupervision._UidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidTurno", AsignacionSupervision._UidTurno, SqlDbType.UniqueIdentifier);

					command.Parameters.Add("@UidAsignacion", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidAsignacion"].Direction = ParameterDirection.Output;

					command.Parameters.Add("@IntResult", SqlDbType.Int);
					command.Parameters["@IntResult"].Direction = ParameterDirection.Output;

					conn.ExecuteCommand(command, false);

					AsignacionSupervision._UidAsignacion = (Guid)command.Parameters["@UidAsignacion"].Value;
					var tmpcode = (int)command.Parameters["@IntResult"].Value;
					code = (ReturnCode)tmpcode;

					command.Dispose();
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error receiving table AsignacionSupervision", e);
				}

				return code;
			}

			public AsignacionSupervision Find(Guid uid)
			{
				AsignacionSupervision AsignacionSupervision = null;

				var command = new SqlCommand();

				try
				{
					command.CommandText = "usp_AsignacionSupervision_Find";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidAsignacion", uid, SqlDbType.UniqueIdentifier);

					var table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
						AsignacionSupervision = new AsignacionSupervision
						{
							_UidAsignacion = (Guid)row["UidAsignacion"],
							_DtFechaInicio = (DateTime)row["DtFechaInicio"],
							_DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
							_UidDepartamento = (Guid)row["UidDepartamento"],
							_UidUsuario = (Guid)row["UidUsuario"],
							_UidTurno = (Guid)row["UidTurno"]
						};
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching data from AsignacionSupervision", e);
				}

				return AsignacionSupervision;
			}

			public AsignacionSupervision Last(Guid departamento, Guid turno)
			{
				AsignacionSupervision AsignacionSupervision = null;

				var command = new SqlCommand();

				try
				{
					command.CommandText = "usp_AsignacionSupervision_Last";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidDepartamento", departamento, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidTurno", turno, SqlDbType.UniqueIdentifier);

					var table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
						AsignacionSupervision = new AsignacionSupervision
						{
							_UidAsignacion = (Guid)row["UidAsignacion"],
							_DtFechaInicio = (DateTime)row["DtFechaInicio"],
							_DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
							_UidDepartamento = (Guid)row["UidDepartamento"],
							_UidUsuario = (Guid)row["UidUsuario"],
							_UidTurno = (Guid)row["UidTurno"]
						};
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching data from AsignacionSupervision", e);
				}

				return AsignacionSupervision;
			}

			public List<AsignacionSupervision> FindAll()
			{
				var periodos = new List<AsignacionSupervision>();
				AsignacionSupervision AsignacionSupervision = null;

				var command = new SqlCommand();

				try
				{
					command.CommandText = "usp_AsignacionSupervision_FindAll";
					command.CommandType = CommandType.StoredProcedure;

					var table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						AsignacionSupervision = new AsignacionSupervision
						{
							_UidAsignacion = (Guid)row["UidAsignacion"],
							_DtFechaInicio = (DateTime)row["DtFechaInicio"],
							_DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : (DateTime)row["DtFechaFin"],
							_UidDepartamento = (Guid)row["UidDepartamento"],
							_UidUsuario = (Guid)row["UidUsuario"],
							_UidTurno = (Guid)row["UidTurno"]
						};

						periodos.Add(AsignacionSupervision);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching data from AsignacionSupervision", e);
				}

				return periodos;
			}

			public List<AsignacionSupervision> FindBy(Criteria criteria, bool fetchExtra = false)
			{
				var periodos = new List<AsignacionSupervision>();
				AsignacionSupervision AsignacionSupervision = null;

				var command = new SqlCommand();

				Usuario.Repository usuarioRepository = null;
				Departamento.Repository departamentoRepository = null;
				Turno.Repository turnoRepository = null;

				if (fetchExtra)
				{
					usuarioRepository = new Usuario.Repository();
					departamentoRepository = new Departamento.Repository();
					turnoRepository = new Turno.Repository();
				}

				try
				{
					command.CommandText = "usp_AsignacionSupervision_Search";
					command.CommandType = CommandType.StoredProcedure;

					InjectCriteria(command, criteria);

					var table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						AsignacionSupervision = new AsignacionSupervision
						{
							_UidAsignacion = (Guid)row["UidAsignacion"],
							_DtFechaInicio = DateTime.Parse(row["DtFechaInicio"].ToString()),
							_DtFechaFin =
								row.IsNull("DtFechaFin")
									? (DateTime?)null
									: DateTime.Parse(row["DtFechaFin"].ToString()),
							_UidDepartamento = (Guid)row["UidDepartamento"],
							_UidUsuario = (Guid)row["UidUsuario"],
							_UidTurno = (Guid)row["UidTurno"]
						};

						if (fetchExtra)
						{
							var usuario = usuarioRepository.Find(AsignacionSupervision._UidUsuario);
							AsignacionSupervision._StrNombreUsuario =
								usuario.STRNOMBRE + " " + usuario.STRAPELLIDOPATERNO;

							var turno = turnoRepository.Find(AsignacionSupervision._UidTurno);
							AsignacionSupervision._StrTurno = turno.StrTurno;

							var departamento = departamentoRepository.Encontrar(AsignacionSupervision._UidDepartamento);
							AsignacionSupervision._StrDepto = departamento.StrNombre;
						}

						periodos.Add(AsignacionSupervision);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching data from AsignacionSupervision", e);
				}

				return periodos;
			}

			private void InjectCriteria(SqlCommand command, Criteria criteria)
			{
				if (criteria.FechaInicioDespuesDe.HasValue)
					command.AddParameter("@DtFechaInicioDespuesDe", criteria.FechaInicioDespuesDe.Value,
						SqlDbType.DateTime);
				if (criteria.FechaInicioAntesDe.HasValue)
					command.AddParameter("@DtFechaInicioAntesDe", criteria.FechaInicioAntesDe.Value,
						SqlDbType.DateTime);
				if (criteria.FechaFinDespuesDe.HasValue)
					command.AddParameter("@DtFechaFinDespuesDe", criteria.FechaFinDespuesDe.Value, SqlDbType.DateTime);
				if (criteria.FechaFinAntesDe.HasValue)
					command.AddParameter("@DtFechaFinAntesDe", criteria.FechaFinAntesDe.Value, SqlDbType.DateTime);
				if (criteria.Usuario.HasValue)
					command.AddParameter("@UidUsuario", criteria.Usuario.Value, SqlDbType.UniqueIdentifier);
				if (!string.IsNullOrWhiteSpace(criteria.Usuarios))
					command.AddParameter("@VchUsuario", criteria.Usuarios, SqlDbType.NVarChar, 4000);
				if (!string.IsNullOrWhiteSpace(criteria.Turnos))
					command.AddParameter("@UidTurno", criteria.Turnos, SqlDbType.NVarChar, 4000);
				if (!string.IsNullOrWhiteSpace(criteria.Departamentos))
					command.AddParameter("@UidDepartamento", criteria.Departamentos, SqlDbType.NVarChar, 4000);
				if (criteria.Sucursal != Guid.Empty)
					command.AddParameter("@UidSucursal", criteria.Sucursal, SqlDbType.UniqueIdentifier);
			}
		}

		public class Criteria
		{
			public DateTime? FechaInicioDespuesDe { get; set; }
			public DateTime? FechaInicioAntesDe { get; set; }
			public DateTime? FechaFinDespuesDe { get; set; }
			public DateTime? FechaFinAntesDe { get; set; }
			public Guid? Usuario { get; set; }
			public string Usuarios { get; set; }
			public string Turnos { get; set; }
			public string Departamentos { get; set; }
			public Guid Sucursal { get; set; }
		}
	}
}