﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.Util;

namespace CodorniX.Modelo
{
    /// <summary>
    /// Tabla de union de <see cref="Area"/> y <see cref="Tarea"/>
    /// </summary>
    [Serializable]
    public class AreaTarea : Area
    {
        private Guid _UidTarea;

        /// <summary>
        /// Identificador único de la tarea.
        /// </summary>
        public Guid UidTarea
        {
            get { return _UidTarea; }
            set { _UidTarea = value; }
        }

        private Guid _UidArea;

        /// <summary>
        /// Identificador único del área,
        /// </summary>
        new public Guid UidArea
        {
            get { return _UidArea; }
            set { _UidArea = value; }
        }

        /// <summary>
        /// Clase repositorio de las áreas.
        /// </summary>
        public class Repositorio : Area.Repository
        {
            /// <summary>
            /// Obtiene todas las áreas correspondientes a una tarea.
            /// </summary>
            /// <param name="uid">identificador único de la tarea.</param>
            /// <returns>Lista de areas.</returns>
            new public List<Area> FindAll(Guid uid)
            {
                DataTable table = new DataTable();
                List<Area> areas = new List<Area>();
                Area area = null;
                try
                {
                    SqlCommand comando = new SqlCommand();
                    comando.CommandText = "usp_ConsultarAreaTarea";
                    comando.CommandType = CommandType.StoredProcedure;

                    comando.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
                    comando.Parameters["@UidTarea"].Value = uid;

                    table = _clsConexion.ExecuteQuery(comando);

                    foreach (DataRow row in table.Rows)
                    {
                        area = new Area()
                        {
                            UidArea = new Guid(row["UidArea"].ToString()),
                            UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
                            StrNombre = row["VchNombre"].ToString(),
                            StrDescripcion = row["VchDescripcion"].ToString(),
                            StrURL = row["VchURL"].ToString(),
                            UidStatus = (Guid)row["UidStatus"]
                        };

                        areas.Add(area);
                    }
                }
                catch (Exception e)
                {
                    throw new DatabaseException("Error populating", e);
                }

                return areas;
            }

            /// <summary>
            /// Guarda una relación Tarea-Area.
            /// </summary>
            /// <param name="areatarea">objeto relación</param>
            /// <returns>true si se guardo correctamente, false en caso contrario.</returns>
            public bool Save(AreaTarea areatarea)
            {
                return InternalSave(areatarea);

            }
            private bool InternalSave(AreaTarea areatarea)
            {
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_TareaArea_Add";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidArea", areatarea.UidArea, SqlDbType.UniqueIdentifier);

                    command.AddParameter("@UidTarea", areatarea._UidTarea, SqlDbType.UniqueIdentifier);


                    return _clsConexion.ExecuteCommand(command);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}
