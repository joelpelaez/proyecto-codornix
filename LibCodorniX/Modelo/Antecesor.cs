﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    public class Antecesor
    {
        private Guid _UidAntecesor;

        public Guid UidAntecesor
        {
            get { return _UidAntecesor; }
            set { _UidAntecesor = value; }
        }

        private Guid _UidTarea;

        public Guid UidTarea
        {
            get { return _UidTarea; }
            set { _UidTarea = value; }
        }

        private Guid _UidTareaAnterior;

        public Guid UidTareaAnterior
        {
            get { return _UidTareaAnterior; }
            set { _UidTareaAnterior = value; }
        }

        private int? _IntRepeticion;

        public int? IntRepeticion
        {
            get { return _IntRepeticion; }
            set { _IntRepeticion = value; }
        }

        private int _IntDiasDespues;

        public int IntDiasDespues
        {
            get { return _IntDiasDespues; }
            set { _IntDiasDespues = value; }
        }

        private bool? _BlUsarNotificacion;

        public bool? BlUsarNotificacion
        {
            get { return _BlUsarNotificacion; }
            set { _BlUsarNotificacion = value; }
        }

        public class Repository
        {
            Connection conn = new Connection();

            public Antecesor Get(Guid uidTarea)
            {
                Antecesor antecesor = null;
                SqlCommand command = new SqlCommand();

                command.CommandText = "usp_Antecesor_Get";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTarea", uidTarea, SqlDbType.UniqueIdentifier);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    antecesor = new Antecesor();
                    antecesor._UidAntecesor = new Guid(row["UidAntecesor"].ToString());
                    antecesor._UidTarea = new Guid(row["UidTarea"].ToString());
                    antecesor._UidTareaAnterior = new Guid(row["UidTareaAnterior"].ToString());
                    antecesor._IntRepeticion = row.IsNull("IntRepeticion") ? (int?)null : (int)row["IntRepeticion"];
                    antecesor._BlUsarNotificacion = row.IsNull("BitUsarNotificacion") ? (bool?)null : Convert.ToBoolean(row["BitUsarNotificacion"].ToString());
                    antecesor._IntDiasDespues = (int)row["IntDiasDespues"];
                }

                return antecesor;
            }

            public void Set(Antecesor antecesor)
            {
                SqlCommand command = new SqlCommand();

                command.CommandText = "usp_Antecesor_Set";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidTarea", antecesor.UidTarea, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidTareaAnterior", antecesor.UidTareaAnterior, SqlDbType.UniqueIdentifier);
                command.AddParameter("@IntDiasDespues", antecesor._IntDiasDespues, SqlDbType.Int);
                if (antecesor._IntRepeticion.HasValue)
                    command.AddParameter("@IntRepeticion", antecesor._IntRepeticion.Value, SqlDbType.Int);
                if (antecesor._BlUsarNotificacion.HasValue)
                    command.AddParameter("@BitUsarNotificacion", antecesor._BlUsarNotificacion.Value, SqlDbType.Bit);

                conn.ExecuteCommand(command);
            }
        }
    }
}
