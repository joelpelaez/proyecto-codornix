﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    public class EstadoNotificacion
    {
        private Guid _UidEstadoNotificacion;

        public Guid UidEstadoNotitifacion
        {
            get { return _UidEstadoNotificacion; }
            set { _UidEstadoNotificacion = value; }
        }

        private string _StrEstadoNotificacion;

        public string StrEstadoNotificacion
        {
            get { return _StrEstadoNotificacion; }
            set { _StrEstadoNotificacion = value; }
        }

        public class Repository
        {
            Connection conn = new Connection();

            public List<EstadoNotificacion> FindAll()
            {
                List<EstadoNotificacion> estados = new List<EstadoNotificacion>();
                EstadoNotificacion estado = null;

                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_EstadoNotificacion_FindAll";
                command.CommandType = CommandType.StoredProcedure;

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    estado = new EstadoNotificacion();
                    estado._UidEstadoNotificacion = new Guid(row["UidEstadoNotificacion"].ToString());
                    estado._StrEstadoNotificacion = row["VchEstadoNotificacion"].ToString();
                    estados.Add(estado);
                }

                return estados;
            }
        }
    }
}
