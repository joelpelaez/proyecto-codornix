﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
    public class Ordinal
    {
        private Guid _UidOrdinal;

        public Guid UidOrdinal
        {
            get { return _UidOrdinal; }
            set { _UidOrdinal = value; }
        }

        private string _StrOrdinal;

        public string StrOrdinal
        {
            get { return _StrOrdinal; }
            set { _StrOrdinal = value; }
        }

        public class Repositorio
        {
            Conexion Conexion = new Conexion();

            public List<Ordinal> ConsultarOrdinal()
            {
                List<Ordinal> ordinales = new List<Ordinal>();

                SqlCommand comando = new SqlCommand();

                try
                {
                    comando.CommandText = "usp_ConsultarOrdinal";
                    comando.CommandType = CommandType.StoredProcedure;

                    DataTable table = new Connection().ExecuteQuery(comando);

                    foreach (DataRow row in table.Rows)
                    {
                        Ordinal ordinal = new Ordinal()
                        {
                            UidOrdinal = (Guid)row["UidOrdinal"],
                            StrOrdinal = (string)row["VchOrdinal"],
                        };
                        ordinales.Add(ordinal);
                    }
                }
                catch (SqlException e)
                {
                    throw;
                }

                return ordinales;


            }

            
            public Ordinal ObtenerOrdinal(string ordinal)
            {
                Ordinal Ordinal = null;

                DataTable table = null;

                SqlCommand comando = new SqlCommand();
                comando.CommandText = "usp_BuscarOrdinal";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add("@VchOrdinal", SqlDbType.NVarChar, 50);
                comando.Parameters["@VchOrdinal"].Value = ordinal;

                table = Conexion.Busquedas(comando);


                foreach (DataRow row in table.Rows)
                {
                    Ordinal = new Ordinal()
                    {
                        UidOrdinal = (Guid)row["UidOrdinal"],
                        StrOrdinal = (string)row["VchOrdinal"],
                    };

                }


                return Ordinal;


            }

            
        }
    }
}
