﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	public class ZonaHoraria
	{
		#region Atributes
		private string id;
		private string displayName;

		public string DisplayName
		{
			get { return displayName; }
			set { displayName = value; }
		}
		public string Id
		{
			get { return id; }
			set { id = value; }
		}

		#endregion

		#region Contructor
		public ZonaHoraria()
		{

		}
		#endregion
	}
}
