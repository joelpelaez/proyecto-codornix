﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    [Serializable]
    public class PeriodoInactividad
    {
        private Guid _UidPeriodoInactividad;

        public Guid UidPeriodoInactividad
        {
            get { return _UidPeriodoInactividad; }
            set { _UidPeriodoInactividad = value; }
        }

        private Guid _UidPeriodo;

        public Guid UidPeriodo
        {
            get { return _UidPeriodo; }
            set { _UidPeriodo = value; }
        }

        private Guid _UidTipoInactividad;

        public Guid UidTipoInactividad
        {
            get { return _UidTipoInactividad; }
            set { _UidTipoInactividad = value; }
        }

        private Guid? _UidPeriodicidad;

        public Guid? UidPeriodicidad
        {
            get { return _UidPeriodicidad; }
            set { _UidPeriodicidad = value; }
        }

        private DateTime _DtFechaInicio;

        public DateTime DtFechaInicio
        {
            get { return _DtFechaInicio; }
            set { _DtFechaInicio = value; }
        }

        private DateTime? _DtFechaFin;

        public DateTime? DtFechaFin
        {
            get { return _DtFechaFin; }
            set { _DtFechaFin = value; }
        }

        private string _StrNotas;

        public string StrNotas
        {
            get { return _StrNotas; }
            set { _StrNotas = value; }
        }

        // EXTRA
        private string _StrTipoInactividad;

        public string StrTipoInactividad
        {
            get { return _StrTipoInactividad; }
            set { _StrTipoInactividad = value; }
        }

        private string _strDepartamento;

        public string StrDepartamento
        {
            get { return _strDepartamento; }
            set { _strDepartamento = value; }
        }

        private Guid _uidSucursal;

        public Guid UidSucursal
        {
            get { return _uidSucursal; }
            set { _uidSucursal = value; }
        }

        private int _intDiasInactividad;

        public int IntDiasInactividad
        {
            get { return _intDiasInactividad; }
            set { _intDiasInactividad = value; }
        }

        private DateTime _dtFecha;

        public DateTime DtFecha
        {
            get { return _dtFecha; }
            set { _dtFecha = value; }
        }

        private bool _blAsignado;

        public bool BlAsignado
        {
            get { return _blAsignado; }
            set { _blAsignado = value; }
        }


        private bool _BlUserCreated = true;

        public bool BlUserCreated { get { return _BlUserCreated; } }

        public class Repository
        {
            Connection conn = new Connection();

            public List<PeriodoInactividad> Search(Guid uidPeriodo, DateTime? fechaInicio, DateTime? fechaFin, string uidTiposInactividad)
            {
                SqlCommand command = new SqlCommand();
                List<PeriodoInactividad> periodos = new List<PeriodoInactividad>();

                command.CommandText = "usp_PeriodoInactividad_Search";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidPeriodo", uidPeriodo, SqlDbType.UniqueIdentifier);
                if (fechaInicio.HasValue)
                    command.AddParameter("@DtFechaInicio", fechaInicio.Value, SqlDbType.DateTime);
                if (fechaFin.HasValue)
                    command.AddParameter("@DtFechaFin", fechaFin.Value, SqlDbType.DateTime);
                if (string.IsNullOrWhiteSpace(uidTiposInactividad))
                    command.AddParameter("@UidTiposInactividad", uidTiposInactividad, SqlDbType.NVarChar, 4000);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    PeriodoInactividad periodo = new PeriodoInactividad();
                    periodo._UidPeriodoInactividad = new Guid(row["UidPeriodoInactividad"].ToString());
                    periodo._UidPeriodo = new Guid(row["UidPeriodo"].ToString());
                    periodo._UidTipoInactividad = new Guid(row["UidTipoInactividad"].ToString());
                    periodo._UidPeriodicidad = row.IsNull("UidPeriodicidad") ? (Guid?)null : new Guid(row["UidPeriodicidad"].ToString());
                    periodo._DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString());
                    periodo._DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaFin"].ToString());
                    periodo._StrNotas = row["VchNotas"].ToString();
                    periodo._StrTipoInactividad = row["VchTipoInactividad"].ToString();
                    periodo._BlUserCreated = false;
                    periodos.Add(periodo);
                }

                return periodos;
            }

            public List<PeriodoInactividad> SearchByDepartament(Guid uidSucursal, DateTime fechaInicio, DateTime fechaFin, string uidDepartamentos)
            {
                SqlCommand command = new SqlCommand();
                List<PeriodoInactividad> periodos = new List<PeriodoInactividad>();

                command.CommandText = "usp_AsignarPeriodoInactividad_Search";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaInicio", fechaInicio, SqlDbType.DateTime);
                command.AddParameter("@DtFechaFin", fechaFin, SqlDbType.DateTime);
                if (string.IsNullOrWhiteSpace(uidDepartamentos))
                    command.AddParameter("@UidDepartamentos", uidDepartamentos, SqlDbType.NVarChar, 4000);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    PeriodoInactividad periodo = new PeriodoInactividad();
                    periodo._UidPeriodoInactividad = new Guid(row["UidPeriodoInactividad"].ToString());
                    periodo._UidPeriodo = new Guid(row["UidPeriodo"].ToString());
                    periodo._DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString());
                    periodo._DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaFin"].ToString());
                    periodo._strDepartamento = row["VchDepartamento"].ToString();
                    periodo._uidSucursal = new Guid(row["UidDepartamento"].ToString());
                    periodo._intDiasInactividad = Convert.ToInt32(row["IntDiasInactividad"].ToString());
                    periodo._BlUserCreated = false;
                    periodos.Add(periodo);
                }

                return periodos;
            }

            public List<PeriodoInactividad> getInactivedDays(Guid uidPeriodoInactividad, DateTime fechaInicio, DateTime fechaFin)
            {
                SqlCommand command = new SqlCommand();
                List<PeriodoInactividad> periodos = new List<PeriodoInactividad>();

                command.CommandText = "usp_AsignacionPeriodoInactividad_ListaDias";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidPeriodoInactividad", uidPeriodoInactividad, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaInicio", fechaInicio, SqlDbType.DateTime);
                command.AddParameter("@DtFechaFin", fechaFin, SqlDbType.DateTime);
                
                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    PeriodoInactividad periodo = new PeriodoInactividad();
                    periodo._UidPeriodoInactividad = new Guid(row["UidPeriodoInactividad"].ToString());
                    periodo._dtFecha = Convert.ToDateTime(row["DtDate"].ToString());
                    periodo._blAsignado = Convert.ToBoolean(row["BitAsignado"]);
                    periodos.Add(periodo);
                }

                return periodos;
            }

            public int AsignarPeriodo(Guid uidPeriodoInactividad, DateTime fecha, Guid uidUsuario)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_AsignarPeriodoInactividad_Asignar";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidPeriodoInactividad", uidPeriodoInactividad, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);
                command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
                command.Parameters.Add("@IntResult", SqlDbType.Int);
                command.Parameters["@IntResult"].Direction = ParameterDirection.Output;

                conn.ExecuteCommand(command, false);

                int result = (int)command.Parameters["@IntResult"].Value;

                command.Dispose();

                return result;
            }

            public void QuitarPeriodo(Guid uidPeriodoInactividad, DateTime fecha)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_AsignarPeriodoInactividad_Quitar";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidPeriodoInactividad", uidPeriodoInactividad, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);
                
                conn.ExecuteCommand(command, false);
            }

            public PeriodoInactividad Find(Guid uidPeriodoInactividad)
            {
                SqlCommand command = new SqlCommand();

                command.CommandText = "usp_PeriodoInactividad_Find";
                command.CommandType = CommandType.StoredProcedure;
                
                command.AddParameter("@UidPeriodoInactividad", uidPeriodoInactividad, SqlDbType.UniqueIdentifier);

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    PeriodoInactividad periodo = new PeriodoInactividad();
                    periodo._UidPeriodoInactividad = new Guid(row["UidPeriodoInactividad"].ToString());
                    periodo._UidPeriodo = new Guid(row["UidPeriodo"].ToString());
                    periodo._UidTipoInactividad = new Guid(row["UidTipoInactividad"].ToString());
                    periodo._UidPeriodicidad = row.IsNull("UidPeriodicidad") ? (Guid?)null : new Guid(row["UidPeriodicidad"].ToString());
                    periodo._DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString());
                    periodo._DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaFin"].ToString());
                    periodo._StrNotas = row["VchNotas"].ToString();
                    periodo._BlUserCreated = false;
                    return periodo;
                }

                return null;
            }

            public void Delete(Guid uidPeriodoInactividad, DateTime fecha) 
            {
                SqlCommand command = new SqlCommand();

                command.CommandText = "usp_PeriodoInactividad_Remove";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidPeriodoInactividad", uidPeriodoInactividad, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

                conn.ExecuteCommand(command);
            }

            public bool Save(PeriodoInactividad periodoInactividad)
            {
                if (periodoInactividad._BlUserCreated)
                    return InteralSave(periodoInactividad);
                else
                    return InteralUpdate(periodoInactividad);
            }

            private bool InteralSave(PeriodoInactividad periodoInactividad)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_PeriodoInactividad_Add";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidPeriodo", periodoInactividad._UidPeriodo, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidTipoInactividad", periodoInactividad._UidTipoInactividad, SqlDbType.UniqueIdentifier);
                command.AddParameter("@UidPeriodicidad", periodoInactividad._UidPeriodicidad, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaInicio", periodoInactividad._DtFechaInicio, SqlDbType.DateTime);
                command.AddParameter("@DtFechaFin", periodoInactividad._DtFechaFin.Value, SqlDbType.DateTime);
                command.AddParameter("@VchNotas", periodoInactividad._StrNotas, SqlDbType.NVarChar, 500);

                conn.ExecuteQuery(command);

                periodoInactividad._BlUserCreated = false;

                return true;
            }

            private bool InteralUpdate(PeriodoInactividad periodoInactividad)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_PeriodoInactividad_Update";
                command.CommandType = CommandType.StoredProcedure;

                command.AddParameter("@UidPeriodoInactividad", periodoInactividad._UidPeriodoInactividad, SqlDbType.UniqueIdentifier);
                command.AddParameter("@DtFechaInicio", periodoInactividad._DtFechaFin, SqlDbType.DateTime);
                command.AddParameter("@DtFechaFin", periodoInactividad._DtFechaFin.Value, SqlDbType.DateTime);
                command.AddParameter("@VchNotas", periodoInactividad._StrNotas, SqlDbType.NVarChar, 500);

                conn.ExecuteQuery(command);

                return true;
            }
        }
    }
}
