﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
    public class Calificacion
    {
        private Guid _UidCalificacion;
        public Guid UidCalificacion
        {
            get => _UidCalificacion; 
            set => _UidCalificacion = value;
        }
		public string strIdCalificacion {
			get { return _UidCalificacion.ToString(); }
		}

        private string _StrCalificacion;
        public string StrCalificacion
        {
            get => _StrCalificacion;
            set => _StrCalificacion = value;
        }

        public class Repository
        {
            Connection conn = new Connection();

            public List<Calificacion> FindAll()
            {
                List<Calificacion> calificaciones = new List<Calificacion>();
                SqlCommand command = new SqlCommand();

                command.CommandText = "usp_Calificacion_FindAll";
                command.CommandType = CommandType.StoredProcedure;

                DataTable table = conn.ExecuteQuery(command);

                foreach (DataRow row in table.Rows)
                {
                    Calificacion calificacion = new Calificacion();
                    calificacion._UidCalificacion = new Guid(row["UidCalificacion"].ToString());
                    calificacion._StrCalificacion = row["VchCalificacion"].ToString();
                    calificaciones.Add(calificacion);
                }

                return calificaciones;
            }
        }
    }
}
