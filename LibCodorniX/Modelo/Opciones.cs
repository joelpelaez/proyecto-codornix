﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	[Serializable]
	public class Opciones
	{
		protected bool _ExistsInDatabase;

		public bool ExistsInDatabase { get { return _ExistsInDatabase; } }

		private Guid _UidOpciones;
		public Guid UidOpciones
		{
			get { return _UidOpciones; }
			set { _UidOpciones = value; }
		}
		public string strIdOpcion{ get { return _UidOpciones.ToString(); } }

        private string _StrOpcion;
        public string StrOpciones
        {
            get { return _StrOpcion; }
            set { _StrOpcion = value; }
        }

        private int _IntOrden;
        public int IntOrden
        {
            get { return _IntOrden; }
            set { _IntOrden = value; }
        }

        private bool _BlVisible;
        public bool BlVisible
        {
            get { return _BlVisible; }
            set { _BlVisible = value; }
        }
		public string strBlVisible { get { return _BlVisible.ToString(); } }

    }
}
