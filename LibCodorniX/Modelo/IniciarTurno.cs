﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using CodorniX.Util;

namespace CodorniX.Modelo
{
	[Serializable]
	public class IniciarTurno
	{
		[NonSerialized]
		Conexion conexion = new Conexion();

		[NonSerialized]
		Connection connection = new Connection();

		private int _intFolio;
		public int IntFolio
		{
			get { return _intFolio; }
			set { _intFolio = value; }
		}

		private Guid _UidInicioTurno;
		public Guid UidInicioTurno
		{
			get { return _UidInicioTurno; }
			set { _UidInicioTurno = value; }
		}
		public string strIdInicioTurno { get { return _UidInicioTurno.ToString(); } }

		private Guid _UidUsuario;
		public Guid UidUsuario
		{
			get { return _UidUsuario; }
			set { _UidUsuario = value; }
		}
		public string strIdUsuario { get { return _UidUsuario.ToString(); } }

		private DateTimeOffset _DtFechaHoraInicio;
		public DateTimeOffset DtFechaHoraInicio
		{
			get { return _DtFechaHoraInicio; }
			set { _DtFechaHoraInicio = value; }
		}
		public string strDtFechaHoraInicio { get { return _DtFechaHoraInicio.ToString(); } }

		private DateTimeOffset? _DtFechaHoraFin;
		public DateTimeOffset? DtFechaHoraFin
		{
			get { return _DtFechaHoraFin; }
			set { _DtFechaHoraFin = value; }
		}
		public string strDtFechaHoraFin { get { return _DtFechaHoraFin.ToString(); } }

		private int? _IntNoCompleto;
		public int? IntNoCompleto
		{
			get { return _IntNoCompleto; }
			set { _IntNoCompleto = value; }
		}
		public string strIntNoCompleto { get { return _IntNoCompleto.ToString(); } }

		private Guid _UidDepartamento;
		public Guid UidDepartamento
		{
			get { return _UidDepartamento; }
			set { _UidDepartamento = value; }
		}
		public string strIdDepartamento { get { return _UidDepartamento.ToString(); } }

		private Guid? _UidPeriodo;
		public Guid? UidPeriodo
		{
			get { return _UidPeriodo; }
			set { _UidPeriodo = value; }
		}
		public string strIdPeriodo { get { return _UidPeriodo.ToString(); } }

		private Guid _UidTurno;
		public Guid UidTurno
		{
			get { return _UidTurno; }
			set { _UidTurno = value; }
		}
		public string strIdTurno { get { return _UidTurno.ToString(); } }

		private Guid _UidEstadoTurno;
		public Guid UidEstadoTurno
		{
			get { return _UidEstadoTurno; }
			set { _UidEstadoTurno = value; }
		}
		public string strIdEstadoTurno { get { return _UidEstadoTurno.ToString(); } }

		private Guid _UidCreador;
		public Guid UidCreador
		{
			get { return _UidCreador; }
			set { _UidCreador = value; }
		}
		public string strIdCreador { get { return _UidCreador.ToString(); } }

		/* Complemento 2018-08-01 */
		private bool lbAbiertoSupervisor;
		public bool _blAbiertoSupervisor
		{
			get { return lbAbiertoSupervisor; }
			set { lbAbiertoSupervisor = value; }
		}

		private bool blAbiertoEncargado;
		public bool _blAbiertoEncargado
		{
			get { return blAbiertoEncargado; }
			set { blAbiertoEncargado = value; }
		}

		public bool GuardarDatos(bool isEncargado, bool isSupervisor)
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			try
			{
				Comando.CommandType = CommandType.StoredProcedure;

				Comando.CommandText = "usp_IniciarTurno_Add";

				Comando.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidUsuario"].Value = UidUsuario;

				Comando.Parameters.Add("@DtFechaHoraInicio", SqlDbType.DateTimeOffset);
				Comando.Parameters["@DtFechaHoraInicio"].Value = DtFechaHoraInicio;

				Comando.Parameters.Add("@UidPeriodo", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidPeriodo"].Value = UidPeriodo;

				Comando.Parameters.Add("@UidInicioTurno", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidInicioTurno"].Direction = ParameterDirection.Output;

				Comando.AddParameter("@UidCreador", _UidCreador, SqlDbType.UniqueIdentifier);

				/* Para determinar quien inicio el turno */
				Comando.Parameters.Add("@isEncargado", SqlDbType.Bit);
				Comando.Parameters["@isEncargado"].Value = isEncargado;

				Comando.Parameters.Add("@isSupervisor", SqlDbType.Bit);
				Comando.Parameters["@isSupervisor"].Value = isSupervisor;

				Resultado = conexion.ManipilacionDeDatos(Comando);
				_UidInicioTurno = (Guid)Comando.Parameters["@UidInicioTurno"].Value;
				Comando.Dispose();
			}
			catch (Exception)
			{
				throw;
			}
			return Resultado;
		}

		public bool ModificarDatos()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			Comando.CommandType = CommandType.StoredProcedure;
			Comando.CommandText = "usp_ModificarInicioTurno";

			Comando.Parameters.Add("@UidInicioTurno", SqlDbType.UniqueIdentifier);
			Comando.Parameters["@UidInicioTurno"].Value = _UidInicioTurno;

			Comando.Parameters.Add("@DtFechaHoraFin", SqlDbType.DateTimeOffset);
			Comando.Parameters["@DtFechaHoraFin"].Value = DtFechaHoraFin;


			Comando.Parameters.Add("@IntNoCumplido", SqlDbType.Int);
			Comando.Parameters["@IntNoCumplido"].Value = IntNoCompleto;

			Resultado = conexion.ManipilacionDeDatos(Comando);

			return Resultado;
		}

		public class Repositorio
		{
			Conexion conexion = new Conexion();

			public IniciarTurno Find(Guid uid)
			{
				IniciarTurno inicioturno = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_InicioTurno_Find";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidInicioTurno", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidInicioTurno"].Value = uid;
				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					inicioturno = new IniciarTurno()
					{
						_intFolio = Convert.ToInt32(row["IntFolio"].ToString()),
						_UidInicioTurno = new Guid(row["UidInicioTurno"].ToString()),
						_DtFechaHoraInicio = Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
						_UidUsuario = new Guid(row["UidUsuario"].ToString()),
						_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
						_IntNoCompleto = row.IsNull("IntNoCompletado") ? (int?)null : Convert.ToInt32(row["IntNoCompletado"].ToString()),
						_UidPeriodo = new Guid(row["UidPeriodo"].ToString()),
						_UidCreador = new Guid(row["UidCreador"].ToString()),
						_blAbiertoEncargado = Convert.ToBoolean(row.IsNull("iniEncargado") ? "false" : row["iniEncargado"].ToString()),
						_blAbiertoSupervisor = Convert.ToBoolean(row.IsNull("iniSupervisor") ? "false" : row["iniSupervisor"].ToString())
					};
					inicioturno._UidEstadoTurno = new Guid(row["UidEstadoTurno"].ToString());
				}

				return inicioturno;
			}

			public IniciarTurno ObtenerInicioTurno(Guid uid)
			{
				IniciarTurno inicioturno = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_ObtenerInicioTurno";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidUsuario"].Value = uid;
				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					inicioturno = new IniciarTurno()
					{
						_intFolio = Convert.ToInt32(row["IntFolio"].ToString()),
						_UidInicioTurno = new Guid(row["UidInicioTurno"].ToString()),
						_DtFechaHoraInicio = Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
						_UidUsuario = new Guid(row["UidUsuario"].ToString()),
						_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
						_IntNoCompleto = row.IsNull("IntNoCompletado") ? (int?)null : Convert.ToInt32(row["IntNoCompletado"].ToString()),
						_UidPeriodo = new Guid(row["UidPeriodo"].ToString()),
						_UidCreador = new Guid(row["UidCreador"].ToString()),
						_UidEstadoTurno = new Guid(row["UidEstadoTurno"].ToString()),
						_blAbiertoEncargado = Convert.ToBoolean(row.IsNull("iniEncargado") ? "false" : row["iniEncargado"].ToString()),
						_blAbiertoSupervisor = Convert.ToBoolean(row.IsNull("iniSupervisor") ? "false" : row["iniSupervisor"].ToString())
					};
				}

				return inicioturno;
			}

			public IniciarTurno ObtenerInicioPorPeriodo(Guid uid, Guid periodo, DateTime fecha)
			{
				IniciarTurno inicioturno = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_InicioTurnoPorPeriodo";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidPeriodo", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidPeriodo"].Value = periodo;

				command.Parameters.Add("@DtFecha", SqlDbType.Date);
				command.Parameters["@DtFecha"].Value = fecha;
				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					inicioturno = new IniciarTurno()
					{
						_intFolio = Convert.ToInt32(row["IntFolio"].ToString()),
						_UidInicioTurno = new Guid(row["UidInicioTurno"].ToString()),
						_DtFechaHoraInicio = Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
						_UidUsuario = new Guid(row["UidUsuario"].ToString()),
						_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
						_IntNoCompleto = row.IsNull("IntNoCompletado") ? (int?)null : Convert.ToInt32(row["IntNoCompletado"].ToString()),
						_UidPeriodo = new Guid(row["UidPeriodo"].ToString()),
						_UidCreador = new Guid(row["UidCreador"].ToString()),
						_UidEstadoTurno = new Guid(row["UidEstadoTurno"].ToString()),
					};
				}

				return inicioturno;
			}

			public IniciarTurno ObtenerHora(DateTime fecha, Guid UidUsuario, Guid uidperiodo)
			{
				IniciarTurno inicioturno = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_ConsultarInicioTurno";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidUsuario"].Value = UidUsuario;

				command.Parameters.Add("@UidPeriodo", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidPeriodo"].Value = uidperiodo;

				command.Parameters.Add("@DtFecha", SqlDbType.Date);
				command.Parameters["@DtFecha"].Value = fecha;

				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					inicioturno = new IniciarTurno()
					{
						_intFolio = Convert.ToInt32(row["IntFolio"].ToString()),
						_UidInicioTurno = new Guid(row["UidInicioTurno"].ToString()),
						_DtFechaHoraInicio = Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
						_UidUsuario = new Guid(row["UidUsuario"].ToString()),
						_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
						_IntNoCompleto = row.IsNull("IntNoCompletado") ? (int?)null : Convert.ToInt32(row["IntNoCompletado"].ToString()),
						_UidPeriodo = row.IsNull("UidPeriodo") ? (Guid?)null : new Guid(row["UidPeriodo"].ToString()),
						_UidCreador = new Guid(row["UidCreador"].ToString()),
					};
				}

				return inicioturno;
			}

			public IniciarTurno ObtenerTurnoUsuario(Guid uidPerido, DateTime fecha)
			{
				IniciarTurno inicioturno = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_ObtenerTurno_Usuario";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidPeriodo", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidPeriodo"].Value = uidPerido;
				command.Parameters.Add("@DtFecha", SqlDbType.Date);
				command.Parameters["@DtFecha"].Value = fecha;


				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					inicioturno = new IniciarTurno()
					{
						_intFolio = Convert.ToInt32(row["IntFolio"].ToString()),
						_UidInicioTurno = new Guid(row["UidInicioTurno"].ToString()),
						_DtFechaHoraInicio = Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
						_UidUsuario = new Guid(row["UidUsuario"].ToString()),
						_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
						_IntNoCompleto = row.IsNull("IntNoCompletado") ? (int?)null : Convert.ToInt32(row["IntNoCompletado"].ToString()),
						_UidPeriodo = new Guid(row["UidPeriodo"].ToString()),
						_UidTurno = new Guid(row["UidTurno"].ToString()),
						_UidCreador = new Guid(row["UidCreador"].ToString()),
						_UidEstadoTurno = new Guid(row["UidEstadoTurno"].ToString()),
						_blAbiertoEncargado = Convert.ToBoolean(row.IsNull("iniEncargado") ? "false" : row["iniEncargado"].ToString()),
						_blAbiertoSupervisor = Convert.ToBoolean(row.IsNull("iniSupervisor") ? "false" : row["iniSupervisor"].ToString())
					};
				}

				return inicioturno;
			}

			public void ModificarEstado(Guid uidTurno, string estado)
			{
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_InicioTurno_CambiarEstado";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidInicioTurno", uidTurno, SqlDbType.UniqueIdentifier);
				command.AddParameter("@VchEstado", estado, SqlDbType.NVarChar, 50);

				conexion.ManipilacionDeDatos(command);
			}
			public void ReAbrirTurno(Guid uidTurno, string estado)
			{
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_InicioTurno_ReAbrir";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidInicioTurno", uidTurno, SqlDbType.UniqueIdentifier);
				command.AddParameter("@VchEstado", estado, SqlDbType.NVarChar, 50);

				conexion.ManipilacionDeDatos(command);
			}

			public bool ActualizarEstatusTurnoEncargado(Guid UIDInicioTurno)
			{
				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_ActualizarEstatusInicioTurnoEncargado";
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@UIDInicioTurno", SqlDbType.UniqueIdentifier);
					command.Parameters["@UIDInicioTurno"].Value = UIDInicioTurno;

					return conexion.ManipilacionDeDatos(command);
				}
				catch (Exception)
				{
					return false;
				}
			}

			public IniciarTurno ObtenerUltimoPorDepartamento(Guid? uidInicioTurno, Guid uidDepartamento)
			{
				IniciarTurno inicioturno = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_InicioTurno_GetLastByDepartment";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidDepartamento"].Value = uidDepartamento;

				if (uidInicioTurno != null)
				{
					command.Parameters.Add("@UidInicioTurno", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidInicioTurno"].Value = uidInicioTurno;
				}

				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					inicioturno = new IniciarTurno()
					{
						_intFolio = Convert.ToInt32(row["IntFolio"].ToString()),
						_UidInicioTurno = new Guid(row["UidInicioTurno"].ToString()),
						_DtFechaHoraInicio = Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
						_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
						_UidPeriodo = new Guid(row["UidPeriodo"].ToString()),
						UidDepartamento = new Guid(row["UidDepartamento"].ToString())
					};
				}

				return inicioturno;
			}
		}

	}
}
