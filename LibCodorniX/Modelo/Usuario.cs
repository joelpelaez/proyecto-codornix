﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.ConexionDB;
using System.Data;
using System.Data.SqlClient;
using CodorniX.Util;

namespace CodorniX.Modelo
{
	[Serializable]
	public class Usuario
	{
		#region Propiedades
		[NonSerialized]
		Conexion Conexion = new Conexion();

		protected Guid _UidUsuario;
		public Guid UIDUSUARIO
		{
			get { return _UidUsuario; }
			set { _UidUsuario = value; }
		}
		public string Id
		{
			get { return _UidUsuario.ToString(); }
		}

		protected string _strNombre;
		public string STRNOMBRE
		{
			get { return _strNombre; }
			set { _strNombre = value; }
		}

		protected string _strApellidoPaterno;
		public string STRAPELLIDOPATERNO
		{
			get { return _strApellidoPaterno; }
			set { _strApellidoPaterno = value; }
		}

		protected string _strApellidoMaterno;
		public string STRAPELLIDOMATERNO
		{
			get { return _strApellidoMaterno; }
			set { _strApellidoMaterno = value; }
		}

		protected DateTime _DtFechaNacimiento;
		public DateTime DtFechaNacimiento
		{
			get { return _DtFechaNacimiento; }
			set { _DtFechaNacimiento = value; }
		}
		public string strDtFechaNacimiento { get { return _DtFechaNacimiento.ToString("dd/MM/yyyy"); } }

		protected string _strCorreo;
		public string STRCORREO
		{
			get { return string.IsNullOrEmpty(_strCorreo) ? "" : _strCorreo; }
			set { _strCorreo = value; }
		}

		protected DateTime _DtFechaInicio;
		public DateTime DtFechaInicio
		{
			get { return _DtFechaInicio; }
			set { _DtFechaInicio = value; }
		}
		public string strDtFechaInicio { get { return _DtFechaInicio.ToString("dd/MM/yyyy"); } }

		protected DateTime? _DtFechaFin;
		public DateTime? DtFechaFin
		{
			get { return _DtFechaFin; }
			set { _DtFechaFin = value; }
		}
		public string strDtFechaFin { get { return _DtFechaFin == null ? "empty" : _DtFechaFin?.ToString("dd/MM/yyyy"); } }

		protected string _strUsuario;
		public string STRUSUARIO
		{
			get { return string.IsNullOrEmpty(_strUsuario) ? "" : _strUsuario; }
			set { _strUsuario = value; }
		}

		protected string _strPassword;
		public string STRPASSWORD
		{
			get { return _strPassword; }
			set { _strPassword = value; }
		}

		protected string _RutaImagen;
		public string RutaImagen
		{
			get { return _RutaImagen; }
			set { _RutaImagen = value; }
		}

		protected Guid _UidSucursal;
		public Guid UidSucursal
		{
			get { return _UidSucursal; }
			set { _UidSucursal = value; }
		}

		protected Guid _UidPerfil;
		public Guid UidPerfil
		{
			get { return _UidPerfil; }
			set { _UidPerfil = value; }
		}
		public string strIdPerfil { get { return _UidPerfil.ToString(); } }

		protected Guid _UidStatus;
		public Guid UidStatus
		{
			get { return _UidStatus; }
			set { _UidStatus = value; }
		}
		public string IdEstatus
		{
			get { return _UidStatus.ToString(); }
		}

		protected Guid _UidEmpresa;
		public Guid UidEmpresa
		{
			get { return _UidEmpresa; }
			set { _UidEmpresa = value; }
		}

		protected string _StrEmpresa;
		public string StrEmpresa
		{
			get { return string.IsNullOrEmpty(_StrEmpresa) ? "" : _StrEmpresa; }
			set { _StrEmpresa = value; }
		}


		protected string _StrPerfil;
		public string StrPerfil
		{
			get { return _StrPerfil; }
			set { _StrPerfil = value; }
		}

		protected string _StrStatus;
		public string StrStatus
		{
			get { return _StrStatus; }
			set { _StrStatus = value; }
		}

		protected string _StrSucursal;
		public string StrSucursal
		{
			get { return _StrSucursal; }
			set { _StrSucursal = value; }
		}

		public string StrNombreCompleto
		{
			get
			{
				string nombre = _strNombre + " " + _strApellidoPaterno;
				if (_strApellidoMaterno != null)
					nombre += " " + _strApellidoMaterno;

				return nombre;
			}
		}

		#endregion

		#region Metodos

		public bool GuardarDatos()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			try
			{
				Comando.CommandType = CommandType.StoredProcedure;

				Comando.CommandText = "sp_InsertarUsuario";

				Comando.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
				Comando.Parameters["@VchNombre"].Value = STRNOMBRE;

				Comando.Parameters.Add("@VchApellidoPaterno", SqlDbType.NVarChar, 50);
				Comando.Parameters["@VchApellidoPaterno"].Value = STRAPELLIDOPATERNO;

				Comando.Parameters.Add("@VchApellidoMaterno", SqlDbType.NVarChar, 50);
				Comando.Parameters["@VchApellidoMaterno"].Value = STRAPELLIDOMATERNO;

				Comando.Parameters.Add("@DtFechaNacimiento", SqlDbType.DateTime);
				Comando.Parameters["@DtFechaNacimiento"].Value = DtFechaNacimiento;

				Comando.Parameters.Add("@VchCorreo", SqlDbType.NVarChar, 50);
				Comando.Parameters["@VchCorreo"].Value = STRCORREO;

				Comando.Parameters.Add("@DtFechaInicio", SqlDbType.DateTime);
				Comando.Parameters["@DtFechaInicio"].Value = DtFechaInicio;

				Comando.Parameters.Add("@DtFechaFin", SqlDbType.DateTime);
				Comando.Parameters["@DtFechaFin"].Value = DtFechaFin;

				Comando.Parameters.Add("@VchUsuario", SqlDbType.NVarChar, 50);
				Comando.Parameters["@VchUsuario"].Value = STRUSUARIO;

				Comando.Parameters.Add("@VchPassword", SqlDbType.NVarChar, 50);
				Comando.Parameters["@VchPassword"].Value = STRPASSWORD;

				Comando.Parameters.Add("@UidStatus", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidStatus"].Value = UidStatus;

				Comando.Parameters.Add("@VchRutaImagen", SqlDbType.NVarChar, 200);
				Comando.Parameters["@VchRutaImagen"].Value = RutaImagen;

				Comando.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidUsuario"].Direction = ParameterDirection.Output;

				Resultado = Conexion.ManipilacionDeDatos(Comando);

				_UidUsuario = (Guid)Comando.Parameters["@UidUsuario"].Value;
				Comando.Dispose();


			}
			catch (Exception)
			{
				throw;
			}




			return Resultado;
		}




		public bool ModificarDatos()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			Comando.CommandType = CommandType.StoredProcedure;
			Comando.CommandText = "sp_ModificarUsuario";

			Comando.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
			Comando.Parameters["@UidUsuario"].Value = _UidUsuario;

			Comando.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
			Comando.Parameters["@VchNombre"].Value = STRNOMBRE;

			Comando.Parameters.Add("@VchApellidoPaterno", SqlDbType.NVarChar, 50);
			Comando.Parameters["@VchApellidoPaterno"].Value = STRAPELLIDOPATERNO;

			Comando.Parameters.Add("@VchApellidoMaterno", SqlDbType.NVarChar, 50);
			Comando.Parameters["@VchApellidoMaterno"].Value = STRAPELLIDOMATERNO;

			Comando.Parameters.Add("@DtFechaNacimiento", SqlDbType.DateTime);
			Comando.Parameters["@DtFechaNacimiento"].Value = DtFechaNacimiento;

			Comando.Parameters.Add("@VchCorreo", SqlDbType.NVarChar, 50);
			Comando.Parameters["@VchCorreo"].Value = STRCORREO;

			Comando.Parameters.Add("@DtFechaInicio", SqlDbType.DateTime);
			Comando.Parameters["@DtFechaInicio"].Value = DtFechaInicio;

			Comando.Parameters.Add("@DtFechaFin", SqlDbType.DateTime);
			Comando.Parameters["@DtFechaFin"].Value = DtFechaFin;

			Comando.Parameters.Add("@VchUsuario", SqlDbType.NVarChar, 50);
			Comando.Parameters["@VchUsuario"].Value = STRUSUARIO;

			Comando.Parameters.Add("@VchPassword", SqlDbType.NVarChar, 50);
			Comando.Parameters["@VchPassword"].Value = STRPASSWORD;


			Comando.Parameters.Add("@UidStatus", SqlDbType.UniqueIdentifier);
			Comando.Parameters["@UidStatus"].Value = UidStatus;

			Comando.Parameters.Add("@VchRutaImagen", SqlDbType.NVarChar, 200);
			Comando.Parameters["@VchRutaImagen"].Value = RutaImagen;


			Resultado = Conexion.ManipilacionDeDatos(Comando);

			return Resultado;
		}


		public class Repository
		{
			Conexion Conexion = new Conexion();

			public List<Usuario> ObtenerCorreos(Guid uidCumplimiento)
			{
				List<Usuario> usuarios = new List<Usuario>();
				Usuario usuario = null;
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_Notificacion_ObtenerCorreos";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidCumplimiento", uidCumplimiento, SqlDbType.UniqueIdentifier);

				DataTable table = new Connection().ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					usuario = new Usuario()
					{
						_UidUsuario = (Guid)row["UidUsuario"],
						_strCorreo = row["VchCorreo"].ToString(),
						_strUsuario = row["VchUsuario"].ToString(),
						_strNombre = row["VchNombre"].ToString(),
						_strApellidoPaterno = row["VchApellidoPaterno"].ToString(),
						_strApellidoMaterno = row["VchApellidoMaterno"].ToString(),
					};
					usuarios.Add(usuario);
				}

				return usuarios;
			}

			public Usuario Find(Guid uid)
			{
				Usuario usuario = null;
				SqlCommand command = new SqlCommand();

				command.CommandText = "SELECT * FROM Usuario WHERE Usuario.UidUsuario = @uid";
				command.CommandType = CommandType.Text;

				command.AddParameter("@uid", uid, SqlDbType.UniqueIdentifier);

				DataTable table = new Connection().ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					usuario = new Usuario()
					{
						_UidUsuario = (Guid)row["UidUsuario"],
						_strNombre = row["VchNombre"].ToString(),
						_strApellidoPaterno = row["VchApellidoPaterno"].ToString(),
						_strApellidoMaterno = row["VchApellidoMaterno"].ToString(),
						_DtFechaNacimiento = Convert.ToDateTime(row["DtFechaNacimiento"].ToString()),
						_strCorreo = row["VchCorreo"].ToString(),
						_DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString()),
						_DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaFin"].ToString()),
						_strUsuario = row["VchUsuario"].ToString(),
						_strPassword = row["VchPassword"].ToString(),
						// _UidPerfil = new Guid(row["UidPerfil"].ToString()),
						_UidStatus = new Guid(row["UidStatus"].ToString()),
					};
				}

				return usuario;
			}

			public Usuario FindByName(string name)
			{
				Usuario usuario = null;
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_User_FindByName";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@VchUsuario", name, SqlDbType.NVarChar, 50);

				DataTable table = new Connection().ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					usuario = new Usuario()
					{
						_UidUsuario = (Guid)row["UidUsuario"],
						_strNombre = row["VchNombre"].ToString(),
						_strApellidoPaterno = row["VchApellidoPaterno"].ToString(),
						_strApellidoMaterno = row["VchApellidoMaterno"].ToString(),
						_DtFechaNacimiento = Convert.ToDateTime(row["DtFechaNacimiento"].ToString()),
						_strCorreo = row["VchCorreo"].ToString(),
						_DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString()),
						_DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaFin"].ToString()),
						_strUsuario = row["VchUsuario"].ToString(),
						_strPassword = row["VchPassword"].ToString(),
						// _UidPerfil = new Guid(row["UidPerfil"].ToString()),
						_UidStatus = new Guid(row["UidStatus"].ToString()),
					};
				}

				return usuario;
			}

			public List<Usuario> CargarUsuarios()
			{
				List<Usuario> Usuarios = new List<Usuario>();
				Usuario usuario = null;

				try
				{

					SqlCommand comando = new SqlCommand();
					comando.CommandType = CommandType.StoredProcedure;
					comando.CommandText = "sp_BuscarUsuario";

					DataTable table = Conexion.Busquedas(comando);

					foreach (DataRow item in Conexion.Busquedas(comando).Rows)
					{
						usuario = new Usuario()
						{
							UIDUSUARIO = new Guid(item["UidUsuario"].ToString()),
							STRNOMBRE = item["VchNombre"].ToString(),
							STRAPELLIDOPATERNO = item["VchApellidoPaterno"].ToString(),
							STRAPELLIDOMATERNO = item["VchApellidoMaterno"].ToString(),
							DtFechaNacimiento = Convert.ToDateTime(item["DtFechaNacimiento"].ToString()),
							STRCORREO = item["VchCorreo"].ToString(),
							DtFechaInicio = Convert.ToDateTime(item["DtFechaInicio"].ToString()),
							DtFechaFin = item.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(item["DtFechaFin"].ToString()),
							STRUSUARIO = item["VchUsuario"].ToString(),
							STRPASSWORD = item["VchPassword"].ToString(),
							UidPerfil = new Guid(item["UidPerfil"].ToString()),
							UidStatus = new Guid(item["UidStatus"].ToString()),
							StrPerfil = item["VchPerfil"].ToString(),
							StrStatus = item["VchStatus"].ToString(),

						};
						Usuarios.Add(usuario);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Cannot load Empresas", e);
				}

				return Usuarios;
			}

			public List<Usuario> BuscarUsuarios(string nombre, string ApellidoPaterno, string ApellidoMaterno,
			string FechaNacimiento, string FechaNacimiento2, string Correo, string FechaInicio, string FechaInicio2, string FechaFin, string FechaFin2, string perfil,
			string usuario, string Status, string empresas)
			{
				Usuario usu = null;
				List<Usuario> list = new List<Usuario>();

				SqlCommand comando = new SqlCommand();
				comando.CommandType = CommandType.StoredProcedure;
				comando.CommandText = "sp_BuscarUsuario";

				if (nombre != string.Empty)
				{
					comando.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
					comando.Parameters["@VchNombre"].Value = nombre;
				}

				if (ApellidoPaterno != string.Empty)
				{
					comando.Parameters.Add("@VchApellidoPaterno", SqlDbType.NVarChar, 50);
					comando.Parameters["@VchApellidoPaterno"].Value = ApellidoPaterno;
				}
				if (ApellidoMaterno != string.Empty)
				{
					comando.Parameters.Add("@VchApellidoMaterno", SqlDbType.NVarChar, 50);
					comando.Parameters["@VchApellidoMaterno"].Value = ApellidoMaterno;
				}
				if (FechaNacimiento != string.Empty)
				{
					comando.Parameters.Add("@DtFechaNacimiento", SqlDbType.Date);
					comando.Parameters["@DtFechaNacimiento"].Value = Convert.ToDateTime(FechaNacimiento);
				}
				if (FechaNacimiento2 != string.Empty)
				{
					comando.Parameters.Add("@DtFechaNacimiento2", SqlDbType.Date);
					comando.Parameters["@DtFechaNacimiento2"].Value = Convert.ToDateTime(FechaNacimiento2);
				}
				if (Correo != string.Empty)
				{
					comando.Parameters.Add("@VchCorreo", SqlDbType.NVarChar, 50);
					comando.Parameters["@VchCorreo"].Value = Correo;
				}

				if (FechaInicio != string.Empty)
				{
					comando.Parameters.Add("@DtFechaInicio", SqlDbType.Date);
					comando.Parameters["@DtFechaInicio"].Value = Convert.ToDateTime(FechaInicio);
				}

				if (FechaInicio2 != string.Empty)
				{
					comando.Parameters.Add("@DtFechaInicio2", SqlDbType.Date);
					comando.Parameters["@DtFechaInicio2"].Value = Convert.ToDateTime(FechaInicio2);
				}

				if (FechaFin != string.Empty)
				{
					comando.Parameters.Add("@DtFechaFin", SqlDbType.Date);
					comando.Parameters["@DtFechaFin"].Value = Convert.ToDateTime(FechaFin);
				}

				if (FechaFin2 != string.Empty)
				{
					comando.Parameters.Add("@DtFechaFin2", SqlDbType.Date);
					comando.Parameters["@DtFechaFin2"].Value = Convert.ToDateTime(FechaFin2);
				}

				if (usuario != string.Empty)
				{
					comando.Parameters.Add("@VchUsuario", SqlDbType.NVarChar, 50);
					comando.Parameters["@VchUsuario"].Value = usuario;
				}
				if (perfil != string.Empty)
				{
					comando.Parameters.Add("@UidPerfil", SqlDbType.NVarChar, 4000);
					comando.Parameters["@UidPerfil"].Value = perfil;
				}
				if (Status != string.Empty)
				{
					comando.Parameters.Add("@UidStatus", SqlDbType.NVarChar, 4000);
					comando.Parameters["@UidStatus"].Value = Status;
				}
				if (empresas != string.Empty)
				{
					comando.AddParameter("@UidEmpresas", empresas, SqlDbType.NVarChar, 4000);
				}

				foreach (DataRow item in Conexion.Busquedas(comando).Rows)
				{
					usu = new Usuario()
					{
						UIDUSUARIO = new Guid(item["UidUsuario"].ToString()),
						STRNOMBRE = item["VchNombre"].ToString(),
						STRAPELLIDOPATERNO = item["VchApellidoPaterno"].ToString(),
						STRAPELLIDOMATERNO = item["VchApellidoMaterno"].ToString(),
						DtFechaNacimiento = Convert.ToDateTime(item["DtFechaNacimiento"].ToString()),
						STRCORREO = item["VchCorreo"].ToString(),
						DtFechaInicio = Convert.ToDateTime(item["DtFechaInicio"].ToString()),
						DtFechaFin = item.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(item["DtFechaFin"].ToString()),
						STRUSUARIO = item["VchUsuario"].ToString(),
						STRPASSWORD = item["VchPassword"].ToString(),
						UidPerfil = new Guid(item["UidPerfil"].ToString()),
						UidStatus = new Guid(item["UidStatus"].ToString()),
						StrPerfil = item["VchPerfil"].ToString(),
						StrStatus = item["VchStatus"].ToString(),

					};
					return list;
				}

				return null;
			}
		}

		#endregion
	}
}