﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CodorniX.Modelo
{
    /// <summary>
    /// Clase representativa de la tabla Area con atributos adicionales, para acceder a la información de
    /// la base de datos debe usar la clase <see cref="Repository"/>.
    /// </summary>
    [Serializable]
    public class Area
    {
        /// <summary>
        /// Muestra si el objeto fue creado por el usuario, o si existe en la base de datos.
        /// </summary>
        private bool _blCreadoPorUsuario = false;

        private Guid _UidArea;
        /// <summary>
        /// Identificador único del <see cref="Area"/>
        /// </summary>
        public Guid UidArea
        {
            get { return _UidArea; }
            set { _UidArea = value; }
        }
		public string strIdArea { get { return _UidArea.ToString(); } }

        private string _StrNombre;
        /// <summary>
        /// Nombre del <see cref="Area"/>
        /// </summary>
        public string StrNombre
        {
            get { return _StrNombre; }
            set { _StrNombre = value; }
        }

        private string _StrDescripcion;
        /// <summary>
        /// Descripción del <see cref="Area"/>
        /// </summary>
        public string StrDescripcion
        {
            get { return _StrDescripcion; }
            set { _StrDescripcion = value; }
        }

        private string _StrURL;
        /// <summary>
        /// URL de la imagen o ícono que representa el área.
        /// </summary>
        public string StrURL
        {
            get { return _StrURL; }
            set { _StrURL = value; }
        }

        private Guid _UidStatus;
        /// <summary>
        /// Identificador único del Status, relacionado a otra entidad.
        /// </summary>
        public Guid UidStatus
        {
            get { return _UidStatus; }
            set { _UidStatus = value; }
        }
		public string strIdStatus { get { return _UidStatus.ToString(); } }

        private Guid _UidDepartamento;
        /// <summary>
        /// Identificador único del departamento al que pertenece el área.
        /// </summary>
        public Guid UidDepartamento
        {
            get { return _UidDepartamento; }
            set { _UidDepartamento = value; }
        }
		public string strIdDepartamento { get { return _UidDepartamento.ToString(); } }
        /// <summary>
        /// Crea un objeto <see cref="Area"/>. Esta variante del constructor es utilizado para la creación interna
        /// en base a la información proporcionada por la base de datos.
        /// </summary>
        /// <param name="uid">Identificador único del área</param>
        /// <param name="nombre">Nombre del área</param>
        /// <param name="descripcion">Descirpción del área</param>
        /// <param name="url">Dirección URL de la imagen o ícono del área.</param>
        /// <param name="status">Identificador único del <see cref="Estado"/> del area.</param>
        /// <param name="departamento">Identificador único del departamento al que pertenece el área.</param>
        private Area(Guid uid, string nombre, string descripcion, string url, Guid status, Guid departamento)
        {
            _UidArea = uid;
            _StrNombre = nombre;
            _StrDescripcion = descripcion;
            _StrURL = url;
            _UidStatus = status;
            _UidDepartamento = departamento;
        }

        /// <summary>
        /// Crea un nuevo objeto <see cref="Area"/>. Este nuevo objeto se identifica como "creado por el usuario"
        /// y al momento de guardarlo se creará en la base de datos.
        /// </summary>
        public Area()
        {
            _blCreadoPorUsuario = true;
        }

        /// <summary>
        /// Clase DAO o Repository encargado de gestionar de forma transparente el acceso a la base de datos,
        /// mapeando el POCO a una tabla.
        /// </summary>
        public class Repository
        {
            /// <summary>
            /// Instancia a la clase <see cref="Connection"/> que gestiona la lógica de conexión.
            /// </summary>
            protected Connection _clsConexion = new Connection();

            /// <summary>
            /// 
            /// </summary>
            /// <param name="area">Objeto a guardar</param>
            /// <returns>true si se guarda correctamente, false en caso contrtario.</returns>
            private bool InternalSave(Area area)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_Area_Add";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
                command.Parameters["@VchNombre"].Value = area._StrNombre;

                command.AddParameter("@VchDescripcion", area._StrDescripcion, SqlDbType.NVarChar, 200);

                command.AddParameter("@VchURL", area._StrURL, SqlDbType.NVarChar, 255);

                command.AddParameter("@UidStatus", area._UidStatus, SqlDbType.UniqueIdentifier);

                command.AddParameter("@UidDepartamento", area._UidDepartamento, SqlDbType.UniqueIdentifier);

                command.Parameters.Add("@UidArea", SqlDbType.UniqueIdentifier);
                command.Parameters["@UidArea"].Direction = ParameterDirection.Output;

                bool result = _clsConexion.ExecuteCommand(command, false);

                area._UidArea = (Guid)command.Parameters["@UidArea"].Value;

                command.Dispose();

                return result;
            }

            /// <summary>
            /// Implementación de la actualización de datos.
            /// </summary>
            /// <param name="area">Objeto <see cref="Area"/></param>
            /// <returns>true si se guardo, false en caso contrario.</returns>
            private bool InternalUpdate(Area area)
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "usp_Area_Update";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@UidArea", SqlDbType.UniqueIdentifier);
                command.Parameters["@UidArea"].Value = area._UidArea;

                command.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
                command.Parameters["@VchNombre"].Value = area._StrNombre;

                command.AddParameter("@VchDescripcion", area._StrDescripcion, SqlDbType.NVarChar, 200);

                command.AddParameter("@VchURL", area._StrURL, SqlDbType.NVarChar, 255);

                command.AddParameter("@UidStatus", area._UidStatus, SqlDbType.UniqueIdentifier);

                command.AddParameter("@UidDepartamento", area._UidDepartamento, SqlDbType.UniqueIdentifier);

                return _clsConexion.ExecuteCommand(command);
            }

            /// <summary>
            /// Guarda o actualiza un objeto <see cref="Area"/> en la base de datos.
            /// </summary>
            /// <param name="area">Objeto <see cref="Area"/></param>
            /// <returns>true si el objeto se guarda correctamente en la base de datos.</returns>
            public bool Save(Area area)
            {
                bool result = false;

                try
                {
                    if (area._blCreadoPorUsuario)
                        result = InternalSave(area);
                    else if (!area._blCreadoPorUsuario && !(area._UidArea == null))
                        result = InternalUpdate(area);
                    else
                        throw new DatabaseException("Invalid User object");

                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error saving User object", e);
                }

                return result;
            }

            /// <summary>
            /// Obtiene un objeto <see cref="Area"/> a partir de su identificador único.
            /// </summary>
            /// <param name="uid">Identificador único del área.</param>
            /// <returns>objeto <see cref="Area"/> o nulo</returns>
            public Area Find(Guid uid)
            {
                Area area = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Area_Find";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@UidArea", SqlDbType.UniqueIdentifier);
                    command.Parameters["@UidArea"].Value = uid;

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                foreach (DataRow row in table.Rows)
                {
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid departamento = (Guid)row["UidDepartamento"];
                    area = new Area(uid, nombre, descripcion, url, status, departamento);
                }

                return area;
            }

            /// <summary>
            /// Obtiene todas las áreas por departamento.
            /// </summary>
            /// <param name="uidDepto">Identificador único del departamento.</param>
            /// <returns>Una lista de objetos <see cref="Area"/>.</returns>
            public List<Area> FindAll(Guid uidDepto)
            {
                List<Area> areas = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Area_Search";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidDepartamento", uidDepto, SqlDbType.UniqueIdentifier);

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                areas = new List<Area>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidArea"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid departamento = (Guid)row["UidDepartamento"];
                    Area area = new Area(uid, nombre, descripcion, url, status, departamento);
                    areas.Add(area);
                }

                return areas;
            }

            /// <summary>
            /// Obtiene una lista de áreas basado en un criterio de búsqueda.
            /// </summary>
            /// <param name="criteria">Criterio de búsqueda dependiente de la entidad.</param>
            /// <returns>Lista de objetos área que coincidan con la búsqueda, puede no contener elementos.</returns>
            public List<Area> FindBy(Criteria criteria)
            {
                List<Area> areas = null;
                DataTable table = null;
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_Area_Search";
                    command.CommandType = CommandType.StoredProcedure;

                    InjectParameters(command, criteria);

                    table = _clsConexion.ExecuteQuery(command);
                }
                catch (SqlException e)
                {
                    throw new DatabaseException("Error populating table Use", e);
                }

                areas = new List<Area>(table.Rows.Count);

                foreach (DataRow row in table.Rows)
                {
                    Guid uid = new Guid(row["UidArea"].ToString());
                    string nombre = row["VchNombre"].ToString();
                    string descripcion = row["VchDescripcion"].ToString();
                    string url = row["VchURL"].ToString();
                    Guid status = (Guid)row["UidStatus"];
                    Guid departamento = (Guid)row["UidDepartamento"];
                    Area area = new Area(uid, nombre, descripcion, url, status, departamento);
                    areas.Add(area);
                }

                return areas;
            }

            /// <summary>
            /// Inyección de parámetros de búsqueda.
            /// </summary>
            /// <param name="command"></param>
            /// <param name="criteria"></param>
            private void InjectParameters(SqlCommand command, Criteria criteria)
            {
                if (!string.IsNullOrWhiteSpace(criteria.Nombre))
                {
                    command.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
                    command.Parameters["@VchNombre"].Value = criteria.Nombre;
                }
                if (!string.IsNullOrWhiteSpace(criteria.Descripcion))
                {
                    command.AddParameter("@VchDescripcion", criteria.Descripcion, SqlDbType.NVarChar, 200);
                }
                if (criteria.Departamento != Guid.Empty)
                {
                    command.AddParameter("@UidDepartamento", criteria.Departamento, SqlDbType.UniqueIdentifier);
                }
            }
        }
        /// <summary>
        /// Clase de criterio.
        /// </summary>
        public class Criteria
        {
            public string Nombre { get; set; }
            public string Descripcion { get; set; }
            public Guid Departamento { get; set; }
        }
    }
}