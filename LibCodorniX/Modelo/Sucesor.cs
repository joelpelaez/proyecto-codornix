﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	public class Sucesor
	{
		private Guid _UidTarea;
		public Guid UidTarea
		{
			get { return _UidTarea; }
			set { _UidTarea = value; }
		}
		public string strIdTarea { get { return _UidTarea.ToString(); } }

		private Guid? _UidCumplimiento;
		public Guid? UidCumplimiento
		{
			get { return _UidCumplimiento; }
			set { _UidCumplimiento = value; }
		}
		public string strIdCumplimiento { get { return _UidCumplimiento == null ? Guid.Empty.ToString() : _UidCumplimiento.ToString(); } }

		private Guid? _UidEstadoCumplimiento;
		public Guid? UidEstadoCumplimiento
		{
			get { return _UidEstadoCumplimiento; }
			set { _UidEstadoCumplimiento = value; }
		}
		public string strIdEstadoCumplimiento { get { return _UidEstadoCumplimiento == null ? Guid.Empty.ToString() : _UidEstadoCumplimiento.ToString(); } }

		private Guid? _UidDepartamento;
		public Guid? UidDepartamento
		{
			get { return _UidDepartamento; }
			set { _UidDepartamento = value; }
		}
		public string strIdDepartamento { get { return _UidDepartamento == null ? Guid.Empty.ToString() : _UidDepartamento.ToString(); } }

		private string _StrTarea;
		public string StrTarea
		{
			get { return _StrTarea; }
			set { _StrTarea = value; }
		}

		private string _StrEstadoCumplimiento;
		public string StrEstadoCumplimiento
		{
			get { return _StrEstadoCumplimiento; }
			set { _StrEstadoCumplimiento = value; }
		}

		private string _StrDepartamento;
		public string StrDepartamento
		{
			get { return _StrDepartamento; }
			set { _StrDepartamento = value; }
		}

		public class Repository
		{
			Connection conn = new Connection();

			public List<Sucesor> FindAll(Guid uidCumplimiento)
			{
				List<Sucesor> sucesores = new List<Sucesor>();
				Sucesor sucesor = null;
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_Revision_Sucesores";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidCumplimiento", uidCumplimiento, SqlDbType.UniqueIdentifier);

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					sucesor = new Sucesor();
					sucesor._UidTarea = new Guid(row["UidTarea"].ToString());
					sucesor._UidCumplimiento = row.IsNull("UidCumplimiento") ? (Guid?)null : new Guid(row["UidCumplimiento"].ToString());
					sucesor._UidEstadoCumplimiento = row.IsNull("UidEstadoCumplimiento") ? (Guid?)null : new Guid(row["UidEstadoCumplimiento"].ToString());
					sucesor._StrTarea = row["VchTarea"].ToString();
					sucesor._StrEstadoCumplimiento = row["VchEstadoCumplimiento"].ToString();
					sucesores.Add(sucesor);
				}

				return sucesores;
			}

			public void Deshabilitar(Guid uidSucesor)
			{
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_Revision_DeshabilitarSucesor";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidCumplimiento", uidSucesor, SqlDbType.UniqueIdentifier);

				conn.ExecuteCommand(command);
			}

			public void Habilitar(Guid uidSucesor)
			{
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_Revision_HabilitarSucesor";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidCumplimiento", uidSucesor, SqlDbType.UniqueIdentifier);

				conn.ExecuteCommand(command);
			}

			public void CrearSucesor(Guid uidCumplimiento, Guid uidTareaSucesor)
			{
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_Revision_CrearSucesor";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidCumplimiento", uidCumplimiento, SqlDbType.UniqueIdentifier);
				command.AddParameter("@UidTareaSucesor", uidTareaSucesor, SqlDbType.UniqueIdentifier);

				conn.ExecuteCommand(command);
			}
		}
	}
}
